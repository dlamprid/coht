/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * tsi148.h - Low level support for the Tundra TSI148 PCI-VME Bridge Chip
 *
 * Copyright (c) 2009 Sebastien Dugue
 *
 * Borrowed and modified from the tsi148.h file from:
 *   Tom Armistead, Updated and maintained by Ajit Prem and
 *   copyrighted 2004 Motorola Inc.
 *
 */

#ifndef _TSI148_H
#define _TSI148_H

#include <linux/interrupt.h>
#include <linux/version.h>
#include <asm-generic/iomap.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "tsi148_hw.h"

#define TSI148_CHIP_NAME "TSI148"

/* Interrupt counters */
enum interrupt_idx {
	TSI148_INT_DMA0 = 0,
	TSI148_INT_DMA1,
	TSI148_INT_MB0,
	TSI148_INT_MB1,
	TSI148_INT_MB2,
	TSI148_INT_MB3,
	TSI148_INT_LM0,
	TSI148_INT_LM1,
	TSI148_INT_LM2,
	TSI148_INT_LM3,
	TSI148_INT_IRQ1,
	TSI148_INT_IRQ2,
	TSI148_INT_IRQ3,
	TSI148_INT_IRQ4,
	TSI148_INT_IRQ5,
	TSI148_INT_IRQ6,
	TSI148_INT_IRQ7,
	TSI148_INT_PERR,
	TSI148_INT_VERR,
	TSI148_INT_SPURIOUS
};

/*
 * TSI148 private data
 */
struct tsi148_chip {
	struct tsi148_regs *pci_map; /* tsi148 registers trough PCI map */
	struct tsi148_regs *vme_map; /* tsi148 registers trough VME mapping */
	int vme_wind_num; /* vme window for CRG space */
};

static inline char *tsi148_get_chip_name(void)
{
	return TSI148_CHIP_NAME;
}

/* Low level misc inline stuff */
static inline void __iomem *tsi148_get_regsaddr(struct vme_bridge_device *vbridge)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	return chip->pci_map;
}

/* Low level misc inline stuff */
static inline int tsi148_get_nr_windows(void)
{
	return TSI148_NR_OUT_WINDOWS;
}

/* Low level misc inline stuff */
static inline int tsi148_get_slotnum(struct vme_bridge_device *vbridge)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_regs *regs = chip->pci_map;

	return  ioread32be(&regs->lcsr.vstat) & 0x1f;
}

static inline int tsi148_get_syscon(struct vme_bridge_device *vbridge)
{
	int syscon = 0;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_regs *regs = chip->pci_map;

	if (ioread32be(&regs->lcsr.vstat) & 0x100)
		syscon = 1;

	return syscon;
}

static inline int tsi148_set_interrupts(struct tsi148_regs *regs,
					unsigned int mask)
{
	iowrite32be(mask, &regs->lcsr.inteo);
	iowrite32be(mask, &regs->lcsr.inten);

	/* Quick sanity check */
	if ((ioread32be(&regs->lcsr.inteo) != mask) ||
	    (ioread32be(&regs->lcsr.inten) != mask))
		return -1;

	return 0;
}

static inline unsigned int tsi148_get_int_enabled(struct tsi148_regs *regs)
{
	return ioread32be(&regs->lcsr.inteo);
}

static inline unsigned int tsi148_get_int_status(struct tsi148_regs *regs)
{
	return ioread32be(&regs->lcsr.ints);
}

static inline void tsi148_clear_int(struct tsi148_regs *regs,
				    unsigned int mask)
{
	iowrite32be(mask, &regs->lcsr.intc);
}

static inline int tsi148_bus_error_check(struct vme_bridge_device *vbridge,
					 int clear)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_regs *regs = chip->pci_map;

	unsigned int veat = ioread32be(&regs->lcsr.veat);

	if (veat & TSI148_LCSR_VEAT_VES) {
		if (clear)
			iowrite32be(TSI148_LCSR_VEAT_VESCL, &regs->lcsr.veat);
		return 1;
	}

	return 0;
}


extern int tsi148_dma_set_ops(struct vme_bridge_device *vbridge);
extern int tsi148_irq_set_ops(struct vme_bridge_device *vbridge);
extern int tsi148_window_set_ops(struct vme_bridge_device *vbridge);
extern int tsi148_procfs_set_ops(struct vme_bridge_device *vbridge);

extern int tsi148_vme_map_crg(struct tsi148_chip *chip);
extern void tsi148_vme_unmap_crg(struct tsi148_chip *chip);

extern int tsi148_am_to_attr(enum vme_address_modifier am,
			     unsigned int *addr_size,
			     unsigned int *transfer_mode,
			     unsigned int *user_access,
			     unsigned int *data_access);


#endif /* _TSI148_H */
