// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * pldz002.c - Low level support for PLDZ002 PCI-VME Bridge chip
 *
 */

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/byteorder.h>

#include "vme_bridge.h"
#include "vme_dmaengine.h"
#include "pldz002.h"


/**
 * pldz002_hw_init() - Release PLDZ002 PCI region
 *
 */
static void pldz002_release_regions(struct vme_bridge_device *vbridge)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	int i;
	struct resource *rsrc;

	for (i = 0; i < PLDZ002_REGION_COUNT; ++i) {
		if (chip->regions[i].vaddr != NULL)
			iounmap(chip->regions[i].vaddr);
		rsrc = chip->regions[i].rsrc;
		if (rsrc != NULL)
			release_mem_region(rsrc->start,
					   resource_size(rsrc));
	}
}

/*
 * vme_bridge_map_regs() - Map VME registers
 * @pdev: PCI device to map
 *
 * Maps VME bridge resources spaces.
 *
 * RETURNS: zero on success or -ERRNO value
 */
static int pldz002_request_regions(struct vme_bridge_device *vbridge)
{
	struct pldz002_chip *chip;
	struct resource *rsrc;
	resource_size_t addr;
	int rc, i;

	chip = vbridge->chip_mgr.private;

	/* Permanent mapping */
	/* BAR0 regs mem region*/
	addr = pci_resource_start(vbridge->pdev, 0) + PLDZ002_REGS_OFFSET;
	rsrc = request_mem_region(addr, PLDZ002_REGS_SIZE, "pldz002-regs");
	if (rsrc == NULL) {
		rc = -EBUSY;
		goto out_free;
	}
	chip->regions[PLDZ002_REGION_REGS].vaddr = ioremap(rsrc->start,
							PLDZ002_REGS_SIZE);
	chip->regions[PLDZ002_REGION_REGS].rsrc = rsrc;

	/* BAR0 iack mem region*/
	addr = pci_resource_start(vbridge->pdev, 0) + PLDZ002_IACK_OFFSET;
	rsrc = request_mem_region(addr, PLDZ002_IACK_SIZE, "pldz002-iack");
	if (rsrc == NULL) {
		rc = -EBUSY;
		goto out_free;
	}
	chip->regions[PLDZ002_REGION_IACK].vaddr = ioremap(rsrc->start,
							PLDZ002_IACK_SIZE);
	chip->regions[PLDZ002_REGION_IACK].rsrc = rsrc;

	/* BAR1 sram mem region*/
	addr = pci_resource_start(vbridge->pdev, 1) + PLDZ002_SRAM_OFFSET;
	rsrc = request_mem_region(addr, PLDZ002_SRAM_SIZE, "pldz002-sram");
	if (rsrc == NULL) {
		rc = -EBUSY;
		goto out_free;
	}
	chip->regions[PLDZ002_REGION_SRAM].vaddr = ioremap(rsrc->start,
							PLDZ002_SRAM_SIZE);
	chip->regions[PLDZ002_REGION_SRAM].rsrc = rsrc;

	for (i = 0; i < PLDZ002_REGION_COUNT; ++i) {
		rsrc = chip->regions[i].rsrc;
		pr_warn(PFX "%s: Region %s: phys:%p, sz: 0x%x, vaddr=%p\n",
			__func__, rsrc->name, (void *)rsrc->start,
			(unsigned int)(resource_size(rsrc)),
			chip->regions[i].vaddr);
	}

	return 0;

out_free:
	pldz002_release_regions(vbridge);
	return rc;
}

static void pldz002_init_bridge(struct vme_bridge_device *vbridge)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	uint8_t val8;
	void __iomem *regs_vaddr;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;
	iowrite8(0x00, regs_vaddr + PLDZ002_INTR);
	iowrite8(0x00, regs_vaddr + PLDZ002_IMASK);
	iowrite8(0x00, regs_vaddr + PLDZ002_MSTR);
	iowrite8(0x00, regs_vaddr + PLDZ002_SLV24);
	val8 = ioread8(regs_vaddr + PLDZ002_SYSCTL);
	iowrite8(val8 & ~PLDZ002_SYSCTL_SYSRES, regs_vaddr + PLDZ002_SYSCTL);

	iowrite8(0, regs_vaddr + PLDZ002_SLV16);
	iowrite8(0, regs_vaddr + PLDZ002_SLV32);

	/* clear DMA */
	iowrite8(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR,
		 regs_vaddr + PLDZ002_DMASTA);

	/* clear locmon */
	iowrite8(PLDZ002_LM_STAT_CTRL_IRQ, regs_vaddr + PLDZ002_LM_STAT_CTRL_0);
	iowrite8(PLDZ002_LM_STAT_CTRL_IRQ, regs_vaddr + PLDZ002_LM_STAT_CTRL_1);

	/* clear mbox */
	iowrite8(0, regs_vaddr + PLDZ002_MAIL_IRQ_CTRL);
	iowrite8(0xff, regs_vaddr + PLDZ002_MAIL_IRQ_STAT);

	iowrite16(0, regs_vaddr + PLDZ002_SLV24_PCI);
	iowrite32(0, regs_vaddr + PLDZ002_SLV32_PCI);
}


static struct vme_bridge_chip_ops pldz002_chip_ops = {
	.get_chip_name = pldz002_get_chip_name,
	.request_regions = pldz002_request_regions,
	.release_regions = pldz002_release_regions,
	.get_regsaddr = pldz002_get_regsaddr,
	.get_slotnum = NULL,
	.get_syscon = pldz002_get_syscon,
	.init_bridge = pldz002_init_bridge,
	.bus_error_check = pldz002_bus_error_check,
};

int pldz002_set_ops(struct vme_bridge_device *vbridge)
{

	/* Allocates pldz002_chip to hold specific information */
	vbridge->chip_mgr.private =
			kzalloc(sizeof(struct pldz002_chip), GFP_KERNEL);
	if (vbridge->chip_mgr.private == NULL) {
		pr_err(PFX "Could not allocate pldz002 chip struct\n");
		return -ENOMEM;
	}
	/* set chip specific operations */
	vbridge->chip_mgr.ops = &pldz002_chip_ops;

	/* bridge specific init for VME windows mgr */
	pldz002_window_set_ops(vbridge);
	/* bridge specific init for dma mgr */
	pldz002_dma_set_ops(vbridge);
	/* bridge specific init for irq domain */
	pldz002_irq_set_ops(vbridge);
	pldz002_procfs_set_ops(vbridge);

	return 0;
}
