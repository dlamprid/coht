#/bin/bash

# Author Adam Wujek, CERN 2017

# wrapper to localize a python

RUN_SCRIPT="$0"

declare -a PYTHON_BIN_LIST

#list of python binaries to try
PYTHON_BIN_LIST+=("/user/bdisoft/operational/bin/Python/PRO/bin/python3.5")
PYTHON_BIN_LIST+=("/usr/local/python3.5/bin/python3.5")
PYTHON_BIN_LIST+=("/acc/dsc/lab/L866/python3.5/bin/python3.5")
PYTHON_BIN_LIST+=("/acc/dsc/lab/L867/python3.5/bin/python3.5")
PYTHON_BIN_LIST+=("/usr/bin/python3.5")

for python_bin in "${PYTHON_BIN_LIST[@]}"; do
    if [ -f $python_bin ]; then
	$python_bin $*
	# run the script only once, so break
	break
    fi
done
