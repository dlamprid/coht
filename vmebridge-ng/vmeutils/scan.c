// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "vmeutils.h"

static int verbose;

static char options[] = "hv";
static const char help_msg[] =
	"Usage: vme-scan [options]\n"
	"\n"
	"By default it scans the VME bus from slot 1 to slot 21\n"
	"\n"
	"General options:\n"
	"--help             Print this message\n"
	"--slot <slot>      Scan a given slot (equivalent to --from <slot> --to <slot>)\n"
	"--from <slot>      Scan from given slot (default "STR(VME_SLOT_MIN)")\n"
	"--to <to>          Scan to given slot (default "STR(VME_SLOT_MAX)")\n"
	"\n"
	"Display options:\n"
	"--verbose, -v      Be verbose (add more v to be more verbose)\n"
	"\n";

enum vme_module_options {
	VME_SCN_OPT_SLOT = 1024,
	VME_SCN_OPT_FROM,
	VME_SCN_OPT_TO,
	VME_SCN_OPT_HELP,
};
static struct option long_options[] =
{
	{"verbose", no_argument, &verbose, 1},
	{"from", required_argument, 0, VME_SCN_OPT_FROM},
	{"to", required_argument, 0, VME_SCN_OPT_TO},
	{"slot", required_argument, 0, VME_SCN_OPT_SLOT},
	{"help", no_argument, 0, VME_SCN_OPT_HELP},
	{0, 0, 0, 0}
};


static int sysfs_rescan(char *path)
{
	int fd, ret;

	fd = open(path, O_WRONLY);
	if (fd < 0)
		return -1;
	ret = write(fd, "1", 1);
	close(fd);

	return ret;
}

static int scan_one(unsigned int slot)
{
#define PATH_SIZE 64
	char path[PATH_SIZE];

	snprintf(path, PATH_SIZE,
		 "/sys/bus/vme/devices/slot.%02u/rescan", slot);
	return sysfs_rescan(path);
#undef PATH_SIZE
}

static int scan_all(void)
{
	return sysfs_rescan("/sys/bus/vme/rescan");
}

static int scan(unsigned int domain, unsigned int from, unsigned int to)
{
	int ret, i;

	if (from < VME_SLOT_MIN || from > VME_SLOT_MAX ||
	    to < VME_SLOT_MIN || to > VME_SLOT_MAX || from > to) {
		errno = VME_ESRANGE;
		return -1;
	}

	if (from == VME_SLOT_MIN && to == VME_SLOT_MAX)
		return scan_all();

	for (i = from; i <= to; ++i) {
		ret = scan_one(i);
		if (ret < 0) {
			fprintf(stderr, "%s: failed to rescan slot %02u:%s\n",
				program_name, i, strerror(errno));
			continue;
		}
	}
	return ret;
}

static void cmd_help(int argc, char *argv[])
{
	if (argc == 2 && !strcmp(argv[1], "--help")) {
		fputs(help_msg, stdout);
		exit(EXIT_SUCCESS);
	}
}

int cmd_scan(int argc, char *argv[])
{
	unsigned int from = VME_SLOT_MIN, to = VME_SLOT_MAX;
	int c;
	int ret;
	int option_index;

	cmd_help(argc, argv);
	while ((c = getopt_long(argc, argv, options, long_options,
				&option_index)) != -1) {
		switch (c) {
		default:
			break;
		case 'v':
			verbose++;
			break;
		case 'h':
		case VME_SCN_OPT_HELP:
			fputs(help_msg, stdout);
			exit(EXIT_SUCCESS);
		case VME_SCN_OPT_SLOT:
			ret = sscanf(optarg, "%u", &from);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			to = from;
			break;
		case VME_SCN_OPT_FROM:
			ret = sscanf(optarg, "%u", &from);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_SCN_OPT_TO:
			ret = sscanf(optarg, "%u",&to);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			break;
		}
	}

	return scan(0, from, to);
}
