#!/bin/bash

# (C) CERN 2018
# author: Adam Wujek <adam.wujek@cern.ch>

# make sure install_drv.sh from the same dir as this file
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# mcedit syntax fix "
source "$SCRIPT_DIR"/install_drv.sh


./load_drivers.sh .
./create_nodes.sh
install_drv men_wrapper.ko
if [ $? -eq 0 ]; then
    sleep 1
    chmod 666 /dev/vme_mwindow /dev/vme_dma /dev/vme_ctl /dev/vme_regs
fi
./check_version.sh
