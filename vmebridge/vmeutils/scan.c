// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "vmeutils.h"



static char options[] = "vs:f:t:";
static const char help_msg[] =
	"Usage: vme-scan [options]\n"
	"\n"
	"By default it scans the VME bus from slot 1 to slot 21\n"
	"\n"
	"General options:\n"
	"--help             Print this message\n"
	"-s <slot>          Scan a given slot (equivalent to -f <slot> -t <slot>)\n"
	"-f <slot>          Scan from given slot (default "STR(VME_SLOT_MIN)")\n"
	"-t <to>            Scan to given slot (default "STR(VME_SLOT_MAX)")\n"
	"\n"
	"Display options:\n"
	"-v                 Be verbose (add more v to be more verbose)\n"
	"\n";

static int sysfs_rescan(char *path)
{
	int fd, ret;

	fd = open(path, O_WRONLY);
	if (fd < 0)
		goto out;
	ret = write(fd, "1", 1);
	close(fd);
out:
	return ret;
}

static int scan_one(unsigned int slot)
{
#define PATH_SIZE 64
	char path[PATH_SIZE];

	snprintf(path, PATH_SIZE,
		 "/sys/bus/vme/devices/slot.%02u/rescan_slot", slot);
	return sysfs_rescan(path);
#undef PATH_SIZE
}

static int scan_all(void)
{
	return sysfs_rescan("/sys/bus/vme/rescan");
}

static int scan(unsigned int domain, unsigned int from, unsigned int to)
{
	int ret, i;

	if (from < VME_SLOT_MIN || from > VME_SLOT_MAX ||
	    to < VME_SLOT_MIN || to > VME_SLOT_MAX || from > to) {
		errno = VME_ESRANGE;
		return -1;
	}

	if (from == VME_SLOT_MIN && to == VME_SLOT_MAX)
		return scan_all();

	for (i = from; i <= to; ++i) {
		ret = scan_one(i);
		if (ret < 0) {
			fprintf(stderr, "%s: failed to rescan slot %02u:%s\n",
				program_name, i, strerror(errno));
			continue;
		}
	}
	return ret;
}

static void cmd_help(int argc, char *argv[])
{
	if (argc == 2 && !strcmp(argv[1], "--help")) {
		fputs(help_msg, stdout);
		exit(EXIT_SUCCESS);
	}
}

int cmd_scan(int argc, char *argv[])
{
	unsigned int from = VME_SLOT_MIN, to = VME_SLOT_MAX;
	unsigned int verbose = 0;
	char c;
	int ret;

	cmd_help(argc, argv);
	while ((c = getopt(argc, argv, options)) != -1) {
		switch (c) {
		default:
			break;
		case 'v':
			verbose++;
			break;
		case 's':
			ret = sscanf(optarg, "%u", &from);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			to = from;
			break;
		case 'f':
			ret = sscanf(optarg, "%u", &from);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			break;
		case 't':
			ret = sscanf(optarg, "%u",&to);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				return -1;
			}
			break;
		}
	}

	return scan(0, from, to);
}
