// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "vmeutils.h"

static int verbose;
static int enable = -1;
static int reset = -1;
static int remove_dev;


static char options[] = "hv";
static const char help_msg[] =
	"Usage: vme-module -s <slot> [options]\n"
	"\n"
	"Disable a module on a given slot\n"
	"\n"
	"General options:\n"
	"--help, -h         Print this message\n"
	"\n"
	"Module parameters:\n"
	"--slot <slot>      Module' slot number to disable\n"
	"--enable           Enable address decoder\n"
	"--disable          Disable address decoder\n"
	"--reset            Reset board\n"
	"--unreset          Unreset board"
	"--ader <func>,0x<ader> ADER value\n"
	"--ader <func>,0x<addr>,0x<am>[,<DSFR>[,<XAM>]]\n"
	"                   ADER value separated in its four components.\n"
	"                   <am> is interpreted as XAM when <XAM> is set.\n"
	"                   <DSFR> and <XAM> are optional and by defualt 0\n"
	"--remove           Remove module instance\n"
	"\n"
	"Display options:\n"
	"--verbose, -v      Be verbose (add more v to be more verbose)\n"
	"\n";

enum vme_module_options {
	VME_MOD_OPT_SLOT = 1024,
	VME_MOD_OPT_ADER,
	VME_MOD_OPT_HELP,
};
static struct option long_options[] =
{
	{"verbose", no_argument, &verbose, 1},
	{"enable", no_argument, &enable, 1},
	{"disable", no_argument, &enable, 0},
	{"reset", no_argument, &reset, 1},
	{"unreset", no_argument, &reset, 0},
	{"remove", no_argument, &remove_dev, 1},

	{"slot", required_argument, 0, VME_MOD_OPT_SLOT},
	{"ader", required_argument, 0, VME_MOD_OPT_ADER},
	{"help", no_argument, 0, VME_MOD_OPT_HELP},
	{0, 0, 0, 0}
};


static int vme_modules_enable(struct vme_access *a, int enable)
{
	struct vme_dev *d;

	if (enable != 0 && enable != 1)
		return -EINVAL;

	for (d = a->devices; d; d = d->next)
		vme_device_enable(d, enable);

	return 0;
}

static void vme_device_reset(struct vme_dev *d, unsigned int reset)
{
	if (d->spec < VME_SPEC_VME64) {
		fprintf(stderr,
			"Board on slot %02d does not support CSR enable\n",
			d->slot);
		return;
	}

	vme_sysfs_set_value(d, "reset", !!reset, NULL);
}

static int vme_modules_reset(struct vme_access *a, int reset)
{
	struct vme_dev *d;

	if (reset != 0 && reset != 1)
		return -EINVAL;

	for (d = a->devices; d; d = d->next)
		vme_device_reset(d, reset);

	return 0;
}

static inline uint32_t vme_module_ader_build(uint32_t addr, uint32_t am,
					     uint32_t dfsr, uint32_t xam)
{
	uint32_t ader = 0;

	if (xam) {
		addr &= 0xFFFFFC00;
		am &= 0xFF;
	} else {
		addr &= 0xFFFFFF00;
		am &= 0x3F;
	}
	ader |= (addr << 0);
	ader |= (am << 2);
	ader |= ((!!dfsr) << 1);
	ader |= ((!!xam) << 0);

	return ader;
}

static void vme_device_ader(struct vme_dev *d, unsigned int func,
			    uint32_t ader)
{
#define BUF_SIZE 16
	char buf[BUF_SIZE];
	if (d->spec < VME_SPEC_VME64x) {
		fprintf(stderr,
			"Board on slot %02d does not support CSR ADER\n",
			d->slot);
		return;
	}

	snprintf(buf, 16, "%u 0x%x", func, ader);

	vme_sysfs_set_value(d, "ader", 0, buf);
#undef BUF_SIZE
}

static int vme_modules_ader(struct vme_access *a, unsigned int func,
			    uint32_t ader)
{
	struct vme_dev *d;

	for (d = a->devices; d; d = d->next) {
		if (func < VME_CR_FUNCTION_MAX)
			vme_device_ader(d, func, ader);
	}

	return 0;
}

static void vme_device_remove(struct vme_dev *d, unsigned int remove)
{
	vme_sysfs_set_value(d, "remove", !!remove, NULL);
}

static int vme_modules_remove(struct vme_access *a, int remove)
{
	struct vme_dev *d;

	if (!remove)
		return 0;

	for (d = a->devices; d; d = d->next)
		vme_device_remove(d, remove);

	return 0;
}

int cmd_module(int argc, char *argv[])
{
	struct vme_filter filter;
	struct vme_access *a;
	int func = -1;
	uint32_t ader;
	int c;
	int ret;
	int option_index;

	memset(&filter, 0, sizeof(filter));

	while ((c = getopt_long(argc, argv, options, long_options,
				&option_index)) != -1) {
		switch (c) {
		default:
			break;
		case 'v':
			verbose++;
			break;
		case 'h':
		case VME_MOD_OPT_HELP:
			fputs(help_msg, stdout);
			exit(EXIT_SUCCESS);
		case VME_MOD_OPT_SLOT:
			ret = sscanf(optarg, "%d", &filter.slot);
			if (ret != 1) {
				errno = VME_ESRANGE;
				return -1;
			}
			break;
		case VME_MOD_OPT_ADER:{
			uint32_t addr, am, dfsr, xam;

			ret = sscanf(optarg, "%d,0x%x,0x%02x,%u,%u",
				     &func, &addr, &am, &dfsr, &xam);
			switch (ret) {
			case 2:
				ader = addr;
				break;
			case 3:
				ader = vme_module_ader_build(addr, am,
							     0, 0);
				break;
			case 4:
				ader = vme_module_ader_build(addr, am,
							     dfsr, 0);
				break;
			case 5:
				ader = vme_module_ader_build(addr, am,
							     dfsr, xam);
				break;
			default:
				fprintf(stderr,
					"Can't parse ADER option: %s\n",
					optarg);
				exit(EXIT_FAILURE);
				break;

			}
			break;
		}
		}
	}

	if (!filter.slot) {
		errno = VME_ESRANGE;
		return -1;
	}

	a = vme_access_alloc();
	vme_sysfs_scan_bus(a, &filter);
	vme_modules_reset(a, reset);
	vme_modules_enable(a, enable);
	vme_modules_ader(a, func, ader);
	vme_modules_remove(a, remove_dev);
	vme_access_cleanup(a);

	return 0;
}
