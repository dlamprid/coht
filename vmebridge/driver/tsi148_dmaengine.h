// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#ifndef _TSI148_DMAENGINE_H_
#define _TSI148_DMAENGINE_H_

#include <linux/kernel.h>
#include <linux/dmaengine.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/scatterlist.h>
#include <linux/interrupt.h>

/**
 * @wait: used by old sync API to detect when the DMA is complete
 */
struct tsi148_dma_chan {
	unsigned int index;
	struct dma_chan dchan;
	struct list_head pending_list;
	struct tsi148_dma_tx_desc *tx_desc_current;
	spinlock_t lock;
	struct dma_slave_config cfg;

	struct tasklet_struct dma_start_task;
	wait_queue_head_t wait;

	struct list_head hw_desc_list;
};

struct tsi148_dma_tx_desc {
	struct dma_async_tx_descriptor tx;
	enum dma_transfer_direction direction;
	struct scatterlist *sgl;
	unsigned int sg_len;
	struct vme_dma desc;
	struct list_head list;
};

/**
 * @dma_pool: pool of contiguous memory where to put DMA transfer descriptors
 */
struct vme_dmaengine_device {
	struct dma_device dma;
	struct tsi148_dma_chan tdchan[TSI148_NUM_DMA_CHANNELS];
	struct tasklet_struct dma_done_task;
	struct dma_pool *dma_pool;
};

#define to_tsi148_dma_chan(_ptr) (container_of(_ptr, struct tsi148_dma_chan, dchan))
#define to_tsi148_dma_tx_desc(_ptr) (container_of(_ptr, struct tsi148_dma_tx_desc, tx))
#define to_vme_dmaengine_device(_ptr) (container_of(_ptr, struct vme_dmaengine_device, dma))

#endif
