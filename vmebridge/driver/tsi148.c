// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * tsi148.c - Low level support for the Tundra TSI148 PCI-VME Bridge Chip
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/byteorder.h>

#include "vme_bridge.h"
#include "tsi148.h"

#undef DEBUG_DMA

struct tsi148_chip *tsi148_chip_global;

static inline void reg_join(u32 high, u32 low, u64 *out)
{
	*out = (unsigned long long)high << 32;
	*out |= low;
}


/*
 * PCI and VME bus interrupt handlers
 */


/**
 * tsi148_handle_pci_error() - Handle a PCI bus error reported by the bridge
 *
 *  This handler simply displays a message on the console and clears
 * the error.
 */
void tsi148_handle_pci_error(void)
{
	/* Display raw error information */
	printk(KERN_ERR PFX
	       "PCI error at address: %.8x %.8x - attributes: %.8x\n"
	       "PCI-X attributes: %.8x - PCI-X split completion: %.8x\n",
	       ioread32be(&tsi148_chip_global->lcsr.edpau),
	       ioread32be(&tsi148_chip_global->lcsr.edpal),
	       ioread32be(&tsi148_chip_global->lcsr.edpat),
	       ioread32be(&tsi148_chip_global->lcsr.edpxa),
	       ioread32be(&tsi148_chip_global->lcsr.edpxs));

	/* Clear error */
	iowrite32be(TSI148_LCSR_EDPAT_EDPCL, &tsi148_chip_global->lcsr.edpat);
}

/**
 * tsi148_handle_vme_error() - Handle a VME bus error reported by the bridge
 *
 * Retrieve and report the offending VME address and Address Modifier, clearing
 * the error.
 */
void tsi148_handle_vme_error(struct vme_bus_error *error)
{
	u32 attr;
	u32 addru, addrl;

	/* Store the address */
	addru = ioread32be(&tsi148_chip_global->lcsr.veau);
	addrl = ioread32be(&tsi148_chip_global->lcsr.veal);
	reg_join(addru, addrl, &error->address);

	/* Get the Address Modifier from the attributes */
	attr = ioread32be(&tsi148_chip_global->lcsr.veat);
	error->am = (attr & TSI148_LCSR_VEAT_AM) >> TSI148_LCSR_VEAT_AM_SHIFT;

	/* Display raw error information */
	if (vme_report_bus_errors) {
		printk(KERN_ERR PFX "VME bus error at address: %.8x %.8x - "
			"AM: 0x%x - tsi148 attributes: %.8x\n",
			addru, addrl, error->am, attr);
	}

	/* Overflow detected: not much we can do about it */
	if (attr & TSI148_LCSR_VEAT_VEOF)
		printk(KERN_ERR PFX "VME Bus Exception Overflow Detected\n");

	/* Clear error */
	iowrite32be(TSI148_LCSR_VEAT_VESCL, &tsi148_chip_global->lcsr.veat);
}

/**
 * tsi148_generate_interrupt() - Generate an interrupt on the VME bus
 * @level: IRQ level (1-7)
 * @vector: IRQ vector (0-255)
 * @msecs: Timeout for IACK in milliseconds
 *
 *  This function generates an interrupt on the VME bus and waits for IACK
 * for msecs milliseconds.
 *
 *  Returns 0 on success or -ETIME if the timeout expired.
 *
 */
int tsi148_generate_interrupt(int level, int vector, signed long msecs)
{
	unsigned int val;
	signed long timeout = msecs_to_jiffies(msecs);


	if ((level < VME_IRQ_LEVEL_MIN) || (level > VME_IRQ_LEVEL_MAX))
		return -EINVAL;

	if ((vector < 0) || (vector > 255))
		return -EINVAL;

	/* Generate VME IRQ */
	val = ioread32be(&tsi148_chip_global->lcsr.vicr) & ~TSI148_LCSR_VICR_STID_M;
	val |= ((level << 8) | vector);
	iowrite32be(val, &tsi148_chip_global->lcsr.vicr);

	/* Wait until IACK */
	while (ioread32be(&tsi148_chip_global->lcsr.vicr) & TSI148_LCSR_VICR_IRQS) {
		set_current_state(TASK_INTERRUPTIBLE);
		timeout = schedule_timeout(timeout);

		if (timeout == 0)
			break;
	}

	if (ioread32be(&tsi148_chip_global->lcsr.vicr) & TSI148_LCSR_VICR_IRQS) {
		/* No IACK received, clear the IRQ */
		val = ioread32be(&tsi148_chip_global->lcsr.vicr);

		val &= ~(TSI148_LCSR_VICR_IRQL_M | TSI148_LCSR_VICR_STID_M);
		val |= TSI148_LCSR_VICR_IRQC;
		iowrite32be(val, &tsi148_chip_global->lcsr.vicr);

		return -ETIME;
	}


	return 0;
}

/*
 * Utility functions
 */

/**
 * am_to_attr() - Convert a standard VME address modifier to TSI148 attributes
 * @am: Address modifier
 * @addr_size: Address size
 * @transfer_mode: Transfer Mode
 * @user_access: User / Supervisor access
 * @data_access: Data / Program access
 *
 */
int am_to_attr(enum vme_address_modifier am,
	       unsigned int *addr_size,
	       unsigned int *transfer_mode,
	       unsigned int *user_access,
	       unsigned int *data_access)
{
	switch (am) {
	case VME_A16_USER:
		*addr_size = TSI148_A16;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A16_SUP:
		*addr_size = TSI148_A16;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_USER_MBLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_DATA_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_PRG_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_BLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_SUP_MBLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_DATA_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_PRG_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_BLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_USER_MBLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_DATA_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_PRG_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_BLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_SUP_MBLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_DATA_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_PRG_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_BLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_SCT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_BLT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_MBLT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_CR_CSR:
		*addr_size = TSI148_CRCSR;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;

	default:
		return -1;
	}

	return 0;
}

/**
 * attr_to_am() - Convert TSI148 attributes to a standard VME address modifier
 * @addr_size: Address size
 * @transfer_mode: Transfer Mode
 * @user_access: User / Supervisor access
 * @data_access: Data / Program access
 * @am: Address modifier
 *
 */
static void attr_to_am(unsigned int addr_size, unsigned int transfer_mode,
		      unsigned int user_access, unsigned int data_access,
		      int *am)
{

	if (addr_size == TSI148_A16) {
		if (user_access == TSI148_USER)
			*am = VME_A16_USER;
		else
			*am = VME_A16_SUP;
	}
	else if (addr_size == TSI148_A24) {
		if (user_access == TSI148_USER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A24_USER_DATA_SCT;
				else
					*am = VME_A24_USER_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A24_USER_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A24_USER_MBLT;
		} else if (user_access == TSI148_SUPER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A24_SUP_DATA_SCT;
				else
					*am = VME_A24_SUP_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A24_SUP_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A24_SUP_MBLT;
		}
	}
	else if (addr_size == TSI148_A32) {
		if (user_access == TSI148_USER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A32_USER_DATA_SCT;
				else
					*am = VME_A32_USER_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A32_USER_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A32_USER_MBLT;
		} else if (user_access == TSI148_SUPER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A32_SUP_DATA_SCT;
				else
					*am = VME_A32_SUP_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A32_SUP_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A32_SUP_MBLT;
		}
	}
	else if (addr_size == TSI148_A64) {
		if (transfer_mode == TSI148_SCT)
			*am = VME_A64_SCT;
		else if (transfer_mode == TSI148_BLT)
			*am = VME_A64_BLT;
		else if (transfer_mode == TSI148_MBLT)
			*am = VME_A64_MBLT;
	}
	else if (addr_size == TSI148_CRCSR)
		*am = VME_CR_CSR;
}

/*
 * sub64hi() - Calculate the upper 32 bits of the 64 bit difference
 * hi/lo0 - hi/lo1
 */
static int sub64hi(unsigned int lo0, unsigned int hi0,
		   unsigned int lo1, unsigned int hi1)
{
	if (lo0 < lo1)
		return hi0 - hi1 - 1;
	return hi0 - hi1;
}

/*
 * sub64hi() - Calculate the upper 32 bits of the 64 bit sum
 * hi/lo0 + hi/lo1
 */
static int add64hi(unsigned int lo0, unsigned int hi0,
		   unsigned int lo1, unsigned int hi1)
{
	if ((lo0 + lo1) < lo1)
		return hi0 + hi1 + 1;
	return hi0 + hi1;
}

/*
 * TSI148 DMA support
 */

/*
 * TSI148 window support
 */

/**
 * tsi148_setup_window_attributes() - Build the window attributes word
 * @v2esst_mode: VME 2eSST transfer speed
 * @data_width: VME data width (D16 or D32 only)
 * @am: VME address modifier
 *
 * This function builds the window attributes word. All parameters are
 * checked.
 *
 * Returns %EINVAL if any parameter is not consistent.
 */
static int tsi148_setup_window_attributes(enum vme_2esst_mode v2esst_mode,
					  enum vme_data_width data_width,
					  enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	switch (v2esst_mode) {
	case VME_SST160:
	case VME_SST267:
	case VME_SST320:
		attrs |= ((v2esst_mode << TSI148_LCSR_OTAT_2eSSTM_SHIFT) &
			  TSI148_LCSR_OTAT_2eSSTM_M);
		break;
	default:
		return -EINVAL;
	}

	switch (data_width) {
	case VME_D16:
		attrs |= ((TSI148_DW_16 << TSI148_LCSR_OTAT_DBW_SHIFT) &
			  TSI148_LCSR_OTAT_DBW_M);
		break;
	case VME_D32:
		attrs |= ((TSI148_DW_32 << TSI148_LCSR_OTAT_DBW_SHIFT) &
			  TSI148_LCSR_OTAT_DBW_M);
		break;
	default:
		return -EINVAL;
	}

	if (am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access))
		return -EINVAL;

	switch (transfer_mode) {
	case TSI148_SCT:
	case TSI148_BLT:
	case TSI148_MBLT:
	case TSI148_2eVME:
	case TSI148_2eSST:
	case TSI148_2eSSTB:
		attrs |= ((transfer_mode << TSI148_LCSR_OTAT_TM_SHIFT) &
			  TSI148_LCSR_OTAT_TM_M);
		break;
	default:
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_OTAT_SUP;
		break;
	case TSI148_USER:
		break;
	default:
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_OTAT_PGM;
		break;
	case TSI148_DATA:
		break;
	default:
		return -EINVAL;
	}

	switch (addr_size) {
	case TSI148_A16:
	case TSI148_A24:
	case TSI148_A32:
	case TSI148_A64:
	case TSI148_CRCSR:
	case TSI148_USER1:
	case TSI148_USER2:
	case TSI148_USER3:
	case TSI148_USER4:
		attrs |= ((addr_size << TSI148_LCSR_OTAT_AMODE_SHIFT) &
			  TSI148_LCSR_OTAT_AMODE_M);
		break;
	default:
		return -EINVAL;
	}

	return attrs;
}

/**
 * tsi148_get_window_attr() - Get a hardware window attributes
 * @desc: Descriptor of the window (only the window number is used)
 *
 */
void tsi148_get_window_attr(struct vme_mapping *desc)
{
	int window_num = desc->window_num;
	unsigned int transfer_mode;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int addr_size;
	int am = -1;
	struct tsi148_otrans *trans = &tsi148_chip_global->lcsr.otrans[window_num];
	unsigned int otat;
	unsigned int otsau, otsal;
	unsigned int oteau, oteal;
	unsigned int otofu, otofl;

	/* Get window Control & Bus attributes register */
	otat = ioread32be(&trans->otat);

	if (otat & TSI148_LCSR_OTAT_EN)
		desc->window_enabled = 1;

	if (!(otat & TSI148_LCSR_OTAT_MRPFD))
		desc->read_prefetch_enabled = 1;

	desc->read_prefetch_size = (otat & TSI148_LCSR_OTAT_PFS_M) >>
		TSI148_LCSR_OTAT_PFS_SHIFT;

	desc->v2esst_mode = (otat & TSI148_LCSR_OTAT_2eSSTM_M) >>
		TSI148_LCSR_OTAT_2eSSTM_SHIFT;

	switch ((otat & TSI148_LCSR_OTAT_DBW_M) >> TSI148_LCSR_OTAT_DBW_SHIFT) {
	case TSI148_LCSR_OTAT_DBW_16:
		desc->data_width = VME_D16;
		break;
	case TSI148_LCSR_OTAT_DBW_32:
		desc->data_width = VME_D32;
		break;
	}

	desc->bcast_select = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].otbs);

	/*
	 *  The VME address modifiers is encoded into the 4 following registers.
	 * Here we then have to map that back to a single value - This is
	 * coming out real ugly (sigh).
	 */
	addr_size = (otat & TSI148_LCSR_OTAT_AMODE_M) >>
		TSI148_LCSR_OTAT_AMODE_SHIFT;

	transfer_mode = (otat & TSI148_LCSR_OTAT_TM_M) >>
		TSI148_LCSR_OTAT_TM_SHIFT;

	user_access = (otat & TSI148_LCSR_OTAT_SUP) ?
		TSI148_SUPER : TSI148_USER;

	data_access = (otat & TSI148_LCSR_OTAT_PGM) ?
		TSI148_PROG : TSI148_DATA;

	attr_to_am(addr_size, transfer_mode, user_access, data_access, &am);

	if (am == -1)
		printk(KERN_WARNING PFX
		       "%s - unsupported AM:\n"
		       "\taddr size: %x TM: %x usr/sup: %d data/prg:%d\n",
		       __func__, addr_size, transfer_mode, user_access,
		       data_access);

	desc->am = am;

	/* Get window mappings */
	otsal = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].otsal);
	otsau = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].otsau);
	oteal = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].oteal);
	oteau = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].oteau);
	otofl = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].otofl);
	otofu = ioread32be(&tsi148_chip_global->lcsr.otrans[window_num].otofu);


	desc->pci_addrl = otsal;
	desc->pci_addru = otsau;

	desc->sizel = oteal - otsal;
	desc->sizeu = sub64hi(oteal, oteau, otsal, otsau);

	desc->vme_addrl = otofl + otsal;

	if (addr_size == TSI148_A64)
		desc->vme_addru = add64hi(otofl, otofu, otsal, otsau);
	else
		desc->vme_addru = 0;
}

/**
 * tsi148_create_window() - Create a PCI-VME window
 * @desc: Window descriptor
 *
 * Setup the TSI148 outbound window registers and enable the window for
 * use.
 *
 * Returns 0 on success, %EINVAL in case of wrong parameter.
 */
int tsi148_create_window(struct vme_mapping *desc)
{
	int window_num = desc->window_num;
	int rc;
	unsigned int addr;
	unsigned int size;
	struct tsi148_otrans *trans = &tsi148_chip_global->lcsr.otrans[window_num];
	unsigned int otat = 0;
	unsigned int oteal, oteau;
	unsigned int otofl, otofu;

	/*
	 * Do some sanity checking on the window descriptor.
	 * PCI address, VME address and window size should be aligned
	 * on 64k boudaries. So round down the VME address and round up the
	 * VME size to 64K boundaries.
	 * The PCI address should already have been rounded.
	 */
	addr = desc->vme_addrl;
	size = desc->sizel;

	if (addr & 0xffff)
		addr = desc->vme_addrl & ~0xffff;

	if (size & 0xffff)
		size = (desc->vme_addrl + desc->sizel + 0x10000) & ~0xffff;

	desc->vme_addrl = addr;
	desc->sizel = size;

	if (desc->pci_addrl & 0xffff)
		return -EINVAL;

	/* Setup the window attributes */
	rc = tsi148_setup_window_attributes(desc->v2esst_mode,
					    desc->data_width,
					    desc->am);

	if (rc < 0)
		return rc;

	otat = (unsigned int)rc;

	/* Enable the window */
	otat |= TSI148_LCSR_OTAT_EN;

	if (desc->read_prefetch_enabled) {
		/* clear Read prefetch disable bit */
		otat &= ~TSI148_LCSR_OTAT_MRPFD;
		otat |= ((desc->read_prefetch_size <<
			  TSI148_LCSR_OTAT_PFS_SHIFT) & TSI148_LCSR_OTAT_PFS_M);
	} else {
		/* Set Read prefetch disable bit */
		otat |= TSI148_LCSR_OTAT_MRPFD;
	}

	/* Setup the VME 2eSST broadcast select */
	iowrite32be(desc->bcast_select & TSI148_LCSR_OTBS_M,
		    &trans->otbs);

	/* Setup the PCI side start address */
	iowrite32be(desc->pci_addrl & TSI148_LCSR_OTSAL_M,
		    &trans->otsal);
	iowrite32be(desc->pci_addru, &trans->otsau);

	/*
	 * Setup the PCI side end address
	 * Note that 'oteal' is formed by the upper 2 bytes common to the
	 * addresses in the last 64K chunk of the window.
	 */
	oteal = (desc->pci_addrl + desc->sizel - 1) & ~0xffff;
	oteau = add64hi(desc->pci_addrl, desc->pci_addru,
			desc->sizel, desc->sizeu);
	iowrite32be(oteal, &trans->oteal);
	iowrite32be(oteau, &trans->oteau);

	/* Setup the VME offset */
	otofl = desc->vme_addrl - desc->pci_addrl;
	otofu = sub64hi(desc->vme_addrl, desc->vme_addru,
			desc->pci_addrl, desc->pci_addru);
	iowrite32be(otofl, &trans->otofl);
	iowrite32be(otofu, &trans->otofu);

	/*
	 * Finally, write the window attributes, also enabling the
	 * window
	 */
	iowrite32be(otat, &trans->otat);
	desc->window_enabled = 1;

	return 0;
}

/**
 * tsi148_remove_window() - Disable a PCI-VME window
 * @desc: Window descriptor (only the window number is used)
 *
 * Disable the PCI-VME window specified in the descriptor.
 */
void tsi148_remove_window(struct vme_mapping *desc)
{
	int window_num = desc->window_num;
	struct tsi148_otrans *trans = &tsi148_chip_global->lcsr.otrans[window_num];

	/* Clear the attribute register thus disabling the window */
	iowrite32be(0, &trans->otat);

	/* Wipe the window registers */
	iowrite32be(0, &trans->otsal);
	iowrite32be(0, &trans->otsau);
	iowrite32be(0, &trans->oteal);
	iowrite32be(0, &trans->oteau);
	iowrite32be(0, &trans->otofl);
	iowrite32be(0, &trans->otofu);
	iowrite32be(0, &trans->otbs);

	desc->window_enabled = 0;
}

/**
 * am_to_crgattrs() - Convert address modifier to CRG Attributes
 * @am: Address modifier
 */
static int am_to_crgattrs(enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	if (am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access))
		return -EINVAL;

	switch (addr_size) {
	case TSI148_A16:
		attrs |= TSI148_LCSR_CRGAT_AS_16;
		break;
	case TSI148_A24:
		attrs |= TSI148_LCSR_CRGAT_AS_24;
		break;
	case TSI148_A32:
		attrs |= TSI148_LCSR_CRGAT_AS_32;
		break;
	case TSI148_A64:
		attrs |= TSI148_LCSR_CRGAT_AS_64;
		break;
	default:
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_CRGAT_PGM;
		break;
	case TSI148_DATA:
		attrs |= TSI148_LCSR_CRGAT_DATA;
		break;
	default:
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_CRGAT_SUPR;
		break;
	case TSI148_USER:
		attrs |= TSI148_LCSR_CRGAT_NPRIV;
		break;
	default:
		return -EINVAL;
	}

	return attrs;
}

/**
 * tsi148_setup_crg() - Setup the CRG inbound mapping
 * @vme_base: VME base address for the CRG mapping
 * @am: Address modifier for the mapping
 */
int tsi148_setup_crg(unsigned int vme_base,
		     enum vme_address_modifier am)
{
	int attrs;

	iowrite32be(0, &tsi148_chip_global->lcsr.cbau);
	iowrite32be(vme_base, &tsi148_chip_global->lcsr.cbal);

	attrs = am_to_crgattrs(am);

	if (attrs < 0) {
		iowrite32be(0, &tsi148_chip_global->lcsr.cbal);
		return -1;
	}

	attrs |= TSI148_LCSR_CRGAT_EN;

	iowrite32be(attrs, &tsi148_chip_global->lcsr.crgat);

	return 0;
}

/**
 * tsi148_disable_crg() - Disable the CRG VME mapping
 *
 */
void tsi148_disable_crg(struct tsi148_chip *regs)
{
	iowrite32be(0, &regs->lcsr.crgat);
	iowrite32be(0, &regs->lcsr.cbau);
	iowrite32be(0, &regs->lcsr.cbal);
}


/**
 * tsi148_quiesce() - Shutdown the TSI148 chip
 *
 * Put VME bridge in quiescent state.  Disable all decoders,
 * clear all interrupts.
 */
void tsi148_quiesce(struct tsi148_chip *regs)
{
	int i;
	unsigned int val;

	/* Shutdown all inbound and outbound windows. */
	for (i = 0; i < TSI148_NUM_OUT_WINDOWS; i++) {
		iowrite32be(0, &regs->lcsr.itrans[i].itat);
		iowrite32be(0, &regs->lcsr.otrans[i].otat);
	}

	for (i= 0; i < TSI148_NUM_DMA_CHANNELS; i++) {
		iowrite32be(0, &regs->lcsr.dma[i].dma_desc.dnlau);
		iowrite32be(0, &regs->lcsr.dma[i].dma_desc.dnlal);
		iowrite32be(0, &regs->lcsr.dma[i].dma_desc.dcnt);
	}

	/* Shutdown Location monitor. */
	iowrite32be(0, &regs->lcsr.lmat);

	/* Shutdown CRG map. */
	tsi148_disable_crg(regs);

	/* Clear error status. */
	iowrite32be(TSI148_LCSR_EDPAT_EDPCL, &regs->lcsr.edpat);
	iowrite32be(TSI148_LCSR_VEAT_VESCL, &regs->lcsr.veat);
	iowrite32be(TSI148_LCSR_PCSR_SRTO | TSI148_LCSR_PCSR_SRTE |
		    TSI148_LCSR_PCSR_DTTE | TSI148_LCSR_PCSR_MRCE,
		    &regs->lcsr.pstat);

	/* Clear VIRQ interrupt (if any) */
	iowrite32be(TSI148_LCSR_VICR_IRQC, &regs->lcsr.vicr);

	/* Disable and clear all interrupts. */
	iowrite32be(0, &regs->lcsr.inteo);
	iowrite32be(0xffffffff, &regs->lcsr.intc);
	iowrite32be(0xffffffff, &regs->lcsr.inten);

	/* Map all Interrupts to PCI INTA */
	iowrite32be(0, &regs->lcsr.intm1);
	iowrite32be(0, &regs->lcsr.intm2);

	/*
	 * Set bus master timeout
	 * The timeout overrides the DMA block size if set too low.
	 * Enable release on request so that there will not be a re-arbitration
	 * for each transfer.
	 */
	val = ioread32be(&regs->lcsr.vmctrl);
	val &= ~(TSI148_LCSR_VMCTRL_VTON_M | TSI148_LCSR_VMCTRL_VREL_M);
	val |= (TSI148_LCSR_VMCTRL_VTON_512 | TSI148_LCSR_VMCTRL_VREL_T_D_R);
	iowrite32be(val, &regs->lcsr.vmctrl);
}

/*
 *
 */

/**
 * tsi148_init() - Chip initialization
 * @regs: Chip registers base address
 *
 */
void tsi148_init(struct tsi148_chip *regs)
{
	unsigned int vme_stat_reg;

	/* Clear board fail, and power-up reset */
	vme_stat_reg = ioread32be(&regs->lcsr.vstat);
	vme_stat_reg &= ~TSI148_LCSR_VSTAT_BDFAIL;
	vme_stat_reg |= TSI148_LCSR_VSTAT_CPURST;
	iowrite32be(vme_stat_reg, &regs->lcsr.vstat);

	/* Store the chip base address */
	tsi148_chip_global = regs;
}
