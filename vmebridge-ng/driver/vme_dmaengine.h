/* SPDX-License-Identifier: GPL-2.0-or-later */
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#ifndef _VME__DMAENGINE_H_
#define _VME__DMAENGINE_H_

//#undef _UAPI__LINUX_UIO_H
#include <linux/kernel.h>
#include <linux/dmaengine.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/scatterlist.h>
#include <linux/interrupt.h>
#include <linux/version.h>

#include "vmebus.h"

/**
 * struct hw_desc_entry - Hardware descriptor
 * @list: Descriptors list
 * @va: Virtual address of the descriptor
 * @phys: Bus address of the descriptor
 *
 *  This data structure is used internally to keep track of the hardware
 * descriptors that are allocated in order to free them when the transfer
 * is done.
 */
struct hw_desc_entry {
	struct list_head	list;
	void			*va;
	dma_addr_t		phys;
};

/**
 * VME DMA control commands
 */
enum vme_dma_ctrl_field {
	VME_DMA_LIST_MODE = 0, /* linked-list mode DMA transaction */
	VME_DMA_DIRECT_MODE, /*direct mode (one transfer) DMA transaction */
	VME_DMA_PAUSE, /* PAUSE DMA transaction */
	VME_DMA_START, /* START DMA transaction */
	VME_DMA_ABORT, /* ABORT DMA transaction */
};

/*
 * VME DMA state
 */
enum vme_dma_state_field {
	VME_DMA_IS_BUSY = 0,
	VME_DMA_IS_DONE,
	VME_DMA_IS_PAUSE,
};

/**
 * @wait: used by old sync API to detect when the DMA is complete
 */
struct vme_dma_chan {
	int chan_idx; /* assigned at instantiation: first got index 0 */
	struct dma_chan dchan;
	struct list_head pending_list;
	struct vme_dma_tx_desc *tx_desc_current;
	spinlock_t lock;
	struct dma_slave_config cfg;

	struct tasklet_struct dma_start_task;
	wait_queue_head_t wait;

	struct list_head hw_desc_list;
	/* dma channel private pointer to hold specific HW info */
	void *private;
};

struct vme_dma_tx_desc {
	struct dma_async_tx_descriptor tx;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
	enum dmaengine_tx_result tx_res;
#endif
	enum dma_transfer_direction direction;
	struct scatterlist *sgl;
	unsigned int sg_len;
	struct vme_dma desc;
	struct list_head list;
};

struct vme_bridge_device;
struct vme_bridge_dma_mgr;

struct vme_bridge_dma_ops {
	/* Create a pool of consistent memory blocks, for dma. */
	struct dma_pool * (*dma_pool_create)(struct pci_dev *pdev);

	/* Returns number of DMA channel implemented by the bridge */
	int (*dma_get_nr_chan)(void);

	/*
	 * Some chip have the ability to throttle DMA transferts by acting on
	 * block size and/or back-off timer.
	 * Returns DMA tuning capabilities of the bridge
	 */
	int (*dma_get_capabilities)(void);

	/* Init dma channel private pointer to hold bridge specific HW info */
	int (*dma_chan_init)(struct vme_bridge_device *vbridge,
			      struct vme_dma_chan *vdchan);

	/* Release private pointer and resources if necessary */
	void (*dma_chan_release)(struct vme_bridge_device *vbridge,
				  struct vme_dma_chan *vdchan);

	/* Write DMA control command as: PAUSE, START, ABORT,... */
	int (*dma_set_ctrl)(struct vme_dma_chan *vdchan,
			      enum vme_dma_ctrl_field cmd);

	/* Returns the bit of the requested state field */
	int (*dma_check_state)(struct vme_dma_chan *vdchan,
				enum vme_dma_state_field state);

	/* */
	int (*dma_setup)(struct vme_dma_chan *dchan);

	/* */
	void (*dma_free)(struct vme_dma_chan *vdchan);

	/*
	 * DMA transfer pre and post processing
	 * Mainly implemented to support PLDZ missing features:
	 * - not aligned DMA size to 4 bytes (which implies that the minimum
	 *   DMA size is 4 bytes)
	 */
	void (*dma_transfer_pre)(struct vme_dma *desc_new,
				 struct vme_dma *desc, int to_user);
	void (*dma_transfer_post)(struct vme_dma *desc, int to_user);
};

/**
 * \brief VME DMA control capabilities
 * DMA transfert can be tune in terms of back-off time and block size, if it is
 * suported by the spcific VME bridge
 */
#define VME_DMA_TRANSFER_BLK_SZ BIT(0)
#define VME_DMA_TRANSFER_BACKOFF_TIME BIT(1)

#define VME_BRIDGE_FLAGS_MAX (32)
#define VME_BRIDGE_DMA_PAUSE BIT(0)
#define VME_DEV_FLAG_FORCE_DMA_CFG BIT(1)


/**
 * @dma_pool: pool of contiguous memory where to put DMA transfer descriptors
 */
struct vme_bridge_dma_mgr {
	DECLARE_BITMAP(flags, VME_BRIDGE_FLAGS_MAX);
	struct vme_dma_ctrl    dma_param;

	struct dma_device dma;
	int dma_is_registered; /* flag to drive ressource freeing */
	unsigned int dma_cap; /* bridge's DMA capabilities: blksz, backoff */
	struct vme_dma_chan *vdchan;
	unsigned int nr_vdchan;
	struct mutex mtx_vme_do_dma;
	struct tasklet_struct dma_done_task;
	struct dma_pool *dma_pool;

	struct vme_bridge_dma_ops *ops;
};

static inline struct vme_dma_chan *to_vme_dma_chan(struct dma_chan *_ptr)
{
	return container_of(_ptr, struct vme_dma_chan, dchan);
}

static inline struct vme_dma_tx_desc *to_vme_dma_tx_desc(
	struct dma_async_tx_descriptor *_ptr)
{
	return container_of(_ptr, struct vme_dma_tx_desc, tx);
}

static inline struct vme_bridge_dma_mgr *to_vme_bridge_dma_mgr(
	struct dma_device *_ptr)
{
	return container_of(_ptr, struct vme_bridge_dma_mgr, dma);
}

#endif
