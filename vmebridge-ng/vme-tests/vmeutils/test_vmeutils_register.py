"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import os
import subprocess
import random

class TestVmeutilsRegister(object):
    """It tests that the tool vme-register"""
    def test_register_missing_param_01(self):
        """missing all arguments"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register", ]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_missing_param_02(self):
        """missing size argument"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_01(self):
        """Invalid slot number 0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(0),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_02(self):
        """Invalid slot number grater than 21"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(22, 4294967295)),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_03(self):
        """Invalid slot number in hex even if in range"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", hex(random.randint(1, 21)),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_04(self):
        """Invalid size 0x0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", hex(0)]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_05(self):
        """Invalid size 0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", str(0)]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_06(self):
        """Invalid irq vector grater than 255"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--irq-vector", str(random.randint(256, 4294967295 - 1))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_07(self):
        """Invalid irq vector grater than 0xFF"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--irq-vector", hex(random.randint(256, 4294967295 - 1 ))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_08(self):
        """Invalid irq level 0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--irq-vector", str(random.randint(0, 255)),
                                "--irq-level", str(0), ]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_09(self):
        """Invalid irq level above 7"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--irq-vector", str(random.randint(0, 255)),
                                "--irq-level", str(random.randint(8, 4294967295)), ]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_10(self):
        """Invalid irq level hex even in range"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--irq-vector", str(random.randint(0, 255)),
                                "--irq-level", hex(random.randint(1, 7)), ]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_11(self):
        """Invalid address modifier as integer"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--address-modifier", str(57)]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_12(self):
        """Invalid partial identifier"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--identifier", "0x{:x}".format(
                                    random.randint(0, 4294967295))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_13(self):
        """Invalid partial identifier"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--identifier", "0x{:x},0x{:x}".format(
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_14(self):
        """Invalid partial identifier with decimal"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--identifier", "0x{:x},0x{:x},{:d}".format(
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_15(self):
        """Invalid partial identifier with decimal"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--identifier", "0x{:x},{:d},0x{:x}".format(
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_16(self):
        """Invalid partial identifier with decimal"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(random.randint(1, 21)),
                                "--size", "0x80000",
                                "--identifier", "{:d},0x{:x},0x{:x}".format(
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295),
                                    random.randint(0, 4294967295))]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)
