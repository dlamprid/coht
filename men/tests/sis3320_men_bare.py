# Author Adam Wujek, CERN 2017

from test_config import *
from vme_men_helpers import *
import pytest
import time

vme = vme_men()

def test_check_firmware_version():
    assert vme.read_val(A32D32, SIS3320_addr + 4) == 0x33202005

def test_write_0x5a5a5a5a_to_ADC1_trigger_threshold_register():
    vme.write(A32D32, SIS3320_addr + 0x2000000 + 0x34, value = 0x5a5a5a5a)
    val = vme.read_val(A32D32, SIS3320_addr + 0x2000000 + 0x34)
    assert val == 0x5a5a5a5a

def test_write_0xa5a5a5a5_to_channel_enable_mask_register():
    vme.write(A32D32, SIS3320_addr + 0x2000000 + 0x34, value = 0xa5a5a5a5)
    val = vme.read_val(A32D32, SIS3320_addr + 0x2000000 + 0x34)
    assert val == 0xa5a5a5a5

def test_read_DMA_A32D32():
    # read from the addres SIS3320_addr + SIS3320_buff_offset using DMA
    val = vme.read_val(A32D32_BLT, SIS3320_addr + SIS3320_buff_offset)
    assert val != 0x0

def test_read_DMA_A32D32_4():
    # read from the addres SIS3320_addr + SIS3320_buff_offset + 4 using DMA
    val = vme.read_val(A32D32_BLT, SIS3320_addr + SIS3320_buff_offset + 4)
    assert val != 0x0

def test_read_DMA_A32D64():
    # read from the addres SIS3320_addr + SIS3320_buff_offset using DMA
    val = vme.read_val(A32D64_MBLT, SIS3320_addr + SIS3320_buff_offset)
    assert val != 0x0

def test_read_DMA_A32D64_8():
    # read from the addres SIS3320_addr + SIS3320_buff_offset + 8 using DMA
    val = vme.read_val(A32D64_MBLT, SIS3320_addr + SIS3320_buff_offset + 8)
    assert val != 0x0

def test_read_A32D32_0x1kw():
    # read 0x1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000
    #ffile = "sis33_men_"+str(hex(addr))+"_sa_"+str(hex(size))+".txt"
    vme.read(A32D32, addr, size)

def test_read_A32D32_0x1Mw():
    # read 0x1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000000
    #ffile = "sis33_men_"+str(hex(addr))+"_sa_"+str(hex(size))+".txt"
    vme.read(A32D32, addr, size)

def test_read_DMA_A32D32_0x1Kw():
    # read 1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000
    #ffile = "sis33_men_"+str(hex(addr))+"_dma32_"+str(hex(size))+".txt"
    vme.read(A32D32_BLT, addr, size)

# does not work for BB DMA
def test_read_DMA_A32D32_0x1Mw():
    # read 1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000000
    #ffile = "sis33_men_"+str(hex(addr))+"_dma32_"+str(hex(size))+".txt"
    vme.read(A32D32_BLT, addr, size)

def test_read_DMA_A32D64_0x1Kw():
    # read 1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000
    #ffile = "sis33_men_"+str(hex(addr))+"_dma64_"+str(hex(size))+".txt"
    vme.read(A32D64_MBLT, addr, size)

def test_read_DMA_A32D64_0x1Mw():
    # read 1M of words from SIS3320_addr + SIS3320_buff_offset using DMA
    addr = SIS3320_addr + SIS3320_buff_offset
    size = 0x1000000
    #ffile = "sis33_men_"+str(hex(addr))+"_dma64_"+str(hex(size))+".txt"
    vme.read(A32D64_MBLT, addr, size)
