/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * pldz002.h - Low level support for the MEN pldz002 PCI-VME IP-core bridge
 *
 */

#ifndef _PLDZ002_H
#define _PLDZ002_H

#include <linux/version.h>
#include <asm-generic/iomap.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "pldz002_hw.h"


#define PLDZ002_CHIP_NAME "PLDZ002"

/* Interrupt counters */
enum interrupt_idx {
	PLDZ002_INT_STATS_SPURIOUS = 0,
	PLDZ002_INT_STATS_IRQ1,
	PLDZ002_INT_STATS_IRQ2,
	PLDZ002_INT_STATS_IRQ3,
	PLDZ002_INT_STATS_IRQ4,
	PLDZ002_INT_STATS_IRQ5,
	PLDZ002_INT_STATS_IRQ6,
	PLDZ002_INT_STATS_IRQ7,
	PLDZ002_INT_STATS_BERR,
	PLDZ002_INT_STATS_ACFAIL,
	PLDZ002_INT_STATS_SYSFAIL,
	PLDZ002_INT_STATS_DMA,
	PLDZ002_INT_STATS_DMAERR,
	PLDZ002_INT_STATS_MBOXRD0,
	PLDZ002_INT_STATS_MBOXWR0,
	PLDZ002_INT_STATS_MBOXRD1,
	PLDZ002_INT_STATS_MBOXWR1,
	PLDZ002_INT_STATS_MBOXRD2,
	PLDZ002_INT_STATS_MBOXWR2,
	PLDZ002_INT_STATS_MBOXRD3,
	PLDZ002_INT_STATS_MBOXWR3,
	PLDZ002_INT_STATS_LOCMON0,
	PLDZ002_INT_STATS_LOCMON1
};


/*
 * PLDZ002 dma handles a DMA transaction by batch of 16 dma blocks. So
 * depending of the total length of the DMA transaction and if physically
 * contiguous pages have been coalesced into a single scatterlist entry, a
 * single DMA transaction must require several batches of 16 dma blocks to be
 * scheduled in sequence. The PLDZ002 raise a DMA interrupt for each batch
 * which is handled internally, and only when the complete DMA transaction is
 * handled, the client is notified that the DMA TX is finished
 */
struct pldz002_dma_attr {
	uint32_t am;
	uint32_t is_blt;
	uint32_t align_vme;
};

struct pldz002_dma_chan {
	wait_queue_head_t wait_dma_batch; // used to wait end of batch if many
	uint32_t nr_batch; // total number of dma batches to handle dma TX
	uint32_t curr_batch_idx; // current dma batch index
	uint32_t curr_vme_addr;
	struct pldz002_dma_attr dma_attr;
	void __iomem *regs_vaddr; /* PLDZ002 control registers */
	void __iomem *dma_bd_vaddr; /* PLDZ002 registers for DMA BD (SRAM) */
};

enum pldz002_vme_spaces {
	PLDZ002_VME_A16D16 = 0,
	PLDZ002_VME_A16D32,
	PLDZ002_VME_A24D16,
	PLDZ002_VME_A24D32,
	PLDZ002_VME_A32D32,
	PLDZ002_VME_CRCSR,
	PLDZ002_VME_SPACES_COUNT
};


/* PLDZ002 (BAR0 and BAR1): 3 permament memory mapping */
#define PLDZ002_REGION_COUNT 3
/* Used as index in pldz_mem table */
#define PLDZ002_REGION_REGS 0
#define PLDZ002_REGION_IACK 1
#define PLDZ002_REGION_SRAM 2
/*
 *
 */
struct pldz002_mem_region {
	struct resource *rsrc;
	void __iomem *vaddr;
};

/*
 * PLDZ002 private data
 */
struct pldz002_chip {
	struct pldz002_mem_region regions[PLDZ002_REGION_COUNT];
};

static inline char *pldz002_get_chip_name(void)
{
	return PLDZ002_CHIP_NAME;
}

/* Low level misc inline stuff */
static inline void __iomem *pldz002_get_regsaddr(struct vme_bridge_device *vbridge)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;

	return chip->regions[PLDZ002_REGION_REGS].vaddr;
}

/* Low level misc inline stuff */
static inline int pldz002_get_nr_windows(void)
{
	return PLDZ002_NR_WINDOWS;
}

/* Low level misc inline stuff */
static inline int pldz002_get_syscon(struct vme_bridge_device *vbridge)
{
	int syscon;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	syscon = ioread32(regs_vaddr + PLDZ002_SYSCTL);
	return syscon & PLDZ002_SYSCTL_SYSCON;
}

static inline int pldz002_bus_error_check(struct vme_bridge_device *vbridge,
					  int clear)
{
	return 0;
}

extern int pldz002_dma_set_ops(struct vme_bridge_device *vbridge);
extern int pldz002_irq_set_ops(struct vme_bridge_device *vbridge);
extern int pldz002_window_set_ops(struct vme_bridge_device *vbridge);
extern int pldz002_procfs_set_ops(struct vme_bridge_device *vbridge);
extern void pldz002_schedule_next_dma_batch(struct vme_dma_chan *vdchan);

#endif /* _PLDZ002_H */
