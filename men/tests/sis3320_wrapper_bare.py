# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *
import pytest
import time

vme = vme_wrapper()

def test_check_firmware_version():
    assert vme.read_val(A32D32, SIS3320_addr + 4) == 0x33202005

def test_write_0x5a5a5a5a_to_ADC1_trigger_threshold_register():
    vme.write(A32D32, SIS3320_addr + 0x2000000 + 0x34, value = 0x5a5a5a5a)
    val = vme.read_val(A32D32, SIS3320_addr + 0x2000000 + 0x34)
    assert val == 0x5a5a5a5a

def test_write_0xa5a5a5a5_to_channel_enable_mask_register():
    vme.write(A32D32, SIS3320_addr + 0x2000000 + 0x34, value = 0xa5a5a5a5)
    val = vme.read_val(A32D32, SIS3320_addr + 0x2000000 + 0x34)
    assert val == 0xa5a5a5a5

def test_read_BLT():
    # read from the addres SIS3320_addr + SIS3320_buff_offset using DMA
    val = vme.read_val(A32D32_BLT, SIS3320_addr + SIS3320_buff_offset)
    assert val != 0x0
    val = vme.read_val(A32D32_BLT, SIS3320_addr + SIS3320_buff_offset + 4)
    assert val != 0x0

def test_read_MBLT():
    # read from the addres SIS3320_addr + SIS3320_buff_offset using DMA
    val = vme.read_val(A32D64_MBLT, SIS3320_addr + SIS3320_buff_offset)
    assert val != 0x0
    val = vme.read_val(A32D64_MBLT, SIS3320_addr + SIS3320_buff_offset + 8)
    assert val != 0x0
