#!/bin/bash

# (C) CERN 2018
# author: Adam Wujek <adam.wujek@cern.ch>

#make sure install_drv.sh from the same dir as this file
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# mcedit syntax fix "
source "$SCRIPT_DIR"/install_drv.sh


if [ "$1" == "L866" ] || [ "$1" == "L867" ]; then
    if [ -d acc/dsc/none/"$1"/*/vmebus_a25/ ]; then
	DRV_DIR=`ls -d acc/dsc/none/$1/*/vmebus_a25/`"$DRV_DIR"
    else
	DRV_DIR=`ls -d ../acc/dsc/none/$1/*/vmebus_a25/`"$DRV_DIR"
    fi
elif [ -n "$1" ]; then
    DRV_DIR="$1"/"$DRV_DIR"
fi

# let all echos to be printed
sleep 1

install_drv men_dbg.ko
install_drv men_vme4l-core.ko # debug=0
install_drv men_oss.ko
install_drv men_chameleon.ko
install_drv men_chameleon_io.ko
install_drv men_lx_chameleon.ko # debug=0
install_drv men_serflash.ko
install_drv men_pldz002_cham.ko # debug=0 # bounce_buffer=1
