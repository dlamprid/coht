/**
 * It lists all VME devices
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "vmeutils.h"

static int verbose;
static int show_slot;
const char program_name[] = "lsvme";

static char options[] = "hv";
static const char help_msg[] =
	"Usage: lsvme [options]\n"
	"\n"
	"General options:\n"
	"--help, -h            Print this message\n"
	"--slot <slot>         Limit to the given slot\n"
	"\n"
	"Display options:\n"
	"--show-slot           Show only slot number instead of full name\n"
	"--verbose, -v         Be verbose (add more v to be more verbose)\n"
	"\n";


enum vme_register_options {
	VME_LS_OPT_SLOT = 1024,
	VME_LS_OPT_HELP,
};
static struct option long_options[] =
{
	{"verbose", no_argument, &verbose, 1},
	{"show-slot", no_argument, &show_slot, 1},
	{"help", no_argument, 0, VME_LS_OPT_HELP},
	{"slot", required_argument, 0, VME_LS_OPT_SLOT},
	{0, 0, 0, 0}
};


struct lsvme_view_opt {
	unsigned int verbose;
	unsigned int slot;
};

static void lsvme_show_device_specification(struct vme_dev *d)
{
	printf("\tspecification: %d - %s\n",
	       d->spec, vme_spec_to_string(d->spec));
}

static void lsvme_print_cr_value(uint8_t *cr,
				 char *name,
				 unsigned int offset,
				 unsigned int len)
{
	int i;

	printf("\t\t%s: 0x", name);
	for (i = 0; i < len; ++i)
		printf("%02x", cr[offset + (i * 4)]);
	putchar('\n');
}

static void lsvme_print_csr_value(uint8_t *cr,
				 char *name,
				 unsigned int offset,
				 unsigned int len)
{
	lsvme_print_cr_value(cr, name, offset - VME_DEV_CSR_START, len);
}


static void lsvme_show_device_cr(struct vme_dev *d)
{
	printf("\tConfiguration ROM:\n");
	if (d->spec == VME_SPEC_UNKNOWN) {
		printf("\t\tNot Supported\n");
		return;
	}
	if (d->spec >= VME_SPEC_VME64) {
		lsvme_print_cr_value(d->cr_dump, "Checksum", 0x3, 1);
		lsvme_print_cr_value(d->cr_dump, "Lenght of ROM", 0x7, 3);
		lsvme_print_cr_value(d->cr_dump, "ROM DAW", 0x13, 1);
		lsvme_print_cr_value(d->cr_dump, "CSR DAW", 0x17, 1);
		lsvme_print_cr_value(d->cr_dump, "Specification", 0x1B, 1);
		lsvme_print_cr_value(d->cr_dump, "ASCII 'C'", 0x1F, 1);
		lsvme_print_cr_value(d->cr_dump, "ASCII 'R'", 0x23, 1);
		lsvme_print_cr_value(d->cr_dump, "Lenght of ROM", 0x7, 3);
		lsvme_print_cr_value(d->cr_dump, "Manufacturer's ID", 0x27, 3);
		lsvme_print_cr_value(d->cr_dump, "Board ID", 0x33, 4);
		lsvme_print_cr_value(d->cr_dump, "Revision ID", 0x43, 4);
		lsvme_print_cr_value(d->cr_dump, "String pointer", 0x53, 3);
		lsvme_print_cr_value(d->cr_dump, "Program ID", 0x7F, 1);
	}

	if (d->spec >= VME_SPEC_VME64x) {
		lsvme_print_cr_value(d->cr_dump, "BEG_USR_CR", 0x83, 3);
		lsvme_print_cr_value(d->cr_dump, "END_USR_CR", 0x8F, 3);
		lsvme_print_cr_value(d->cr_dump, "BEG_CRAM", 0x9B, 1);
		lsvme_print_cr_value(d->cr_dump, "END_CRAM", 0xA7, 1);
		lsvme_print_cr_value(d->cr_dump, "BEG_USR_CSR", 0xB3, 1);
		lsvme_print_cr_value(d->cr_dump, "END_USR_CSR", 0xBF, 1);
		lsvme_print_cr_value(d->cr_dump, "BEG_SN", 0xCB, 1);
		lsvme_print_cr_value(d->cr_dump, "END_SN", 0xD7, 1);
		lsvme_print_cr_value(d->cr_dump, "Slave Characteristics Parameter",
				     0xE3, 1);
		lsvme_print_cr_value(d->cr_dump, "User Slave Characteristics",
				     0xE7, 1);
		lsvme_print_cr_value(d->cr_dump, "Master Characteristics Parameter",
				     0xEB, 1);
		lsvme_print_cr_value(d->cr_dump, "User Master Characteristics",
				     0xEF, 1);
		lsvme_print_cr_value(d->cr_dump, "Interrupt Handler Capabilities",
				     0xF3, 1);
		lsvme_print_cr_value(d->cr_dump, "Interrupter Capabilities",
				     0xF7, 1);
		lsvme_print_cr_value(d->cr_dump, "CRAM_ACCESS_WIDTH", 0xFF, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 0 DAW", 0x103, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 1 DAW", 0x107, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 2 DAW", 0x10B, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 3 DAW", 0x10F, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 4 DAW", 0x113, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 5 DAW", 0x117, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 6 DAW", 0x11B, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 7 DAW", 0x11F, 1);
		lsvme_print_cr_value(d->cr_dump, "Function 0 AMCAP", 0x123, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 1 AMCAP", 0x143, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 2 AMCAP", 0x163, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 3 AMCAP", 0x183, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 4 AMCAP", 0x1A3, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 5 AMCAP", 0x1C3, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 6 AMCAP", 0x1E3, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 7 AMCAP", 0x203, 8);
		lsvme_print_cr_value(d->cr_dump, "Function 0 XAMCAP",
				     0x223, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 1 XAMCAP",
				     0x2A3, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 2 XAMCAP",
				     0x323, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 3 XAMCAP",
				     0x3A3, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 4 XAMCAP",
				     0x423, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 5 XAMCAP",
				     0x4A3, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 6 XAMCAP",
				     0x523, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 7 XAMCAP",
				     0x5A3, 32);
		lsvme_print_cr_value(d->cr_dump, "Function 0 ADEM", 0x623, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 1 ADEM", 0x633, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 2 ADEM", 0x643, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 3 ADEM", 0x653, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 4 ADEM", 0x663, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 5 ADEM", 0x673, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 6 ADEM", 0x683, 4);
		lsvme_print_cr_value(d->cr_dump, "Function 7 ADEM", 0x693, 4);
		lsvme_print_cr_value(d->cr_dump, "Master DAWPR", 0x6AF, 1);
		lsvme_print_cr_value(d->cr_dump, "Master AMCAP", 0x6B3, 8);
		lsvme_print_cr_value(d->cr_dump, "Master XAMCAP", 0x6D3, 32);
	}
}

static void lsvme_show_device_csr(struct vme_dev *d)
{
	printf("\tControl Status Register:\n");
	if (d->spec == VME_SPEC_UNKNOWN) {
		printf("\t\tNot Supported\n");
		return;
	}
	if (d->spec >= VME_SPEC_VME64) {
		lsvme_print_csr_value(d->csr_dump, "Base Address Register",
				      0x7FFFF, 1);
		lsvme_print_csr_value(d->csr_dump, "Bit Set",
				      0x7FFFB, 1);
		lsvme_print_csr_value(d->csr_dump, "Bit Clear",
				      0x7FFF7, 1);
	}
	if (d->spec >= VME_SPEC_VME64x) {
		lsvme_print_csr_value(d->csr_dump, "CRAM_OWNER",
				      0x7FFF3, 1);
		lsvme_print_csr_value(d->csr_dump, "User Bit Set",
				      0x7FFEF, 1);
		lsvme_print_csr_value(d->csr_dump, "User Bit Clear",
				      0x7FFEB, 1);
		lsvme_print_csr_value(d->csr_dump, "Function 0 ADER",
				      0x7FF63, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 1 ADER",
				      0x7FF73, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 2 ADER",
				      0x7FF83, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 3 ADER",
				      0x7FF93, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 4 ADER",
				      0x7FFA3, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 5 ADER",
				      0x7FFB3, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 6 ADER",
				      0x7FFC3, 4);
		lsvme_print_csr_value(d->csr_dump, "Function 7 ADER",
				      0x7FFD3, 4);
	}
}

static void lsvme_show_device(struct vme_dev *d, struct lsvme_view_opt *vopt)
{
	if (vopt->slot)
		printf("%02d\n", d->slot);
	else
		printf("VME slot %02d - %s\n", d->slot, d->name);
	if (vopt->verbose > 0) {
		lsvme_show_device_specification(d);
	}
	if (vopt->verbose > 1) {
		lsvme_show_device_cr(d);
		lsvme_show_device_csr(d);
	}
}

static void lsvme_show(struct vme_access *a, struct lsvme_view_opt *vopt)
{
	struct vme_dev *d;

	for (d = a->devices; d; d = d->next)
		lsvme_show_device(d, vopt);
}

int main(int argc, char *argv[])
{
	struct lsvme_view_opt vopt;
	struct vme_filter filter;
	struct vme_access *a;
	int c;
	int ret;
	int option_index;

	command_version(argc, argv);

	memset(&filter, 0, sizeof(filter));
	memset(&vopt, 0, sizeof(vopt));
	while ((c = getopt_long(argc, argv, options, long_options,
				&option_index)) != -1) {
		switch (c) {
		default:
			break;
		case 'h':
		case VME_LS_OPT_HELP:
			fprintf(stderr, help_msg);
			exit(EXIT_SUCCESS);
		case 'v':
			vopt.verbose++;
			break;
		case VME_LS_OPT_SLOT:
			ret = sscanf(optarg, "%i", &filter.slot);
			if (ret != 1) {
				fprintf(stderr, "Invalid slot number %s\n",
					optarg);
				exit(EXIT_FAILURE);
			}
			break;
		}
	}
	if (verbose)
		vopt.verbose++;
	vopt.slot = show_slot;
	a = vme_access_alloc();
	vme_sysfs_scan_bus(a, &filter);
	lsvme_show(a, &vopt);
	vme_access_cleanup(a);

	exit(EXIT_SUCCESS);
}
