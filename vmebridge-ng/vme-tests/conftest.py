import pytest
import subprocess
from PyVME.PyVmeDevice import PyVmeDevice
import time

@pytest.fixture(scope="module")
def vme_dev_param():
    return {"slot": 11,
            "am": 0x39,
            "addr": 0x580000,
            "size": 0x80000,
            "irq_vector": 0xd9,
            "irq_level": 2,
            "devname": "fmc-svec-a24"
            }

@pytest.fixture(scope="module")
def vme_device_static(vme_dev_param):
    vme_device_static = VMEDevice("vme.{:02d}".format(vme_dev_param["slot"]),
                                  vme_dev_param["slot"],
                                  vme_dev_param["am"],
                                  vme_dev_param["addr"],
                                  vme_dev_param["size"],
                                  vme_dev_param["irq_vector"],
                                  vme_dev_param["irq_level"],
                                  vme_dev_param["devname"])
    vme_device_static.register()
    yield vme_device_static
    vme_device_static.unregister()

@pytest.fixture(scope="module")
def vme_device_vd80():
    vdev = PyVmeDevice(9)
    vdev.rescan()
    yield vdev
    vdev.unregister()

VME64X_SVEC_SLOTS = [8, 10, 11, 12]
@pytest.fixture(scope="module")
def svec():
    for slot in VME64X_SVEC_SLOTS:
        vdev = PyVmeDevice(slot)
        vdev.rescan()
        yield vdev
        vdev.unregister()
        # Some tests may load the driver - remove it
        subprocess.run(["rmmod", "svec-fmc-carrier"], check=False)

@pytest.fixture(scope="module")
def svec_empty():
    vdev = PyVmeDevice(VME64X_SVEC_SLOTS[1])
    vdev.rescan()
    yield vdev
    vdev.unregister()
    # Some tests may load the driver - remove it
    subprocess.run(["rmmod", "svec-fmc-carrier"], check=False)

@pytest.fixture(scope="module")
def svec_all():
    svecs = []
    for slot in VME64X_SVEC_SLOTS:
        vdev = PyVmeDevice(slot)
        vdev.rescan()
        svecs.append(vdev)
    yield svecs

    for vdev in svecs:
        vdev.unregister()

# Execute this at the beginning

def pytest_addoption(parser):
    parser.addoption("--no-svec-program", action="store_true", default=False)

@pytest.fixture(scope="session", autouse=True)
def svec_program_all(request):
    if request.config.getoption("--no-svec-program"):
       return
    # Program all SVECs with golden bitstream
    subprocess.run(["modprobe",
                    "-d", "/user/fvaga/deployment/L867/",
                    "svec-fmc-carrier"], check=True)
    for slot in VME64X_SVEC_SLOTS:
        with PyVmeDevice(slot) as vdev:
            vdev.register(None, None, 0x80000, None, None, "fmc-svec-a24")
            with open("/sys/kernel/debug/vme.{:d}/fpga_firmware".format(slot), "w") as f_fpga:
                f_fpga.write("svec_golden.bin")
            time.sleep(0.5)
    time.sleep(1)
    subprocess.run(["rmmod", "svec-fmc-carrier"], check=True)
