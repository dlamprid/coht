/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2018 CERN
 * Author: Federico Vaga <federico.vaga@vaga.pv.it>
 */
#ifndef __VME_COMPAT_H__
#define __VME_COMPAT_H__

#include <linux/version.h>
#include <linux/mm_types.h>
#include <linux/ioport.h>
#include <linux/device.h>
#include <linux/sysfs.h>
#include <linux/proc_fs.h>

#ifndef  __ATTR_RW
#define __ATTR_RW(_name) __ATTR(_name, (0644),	\
			 _name##_show, _name##_store)
#endif

#ifndef __ATTR_WO
#define __ATTR_WO(_name) {						\
	.attr	= { .name = __stringify(_name), .mode = 0200 },	\
	.store	= _name##_store,					\
}
#endif

#ifndef DEVICE_ATTR_RW
#define DEVICE_ATTR_RW(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_RW(_name)
#endif

#ifndef DEVICE_ATTR_RO
#define DEVICE_ATTR_RO(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_RO(_name)
#endif

#ifndef  DEVICE_ATTR_WO
#define DEVICE_ATTR_WO(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_WO(_name)
#endif

#ifndef __BIN_ATTR
#define __BIN_ATTR(_name, _mode, _read, _write, _size) {		\
	.attr = { .name = __stringify(_name), .mode = _mode },		\
	.read	= _read,						\
	.write	= _write,						\
	.size	= _size,						\
}
#endif

#ifndef BIN_ATTR
#define BIN_ATTR(_name, _mode, _read, _write, _size)			\
struct bin_attribute bin_attr_##_name = __BIN_ATTR(_name, _mode, _read, \
					_write, _size)
#endif

#if KERNEL_VERSION(3, 10, 0) > LINUX_VERSION_CODE
#ifndef PDE_DATA
#define PDE_DATA(inode) PDE(inode)->data
#endif
#endif

#if KERNEL_VERSION(3, 12, 0) > LINUX_VERSION_CODE
#ifdef RHEL_RELEASE_VERSION
#if RHEL_RELEASE_VERSION(7, 7) > RHEL_RELEASE_CODE
#define BUS_ATTR_WO(_name) \
	struct bus_attribute bus_attr_##_name = __ATTR_WO(_name)
#define BUS_ATTR_RO(_name) \
	struct bus_attribute bus_attr_##_name = __ATTR_RO(_name)
#endif
#else
#define BUS_ATTR_RW(_name)
#define BUS_ATTR_WO(_name)
#define BUS_ATTR_RO(_name)
#endif
#endif

extern long get_user_pages_l(unsigned long start, unsigned long nr_pages,
			     unsigned int gup_flags, struct page **pages,
			     struct vm_area_struct **vmas);

extern int insert_resource_l(struct resource *parent, struct resource *new);
#endif

#if KERNEL_VERSION(3, 10, 0) > LINUX_VERSION_CODE
enum dmaengine_tx_result {
	DMA_TRANS_NOERROR = 0,		/* SUCCESS */
	DMA_TRANS_READ_FAILED,		/* Source DMA read failed */
	DMA_TRANS_WRITE_FAILED,		/* Destination DMA write failed */
	DMA_TRANS_ABORTED,		/* Op never submitted / aborted */
};

struct dmaengine_result {
	enum dmaengine_tx_result result;
	u32 residue;
};
#endif
