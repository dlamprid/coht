// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * pldz002_irq.c - pldz002 PCI-VME bridge interrupt management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge interrupt management code.
 */

#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/irqdomain.h>

#include "pldz002.h"
#include "vme_irq.h"
#include "vme_bridge.h"

static struct interrupt_stats int_stats[] = {
	{.name = "SPURIOUS"},
	{.name = "IRQ1"}, {.name = "IRQ2"}, {.name = "IRQ3"}, {.name = "IRQ4"},
	{.name = "IRQ5"}, {.name = "IRQ6"}, {.name = "IRQ7"},
	{.name = "BUSERR"}, {.name = "ACFAIL"}, {.name = "SYSFAIL"},
	{.name = "DMA"}, {.name = "DMAERR"},
	{.name = "MBOXRD0"}, {.name = "MBOXWR0"},
	{.name = "MBOXRD1"}, {.name = "MBOXWR1"},
	{.name = "MBOXRD2"}, {.name = "MBOXWR2"},
	{.name = "MBOXRD3"}, {.name = "MBOXWR3"},
	{.name = "LOCMON0"}, {.name = "LOCMON1"}
};

static void pldz002_vme_error(struct vme_bridge_device *vbridge,
			void __iomem *regs_vaddr, struct vme_bus_error *error)
{
	error->address = ioread32(regs_vaddr + PLDZ002_BERR_ADDR);
	error->am = ioread32(regs_vaddr + PLDZ002_BERR_ACC) &
					PLDZ002_BERR_ACC_AM_MASK; /*AM  mask */
}

static inline int pldz002_handle_vme_error_interrupt(
		struct vme_bridge_device *vbridge, void __iomem *regs_vaddr)
{
	struct vme_bus_error error;
	unsigned long flags;
	spinlock_t *lock;
	uint32_t berr;

	berr = ioread8(regs_vaddr + PLDZ002_MSTR) & PLDZ002_MSTR_BERR;
	if (berr == 0)
		return 0; // No VME bus error

	lock = &vbridge->verr.lock;

	spin_lock_irqsave(lock, flags);

	pldz002_vme_error(vbridge, regs_vaddr, &error);
	/* clear error */
	iowrite8(PLDZ002_MSTR_BERR | PLDZ002_MSTR_IBERREN,
		 regs_vaddr + PLDZ002_MSTR);
	/* dispatch bus error to clients */
	vme_bus_error_dispatch(vbridge, &error);

	spin_unlock_irqrestore(lock, flags);
	int_stats[PLDZ002_INT_STATS_BERR].count++;
	return 1; // VME bus error occurred
}

int pldz002_iack(void *chip, int irq)
{
	struct pldz002_chip *pldz002 = chip;
	void __iomem *iack_vaddr = pldz002->regions[PLDZ002_REGION_IACK].vaddr;
	int vec;

	/*
	 * fetch vector (VME IACK cycle)
	 * See Men Arch spec (cahp 3.12) for an explanation of IACK
	 * address space
	 */
	vec = ioread8(iack_vaddr + (irq << 1) + 1);
	int_stats[PLDZ002_INT_STATS_IRQ1 + irq - 1].count++;
	return vec & 0xff;
}

static inline int pldz002_handle_irq_interrupt(struct vme_bridge_device *vbridge,
					       void __iomem *regs_vaddr)
{
	uint32_t istat;

	istat = ioread8(regs_vaddr + PLDZ002_ISTAT) & PLDZ002_INT_IRQM;
	if (istat == 0)
		return 0; // No IRQ found
	vme_handle_interrupt(istat, vbridge);
	return 1;
}

#define PLDZ002_CHAN_MASK 1 // single DMA channel
static inline int pldz002_handle_dma_interrupt(struct vme_bridge_device *vbridge,
					       void __iomem *regs_vaddr)
{
	uint32_t istat;
	struct vme_dma_chan *vdchan = &vbridge->dma_mgr.vdchan[0];
	struct pldz002_dma_chan *dchan = vdchan->private;
	int err;

	istat = ioread8(regs_vaddr + PLDZ002_DMASTA) &
			(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR);
	if (istat == 0)
		return 0; // No IRQ found
	err = 0;
	if (istat & PLDZ002_DMASTA_ERR) {
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
		// DMA transaction failed. Set the error in current tx, in
		// order to report it to the client
		vdchan->tx_desc_current->tx_res =
			(vdchan->tx_desc_current->direction == DMA_DEV_TO_MEM) ?
			DMA_TRANS_READ_FAILED : DMA_TRANS_WRITE_FAILED;
#endif
		err = 1;
	}
	/* Ack DMA success and Error just in case */
	iowrite8(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR,
					regs_vaddr + PLDZ002_DMASTA);

	/* if DMA error or no batch to schedule transaction is completed */
	if (err || dchan->nr_batch == dchan->curr_batch_idx) {
		vme_dmaengine_irq_handler(PLDZ002_CHAN_MASK, vbridge);
		if (!err)
			int_stats[PLDZ002_INT_STATS_DMA].count++;
		else
			int_stats[PLDZ002_INT_STATS_DMAERR].count++;
	} else { /* DMA TX is not completed: remains batch to schedule */
		pldz002_schedule_next_dma_batch(vdchan);
	}
	return 1;
}


/**
 * vme_bridge_interrupt() - VME bridge main interrupt handler
 *
 */
irqreturn_t pldz002_interrupt_handler(int irq, void *arg)
{
	struct vme_bridge_device *vbridge = arg;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;
	int handled, something_handled;

	something_handled = 0;
	do {
		handled = 0;
		handled |= pldz002_handle_vme_error_interrupt(vbridge,
							      regs_vaddr);
		handled |= pldz002_handle_irq_interrupt(vbridge, regs_vaddr);
		handled |= pldz002_handle_dma_interrupt(vbridge, regs_vaddr);
		if (handled)
			something_handled++;
	} while (handled);
	if (something_handled == 0) { //spurious interrupt
		int_stats[PLDZ002_INT_STATS_SPURIOUS].count++;
		return IRQ_NONE;
	}

	return IRQ_HANDLED;
}

/**
 * pldz002_generate_interrupt() - Generate an interrupt on the VME bus
 * @level: IRQ level (1-7)
 * @vector: IRQ vector (0-255)
 * @msecs: Timeout for IACK in milliseconds
 *
 *  This function generates an interrupt on the VME bus and waits for IACK
 * for msecs milliseconds.
 *
 *  Returns 0 on success or -ETIME if the timeout expired.
 *
 */
#define _PLDZ002_INTERRUPTER_ID 1 /** interrupter dummy ID */
int pldz002_generate_interrupt(struct vme_bridge_device *vbridge,
			      int level, int vector, signed long msecs)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	signed long timeout = msecs_to_jiffies(msecs);
	void __iomem *regs_vaddr;
	uint8_t val;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	val = ioread8(regs_vaddr + PLDZ002_INTR) & PLDZ002_INTR_INTEN;
	if (val)
		return -EBUSY; /* interrupter busy */

	iowrite8(vector, regs_vaddr + PLDZ002_INTID);
	iowrite8(level | PLDZ002_INTR_INTEN, regs_vaddr + PLDZ002_INTR);

	/* Wait for timeout */
	set_current_state(TASK_INTERRUPTIBLE);
	timeout = schedule_timeout(timeout);

	/* Check if generated interrupt has been acknowledged */
	val = ioread8(regs_vaddr + PLDZ002_INTR) & PLDZ002_INTR_INTEN;
	if (val)
		return -ETIME; /* interrupt not yet acked */
	return 0; /* interrupt acked */
}

static int pldz002_enable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	/* Enable LM Interrupts */
	intmask = 0x0;
	if (vbridge->syscon)
		intmask |= PLDZ002_INT_IRQM;
	iowrite8(intmask, regs_vaddr + PLDZ002_IMASK);
	vbridge->irq_mgr.vme_interrupts_enabled = intmask;

	/* Enable Bus Error Interrupt */
	iowrite8(PLDZ002_MSTR_BERR, regs_vaddr + PLDZ002_MSTR);
	return 0;
}

static int pldz002_disable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	/* Disable LM Interrupts */
	intmask = 0x0;
	iowrite8(intmask, regs_vaddr + PLDZ002_IMASK);
	vbridge->irq_mgr.vme_interrupts_enabled = intmask;

	/* Disable Bus Error Interrupt */
	iowrite8(0x0, regs_vaddr + PLDZ002_MSTR);
	return 0;
}

static struct vme_bridge_irq_ops pldz002_irq_ops = {
	.enable_interrupts = pldz002_enable_interrupts,
	.disable_interrupts = pldz002_disable_interrupts,
	.interrupt_handler = pldz002_interrupt_handler,
	.iack = pldz002_iack,
	.generate_interrupt = pldz002_generate_interrupt,
};

int pldz002_irq_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->irq_mgr.ops = &pldz002_irq_ops;
	vbridge->int_stats = int_stats;
	vbridge->int_stats_count = ARRAY_SIZE(int_stats);
	return 0;
}
