#
# svec-fd-luns.awk - create lun'ed devnodes from transfer.ref
#
# usage: svec-fd-luns.awk DEVICE_NAME [transfer_file]
#
# e.g.:
#  $ awk -f svec-fd-luns.awk SVECFDELAY /acc/dsc/tst/cfv-864-cdv28/etc/transfer.ref
# produces as output
#   FIXME: sh command

BEGIN {
	device_name = ARGV[1]
	slot = ARGV[2]
	delete ARGV[2]
	delete ARGV[1]
}

/^#\+#/ && $6 ~ device_name && $20 ~ slot {
	# decode transfer.ref line
	lun = $7
	printf("%d", lun)
}
