// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_bridge.h - PCI-VME bridge driver definitions
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the data structures and definitions internal to the
 * driver.
 */

#ifndef _VME_BRIDGE_H
#define _VME_BRIDGE_H

#include <linux/debugfs.h>
#include <linux/device.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/bitmap.h>
#include <linux/spinlock.h>
#include <linux/idr.h>

#include "tsi148_hw.h"
#include "vmebus.h"

#define PFX			"VME Bridge: "
#define DRV_MODULE_NAME	"vmebus"
#define DRV_MODULE_VERSION	"1.7"
#define DRV_MODULE_RELDATE	"Feb, 04 2019"
#define VME_NUM_VECTORS 256

/*
 * We just keep the last VME error caught, protecting it with a spinlock.
 * A new VME bus error overwrites it.
 * This is very simple yet good enough for most (sane) purposes.
 * A linked list of handlers is kept for async notification of bus errors.
 */
struct vme_verr {
	spinlock_t			lock;
	struct vme_bus_error_desc	desc;
	struct list_head		h_list;
};

#define VME_SLOT_FLAGS_MAX 32
#define VME_SLOT_F_BUSY BIT(0)

/**
 * Describe a VME slot
 * @dev:
 * @index: slot number in range [1, 21]
 * @lock: protect: flags
 * @flags: flags
 */
struct vme_slot {
	struct device dev;
	uint8_t index;
	struct spinlock lock;
	DECLARE_BITMAP(flags, VME_SLOT_FLAGS_MAX);
};

static inline struct vme_slot *to_vme_slot(struct device *d)
{
	return container_of(d, struct vme_slot, dev);
}

#define VME_BRIDGE_FLAGS_MAX (32)
#define VME_BRIDGE_DMA_PAUSE BIT(0)
#define VME_DEV_FLAG_FORCE_DMA_CFG BIT(1)

/**
 * struct vme_bridge_device - VME bridge descriptor
 * @flags bit mask for flags
 * @dma_param manaual tuning parameters for DMA transfers
 * @lock it protects the following variables: flags
 * @bus_slot: list al all possible slots on the bus
 * @mtx_vme_do_dma: used to serialize dma_request_channel() in vme_do_dma()
 */
struct vme_bridge_device {
	int			rev;		/* chip revision */
	int			irq;		/* chip interrupt */
	int			slot;		/* the VME slot we're in */
	int			syscon;		/* syscon status */
	struct tsi148_chip	*regs;
	struct pci_dev		*pdev;
	struct vme_verr		verr;

	DECLARE_BITMAP(flags, VME_BRIDGE_FLAGS_MAX);
	struct vme_dma_ctrl dma_param;
	struct spinlock lock;

#ifdef CONFIG_PROC_FS
	struct proc_dir_entry *proc_root;
	struct proc_dir_entry *proc_tsi148;
#endif
	unsigned int base_irq;
	DECLARE_BITMAP(irq_mask, VME_NUM_VECTORS);
	struct irq_domain *domain;
	struct ida ida_irq;

	struct dentry *dbg_dir;
#define VBRIDGE_DBG_INFO_NAME "info"
	struct dentry *dbg_info;
	struct vme_dmaengine_device *dmaengine;
	struct vme_slot bus_slot[VME_SLOT_MAX];

	struct mutex mtx_vme_do_dma;
};

/**
 * struct vme_berr_handler - VME bus error handler descriptor
 * @h_list:	handler's list entry
 * @offset:	VME Bus error descriptor (Initial address + Address Modifier)
 * @size:	Size of the VME address range of interest
 * @func:	Handler function
 */
struct vme_berr_handler {
	struct list_head	h_list;
	struct vme_bus_error	error;
	size_t			size;
	vme_berr_handler_t	func;
};

extern struct vme_bridge_device *vme_bridge;
extern struct resource *vmepcimem;
extern void *crg_base;
extern unsigned int vme_report_bus_errors;

/* Use the standard VME Major */
#define VME_MAJOR	221

/*
 * Define our own minor numbers
 * If a device get added, do not forget to update devlist[] in vme_bridge.c
 */
#define VME_MINOR_MWINDOW	0
#define VME_MINOR_DMA		1
#define VME_MINOR_CTL		3
#define VME_MINOR_REGS		4

#define VME_MINOR_NR	VME_MINOR_LM + 1

/* Devices access rules */
#define DEV_RW		1	/* Device implements read/write */

struct dev_entry {
	unsigned int		minor;
	char			*name;
	unsigned int		flags;
	struct file_operations	*fops;
};

extern const struct dev_entry devlist[];

extern struct bus_type vme_bus_type;

/* Prototypes */

/* vme_irq.c */
extern void account_dma_interrupt(int);
extern irqreturn_t vme_bridge_interrupt(int, void *);
extern int vme_enable_interrupts(unsigned int);
extern int vme_disable_interrupts(unsigned int);
extern int tsi148_irq_domain_create(struct vme_bridge_device *vbridge);
extern void tsi148_irq_domain_destroy(struct vme_bridge_device *vbridge);

/* tsi dmaengine */
extern int vme_dmaengine_init(struct vme_bridge_device *vbridge);
extern void vme_dmaengine_exit(struct vme_bridge_device *vbridge);
extern void vme_dmaengine_irq_handler(int channel_mask,
				      struct vme_bridge_device *vbridge);

/* vme_window.c */
extern int vme_window_release(struct inode *, struct file *);
extern long vme_window_ioctl(struct file *, unsigned int, unsigned long);
extern int vme_window_mmap(struct file *, struct vm_area_struct *);
extern void vme_window_init(void);
extern void vme_window_exit(void);

/* vme_dma.c */
extern wait_queue_head_t channel_wait[TSI148_NUM_DMA_CHANNELS];
extern void handle_dma_interrupt(int, struct vme_bridge_device *);
extern long vme_dma_ioctl(struct file *, unsigned int, unsigned long);
extern int vme_dma_init(void);
extern void vme_dma_exit(void);

/* vme_misc.c */
extern ssize_t vme_misc_read(struct file *, char *, size_t, loff_t *);
extern ssize_t vme_misc_write(struct file *, const char *, size_t, loff_t *);
extern long vme_misc_ioctl(struct file *, unsigned int, unsigned long);
extern int vme_bus_error_check(int clear);
extern int vme_bus_error_check_clear(struct vme_bus_error *);

extern int vme_bus_register(struct vme_bridge_device *b);
extern void vme_bus_unregister(struct vme_bridge_device *b);
extern int vme_device_irq_assign(struct vme_dev *vme_dev, int start, int end);
extern void vme_device_irq_unassign(struct vme_dev *vme_dev);

/* vme_crcsr */
extern int vme_csr_map(struct vme_dev *vdev);
extern void vme_csr_unmap(struct vme_dev *vdev);
extern int vme_cr_area_dump_all(struct vme_dev *vdev);
extern void vme_cr_area_release_all(struct vme_dev *vdev);
extern int vme_cr_get(struct vme_dev *vdev, struct vme_cr *cr);
extern int vme_csr_get(struct vme_dev *vdev, struct vme_csr *csr);
extern int vme_csr_ader_set(struct vme_dev *vdev,
			    unsigned int function, uint32_t ader);
extern void vme_csr_ader_set_raw(struct vme_dev *vdev,
				 unsigned int func, uint32_t ader);
extern int vme_csr_bit(struct vme_dev *vdev, uint8_t mask, unsigned int val);
extern int vme_csr_bit_usr(struct vme_dev *vdev,
			   uint8_t mask, unsigned int val);
extern int vme_csr_resource_create(struct vme_dev *vdev, unsigned int func);
extern void vme_csr_resource_remove(struct vme_dev *vdev, unsigned int func);
extern struct bin_attribute vme_crcsr_bin_attr;
extern int vme_cr_is_valid(unsigned int slot);

extern uint32_t vme_cr_manufacturer(unsigned int slot);
extern uint32_t vme_cr_device(unsigned int slot);
extern uint32_t vme_cr_revision(unsigned int slot);
extern uint8_t vme_cr_irq_cap(unsigned int slot);

/* Procfs stuff grouped here for comodity */
#ifdef CONFIG_PROC_FS
void vme_procfs_register(struct vme_bridge_device *vbridge);
void vme_procfs_unregister(struct vme_bridge_device *vbridge);
#else
void vme_procfs_register(struct vme_bridge_device *vbridge) {}
void vme_procfs_unregister(struct vme_bridge_device *vbridge) {}
#endif /* CONFIG_PROC_FS */

#endif /* _VME_BRIDGE_H */
