// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "vmeutils.h"

static int verbose;

static char options[] = "hv";
static const char help_msg[] =
	"Usage: vme-register [options]\n"
	"\n"
	"By default it scans the VME bus from slot 1 to slot 21\n"
	"\n"
	"General options:\n"
	"--help, -h             Print this message\n"
	"\n"
	"Registration parameters:\n"
	"--slot <slot>                              Slot number\n"
	"--address-modifier 0x<am>                  Address modifier\n"
	"--address 0x<address>                      Base address\n"
	"--size <size>                              Address space size in Bytes\n"
	"--irq-vector <irq_vector>                  IRQ vector\n"
	"--irq-level <irq_level>                    IRQ level\n"
	"--identifier <vendor>,<device>,<revision>  identifier\n"
	"--name <name>                              Device name\n"
	"\n"
	"Display options:\n"
	"--verbose, -v                              Be verbose (add more 'v' to be more verbose, -vvv)\n"
	"\n"
	"Parameters details:\n"
	"--irq-vector <irq_vector> (default: auto-assignment)\n"
	"   The <irq_vector> is a number inrange [0, 255], it can be written\n"
	"   as decimal number (e.g. 181) or hexadecimal number (e.g. 0xB5).\n"
	"--irq-level <irq_level>"
	"   The <irq_level> is a number in range [1, 7].\n"
	"   (default: IRQ vector automatic assigment and IRQ level 2)"
	"\n"
	;

enum vme_register_options {
	VME_REG_OPT_SLOT = 1024,
	VME_REG_OPT_AM,
	VME_REG_OPT_ADDR,
	VME_REG_OPT_SIZE,
	VME_REG_OPT_IRQ_VECT,
	VME_REG_OPT_IRQ_LEVL,
	VME_REG_OPT_ID,
	VME_REG_OPT_NAME,
	VME_REG_OPT_HELP,
};
static struct option long_options[] =
{
	{"verbose", no_argument, &verbose, 1},
	{"slot", required_argument, 0, VME_REG_OPT_SLOT},
	{"address-modifier", required_argument, 0, VME_REG_OPT_AM},
	{"address", required_argument, 0, VME_REG_OPT_ADDR},
	{"size", required_argument, 0, VME_REG_OPT_SIZE},
	{"irq-vector", required_argument, 0, VME_REG_OPT_IRQ_VECT},
	{"irq-level", required_argument, 0, VME_REG_OPT_IRQ_LEVL},
	{"identifier", required_argument, 0, VME_REG_OPT_ID},
	{"name", required_argument, 0, VME_REG_OPT_NAME},
	{"help", no_argument, 0, VME_REG_OPT_HELP},
	{0, 0, 0, 0}
};

static int module_register(unsigned int slot,
			   struct vme_device_register *reg)
{
#define PATH_SIZE 64
	int fd, ret;
	char path[PATH_SIZE];


	if (!reg->irq_vector) {
		fprintf(stderr, "Register: IRQ vector can't be 0\n");
		return -EINVAL;
	}
	if (!reg->irq_level) {
		fprintf(stderr, "Register: IRQ level can't be 0\n");
		return -EINVAL;
	}
	if (!reg->size) {
		fprintf(stderr, "Register: memory size can't be 0\n");
		return -EINVAL;
	}

	snprintf(path, PATH_SIZE,
		 "/sys/bus/vme/devices/slot.%02u/register", slot);

	fd = open(path, O_WRONLY);
	if (fd < 0)
		goto out;
	ret = write(fd, reg, sizeof(*reg));
	close(fd);
out:
	return ret;
}

int cmd_register(int argc, char *argv[])
{
	struct vme_device_register reg;
	unsigned int slot = 0;
	int option_index = 0;
	int c;
	int ret;

	memset(&reg, 0, sizeof(reg));
	reg.struct_size = sizeof(struct vme_device_register);
	reg.irq_vector = VME_IRQ_VECTOR_AUTO;
	reg.irq_level = 2;
	while ((c = getopt_long(argc, argv, options, long_options,
				&option_index)) != -1) {
		switch (c) {
		default:
			break;
		case 'v':
			verbose++;
			break;
		case 'h':
		case VME_REG_OPT_HELP:
			fputs(help_msg, stdout);
			exit(EXIT_SUCCESS);
		case VME_REG_OPT_SLOT:
			ret = sscanf(optarg, "%u", &slot);
			if (ret != 1) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid slot number %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_REG_OPT_AM:
			ret = sscanf(optarg, "0x%x", &reg.am);
			if (ret != 1) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid address modifier %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_REG_OPT_ADDR:
			ret = sscanf(optarg, "0x%x", &reg.base_address);
			if (ret != 1) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid base address %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_REG_OPT_SIZE:
			ret = sscanf(optarg, "0x%x", &reg.size);
			if (ret != 1) {
				ret = sscanf(optarg, "%u", &reg.size);
				if (ret != 1) {
					errno = EINVAL;
					fprintf(stderr,
						"Invalid address space length %s\n",
						optarg);
					return -1;
				}
			}
			break;
		case VME_REG_OPT_IRQ_VECT:
			ret = sscanf(optarg, "0x%x", &reg.irq_vector);
			if (ret != 1) {
				ret = sscanf(optarg, "%u", &reg.irq_vector);
				if (ret != 1) {
					errno = EINVAL;
					fprintf(stderr,
						"Invalid IRQ vector %s\n",
						optarg);
					return -1;
				}
			}
			break;
		case VME_REG_OPT_IRQ_LEVL:
			ret = sscanf(optarg, "%u", &reg.irq_level);
			if (ret != 1) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid IRQ level %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_REG_OPT_ID:
			ret = sscanf(optarg, "0x%x,0x%x,0x%x",
				     &reg.id.vendor, &reg.id.device,
				     &reg.id.revision);
			if (ret != 3) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid identifier %s\n",
					optarg);
				return -1;
			}
			break;
		case VME_REG_OPT_NAME:
			if (strnlen(optarg, VME_NAME_ID_LEN + 1) > VME_NAME_ID_LEN) {
				errno = EINVAL;
				fprintf(stderr,
					"Invalid name (too long) %s\n",
					optarg);
				return -1;
			}
			strncpy(reg.id.name, optarg, VME_NAME_ID_LEN);
			break;
		}
	}

	ret = module_register(slot, &reg);
	if (ret < 0) {
		fprintf(stderr,
			"Registration failed.\n\t"PRIvdevREG"\n",
			PRIvdevREGVAL(&reg));
	}
	return ret;
}
