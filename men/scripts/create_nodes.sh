#!/bin/sh

# (C) CERN 2017
# author: Adam Wujek <adam.wujek@cern.ch>

mk_dev() {
    [ -c $1 ] || mknod $1 $2 $3 $4
}

mk_dev /dev/vme4l_a16d16 c 230 0
mk_dev /dev/vme4l_a16d32 c 230 2
mk_dev /dev/vme4l_a24d16 c 230 4
mk_dev /dev/vme4l_a24d16_blt c 230 5
mk_dev /dev/vme4l_a24d64_blt c 230 1
mk_dev /dev/vme4l_a24d32 c 230 6
mk_dev /dev/vme4l_a24d32_blt c 230 7
mk_dev /dev/vme4l_a32d32 c 230 8
mk_dev /dev/vme4l_a32d32_blt c 230 9
mk_dev /dev/vme4l_a32d64_blt c 230 10
mk_dev /dev/vme4l_cr_csr c 230 30
