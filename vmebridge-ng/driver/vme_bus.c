// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_bridge.c - PCI-VME bridge driver
 *
 * Copyright (c) 2018 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 * Author: Sebastien Dugue
 */

#include <linux/device.h>
#include <linux/irqdomain.h>
#include <linux/version.h>

#include "vme_bridge.h"
#include "vme_compat.h"

static int vme_bus_match(struct device *dev, struct device_driver *driver)
{
	struct vme_driver *vme_driver = to_vme_driver(driver);
	struct vme_dev *vdev = to_vme_dev(dev);
	const struct vme_device_id *id = vme_driver->id_table;

	/* Deprecated way of matching */
	if (vdev->flags & VME_DEV_FLAG_DRIVER_REG) {
		if (dev->platform_data == vme_driver) {
			if (!vme_driver->match ||
			    vme_driver->match(dev, to_vme_dev(dev)->id))
				return 1;
			/* Device does not match, unbind the device */
			dev->platform_data = NULL;
		}
		return 0;
	}

	/* Check if the given vendor/device ID is compatible with the driver */
	while (id && id->vendor && id->device) {
		if (vdev->spec == VME_SPEC_VME64 ||
		    vdev->spec == VME_SPEC_VME64x) {
			/* Possible only on VME64 or VME64x devices */
			if (id->vendor == vdev->devid.vendor &&
			    id->device == vdev->devid.device &&
			    id->revision == vdev->devid.revision)
				return 1;
		}
		if (strncmp(id->name, vdev->devid.name, VME_NAME_ID_LEN) == 0)
			return 1;
		++id; /* move to the next id compatible with the driver */
	}
	return 0;
}

int vme_device_irq_assign(struct vme_dev *vme_dev, int start, int end)
{
	struct vme_bridge_device *vbridge;
	int irq_vector;

	vbridge = pci_get_drvdata(to_pci_dev(vme_dev->dev.parent->parent));
	irq_vector = ida_simple_get(&vbridge->irq_mgr.ida_irq,
				    start, end,
				    GFP_KERNEL);
	if (irq_vector < 0)
		return -EBUSY;
	vme_dev->irq_vector = irq_vector;

	vme_dev->irq = irq_find_mapping(vbridge->irq_mgr.domain,
					vme_dev->irq_vector);

	return 0;
}

void vme_device_irq_unassign(struct vme_dev *vme_dev)
{
	struct vme_bridge_device *vbridge;

	vbridge = pci_get_drvdata(to_pci_dev(vme_dev->dev.parent->parent));
	ida_simple_remove(&vbridge->irq_mgr.ida_irq, vme_dev->irq_vector);
}

/**
 * Run some compatibility fixup
 */
static int vme_bus_probe_compat_fixup(struct device *dev)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	char name[128];
	int err = 0;

	if (!(vdev->flags & VME_DEV_FLAG_DRIVER_REG))
		return 0;

	/* vvvvvv Driver is using the old API vvvvvv */

	if (vdev->slot != -1) {
		/*
		 * Users should set irq_vector and irq_level during probe
		 * when they use the old API. If they don't it is not a big
		 * issue, simply the information about the Linux IRQ number
		 * on sysfs is crap
		 */
		if (vdev->irq_vector < VME_NUM_VECTORS) {
			err = vme_device_irq_assign(vdev,
						    vdev->irq_vector,
						    vdev->irq_vector + 1);
			if (err) {
				pr_err(PFX "Can't assign VME IRQ vector to '%s'\n",
				       dev_name(dev));
				return err;
			}
		} else {
			switch (vdev->irq_vector) {
			case VME_IRQ_VECTOR_AUTO:
				pr_warn(PFX "Device '%s' is asking for IRQ vector automatic assignment but it is not supported with the OLD API\n",
					dev_name(dev));
				return -EINVAL;
			case VME_IRQ_VECTOR_OLDAPI:
				pr_warn(PFX "Device '%s' is not assigning IRQ vector in new API\n",
					dev_name(dev));
				break;
			default:
				pr_err(PFX "Invalid IRQ vector assignment option\n");
				return -EINVAL;
			}
		}

		snprintf(name, 128, "vme.%u", vdev->slot);
		err = device_rename(dev, name);
		if (err) {
			dev_info(dev, "cannot rename device to '%s'",
				 name);
			err = 0;
		}
	}

	return err;
}

static int vme_bus_probe(struct device *dev)
{
	struct vme_driver *vme_driver;
	struct vme_dev *vdev = to_vme_dev(dev);
	int err = 0;

	if (dev->driver == NULL)
		return -EINVAL;

	vme_driver = container_of(dev->driver, struct vme_driver, driver);

	if (vme_driver->probe)
		err = vme_driver->probe(dev, vdev->id);
	if (err)
		goto out;

	vme_bus_probe_compat_fixup(dev);

out:
	return err;
}

static int vme_bus_remove(struct device *dev)
{
	struct vme_driver *vme_driver;

	if (dev->driver == NULL)
		return -EINVAL;

	vme_driver = container_of(dev->driver, struct vme_driver, driver);

	if (vme_driver->remove)
		return vme_driver->remove(dev, to_vme_dev(dev)->id);

	return 0;
}

static void vme_bus_shutdown(struct device *dev)
{
	struct vme_driver *vme_driver;

	if (dev->driver == NULL)
		return;

	vme_driver = container_of(dev->driver, struct vme_driver, driver);

	if (vme_driver->shutdown)
		vme_driver->shutdown(dev, to_vme_dev(dev)->id);
}

static int vme_bus_suspend(struct device *dev, pm_message_t state)
{
	struct vme_driver *vme_driver;

	if (dev->driver == NULL)
		return -EINVAL;

	vme_driver = container_of(dev->driver, struct vme_driver, driver);

	if (vme_driver->suspend)
		return vme_driver->suspend(dev, to_vme_dev(dev)->id, state);

	return 0;
}

static int vme_bus_resume(struct device *dev)
{
	struct vme_driver *vme_driver;

	if (dev->driver == NULL)
		return -EINVAL;

	vme_driver = container_of(dev->driver, struct vme_driver, driver);

	if (vme_driver->resume)
		return vme_driver->resume(dev, to_vme_dev(dev)->id);

	return 0;
}

static ssize_t register_write(struct file *f, struct kobject *kobj,
			      struct bin_attribute *attr,
			      char *buf, loff_t off, size_t count)
{
	struct vme_slot *s = to_vme_slot(container_of(kobj, struct device,
						      kobj));
	struct vme_dev *vdev;
	struct vme_device_register *r = (struct vme_device_register *)buf;
	struct vme_device_register vreg;
	int err;

	dev_dbg(&s->dev, "Register a new VME device\n");

	memset(&vreg, 0, sizeof(vreg));
	memcpy(&vreg, r, min_t(size_t, r->struct_size, sizeof(vreg)));

	vdev = kzalloc(sizeof(struct vme_dev), GFP_KERNEL);
	if (!vdev)
		return -ENOMEM;

	err = vme_device_slot_set(&vdev->dev, s->index);
	if (err)  {
		pr_err(PFX "Invalid slot number %d\n", s->index);
		goto err;
	}

	strncpy(vdev->devid.name, vreg.id.name, VME_NAME_ID_LEN);
	vdev->devid.vendor = vreg.id.vendor;
	vdev->devid.device = vreg.id.device;
	vdev->devid.revision = vreg.id.revision;
	vdev->am = vreg.am;
	vdev->base_address = vreg.base_address;
	vdev->size = vreg.size;

	err = vme_device_irq_vector_and_level_set(&vdev->dev,
						  vreg.irq_vector,
						  vreg.irq_level);
	if (err) {
		pr_err(PFX "Invalid IRQ description: vector: 0x%x, level: %d\n",
		       vreg.irq_vector, vreg.irq_level);
		goto err;
	}

	err = vme_register_device(vdev);
	if (err)
		goto err;

	return count;

err:
	kfree(vdev);
	return err;
}
BIN_ATTR(register, 0220, NULL, register_write, PAGE_SIZE);

static ssize_t slot_show(struct device *dev, struct device_attribute *attr,
			 char *buf)
{
	return snprintf(buf, PAGE_SIZE, "%d\n", to_vme_slot(dev)->index);
}
static DEVICE_ATTR_RO(slot);

/**
 * Scan a VME slot for VME64x devices
 * @slot: slot to scan [1, N]
 *
 * Return: 0 on success, otherwise a negative error number
 */
static int vme_bus_scan_one(unsigned int slot)
{
	static int irq_vector = 0; /* FIXME HACK */
	struct vme_dev *vdev;
	uint8_t level;
	int err;

	if (!vme_cr_is_valid(slot)) {
		pr_debug(PFX"Slot %02d does not contain a VME64x module\n",
			slot);
		return 0;
	}

	/* TODO Check if it was not already registered */
	vdev = kzalloc(sizeof(*vdev), GFP_KERNEL);
	if (!vdev)
		return -ENOMEM;

	/* use the lowest level */
	level = vme_cr_irq_cap(slot);
	vdev->irq_level = find_first_bit((unsigned long *)&level, 8);
	irq_vector = (irq_vector + 1) & (VME_NUM_VECTORS - 1);
	vdev->irq_vector = irq_vector;
	vdev->slot = slot;
	err = vme_register_device(vdev);
	if (err < 0 && err != -EBUSY) {
		/*
		 * it is fine, if we are rescanning the bus to get
		 * EBUSY on some slots: static declarations or
		 * previous scans
		 */
		goto err;
	}

	return 0;

err:
	kfree(vdev);

	return err;
}

static ssize_t rescan_slot_store(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	int scan, ret;

	ret = sscanf(buf, "%i", &scan);
	if (ret != 1)
		return -EINVAL;
	if (scan) {
		struct vme_slot *s = to_vme_slot(dev);

		ret = vme_bus_scan_one(s->index);
		if (ret < 0)
			return ret;
	}
	return count;
}
static DEVICE_ATTR(rescan, 0220, NULL, rescan_slot_store);

static ssize_t force_configuration_store(struct bus_type *bus,
					 const char *buf, size_t count)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	unsigned int val;
	unsigned long flags;
	int rv;

	rv = kstrtouint(buf, 0, &val);
	if (rv < 0)
		return rv;
	else if (rv != 1)
		return -EINVAL;

	spin_lock_irqsave(&vbridge->lock, flags);
	if (val)
		set_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags);
	else
		clear_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags);
	spin_unlock_irqrestore(&vbridge->lock, flags);
	return count;
}
static ssize_t force_configuration_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;

	return snprintf(buf, PAGE_SIZE, "%d\n",
		test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags));
}
BUS_ATTR_RW(force_configuration);

static ssize_t backoff_time_vme_store(struct bus_type *bus,
				      const char *buf, size_t count)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	unsigned int val, i;
	int rv, dma_cap;

	dma_cap = vbridge->dma_mgr.ops->dma_get_capabilities();
	if (!(dma_cap & VME_DMA_TRANSFER_BACKOFF_TIME))
		return -EPERM;

	if (test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags))
		return -EBUSY;

	rv = kstrtouint(buf, 0, &val);
	if (rv < 0)
		return rv;
	else if (rv != 1)
		return -EINVAL;


	for (i = 0; val; ++i)
		val >>= 1;
	val = i;
	if (val >= __VME_DMA_BACKOFF_MAX)
		return -EINVAL;

	vbridge->dma_mgr.dma_param.vme_backoff_time = val;

	return count;
}
static ssize_t backoff_time_vme_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	int val;

	val = 1 << vbridge->dma_mgr.dma_param.vme_backoff_time;
	val >>= 1;
	return snprintf(buf, PAGE_SIZE, "%d\n", val);
}
BUS_ATTR_RW(backoff_time_vme);

static ssize_t backoff_time_pci_store(struct bus_type *bus,
				      const char *buf, size_t count)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	unsigned int val, i;
	int rv, dma_cap;

	dma_cap = vbridge->dma_mgr.ops->dma_get_capabilities();
	if (!(dma_cap & VME_DMA_TRANSFER_BACKOFF_TIME))
		return -EPERM;

	if (test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags))
		return -EBUSY;

	rv = kstrtouint(buf, 0, &val);
	if (rv < 0)
		return rv;
	else if (rv != 1)
		return -EINVAL;

	for (i = 0; val; ++i)
		val >>= 1;
	val = i;
	if (val >= __VME_DMA_BACKOFF_MAX)
		return -EINVAL;

	vbridge->dma_mgr.dma_param.pci_backoff_time = val;

	return count;
}
static ssize_t backoff_time_pci_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	int val;

	val = 1 << vbridge->dma_mgr.dma_param.pci_backoff_time;
	val >>= 1;
	return snprintf(buf, PAGE_SIZE, "%d\n", val);
}
BUS_ATTR_RW(backoff_time_pci);

static ssize_t block_size_vme_store(struct bus_type *bus,
				    const char *buf, size_t count)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	unsigned int val, i;
	int rv;

	if (test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags))
		return -EBUSY;

	rv = kstrtouint(buf, 0, &val);
	if (rv < 0)
		return rv;
	else if (rv != 1 || val < 32)
		return -EINVAL;

	val >>= 6;
	for (i = 0; val; ++i)
		val >>= 1;
	val = i;
	if (val >= __VME_DMA_BSIZE_MAX)
		return -EINVAL;

	vbridge->dma_mgr.dma_param.vme_block_size = val;

	return count;
}
static ssize_t block_size_vme_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;

	return snprintf(buf, PAGE_SIZE, "%d\n",
			1 << (vbridge->dma_mgr.dma_param.vme_block_size + 5));
}
BUS_ATTR_RW(block_size_vme);

static ssize_t block_size_pci_store(struct bus_type *bus,
				    const char *buf, size_t count)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	unsigned int val, i;
	int rv;

	if (test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags))
		return -EBUSY;

	rv = kstrtouint(buf, 0, &val);
	if (rv < 0)
		return rv;
	else if (rv != 1 || val < 32)
		return -EINVAL;

	val >>= 6;
	for (i = 0; val; ++i)
		val >>= 1;
	val = i;
	if (val >= __VME_DMA_BSIZE_MAX)
		return -EINVAL;

	vbridge->dma_mgr.dma_param.pci_block_size = val;

	return count;
}
static ssize_t block_size_pci_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;

	return snprintf(buf, PAGE_SIZE, "%d\n",
			1 << (vbridge->dma_mgr.dma_param.pci_block_size + 5));
}
BUS_ATTR_RW(block_size_pci);

/**
 * This function scans the VME bus for VME64x devices
 * @bus
 */
static void vme_bus_scan_all(struct bus_type *bus)
{
	int err, i;

	for (i = 0; i < VME_SLOT_MAX; ++i) {
		err = vme_bus_scan_one(i + 1);
		if (err) {
			pr_err("Cannot register device on slot %d: %d\n",
			       i + 1, err);
		}
	}
}

static ssize_t rescan_store(struct bus_type *bus,
			    const char *buf, size_t count)
{
	int scan, ret;

	ret = sscanf(buf, "%i", &scan);
	if (ret != 1)
		return -EINVAL;
	if (scan)
		vme_bus_scan_all(bus);
	return count;
}
BUS_ATTR_WO(rescan);

static ssize_t bridge_type_show(struct bus_type *bus, char *buf)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;

	return snprintf(buf, PAGE_SIZE, "%s\n", vbridge->name);
}
BUS_ATTR_RO(bridge_type);

#if KERNEL_VERSION(3, 12, 0) <= LINUX_VERSION_CODE
static struct attribute *vme_bus_attr_list_dma[] = {
	&bus_attr_rescan.attr,
	&bus_attr_force_configuration.attr,
	&bus_attr_backoff_time_vme.attr,
	&bus_attr_backoff_time_pci.attr,
	&bus_attr_block_size_vme.attr,
	&bus_attr_block_size_pci.attr,
	&bus_attr_bridge_type,
	NULL,
};

static const struct attribute_group vme_dev_group_basic_dma = {
	.name = "dma",
	.attrs = vme_dev_attr_list_basic_dma,
};


static const struct attribute_group *vme_bus_attrs_groups[] = {
	&vme_bus_attrs_group,
	NULL,
};
#else
struct bus_attribute vme_bus_attrs[] = {
	__ATTR(bridge_type, 0444,
	       bridge_type_show, NULL),
	__ATTR(rescan, 0220, NULL, rescan_store),
	__ATTR(dma_force_configuration, 0664,
	       force_configuration_show, force_configuration_store),
	__ATTR(dma_backoff_time_vme, 0664,
	       backoff_time_vme_show, backoff_time_vme_store),
	__ATTR(dma_backoff_time_pci, 0664,
	       backoff_time_pci_show, backoff_time_pci_store),
	__ATTR(dma_block_size_vme, 0664,
	       block_size_vme_show, block_size_vme_store),
	__ATTR(dma_block_size_pci, 0664,
	       block_size_pci_show, block_size_pci_store),
	__ATTR_NULL,
};
#endif

struct bus_type vme_bus_type = {
	.name           = "vme",
	.match          = vme_bus_match,
	.probe          = vme_bus_probe,
	.remove         = vme_bus_remove,
	.shutdown       = vme_bus_shutdown,
	.suspend        = vme_bus_suspend,
	.resume         = vme_bus_resume,
#if KERNEL_VERSION(3, 12, 0) <= LINUX_VERSION_CODE
	.bus_groups = vme_bus_attrs_groups,
#else
	.bus_attrs = vme_bus_attrs,
#endif
};


static struct attribute *vme_slot_type_attrs[] = {
	&dev_attr_slot.attr,
	&dev_attr_rescan.attr,
	NULL,
};

static const struct attribute_group vme_slot_type_group = {
	.attrs = vme_slot_type_attrs,
};


static const struct attribute_group *vme_slot_type_groups[] = {
	&vme_slot_type_group,
	NULL,
};

static void vme_slot_type_release(struct device *dev)
{

}

const struct device_type vme_slot_type = {
	.name = "slot",
	.groups = vme_slot_type_groups,
	.release = vme_slot_type_release,
};

int vme_bus_register(struct vme_bridge_device *b)
{
	int i, err;

	b->vme_bus_type = &vme_bus_type;

	err = bus_register(b->vme_bus_type);
	if (err)
		return err;

	for (i = 0; i < VME_SLOT_MAX; ++i) {
		struct vme_slot *s = &b->bus_slot[i];

		memset(s, 0, sizeof(*s));

		spin_lock_init(&s->lock);
		s->index = i + 1;
		dev_set_name(&s->dev, "slot.%02u", s->index);
		s->dev.id = i + 1;
		s->dev.parent = &b->pdev->dev;
		s->dev.bus = b->vme_bus_type;
		s->dev.type = &vme_slot_type;
		err = device_register(&s->dev);
		if (err)
			goto err_dev_reg;
		err = device_create_bin_file(&s->dev, &bin_attr_register);
		if (err) {
			device_unregister(&s->dev);
			goto err_dev_reg;
		}
	}

	return 0;

err_dev_reg:
	while (--i >= 0) {
		device_remove_bin_file(&b->bus_slot[i].dev,
				       &bin_attr_register);
		device_unregister(&b->bus_slot[i].dev);
	}
	bus_unregister(b->vme_bus_type);
	return err;
}

void vme_bus_unregister(struct vme_bridge_device *b)
{
	int i;

	for (i = 0; i < VME_SLOT_MAX; ++i) {
		struct vme_slot *s = &b->bus_slot[i];

		device_remove_bin_file(&b->bus_slot[i].dev,
				       &bin_attr_register);
		device_unregister(&s->dev);
	}

	bus_unregister(b->vme_bus_type);
}
