// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 * This DMA engine must be compatible with kernel 3.2 and 3.6 so it does not
 * use latest DMA engine API.
 * Another point to take in account is that I'm developing this within
 * the vmebus module, so I do not have access to some linux kernel internals.
 * The internal kernel functions that I would like to use are copied here
 * within a pre-processor condition
 *
 * We have to keep the compatibility with the ad-hoc DMA interface
 * provided by the vmebus driver throught the vme_do_dma() function call.
 * By keeping the compatibily we should be able to run together drivers that
 * use different interfaces.
 *
 * This DMA engine implementation works more or less as any other
 * implementation available within the kernel tree.
 * - alloc/free channel gives exclusive access to the caller
 * - prepare slave SG builds a DMA transfer descriptor
 * - submit enqueue a transfer descriptor to a list of pending transfers
 * - issue pending start the first pending DMA transfer
 * - when a DMA transfer complete run the caller callback and start the
 *   next pending DMA transfer.
 */

#include <linux/dmaengine.h>
#include <linux/delay.h>
#include <linux/sched.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "tsi148.h"
#include "tsi148_dmaengine.h"


/**
 * tsi148_dma_get_status() - Get the status of a DMA channel
 * @chan: DMA channel descriptor
 *
 **/
int tsi148_dma_get_status(struct tsi148_dma_chan *tchan)
{
	return ioread32be(&tsi148_chip_global->lcsr.dma[tchan->index].dsta);
}

/**
 * tsi148_dma_busy() - Get the busy status of a DMA channel
 * @chan: DMA channel descriptor
 *
 **/
int tsi148_dma_busy(struct tsi148_dma_chan *tchan)
{
	return tsi148_dma_get_status(tchan) & TSI148_LCSR_DSTA_BSY;
}

/**
 * tsi148_dma_done() - Get the done status of a DMA channel
 * @chan: DMA channel descriptor
 *
 */
int tsi148_dma_done(struct tsi148_dma_chan *tchan)
{
	return tsi148_dma_get_status(tchan) & TSI148_LCSR_DSTA_DON;
}


/**
 * tsi148_dma_pause() - Get the pause status of a DMA channel
 * @chan: DMA channel descriptor
 *
 */
int tsi148_dma_pause(struct tsi148_dma_chan *chan)
{
	return tsi148_dma_get_status(chan) & TSI148_LCSR_DSTA_PAU;
}


static void tsi148_dctl_bit_set(struct tsi148_dma_chan *tchan,
				unsigned long bitmask)
{
	unsigned int dctl;
	void *ptr = &tsi148_chip_global->lcsr.dma[tchan->dchan.chan_id].dctl;

	dctl = ioread32be(ptr);
	dctl |= bitmask;

	iowrite32be(dctl, ptr);

}

static void tsi148_dctl_bit_clr(struct tsi148_dma_chan *tchan,
				unsigned long bitmask)
{
	unsigned int dctl;
	void *ptr = &tsi148_chip_global->lcsr.dma[tchan->dchan.chan_id].dctl;

	dctl = ioread32be(ptr);
	dctl &= ~bitmask;

	iowrite32be(dctl, ptr);

}

/**
 * tsi148_dma_start() - Start DMA transfer on a channel
 * @chan: DMA channel descriptor
 *
 */
static void tsi148_dma_start(struct tsi148_dma_chan *tchan)
{
	tsi148_dctl_bit_set(tchan, TSI148_LCSR_DCTL_DGO);
}

/**
 * tsi148_dma_abort() - Abort a DMA transfer
 * @chan: DMA channel descriptor
 *
 */
static void tsi148_dma_abort(struct tsi148_dma_chan *tchan)
{
	tsi148_dctl_bit_set(tchan, TSI148_LCSR_DCTL_ABT);
}

/**
 * tsi148_dma_do_pause() - Pause a DMA transfer
 * @chan: DMA channel descriptor
 *
 */
void tsi148_dma_do_pause(struct tsi148_dma_chan *tchan)
{
	tsi148_dctl_bit_set(tchan, TSI148_LCSR_DCTL_PAU);
}



/**
 * tsi148_setup_dma_attributes() - Build the DMA attributes word
 * @v2esst_mode: VME 2eSST transfer speed
 * @data_width: VME data width (D16 or D32 only)
 * @am: VME address modifier
 *
 * This function builds the DMA attributes word. All parameters are
 * checked.
 *
 * NOTE: The function relies on the fact that the DSAT and DDAT registers have
 *       the same layout.
 *
 * Returns %EINVAL if any parameter is not consistent.
 */
static int tsi148_setup_dma_attributes(enum vme_2esst_mode v2esst_mode,
				       enum vme_data_width data_width,
				       enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	switch (v2esst_mode) {
	case VME_SST160:
	case VME_SST267:
	case VME_SST320:
		attrs |= ((v2esst_mode << TSI148_LCSR_DSAT_2eSSTM_SHIFT) &
			  TSI148_LCSR_DSAT_2eSSTM_M);
		break;
	default:
		printk(KERN_ERR
		       "%s: invalid v2esst_mode %d\n",
		       __func__, v2esst_mode);
		return -EINVAL;
	}

	switch (data_width) {
	case VME_D16:
		attrs |= ((TSI148_DW_16 << TSI148_LCSR_DSAT_DBW_SHIFT) &
			  TSI148_LCSR_DSAT_DBW_M);
		break;
	case VME_D32:
		attrs |= ((TSI148_DW_32 << TSI148_LCSR_DSAT_DBW_SHIFT) &
			  TSI148_LCSR_DSAT_DBW_M);
		break;
	default:
                printk(KERN_ERR
                       "%s: invalid data_width %d\n",
		       __func__, data_width);
		return -EINVAL;
	}

	if (am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access)) {
		printk(KERN_ERR
                       "%s: invalid am %x\n",
		       __func__, am);
		return -EINVAL;
	}

	switch (transfer_mode) {
	case TSI148_SCT:
	case TSI148_BLT:
	case TSI148_MBLT:
	case TSI148_2eVME:
	case TSI148_2eSST:
	case TSI148_2eSSTB:
		attrs |= ((transfer_mode << TSI148_LCSR_DSAT_TM_SHIFT) &
			  TSI148_LCSR_DSAT_TM_M);
		break;
	default:
		printk(KERN_ERR
                       "%s: invalid transfer_mode %d\n",
		       __func__, transfer_mode);
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_DSAT_SUP;
		break;
	case TSI148_USER:
		break;
	default:
		printk(KERN_ERR
                       "%s: invalid user_access %d\n",
		       __func__, user_access);
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_DSAT_PGM;
		break;
	case TSI148_DATA:
		break;
	default:
		printk(KERN_ERR
                       "%s: invalid data_access %d\n",
		       __func__, data_access);
		return -EINVAL;
	}

	switch (addr_size) {
	case TSI148_A16:
	case TSI148_A24:
	case TSI148_A32:
	case TSI148_A64:
	case TSI148_CRCSR:
	case TSI148_USER1:
	case TSI148_USER2:
	case TSI148_USER3:
	case TSI148_USER4:
		attrs |= ((addr_size << TSI148_LCSR_DSAT_AMODE_SHIFT) &
			  TSI148_LCSR_DSAT_AMODE_M);
		break;
	default:
		printk(KERN_ERR
                       "%s: invalid addr_size %d\n",
		       __func__, addr_size);
		return -EINVAL;
	}

	return attrs;
}

/**
 * tsi148_dma_setup_src() - Setup the source attributes for a DMA transfer
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_src(struct vme_dma *desc)
{
	int rc;

	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* src = PCI */
		rc = TSI148_LCSR_DSAT_TYP_PCI;
		break;
	case VME_DMA_FROM_DEVICE: /* src = VME */
		rc = tsi148_setup_dma_attributes(desc->src.v2esst_mode,
						 desc->src.data_width,
						 desc->src.am);

		if (rc >= 0)
			rc |= TSI148_LCSR_DSAT_TYP_VME;

		break;
	default:
		printk(KERN_ERR
                       "%s: invalid direction %d\n",
		       __func__, desc->dir);
		rc = -EINVAL;
	}

	return rc;
}

/**
 * tsi148_dma_setup_dst() - Setup the destination attributes for a DMA transfer
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_dst(struct vme_dma *desc)
{
	int rc;

	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* dst = VME */
		rc = tsi148_setup_dma_attributes(desc->dst.v2esst_mode,
						 desc->dst.data_width,
						 desc->dst.am);

		if (rc >= 0)
			rc |= TSI148_LCSR_DDAT_TYP_VME;

		break;
	case VME_DMA_FROM_DEVICE: /* dst = PCI */
		rc = TSI148_LCSR_DDAT_TYP_PCI;
		break;
	default:
		printk(KERN_ERR
                       "%s: invalid direction %d\n",
		       __func__, desc->dir);
		rc = -EINVAL;
	}

	return rc;
}

/**
 * tsi148_dma_setup_ctl() - Setup the DMA control word
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_ctl(struct vme_dma *desc)
{
	int rc = 0;

	/* A little bit of checking. Could be done earlier, dunno. */
	if ((desc->ctrl.vme_block_size < VME_DMA_BSIZE_32) ||
	    (desc->ctrl.vme_block_size > VME_DMA_BSIZE_4096))
		return -EINVAL;

	if ((desc->ctrl.vme_backoff_time < VME_DMA_BACKOFF_0) ||
	    (desc->ctrl.vme_backoff_time > VME_DMA_BACKOFF_64))
		return -EINVAL;

	if ((desc->ctrl.pci_block_size < VME_DMA_BSIZE_32) ||
	    (desc->ctrl.pci_block_size > VME_DMA_BSIZE_4096))
		return -EINVAL;

	if ((desc->ctrl.pci_backoff_time < VME_DMA_BACKOFF_0) ||
	    (desc->ctrl.pci_backoff_time > VME_DMA_BACKOFF_64))
		return -EINVAL;

	rc |= (desc->ctrl.vme_block_size << TSI148_LCSR_DCTL_VBKS_SHIFT) &
		TSI148_LCSR_DCTL_VBKS_M;

	rc |= (desc->ctrl.vme_backoff_time << TSI148_LCSR_DCTL_VBOT_SHIFT) &
		TSI148_LCSR_DCTL_VBOT_M;

	rc |= (desc->ctrl.pci_block_size << TSI148_LCSR_DCTL_PBKS_SHIFT) &
		TSI148_LCSR_DCTL_PBKS_M;

	rc |= (desc->ctrl.pci_backoff_time << TSI148_LCSR_DCTL_PBOT_SHIFT) &
		TSI148_LCSR_DCTL_PBOT_M;

	return rc;
}

/**
 * tsi148_dma_free_chain() - Free all the linked HW DMA descriptors of a channel
 * @chan: DMA channel
 *
 */
void tsi148_dma_free_chain(struct tsi148_dma_chan *tchan)
{
	struct vme_dmaengine_device *ddev = to_vme_dmaengine_device(tchan->dchan.device);
	struct hw_desc_entry *hw_desc;
	struct hw_desc_entry *tmp;
	int count = 1;

	list_for_each_entry_safe(hw_desc, tmp, &tchan->hw_desc_list, list) {
		pci_pool_free(ddev->dma_pool, hw_desc->va, hw_desc->phys);
		list_del(&hw_desc->list);
		kfree(hw_desc);
		count++;
	}
}


static inline int get_vmeaddr(struct vme_dma *desc, unsigned int *vme_addr)
{
	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* src = PCI - dst = VME */
		*vme_addr = desc->dst.addrl;
		return 0;
	case VME_DMA_FROM_DEVICE: /* src = VME - dst = PCI */
		*vme_addr = desc->src.addrl;
		return 0;
	default:
		return -EINVAL;
	}
}

static int
hwdesc_init(struct tsi148_dma_chan *tchan, dma_addr_t *phys,
	    struct tsi148_dma_desc **virt, struct hw_desc_entry **hw_desc)
{
	struct vme_dmaengine_device *ddev = to_vme_dmaengine_device(tchan->dchan.device);
	struct hw_desc_entry *entry;

	/* Allocate a HW DMA descriptor from the pool */
	*virt = pci_pool_alloc(ddev->dma_pool, GFP_KERNEL, phys);
	if (!*virt)
		return -ENOMEM;

	/* keep the virt. and phys. addresses of the descriptor in a list */
	*hw_desc = kmalloc(sizeof(struct hw_desc_entry), GFP_KERNEL);
	if (!*hw_desc) {
		pci_pool_free(ddev->dma_pool, *virt, *phys);
		return -ENOMEM;
	}

	entry = *hw_desc;
	entry->va = *virt;
	entry->phys = *phys;
	list_add_tail(&entry->list, &tchan->hw_desc_list);

	return 0;
}

/*
 * Setup the hardware descriptors for the link list.
 * Beware that the fields have to be setup in big endian mode.
 */
static int
tsi148_fill_dma_desc(struct tsi148_dma_tx_desc *ttx_desc, struct tsi148_dma_desc *tsi,
		unsigned int vme_addr, dma_addr_t dma_addr, unsigned int len,
		unsigned int dsat, unsigned int ddat)
{
	struct vme_dma *desc = &ttx_desc->desc;

	/* Setup the source and destination addresses */
	tsi->dsau = 0;
	tsi->ddau = 0;

	switch (ttx_desc->direction) {
	case DMA_MEM_TO_DEV: /* src = PCI - dst = VME */
		tsi->dsal = cpu_to_be32(dma_addr);
		tsi->ddal = cpu_to_be32(vme_addr);
		tsi->ddbs = cpu_to_be32(desc->dst.bcast_select);
		break;

	case DMA_DEV_TO_MEM: /* src = VME - dst = PCI */
		tsi->dsal = cpu_to_be32(vme_addr);
		tsi->ddal = cpu_to_be32(dma_addr);
		tsi->ddbs = 0;
		break;

	default:
		return -EINVAL;
	}

	tsi->dcnt = cpu_to_be32(len);
	tsi->dsat = cpu_to_be32(dsat);
	tsi->ddat = cpu_to_be32(ddat);

	/* By default behave as if we were at the end of the list */
	tsi->dnlau = 0;
	tsi->dnlal = cpu_to_be32(TSI148_LCSR_DNLAL_LLA);

	return 0;
}


#ifdef DEBUG_DMA
static void tsi148_dma_debug_info(struct tsi148_dma_desc *tsi, int i, int j)
{
	printk(KERN_DEBUG PFX "descriptor %d-%d @%p\n", i, j, tsi);
	printk(KERN_DEBUG PFX "  src : %08x:%08x  %08x\n",
		be32_to_cpu(tsi->dsau), be32_to_cpu(tsi->dsal),
		be32_to_cpu(tsi->dsat));
	printk(KERN_DEBUG PFX "  dst : %08x:%08x  %08x\n",
		be32_to_cpu(tsi->ddau), be32_to_cpu(tsi->ddal),
		be32_to_cpu(tsi->ddat));
	printk(KERN_DEBUG PFX "  cnt : %08x\n",	be32_to_cpu(tsi->dcnt));
	printk(KERN_DEBUG PFX "  nxt : %08x:%08x\n",
		be32_to_cpu(tsi->dnlau), be32_to_cpu(tsi->dnlal));
}
#else
static void tsi148_dma_debug_info(struct tsi148_dma_desc *tsi, int i, int j)
{
}
#endif


/*
 * verbatim from the VME64 standard:
 * 2.3.7 Block Transfer Capabilities, p. 42.
 *	RULE 2.12a: D08(EO), D16, D32 and MD32 block transfer cycles
 *	  (BLT) *MUST NOT* cross any 256 byte boundary.
 * 	RULE 2.78: MBLT cycles *MUST NOT* cross any 2048 byte boundary.
 */
static inline int __tsi148_get_bshift(int am)
{
	switch (am) {
	case VME_A64_MBLT:
	case VME_A32_USER_MBLT:
	case VME_A32_SUP_MBLT:
	case VME_A24_USER_MBLT:
	case VME_A24_SUP_MBLT:
		return 11;
	case VME_A64_BLT:
	case VME_A32_USER_BLT:
	case VME_A32_SUP_BLT:
	case VME_A40_BLT:
	case VME_A24_USER_BLT:
	case VME_A24_SUP_BLT:
		return 8;
	default:
		return 0;
	}
}

static int tsi148_get_bshift(struct tsi148_dma_tx_desc *ttx_desc)
{
	struct vme_dma *desc = &ttx_desc->desc;
	int am;

	if (ttx_desc->direction == DMA_DEV_TO_MEM)
		am = desc->src.am;
	else
		am = desc->dst.am;
	return __tsi148_get_bshift(am);
}

/* Note: block sizes are always powers of 2 */
static inline unsigned long tsi148_get_bsize(int bshift)
{
	return 1 << bshift;
}

static inline unsigned long tsi148_get_bmask(unsigned long bsize)
{
	return bsize - 1;
}

/*
 * Add a certain chunk of data to the TSI DMA linked list.
 * Note that this function deals with physical addresses on the
 * host CPU and VME addresses on the VME bus.
 */
static int
tsi148_dma_link_add(struct tsi148_dma_tx_desc *ttx_desc,
		    struct tsi148_dma_desc **virt,
		unsigned int vme_addr, dma_addr_t dma_addr, unsigned int size,
		int numpages, int index, int bshift, unsigned int dsat,
		unsigned int ddat)

{
	struct tsi148_dma_chan *tchan = to_tsi148_dma_chan(ttx_desc->tx.chan);
	struct hw_desc_entry *hw_desc = NULL;
	struct tsi148_dma_desc *curr;
	struct tsi148_dma_desc *next = NULL;
	struct vme_dma *desc = &ttx_desc->desc;
	dma_addr_t phys_next;
	dma_addr_t dma;
	dma_addr_t dma_end;
	unsigned int vme;
	unsigned int vme_end;
	unsigned int len;
	int rc;
	int i = 0;

	/*
	 * The function expects **virt to be already initialised.
	 * When calling this function in a loop, it will always set virt
	 * to point to the next link in the chain, or to NULL when the
	 * current link is the last in the chain.
	 */
	BUG_ON(virt == NULL || *virt == NULL);
	curr = *virt;

	vme = vme_addr;
	vme_end = vme_addr + size;

	dma = dma_addr;
	dma_end = dma_addr + size;

	len = size;

	while (vme < vme_end && dma < dma_end) {

		/* calculate the length up to the next block boundary */
		if (bshift) {
			unsigned long bsize = tsi148_get_bsize(bshift);
			unsigned long bmask = tsi148_get_bmask(bsize);
			unsigned int unaligned = vme & bmask;

			len = bsize - unaligned;
		}

		/* check the VME block won't overflow */
		if (dma + len > dma_end)
			len = dma_end - dma;

		tsi148_fill_dma_desc(ttx_desc, curr, vme, dma, len, dsat, ddat);

		/* increment the VME address unless novmeinc is set */
		if (!desc->novmeinc)
			vme += len;
		dma += len;

		/* chain consecutive links together */
		if (index < numpages - 1 || dma < dma_end) {
			rc = hwdesc_init(tchan, &phys_next, &next, &hw_desc);
			if (rc)
				return rc;

			curr->dnlau = 0;
			curr->dnlal = cpu_to_be32(phys_next &
						TSI148_LCSR_DNLAL_DNLAL_M);
		}

		tsi148_dma_debug_info(curr, index, i);

		curr = next;
		i++;
	}

	*virt = next;
	return 0;
}

/**
 * tsi148_dma_setup_chain() - Setup the linked list of TSI148 DMA descriptors
 * @chan: DMA channel descriptor
 * @dsat: DMA source attributes
 * @ddat: DMA destination attributes
 *
 */
static int tsi148_dma_setup_chain(struct tsi148_dma_tx_desc *ttx_desc,
				  unsigned int dsat, unsigned int ddat)
{
	struct tsi148_dma_chan *tchan = to_tsi148_dma_chan(ttx_desc->tx.chan);
	int i;
	int rc;
	struct scatterlist *sg;
	struct vme_dma *desc = &ttx_desc->desc;
	dma_addr_t phys_start;
	struct tsi148_dma_desc *curr = NULL;
	struct hw_desc_entry *hw_desc = NULL;
	unsigned int vme_addr = 0;
	dma_addr_t dma_addr;
	unsigned int len;
	int bshift = tsi148_get_bshift(ttx_desc);

	rc = hwdesc_init(tchan, &phys_start, &curr, &hw_desc);
	if (rc)
		return rc;

	rc = get_vmeaddr(desc, &vme_addr);
	if (rc)
		goto out_free;

	for_each_sg(ttx_desc->sgl, sg, ttx_desc->sg_len, i) {
		dma_addr = sg_dma_address(sg);
		len = sg_dma_len(sg);

		rc = tsi148_dma_link_add(ttx_desc, &curr, vme_addr, dma_addr, len,
					ttx_desc->sg_len, i, bshift, dsat, ddat);
		if (rc)
			goto out_free;
		/* For non incrementing DMA, reset the VME address */
		if (!desc->novmeinc)
			vme_addr += len;
	}

	/* Now program the DMA registers with the first entry in the list */
	iowrite32be(0, &tsi148_chip_global->lcsr.dma[tchan->index].dma_desc.dnlau);
	iowrite32be(phys_start & TSI148_LCSR_DNLAL_DNLAL_M,
		    &tsi148_chip_global->lcsr.dma[tchan->index].dma_desc.dnlal);

	return 0;

out_free:
	tsi148_dma_free_chain(tchan);


	return rc;
}

/**
 * tsi148_dma_setup() - Setup a TSI148 DMA channel for a transfer
 * @chan: DMA channel descriptor
 *
 */
int tsi148_dma_setup(struct tsi148_dma_tx_desc *tx_desc)
{
	int rc;
	struct tsi148_dma_chan *tchan = to_tsi148_dma_chan(tx_desc->tx.chan);
	struct vme_dma *desc = &tx_desc->desc;
	unsigned int dsat;
	unsigned int ddat;
	unsigned int dctl;

	/* Setup DMA source attributes */
	if ((rc = tsi148_dma_setup_src(desc)) < 0) {
		printk(KERN_ERR "%s: src setup failed\n", __func__);
		return rc;
	}

	dsat = rc;

	/* Setup DMA destination attributes */
	if ((rc = tsi148_dma_setup_dst(desc)) < 0) {
		printk(KERN_ERR "%s: dst setup failed\n", __func__);
		return rc;
	}

	ddat = rc;

	/* Setup DMA control */
	if ((rc = tsi148_dma_setup_ctl(desc)) < 0) {
		printk(KERN_ERR "%s: ctl setup failed\n", __func__);
		return rc;
	}

	dctl = rc;

	/* always use the scatter-gather list */
	dctl &= ~TSI148_LCSR_DCTL_MOD;
	iowrite32be(dctl, &tsi148_chip_global->lcsr.dma[tchan->index].dctl);
	rc = tsi148_dma_setup_chain(tx_desc, dsat, ddat);

	return rc;
}


/**
 * tsi148_dma_release() - Release resources used for a DMA transfer
 * @chan: DMA channel descriptor
 *
 *  Only chained transfer are allocating memory. For direct transfer, there
 * is nothig to free.
 */
void tsi148_dma_release(struct tsi148_dma_chan *tchan)
{
	tsi148_dma_free_chain(tchan);
}

/* Kernel functions not exported */
#if 1

/**
 * dma_cookie_complete - complete a descriptor
 * @tx: descriptor to complete
 *
 * Mark this descriptor complete by updating the channels completed
 * cookie marker.  Zero the descriptors cookie to prevent accidental
 * repeated completions.
 *
 * Note: caller is expected to hold a lock to prevent concurrency.
 */
static inline void dma_cookie_complete(struct dma_async_tx_descriptor *tx)
{
	BUG_ON(tx->cookie < DMA_MIN_COOKIE);
	tx->chan->completed_cookie = tx->cookie;
	tx->cookie = 0;
}

/**
 * dma_cookie_init - initialize the cookies for a DMA channel
 * @chan: dma channel to initialize
 */
static inline void dma_cookie_init(struct dma_chan *chan)
{
	chan->cookie = DMA_MIN_COOKIE;
	chan->completed_cookie = DMA_MIN_COOKIE;
}

/**
 * dma_cookie_assign - assign a DMA engine cookie to the descriptor
 * @tx: descriptor needing cookie
 *
 * Assign a unique non-zero per-channel cookie to the descriptor.
 * Note: caller is expected to hold a lock to prevent concurrency.
 */
static inline dma_cookie_t dma_cookie_assign(struct dma_async_tx_descriptor *tx)
{
	struct dma_chan *chan = tx->chan;
	dma_cookie_t cookie;

	cookie = chan->cookie + 1;
	if (cookie < DMA_MIN_COOKIE)
		cookie = DMA_MIN_COOKIE;
	tx->cookie = chan->cookie = cookie;

	return cookie;
}

/**
 * dma_cookie_status - report cookie status
 * @chan: dma channel
 * @cookie: cookie we are interested in
 * @state: dma_tx_state structure to return last/used cookies
 *
 * Report the status of the cookie, filling in the state structure if
 * non-NULL.  No locking is required.
 */
static inline enum dma_status dma_cookie_status(struct dma_chan *chan,
	dma_cookie_t cookie, struct dma_tx_state *state)
{
	dma_cookie_t used, complete;

	if (cookie < 0)
		return DMA_ERROR;

	used = chan->cookie;
	complete = chan->completed_cookie;
	barrier();
	if (state) {
		state->last = complete;
		state->used = used;
		state->residue = 0;
	}
	return dma_async_is_complete(cookie, complete, used);
}
#endif


enum vme_dmaengine_mode {
	VME_DMAENGINE_MODE_LIST = 0,
	VME_DMAENGINE_MODE_DIRECT,
};

static dma_cookie_t tsi148_tx_submit(struct dma_async_tx_descriptor *tx);

/**
 * The the DMA transfer mode
 */
static void tsi148_dma_mode(struct tsi148_dma_chan *tchan,
			    enum vme_dmaengine_mode mode)
{
	if (mode == VME_DMAENGINE_MODE_LIST)
		tsi148_dctl_bit_clr(tchan, TSI148_LCSR_DCTL_MOD);
	else
		tsi148_dctl_bit_set(tchan, TSI148_LCSR_DCTL_MOD);
}

static void tsi148_dma_terminate_all(struct dma_chan *dchan)
{
	struct tsi148_dma_chan *tdchan = to_tsi148_dma_chan(dchan);
	struct tsi148_dma_tx_desc *desc, *_desc;
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&tdchan->lock, flags);
	ret = tsi148_dma_busy(tdchan);
	if (ret)
		tsi148_dma_abort(tdchan);
	/* leave some time for the bridge to clear the DMA channel */
	udelay(10);

	list_for_each_entry_safe(desc, _desc, &tdchan->pending_list, list) {
		/*
		 * Make the transfer invalid, so that clients can detect
		 * that the transfer has been terminated
		 */
		desc->tx.cookie = -ENODEV;
		list_del(&desc->list);
	}

	if (tdchan->tx_desc_current == NULL)
		goto out;

	tsi148_dma_release(tdchan);
	/*
	 * Make the transfer invalid, so that clients can detect
	 * that the transfer has been terminated
	 */
	tdchan->tx_desc_current->tx.cookie = -ENODEV;
	tdchan->tx_desc_current = NULL;
out:
	spin_unlock_irqrestore(&tdchan->lock, flags);
}


/**
 * Allocate resources for the given channel. This function must keep the
 * compatibility with the vme_do_dma() interface.
 */
static int vme_dmaengine_alloc_chan_resources(struct dma_chan *dchan)
{
	struct tsi148_dma_chan *tchan = to_tsi148_dma_chan(dchan);

	if (WARN_ON(!list_empty(&tchan->pending_list)))
		return -EBUSY;
	if (WARN_ON(tchan->tx_desc_current))
		return -EBUSY;

	tchan->tx_desc_current = NULL;
	dma_cookie_init(dchan);

	tsi148_dma_mode(tchan, VME_DMAENGINE_MODE_LIST);

	return 0;
}


/**
 * Release resources for the given channel. This function must keep the
 * compatibility with the vme_do_dma() interface.
 */
static void vme_dmaengine_free_chan_resources(struct dma_chan *dchan)
{
	tsi148_dma_terminate_all(dchan);
}

static struct dma_async_tx_descriptor *vme_dmaengine_prep_slave_sg(
	struct dma_chan *dchan, struct scatterlist *sgl, unsigned int sg_len,
	enum dma_transfer_direction direction, unsigned long flags,
	void *context)
{
	struct tsi148_dma_tx_desc *desc;
	struct vme_dma *vme_desc = context;

	if (unlikely(!vme_desc)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: missing VME context\n");
		return NULL;
	}
	if (unlikely(direction != DMA_DEV_TO_MEM
		     && direction != DMA_MEM_TO_DEV)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: Support only DEV <-> MEM transfers\n");
		return NULL;
	}

	if (unlikely(!sgl || !sg_len)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: empty DMA scatterlist\n");
		return NULL;
	}

	/* Check we're within a 32-bit address space */
	if (BITS_PER_LONG < 64 &&
	    (vme_desc->src.addru || vme_desc->dst.addru)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: 64bit address not supported\n");
		return NULL;
	}

	if (unlikely(vme_desc->src.data_width == VME_D64 ||
		     vme_desc->dst.data_width == VME_D64)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: 64bit DMA data width not supported\n");
		return NULL;
	}

	if (unlikely(vme_desc->src.data_width == VME_D8 ||
		     vme_desc->dst.data_width == VME_D8)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: 8bit DMA data width not supported\n");
		return NULL;
	}

	desc = kzalloc(sizeof(struct tsi148_dma_tx_desc), GFP_NOWAIT);
	if (!desc)
		return NULL;

	dma_async_tx_descriptor_init(&desc->tx, dchan);
	desc->tx.tx_submit = tsi148_tx_submit;
	desc->direction = direction;
	desc->sgl = sgl;
	desc->sg_len = sg_len;
	memcpy(&desc->desc, vme_desc, sizeof(desc->desc));

	return &desc->tx;
}


/**
 * Add a descriptor to the pending DMA transfer queue.
 * This will not trigger any DMA transfer: here we just collect DMA
 * transfer descriptions.
 */
static dma_cookie_t tsi148_tx_submit(struct dma_async_tx_descriptor *tx)
{
	struct tsi148_dma_tx_desc *desc =
		container_of(tx, struct tsi148_dma_tx_desc, tx);
	struct tsi148_dma_chan *tchan = to_tsi148_dma_chan(tx->chan);
	dma_cookie_t cookie;
	unsigned long flags;

	spin_lock_irqsave(&tchan->lock, flags);
	cookie = dma_cookie_assign(tx);
	list_add_tail(&desc->list, &tchan->pending_list);
	spin_unlock_irqrestore(&tchan->lock, flags);

	return cookie;
}


/**
 * It starts a tasklet which will start the DMA transfers. We must use a tasklet
 * because this function can be called in interrupt context.
 */
static void vme_dmaengine_issue_pending(struct dma_chan *dchan)
{
	struct tsi148_dma_chan *tdchan = to_tsi148_dma_chan(dchan);

	if (list_empty(&tdchan->pending_list))
		return; /* Nothing pending to send on this channel */

	tasklet_schedule(&tdchan->dma_start_task);
}


/**
 * It allows drivers to send commands to the engine.
 * For the time being we only support the TERMINATE command
 */
static int vme_dmaengine_device_control(struct dma_chan *dchan,
					enum dma_ctrl_cmd cmd,
					unsigned long arg)
{
	struct tsi148_dma_chan *tdchan = to_tsi148_dma_chan(dchan);
	int err = 0;

	switch (cmd) {
	case DMA_TERMINATE_ALL:
		tsi148_dma_terminate_all(dchan);
		break;
	case DMA_PAUSE:
		/*
		 * Pause works only with linked-list mode. This engine will
		 * perform *only* linked-list transfers. So, pause always work
		 */
		tsi148_dma_pause(tdchan);
		udelay(10);
		break;
	case DMA_RESUME:
		tsi148_dma_start(tdchan);
		break;
	default:
		err = -ENXIO;
		break;
	}

	return err;
}


/**
 * The DMA transfer is complete, remove it from the list and try to run
 * the next pending transfer
 */
static void vme_dmaengine_tx_complete(struct tsi148_dma_chan *tdchan)
{
	struct tsi148_dma_tx_desc *tx_desc;
	dma_async_tx_callback callback = NULL;
	void *callback_param = NULL;
	unsigned long flags;

	spin_lock_irqsave(&tdchan->lock, flags);
	tx_desc = tdchan->tx_desc_current;
	if (unlikely(!tx_desc)) {
		spin_unlock_irqrestore(&tdchan->lock, flags);
		dev_warn(&tdchan->dchan.dev->device,
			 "Spurious DMA interrupt\n");
		return;
	}

	dma_cookie_complete(&tx_desc->tx);

	callback = tx_desc->tx.callback;
	callback_param = tx_desc->tx.callback_param;

	kfree(tx_desc);

	tsi148_dma_release(tdchan);
	tdchan->tx_desc_current = NULL;
	spin_unlock_irqrestore(&tdchan->lock, flags);

	/* Inform the driver that the DMA is over */
	callback(callback_param);

	/* try to send the next one */
	if (!list_empty(&tdchan->pending_list))
		tasklet_schedule(&tdchan->dma_start_task);
}


/**
 * It handles DMA completion.
 */
void vme_dmaengine_irq_handler(int channel_mask,
			       struct vme_bridge_device *vbridge)
{
	int i;

	for (i = 0; i < TSI148_NUM_DMA_CHANNELS; i++) {
		if (!(channel_mask & (1 << i)))
			continue; /* no DMA done on this channel */

		vme_dmaengine_tx_complete(&vbridge->dmaengine->tdchan[i]);
	}
}


/**
 * It starts the first pending DMA transaction
 */
static void vme_dmaengine_start_task(unsigned long arg)
{
	struct tsi148_dma_chan *tdchan = (struct tsi148_dma_chan *)arg;
	struct tsi148_dma_tx_desc *tx_desc;
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&tdchan->lock, flags);
	/*
	 * How much time does it take for the BUSY bit to get high? Here
	 * I assume that as soon as I do START, BUSY become 1.  If this is not
	 * the case then we have a critical section as large as the time
	 * necessary to set BUSY.  The TSI148 manual is not clear about this.
	 */
	ret = tsi148_dma_busy(tdchan);
	if (unlikely(ret) || unlikely(tdchan->tx_desc_current)) {
		dev_warn(&tdchan->dchan.dev->device,
			"Trying to run a DMA transfer while another one is still in progress\n");
		goto err_busy;
	}

	if (list_empty(&tdchan->pending_list))
		goto out_empty;

	tx_desc = list_first_entry(&tdchan->pending_list,
				   struct tsi148_dma_tx_desc, list);
	list_del(&tx_desc->list);

	ret = tsi148_dma_setup(tx_desc);
	if (ret) {
		tx_desc->tx.cookie = -EINVAL;
		wake_up(&tdchan->wait);
		goto err_setup;
	}
	tdchan->tx_desc_current = tx_desc;

	tsi148_dma_start(tdchan);

err_setup:
out_empty:
err_busy:
	spin_unlock_irqrestore(&tdchan->lock, flags);
}


/**
 * It configures the DMA engine
 */
int vme_dmaengine_init(struct vme_bridge_device *vbridge)
{
	struct vme_dmaengine_device *dmaengine;
	int i, err = 0;

	err = pci_set_dma_mask(vbridge->pdev, DMA_BIT_MASK(BITS_PER_LONG));
	if (err) {
		dev_err(&vbridge->pdev->dev,
			"Bridge does not support 32 bit PCI DMA\n");
		goto err_mask;
	}

	dmaengine = kzalloc(sizeof(*dmaengine), GFP_KERNEL);
	if (!dmaengine) {
		err = -ENOMEM;
		goto err_alloc;
	}

	dmaengine->dma.dev = &vbridge->pdev->dev;
	INIT_LIST_HEAD(&dmaengine->dma.channels);

	/* Capabilities */
	dma_cap_zero(dmaengine->dma.cap_mask);
	dma_cap_set(DMA_SLAVE, dmaengine->dma.cap_mask);
	dma_cap_set(DMA_PRIVATE, dmaengine->dma.cap_mask);

	/* Operations */
	dmaengine->dma.device_alloc_chan_resources =
		vme_dmaengine_alloc_chan_resources;
	dmaengine->dma.device_free_chan_resources =
		vme_dmaengine_free_chan_resources;
	dmaengine->dma.device_prep_slave_sg =
		vme_dmaengine_prep_slave_sg;
	dmaengine->dma.device_control =
		vme_dmaengine_device_control;
	dmaengine->dma.device_tx_status =
		dma_cookie_status;
	dmaengine->dma.device_issue_pending =
		vme_dmaengine_issue_pending;

	/*
	 * Create the DMA chained descriptor pool.
	 * Those descriptors must be 64-bit aligned as specified in the
	 * TSI148 User Manual. Also do not allow descriptors to cross a
	 * page boundary as the 2 pages may not be contiguous.
	 */
	dmaengine->dma_pool = pci_pool_create("vme_dma_desc_pool",
						      vbridge->pdev,
						      sizeof(struct tsi148_dma_desc),
						      8, 4096);
	if (!dmaengine->dma_pool) {
		dev_err(&vbridge->pdev->dev, "Can't allocare DMA pool\n");
		err = -ENOMEM;
		goto err_pool;
	}

	/* DMA channels configuration */
	for (i = 0; i < TSI148_NUM_DMA_CHANNELS; i++) {
		struct tsi148_dma_chan *tdchan = &dmaengine->tdchan[i];

		tdchan->index = i;
		tdchan->dchan.device = &dmaengine->dma;
		INIT_LIST_HEAD(&tdchan->hw_desc_list);
		INIT_LIST_HEAD(&tdchan->pending_list);
		spin_lock_init(&tdchan->lock);
		init_waitqueue_head(&tdchan->wait);

		list_add_tail(&tdchan->dchan.device_node,
			      &dmaengine->dma.channels);

		tasklet_init(&tdchan->dma_start_task,
			     vme_dmaengine_start_task,
			     (unsigned long)tdchan);
	}

	/* Register the engine */
	err = dma_async_device_register(&dmaengine->dma);
	if (err)
		goto err_reg;

	vbridge->dmaengine = dmaengine;

	return 0;

err_reg:
	pci_pool_destroy(dmaengine->dma_pool);
err_pool:
	kfree(dmaengine);
err_alloc:
err_mask:
	vbridge->dmaengine = NULL;
	return err;
}

void vme_dmaengine_exit(struct vme_bridge_device *vbridge)
{
	if (vbridge->dmaengine == NULL)
		return;

	dma_async_device_unregister(&vbridge->dmaengine->dma);
	pci_pool_destroy(vbridge->dmaengine->dma_pool);
	kfree(vbridge->dmaengine);
}
