"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import os
import stat

class TestVmebusSysfs(object):
    """It tests basic things about the VME bus"""

    def test_sysfs_bridge_type(self):
        """Check if 'bridge_type' exists and permissions are RO"""
        path ="/sys/bus/vme/bridge_type"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR

    def test_sysfs_dma_force_configuration(self):
        """Check if 'dma_force_configuration' exists and permissions are RW"""
        path ="/sys/bus/vme/dma_force_configuration"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    def test_sysfs_dma_backoff_time_pci(self):
        """Check if 'dma_backoff_time_pci' exists and permissions are RW"""
        path = "/sys/bus/vme/dma_backoff_time_pci"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    def test_sysfs_dma_backoff_time_vme(self):
        """Check if 'dma_backoff_time_vme' exists and permissions are RW"""
        path = "/sys/bus/vme/dma_backoff_time_vme"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    def test_sysfs_dma_block_size_pci(self):
        """Check if 'dma_block_size_pci' exists and permissions are RW"""
        path = "/sys/bus/vme/dma_block_size_pci"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    def test_sysfs_rescan(self):
        """Check if 'rescan' exists and permissions are WO"""
        path = "/sys/bus/vme/rescan"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IWUSR

    def test_sysfs_dma_block_size_vme(self):
        """Check if 'dma_block_size_vme' exists and permissions are RW"""
        path = "/sys/bus/vme/dma_block_size_vme"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    @pytest.mark.parametrize("slot", range(1, 22))
    def test_sysfs_slots(self, slot):
        """Check if 'slot-DD' exists and it is a directory"""
        path = "/sys/bus/vme/devices/slot.{:02d}/".format(slot)
        assert os.path.exists(path)
        mode = os.stat(path).st_mode
        assert stat.S_ISDIR(mode)

    @pytest.mark.parametrize("slot", range(1, 22))
    def test_sysfs_slots_slot(self, slot):
        """Check if 'slot-DD/slot' exists, permissions are RO and it returns
        the correct slot number"""
        path = "/sys/bus/vme/devices/slot.{:02d}/slot".format(slot)
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR
        with open(path) as f:
            try:
                slot_rb = int(f.read())
            except:
                pass
            assert slot_rb == slot

    @pytest.mark.parametrize("slot", range(1, 22))
    def test_sysfs_slots_register(self, slot):
        """Check if 'slot-DD/register' exists and permissions are WO"""
        path = "/sys/bus/vme/devices/slot.{:02d}/register".format(slot)
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IWUSR

    @pytest.mark.parametrize("slot", range(1, 22))
    def test_sysfs_slots_rescan(self, slot):
        """Check if 'slot-DD/rescan' exists and permissions are WO"""
        path = "/sys/bus/vme/devices/slot.{:02d}/rescan".format(slot)
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IWUSR
