#!/bin/bash

# (C) CERN 2017
# author: Adam Wujek <adam.wujek@cern.ch>

/sbin/rmmod men_wrapper
/sbin/rmmod men_pldz002_cham
/sbin/rmmod men_serflash
/sbin/rmmod men_lx_chameleon
/sbin/rmmod men_chameleon
/sbin/rmmod men_chameleon_io
/sbin/rmmod men_oss
/sbin/rmmod men_vme4l-core
/sbin/rmmod men_dbg
