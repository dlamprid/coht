"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""


import pytest
import stat
import subprocess
import os
import time
import errno
from PyVME.PyVmeDevice import PyVmeDevice

VME64X_VD80_SLOTS = [9, ]
VME64X_SVEC_SLOTS = [8, 10, 11, 12]

class TestVmebusVme64x(object):
    """Registration tests specific to VME64x"""

    @pytest.mark.parametrize("slot", VME64X_VD80_SLOTS)
    def test_registration_vme64x(self, slot):
        """Register a known VME64x device"""
        path = "/sys/bus/vme/devices/slot.{:02d}/vme.{:d}".format(slot, slot)
        assert not os.path.exists(path)
        with PyVmeDevice(slot) as vdev:
            vdev.register(size=0x80000)
            assert os.path.exists(path)
            vdev.unregister()
            assert not os.path.exists(path)

    @pytest.mark.parametrize("slot", VME64X_VD80_SLOTS)
    def test_rescan_vme64x(self, slot):
        """Register a known VME64x device using rescan"""
        path = "/sys/bus/vme/devices/slot.{:02d}/vme.{:d}".format(slot, slot)
        assert not os.path.exists(path)
        with PyVmeDevice(slot) as vdev:
            vdev.rescan()
            assert os.path.exists(path)
            vdev.unregister()
            assert not os.path.exists(path)

    def test_enable_disable(self, vme_device_vd80):
        """Check if a VME64x card can be enabled/disabled"""
        for i in range(3):
            vme_device_vd80.enable()
            assert vme_device_vd80.is_enabled()
            vme_device_vd80.disable()
            assert not vme_device_vd80.is_enabled()

#    @pytest.mark.parametrize("func,am", [(func, am) for func in [0, 1] for am in [0x38, 0x39, 0x3B, 0x3C, 0x3D, 0x3F]])
    @pytest.mark.parametrize("func,addr,am", [(func, addr, am) for func in [0, 1] for addr in [1 << x for x in range(14, 23)] for am in [0x39, 0x3D]])
    def test_ader_vd80_valid(self, vme_device_vd80, func, addr, am):
        """Check if we can correctly set an ADER on VD80 (NO BLT,MBLT not
           sure if the configuration is correct)"""
        ader = (addr & 0xFFFFFF00) | ((am & 0xFF) << 2)
        vme_device_vd80.ader_set(func, addr, am)
        ader_rb = vme_device_vd80.ader_get(func)
        assert ader_rb == ader

    @pytest.mark.parametrize("func,addr,am", [(1, addr, am) for addr in [1 << x for x in range(19, 23)] for am in [0x39, 0x3D]])
    def test_ader_svec_valid(self, svec_empty, func, addr, am):
        """Check if we can correctly set an ADER on SVEC (NO BLT,MBLT not
           sure if the configuration is correct)"""
        ader = (addr & 0xFFFFFF00) | ((am & 0xFF) << 2)
        svec_empty.ader_set(func, addr, am)
        ader_rb =    svec_empty.ader_get(func)
        assert ader_rb == ader
        assert os.path.exists(os.path.join(svec_empty.path_to_dev,
                                           "resource{:d}".format(func)))
        mode = stat.S_IMODE(os.stat(os.path.join(svec_empty.path_to_dev,
                                                 "resource{:d}".format(func))).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR | stat.S_IWUSR

    @pytest.mark.parametrize("func,addr,am", [(1, 0x100000, 0x39), ])
    def test_ader_change_while_in_use(self, svec_empty, func, addr, am):
        """It should not be possible to change ADER while a driver is
        handling a device"""
        ader = (addr & 0xFFFFFF00) | ((am & 0xFF) << 2)
        svec_empty.ader_set(func, addr, am)
        subprocess.run(["modprobe",
                        "-d", "/user/fvaga/deployment/L867/",
                        "svec-fmc-carrier"], check=True)
        assert ader == svec_empty.ader_get(func)

        ader = ((addr * 2) & 0xFFFFFF00) | ((am & 0xFF) << 2)
        with pytest.raises(OSError) as err:
            svec_empty.ader_set(func, addr * 2, am)
        assert err.value.errno == errno.EBUSY
        with open("/sys/bus/vme/drivers/svec-fmc-carrier/unbind", "w") as f_unbind:
            f_unbind.write("vme.{:d}".format(svec_empty.slot))

        svec_empty.ader_set(func, addr * 2, am)
        assert ader == svec_empty.ader_get(func)

    def test_ader_overlap_detection(self, svec_all):
        addr = 0x100000
        # set all ADER
        for vdev in svec_all:
            vdev.ader_set(1, addr, 0x39)
            addr = addr + 0x80000
        svec_all[0].ader_set(1, 0x200000, 0x3D)
        with pytest.raises(OSError) as err:
            svec_all[0].ader_set(1, 0x200000, 0x39)
