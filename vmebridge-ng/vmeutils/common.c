// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "vmeutils.h"

#define SYSFS_PATH_LEN 1024


static const char *spec_string[] = {
	[VME_SPEC_UNKNOWN] = "Non Standard or before VME64",
	[VME_SPEC_VME64] = "VME64",
	[VME_SPEC_VME64x] = "VME64x",
};

const char *vme_spec_to_string(enum vme_specification spec)
{
	return spec_string[spec];
}

static void print_version(const char *name)
{

	fprintf(stdout, "%s version " VMEUTILS_VERSION "\n", name);
}

void command_help(int argc, char *argv[], const char *help_msg)
{
	int i;

	for (i = 0; i < argc; ++i) {
		if (!strcmp(argv[i], "--help")) {
			fputs(help_msg, stdout);
			exit(EXIT_SUCCESS);
		}
	}
}

void command_version(int argc, char *argv[])
{
	int i;

	for (i = 0; i < argc; ++i) {
		if (!strcmp(argv[i], "--version"))
		{
			print_version(program_name);
			exit(EXIT_SUCCESS);
		}
	}
}

void vme_generic_error(const char *msg, ...)
{
	va_list args;

	if (!msg) {
		fputs("Error but message is invalid as well", stderr);
		exit(EXIT_FAILURE);
	}

	va_start(args, msg);
	fprintf(stderr, "%s: ", program_name);
	vfprintf(stderr, msg, args);
	fputc('\n', stderr);
	va_end(args);
	exit(EXIT_FAILURE);
}

int vme_sysfs_get_value(struct vme_dev *d, char *object)
{
	int fd, n;
	char namebuf[SYSFS_PATH_LEN], buf[256];

	n = snprintf(namebuf, sizeof(namebuf),
		     "/sys/bus/vme/devices/slot.%02d/%s/%s",
		     d->slot, d->name, object);
	if (n <0 || n >= SYSFS_PATH_LEN)
		vme_generic_error("file name too long");
	fd = open(namebuf, O_RDONLY);
	if (fd < 0)
		vme_generic_error("Cannot open %s: %s", namebuf, strerror(errno));
	n = read(fd, buf, sizeof(buf));
	close(fd);
	if (n < 0)
		vme_generic_error("Error reading %s: %s", namebuf, strerror(errno));
	if (n >= (int) sizeof(buf))
		vme_generic_error("Value in %s too long", namebuf);
	buf[n] = 0;
	return strtol(buf, NULL, 0);
}

void vme_sysfs_set_value(struct vme_dev *d, char *object, int val, char *str)
{
	int fd, n;
	char namebuf[SYSFS_PATH_LEN], buf[256];

	n = snprintf(namebuf, sizeof(namebuf),
		     "/sys/bus/vme/devices/slot.%02d/%s/%s",
		     d->slot, d->name, object);
	if (n <0 || n >= SYSFS_PATH_LEN)
		vme_generic_error("file name too long");
	fd = open(namebuf, O_WRONLY);
	if (fd < 0)
		vme_generic_error("Cannot open %s: %s", namebuf, strerror(errno));
	if (str)
		strncpy(buf, str, sizeof(buf));
	else
		snprintf(buf, sizeof(buf), "%i", val);
	n = write(fd, buf, sizeof(buf));
	close(fd);
	if (n < 0)
		vme_generic_error("Error writing %s: %s", namebuf, strerror(errno));
}


static void sysfs_get_cr(struct vme_dev *d)
{
	int fd, n, total, ret;
	char namebuf[SYSFS_PATH_LEN];

	d->cr_dump = malloc(VME_DEV_CR_SIZE);
	if (!d->cr_dump)
		vme_generic_error("Cannot allocate memory for CR");

	n = snprintf(namebuf, sizeof(namebuf), "/sys/bus/vme/devices/%s/cr",
		     d->name);
	if (n <0 || n >= SYSFS_PATH_LEN)
		vme_generic_error("file name too long");
	fd = open(namebuf, O_RDONLY);
	if (fd < 0)
		vme_generic_error("Cannot open %s: %s", namebuf, strerror(errno));

	n = 0;
	total = VME_DEV_CR_SIZE;
	do {
		ret = read(fd, d->cr_dump + n, total);
		if (ret <= 0)
			break;
		total -= ret;
		n += ret;
	} while (total);
	close(fd);
	if (ret < 0)
		vme_generic_error("Error reading %s: %s", namebuf, strerror(errno));
}

static void sysfs_get_csr(struct vme_dev *d)
{
	int fd, n, total, ret;
	char namebuf[SYSFS_PATH_LEN];

	d->csr_dump = malloc(VME_DEV_CSR_SIZE);
	if (!d->csr_dump)
		vme_generic_error("Cannot allocate memory for CSR");

	n = snprintf(namebuf, sizeof(namebuf), "/sys/bus/vme/devices/%s/csr",
		     d->name);
	if (n <0 || n >= SYSFS_PATH_LEN)
		vme_generic_error("file name too long");
	fd = open(namebuf, O_RDONLY);
	if (fd < 0)
		vme_generic_error("Cannot open %s: %s", namebuf, strerror(errno));

	n = 0;
	total = VME_DEV_CSR_SIZE;
	do {
		ret = read(fd, d->csr_dump + n, total);
		if (ret <= 0)
			break;
		total -= ret;
		n += ret;
	} while (total);
	close(fd);
	if (ret < 0)
		vme_generic_error("Error reading %s: %s", namebuf, strerror(errno));
}

static struct vme_dev *alloc_dev()
{
	struct vme_dev *d;

	d = malloc(sizeof(*d));
	if (!d)
		vme_generic_error("Out of memory (allocation of %d bytes failed)",
				    sizeof(*d));
	memset(d, 0, sizeof(*d));

	return d;
}

static void free_dev(struct vme_dev *d)
{
	if (d) {
		free(d->cr_dump);
		free(d->csr_dump);
		free(d);
	}
}

static void link_dev(struct vme_access *a, struct vme_dev *d)
{
	d->next = a->devices;
	a->devices = d;
}

/**
 * It allocates an access descriptor
 * Return: an access descriptor
 */
struct vme_access *vme_access_alloc(void)
{
	struct vme_access *a;

	a = malloc(sizeof(*a));
	if (!a)
		vme_generic_error("Out of memory (allocation of %d bytes failed)",
				    sizeof(*a));
	memset(a, 0, sizeof(*a));

	return a;
}

/**
 * It release an access descriptor
 * @a access descriptor
 */
static void free_access(struct vme_access *a)
{
	if (a)
		free(a);
}

/**
 * It cleans up all the mess we did
 * @a access descriptor
 */
void vme_access_cleanup(struct vme_access *a)
{
	struct vme_dev *d, *e;

	for (d = a->devices; d; d = e) {
		e = d->next;
		free_dev(d);
	}

	free_access(a);
}

/**
 * It gathers information from the device
 * @d the device to inspect
 */
static void sysfs_scan_device(struct vme_dev *d)
{
	d->spec = vme_sysfs_get_value(d, "specification");
	if (d->spec > VME_SPEC_VME64) {
		sysfs_get_cr(d);
		sysfs_get_csr(d);
	}
}

/**
 * @d the device to inspect
 * @f device filter
 *
 * Return: 1 if the device is good, 0 otherwise
 */
static int apply_filter(struct vme_dev *d, struct vme_filter *f)
{
	if (f->slot > 0 && d->slot != f->slot)
		return 0;

	return 1;
}

static void vme_sysfs_scan_slot(struct vme_access *a,
				struct vme_filter *f,
				unsigned int slot)
{
	struct vme_dev *d;
	char dirname[1024];
	DIR *dir;
	struct dirent *entry;
	int n;

	n = snprintf(dirname, sizeof(dirname),
		     "/sys/bus/vme/devices/slot.%02u",
		     slot);
	if (n < 0 || n >= (int) sizeof(dirname))
		vme_generic_error("Directory name too long");
	dir = opendir(dirname);
	if (!dir)
		vme_generic_error("Cannot open %s", dirname);
	while ((entry = readdir(dir))) {
		/* skip sysfs attributes */
		if ((entry->d_name[0] != '.') &&
		    (strcmp(entry->d_name, "power") != 0) &&
		    (strcmp(entry->d_name, "subsystem") != 0) &&
		    (strcmp(entry->d_name, "rescan") != 0) &&
		    (strcmp(entry->d_name, "register") != 0) &&
		    (strcmp(entry->d_name, "uevent") != 0) &&
		    (strcmp(entry->d_name, "slot") != 0))
			break;
	}
	if (!entry)
		goto out;

	d = alloc_dev();
	if (strnlen(entry->d_name, VMEDEV_NAME_LEN + 1) > VMEDEV_NAME_LEN)
		vme_generic_error("Device name too long %s",
				  entry->d_name);
	strncpy(d->name, entry->d_name, VMEDEV_NAME_LEN);
	d->access = a;
	d->slot = slot;
	sysfs_scan_device(d);
	if (apply_filter(d, f))
		link_dev(a, d);
	else
		free_dev(d);
out:
	closedir(dir);
}

/**
 * It scans for all VME devices
 * @a access descriptor
 * @f device filter
 */
void vme_sysfs_scan_bus(struct vme_access *a, struct vme_filter *f)
{
	char dirname[1024];
	DIR *dir;
	struct dirent *entry;
	int n;

	n = snprintf(dirname, sizeof(dirname), "/sys/bus/vme/devices");
	if (n < 0 || n >= (int) sizeof(dirname))
		vme_generic_error("Directory name too long");
	dir = opendir(dirname);
	if (!dir)
		vme_generic_error("Cannot open %s", dirname);
	while ((entry = readdir(dir))) {
		unsigned int slot;

		if (strncmp("slot.", entry->d_name, 5) != 0)
			continue;

		sscanf(entry->d_name, "slot.%u", &slot);
		vme_sysfs_scan_slot(a, f, slot);
	}
	closedir(dir);
}

void vme_device_enable(struct vme_dev *d, unsigned int enable)
{
	if (d->spec < VME_SPEC_VME64) {
		fprintf(stderr,
			"Board on slot %02d does not support CSR enable\n",
			d->slot);
		return;
	}

	vme_sysfs_set_value(d, "enable", !!enable, NULL);
}

static const char *vme_error[] = {
	"Invalid slot number or range",
};

const char *vme_strerror(int err)
{
	if (err < __VME_ERRORS_MIN || err > __VME_ERRORS_MAX)
		return strerror(err);
	return vme_error[err - __VME_ERRORS_MIN];
}
