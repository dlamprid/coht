// SPDX-License-Identifier: GPL-2.0-or-later
/*
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/version.h>
#include <linux/irq.h>
#include <linux/irqdomain.h>

#include "vme_irq.h"
#include "vme_bridge.h"


struct vme_irq {
	int	(*handler)(void *arg);
	void	*arg;
};

/* Mutex to prevent concurrent access to the IRQ table */
static DEFINE_MUTEX(vme_irq_table_lock);
static struct vme_irq vme_irq_table[VME_NUM_VECTORS];

static irqreturn_t vme_irq_handler(int irq, void *data)
{
	struct vme_irq *virq = data;

	return virq->handler(virq->arg);
}

/**
 * handle_vme_interrupt() - VME IRQ handler
 * @irq_mask: Mask of the raised IRQs
 *
 *  Get the IRQ vector through an IACK cycle and call the handler for
 * that vector if installed.
 */
void vme_handle_interrupt(int irq_mask, struct vme_bridge_device *vbridge)
{
	int i, vec, cascade_irq;
	void *chip = vbridge->chip_mgr.private;

	for (i = VME_IRQ_LEVEL_MAX; i >= VME_IRQ_LEVEL_MIN; i--) {
		if (irq_mask & (1 << i)) {
			/* Generate a IACK cycle and get the vector */
			vec = vbridge->irq_mgr.ops->iack(chip, i);
			if (unlikely(vbridge->irq_mgr.domain) &&
			    test_bit(vec, vbridge->irq_mgr.irq_mask)) {
				/*
				 * We have a valid domain and the IRQ
				 * is enabled
				 */
				cascade_irq = irq_find_mapping(
						vbridge->irq_mgr.domain, vec);
				handle_nested_irq(cascade_irq);
			}
		}
	}
}

static int
vme_berr_match(struct vme_bus_error *error, struct vme_berr_handler *handler)
{
	struct vme_bus_error *err = &handler->error;

	return error->am == err->am && error->address >= err->address &&
		error->address < err->address + handler->size;
}

void vme_bus_error_dispatch(struct vme_bridge_device *vbridge,
			     struct vme_bus_error *error)
{
	struct vme_berr_handler *handler;
	int handled;

	memcpy(&vbridge->verr.desc.error, error, sizeof(*error));
	vbridge->verr.desc.valid = 1; /* valid the error */
	/* Display raw error information */
	if (vbridge->mod_param.vme_report_bus_errors) {
		pr_err(PFX "VME bus error at address: 0x%llx - AM: 0x%x\n",
			error->address, error->am);
	}
	handled = 0;
	list_for_each_entry(handler, &vbridge->verr.h_list, h_list) {
		if (vme_berr_match(&vbridge->verr.desc.error, handler)) {
			handler->func(&vbridge->verr.desc.error);
			handled = 1;
		}
	}
	if (!handled) {
		pr_err(PFX "Bus error not handled! Address 0x%llx am 0x%02x\n",
		       vbridge->verr.desc.error.address,
		       vbridge->verr.desc.error.am);
	}
}

/**
 * vme_request_irq() - Install handler for a given VME IRQ vector
 * @vec: VME IRQ vector
 * @handler: Interrupt handler
 * @arg: Interrupt handler argument
 * @name: Interrupt name (only used for stats in Procfs)
 *
 */
int vme_request_irq(unsigned int vec, int (*handler)(void *),
		    void *arg, const char *name)
{
	struct vme_irq *virq;
	int rc = 0;

	/* Check the vector is within the bound */
	if (vec >= VME_NUM_VECTORS)
		return -EINVAL;

	rc = mutex_lock_interruptible(&vme_irq_table_lock);
	if (rc)
		return rc;

	virq = &vme_irq_table[vec];

	if (virq->handler) {
		mutex_unlock(&vme_irq_table_lock);
		return -EBUSY;
	}
	virq->handler = handler;
	virq->arg = arg;

	mutex_unlock(&vme_irq_table_lock);

	return request_threaded_irq(irq_find_mapping(
				    vbridge_gbl->irq_mgr.domain, vec),
				    NULL, vme_irq_handler, 0, name, virq);
}
EXPORT_SYMBOL_GPL(vme_request_irq);

/**
 * vme_free_irq() - Uninstall handler for a given VME IRQ vector
 * @vec: VME IRQ vector
 *
 */
int vme_free_irq(unsigned int vec)
{
	struct vme_irq *virq;
	int rc = 0;

	/* Check the vector is within the bound */
	if (vec >= VME_NUM_VECTORS)
		return -EINVAL;

	rc = mutex_lock_interruptible(&vme_irq_table_lock);
	if (rc)
		return rc;

	virq = &vme_irq_table[vec];

	if (!virq->handler) {
		mutex_unlock(&vme_irq_table_lock);
		return -EINVAL;
	}

	free_irq(irq_find_mapping(vbridge_gbl->irq_mgr.domain, vec),
		 virq);

	virq->handler = NULL;
	virq->arg = NULL;
	mutex_unlock(&vme_irq_table_lock);

	return 0;
}
EXPORT_SYMBOL_GPL(vme_free_irq);

/**
 * vme_generate_interrupt() - Generate an interrupt on the VME bus
 * @level: IRQ level (1-7)
 * @vector: IRQ vector (0-255)
 * @msecs: Timeout for IACK in milliseconds
 *
 *  This function generates an interrupt on the VME bus and waits for IACK
 *  for msecs milliseconds.
 *
 *  Returns 0 on success or -ETIME if the timeout expired.
 *
 */
int vme_generate_interrupt(int level, int vector, signed long msecs)
{
	if ((level < 1) || (level > 7)) {
		pr_err(PFX " " VBRIDGE_DBG_GEN_IRQ_NAME "generate-interrupt: invalid level number %d\n",
		       level);
		return -EINVAL;
	}

	if ((vector < 0) || (vector > 255)) {
		pr_err(PFX " " VBRIDGE_DBG_GEN_IRQ_NAME "generate-interrupt: invalid vector number %d\n",
		       vector);
		return -EINVAL;
	}

	return vbridge_gbl->irq_mgr.ops->generate_interrupt(
					vbridge_gbl, level, vector, msecs);

	return 0;
}
EXPORT_SYMBOL_GPL(vme_generate_interrupt);

/**
 * It sets the IRQ vector and level
 * @dev the device instance for a VME device (the one got from probe())
 * @vect the VME IRQ vector
 * @lev the VME IRQ level [1, 7]
 *
 * If you are using the old API, call this function as soon as you know
 * those two parameters (typically on vme->probe()).
 *
 * Return: 0 on success, otherwise -EINVAL if one of the parameters is
 * not valid
 */
int vme_device_irq_vector_and_level_set(struct device *dev,
					unsigned int vect,
					unsigned int lev)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	if (lev == 0 || VME_IRQ_LEVEL_MAX <= lev)
		return -EINVAL;

	if (vect != VME_IRQ_VECTOR_AUTO &&
	    vect != VME_IRQ_VECTOR_OLDAPI &&
	    vect > VME_NUM_VECTORS)
		return -EINVAL;

	vdev->irq_level = lev;
	vdev->irq_vector = vect;

	return 0;
}
EXPORT_SYMBOL_GPL(vme_device_irq_vector_and_level_set);

/**
 * It unmask (a.k.a. enable) an interrupt line
 */
static void vme_irq_unmask(struct irq_data *data)
{
	struct vme_bridge_device *vbridge = data->domain->host_data;

	set_bit(data->hwirq, vbridge->irq_mgr.irq_mask);
}

/**
 * It mask (a.k.a. disable) an interrupt line
 */
static void vme_irq_mask(struct irq_data *data)
{
	struct vme_bridge_device *vbridge = data->domain->host_data;

	clear_bit(data->hwirq, vbridge->irq_mgr.irq_mask);
}

/**
 * It acknowledge an interrupt
 */
static void vme_irq_ack(struct irq_data *data)
{
}

/*
 * IRQ Chip for Tundra TSI148 devices.
 */
static struct irq_chip vme_chip = {
	.irq_unmask		= vme_irq_unmask,
	.irq_mask		= vme_irq_mask,
	.irq_ack		= vme_irq_ack,
};


static int vme_irq_domain_map(struct irq_domain *h,
				 unsigned int virtirq,
				 irq_hw_number_t hwirq)
{
	struct vme_bridge_device *vbridge = h->host_data;

	/* Set irq chip name */
	vme_chip.name = vbridge->chip_mgr.ops->get_chip_name();
	irq_set_chip(virtirq, &vme_chip);
	__irq_set_handler(virtirq, handle_edge_irq, 0, NULL);
	irq_set_chip_data(virtirq, vbridge);
	/* all handlers are directly nested to the VME bridge one */
	irq_set_nested_thread(virtirq, 1);

	/*
	 * It MUST be no-thread (nested) because the VME handler need to have
	 * control over the handler execution in order to properly use
	 * DMA-pause-restart. Here the VME bridge handler is threaded, so in
	 * cascade will run within this thread
	 */

	return 0;
}

static const struct irq_domain_ops vme_irq_domain_ops = {
	.map = vme_irq_domain_map,
};


/**
 * Configure the VME bridge to be linked to the standard PCI irq controller
 */
static int vme_irq_domain_create(struct vme_bridge_device *vbridge)
{
	struct vme_bridge_irq_mgr *irq_mgr = &vbridge->irq_mgr;
	unsigned int irq;
	int i, err;

	irq_mgr->domain = irq_domain_add_linear(NULL, VME_NUM_VECTORS,
						&vme_irq_domain_ops, vbridge);
	if (!irq_mgr->domain)
		return -ENOMEM;

	/* Maps HW interrupt number with Linux IRQ number*/
	for (i = 0; i < VME_NUM_VECTORS; ++i) {
		irq = irq_create_mapping(irq_mgr->domain, i);
		if (!irq) {
			err = -EPERM;
			goto out;
		}
	}
	/* Disable all interrupts */
	bitmap_clear(irq_mgr->irq_mask, 0, VME_NUM_VECTORS);

	/* Before requesting an irq check if bridge device  supports MSI */
	if (pci_find_capability(vbridge->pdev, PCI_CAP_ID_MSI)) {
		err = pci_enable_msi(vbridge->pdev);
		if (err) {
			pr_err(PFX "Could not enable msi interrupt:%d\n",
			       err);
			goto out;
		}
		irq_mgr->msi_enabled = 1; /* used to disable msi */
		pr_debug(PFX "Enabling MSI capability.\n");
	}
	err = request_irq(vbridge->pdev->irq,
			  irq_mgr->ops->interrupt_handler, 0,
			  vbridge->pdev->dev.driver->name, vbridge);
	if (err)
		goto out;

	return 0;

out:
	irq_domain_remove(irq_mgr->domain);
	irq_mgr->domain = NULL;
	return err;
}

static void vme_irq_domain_destroy(struct vme_bridge_device *vbridge)
{
	struct vme_bridge_irq_mgr *irq_mgr = &vbridge->irq_mgr;
	int i;

	for (i = 0; i < VME_NUM_VECTORS; ++i)
		irq_dispose_mapping(irq_find_mapping(irq_mgr->domain, i));

	/* Disable all interrupts */
	bitmap_clear(irq_mgr->irq_mask, 0, VME_NUM_VECTORS);
	irq_domain_remove(irq_mgr->domain);
	irq_mgr->domain = NULL;

	free_irq(vbridge->pdev->irq, vbridge);
	if (irq_mgr->msi_enabled)
		pci_disable_msi(vbridge->pdev);
}

/**
 * vme_interrupts_init() - Initialize the bridge interrupts
 *
 */
int vme_interrupts_init(struct vme_bridge_device *vbridge)
{
	struct vme_bridge_irq_mgr *irq_mgr = &vbridge->irq_mgr;
	int err;

	ida_init(&irq_mgr->ida_irq);

	err = vme_irq_domain_create(vbridge);
	if (err) {
		pr_err(PFX "Failed to create irq domanin\n");
		return err;
	}

	err = irq_mgr->ops->enable_interrupts(vbridge);
	if (err) {
		pr_err(PFX "Failed to enable interrupts");
		vme_irq_domain_destroy(vbridge);
		return -err;
	}

	return 0;
}

/**
 * vme_interrupts_exit() - Initialize the bridge interrupts
 *
 */
void vme_interrupts_exit(struct vme_bridge_device *vbridge)
{
	/* Disable all interrupts */
	vbridge->irq_mgr.ops->disable_interrupts(vbridge);

	/* shutdown the bridge */
	vbridge->chip_mgr.ops->init_bridge(vbridge);

	vme_irq_domain_destroy(vbridge);
	/* Disable and free interrupts */
	disable_irq(vbridge->pdev->irq);
}
