// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 * This DMA engine must be compatible with kernel 3.2 and 3.6 so it does not
 * use latest DMA engine API.
 * Another point to take in account is that I'm developing this within
 * the vmebus module, so I do not have access to some linux kernel internals.
 * The internal kernel functions that I would like to use are copied here
 * within a pre-processor condition
 *
 * We have to keep the compatibility with the ad-hoc DMA interface
 * provided by the vmebus driver throught the vme_do_dma() function call.
 * By keeping the compatibily we should be able to run together drivers that
 * use different interfaces.
 *
 * This DMA engine implementation works more or less as any other
 * implementation available within the kernel tree.
 * - alloc/free channel gives exclusive access to the caller
 * - prepare slave SG builds a DMA transfer descriptor
 * - submit enqueue a transfer descriptor to a list of pending transfers
 * - issue pending start the first pending DMA transfer
 * - when a DMA transfer complete run the caller callback and start the
 *   next pending DMA transfer.
 */

#include <linux/dmaengine.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/semaphore.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "vme_dmaengine.h"
#include "vme_compat.h"

/**
 * vme_dma_release() - Release resources used for a DMA transfer
 * @chan: DMA channel descriptor
 *
 *  Only chained transfer are allocating memory. For direct transfer, there
 * is nothig to free.
 */
static void vme_dma_release(struct vme_dma_chan *vdchan)
{
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	dma_mgr->ops->dma_free(vdchan);
}

/* Kernel functions not exported */
#if 1

/**
 * dma_cookie_complete - complete a descriptor
 * @tx: descriptor to complete
 *
 * Mark this descriptor complete by updating the channels completed
 * cookie marker.  Zero the descriptors cookie to prevent accidental
 * repeated completions.
 *
 * Note: caller is expected to hold a lock to prevent concurrency.
 */
static inline void dma_cookie_complete(struct dma_async_tx_descriptor *tx)
{
	BUG_ON(tx->cookie < DMA_MIN_COOKIE);
	tx->chan->completed_cookie = tx->cookie;
	tx->cookie = 0;
}

/**
 * dma_cookie_init - initialize the cookies for a DMA channel
 * @chan: dma channel to initialize
 */
static inline void dma_cookie_init(struct dma_chan *chan)
{
	chan->cookie = DMA_MIN_COOKIE;
	chan->completed_cookie = DMA_MIN_COOKIE;
}

/**
 * dma_cookie_assign - assign a DMA engine cookie to the descriptor
 * @tx: descriptor needing cookie
 *
 * Assign a unique non-zero per-channel cookie to the descriptor.
 * Note: caller is expected to hold a lock to prevent concurrency.
 */
static inline dma_cookie_t dma_cookie_assign(struct dma_async_tx_descriptor *tx)
{
	struct dma_chan *chan = tx->chan;
	dma_cookie_t cookie;

	cookie = chan->cookie + 1;
	if (cookie < DMA_MIN_COOKIE)
		cookie = DMA_MIN_COOKIE;
	tx->cookie = chan->cookie = cookie;

	return cookie;
}

/**
 * dma_cookie_status - report cookie status
 * @chan: dma channel
 * @cookie: cookie we are interested in
 * @state: dma_tx_state structure to return last/used cookies
 *
 * Report the status of the cookie, filling in the state structure if
 * non-NULL.  No locking is required.
 */
static inline enum dma_status dma_cookie_status(struct dma_chan *dchan,
	dma_cookie_t cookie, struct dma_tx_state *state)
{
	dma_cookie_t used, complete;

	if (cookie < 0)
		return DMA_ERROR;

	used = dchan->cookie;
	complete = dchan->completed_cookie;
	barrier();
	if (state) {
		state->last = complete;
		state->used = used;
		state->residue = 0;
	}
	return dma_async_is_complete(cookie, complete, used);
}
#endif


enum vme_dmaengine_mode {
	VME_DMAENGINE_MODE_LIST = 0,
	VME_DMAENGINE_MODE_DIRECT,
};

static void vme_dma_terminate_all(struct dma_chan *dchan)
{
	struct vme_dma_chan *vdchan = to_vme_dma_chan(dchan);
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	struct vme_dma_tx_desc *desc, *_desc;
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&vdchan->lock, flags);
	ret = dma_mgr->ops->dma_check_state(vdchan, VME_DMA_IS_BUSY);
	if (ret)
		dma_mgr->ops->dma_set_ctrl(vdchan, VME_DMA_ABORT);
	/* leave some time for the bridge to clear the DMA channel */
	udelay(10);

	list_for_each_entry_safe(desc, _desc, &vdchan->pending_list, list) {
		/*
		 * Make the transfer invalid, so that clients can detect
		 * that the transfer has been terminated
		 */
		desc->tx.cookie = -ENODEV;
		list_del(&desc->list);
	}

	if (vdchan->tx_desc_current == NULL)
		goto out;

	vme_dma_release(vdchan);
	/*
	 * Make the transfer invalid, so that clients can detect
	 * that the transfer has been terminated
	 */
	vdchan->tx_desc_current->tx.cookie = -ENODEV;
	vdchan->tx_desc_current = NULL;
out:
	spin_unlock_irqrestore(&vdchan->lock, flags);
}


/**
 * Allocate resources for the given channel. This function must keep the
 * compatibility with the vme_do_dma() interface.
 */
static int vme_dmaengine_alloc_chan_resources(struct dma_chan *dchan)
{
	struct vme_dma_chan *vdchan = to_vme_dma_chan(dchan);

	if (WARN_ON(!list_empty(&vdchan->pending_list)))
		return -EBUSY;
	if (WARN_ON(vdchan->tx_desc_current))
		return -EBUSY;

	vdchan->tx_desc_current = NULL;
	dma_cookie_init(dchan);

	return 0;
}


/**
 * Add a descriptor to the pending DMA transfer queue.
 * This will not trigger any DMA transfer: here we just collect DMA
 * transfer descriptions.
 */
static dma_cookie_t vme_tx_submit(struct dma_async_tx_descriptor *tx)
{
	struct vme_dma_tx_desc *desc =
		container_of(tx, struct vme_dma_tx_desc, tx);
	struct vme_dma_chan *vdchan = to_vme_dma_chan(tx->chan);
	dma_cookie_t cookie;
	unsigned long flags;

	spin_lock_irqsave(&vdchan->lock, flags);
	cookie = dma_cookie_assign(tx);
	list_add_tail(&desc->list, &vdchan->pending_list);
	spin_unlock_irqrestore(&vdchan->lock, flags);

	return cookie;
}


/**
 * Release resources for the given channel. This function must keep the
 * compatibility with the vme_do_dma() interface.
 */
static void vme_dmaengine_free_chan_resources(struct dma_chan *dchan)
{
	vme_dma_terminate_all(dchan);
}

static struct dma_async_tx_descriptor *vme_dmaengine_prep_slave_sg(
	struct dma_chan *dchan, struct scatterlist *sgl, unsigned int sg_len,
	enum dma_transfer_direction direction, unsigned long flags,
	void *context)
{
	struct vme_dma_tx_desc *desc;
	struct vme_dma *vme_desc = context;
	struct vme_dma_attr *vme_attr;
	int vme_align;

	if (unlikely(!vme_desc)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: missing VME context\n");
		return NULL;
	}
	if (unlikely(direction != DMA_DEV_TO_MEM
		     && direction != DMA_MEM_TO_DEV)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: Support only DEV <-> MEM transfers\n");
		return NULL;
	}

	if (unlikely(!sgl || !sg_len)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: empty DMA scatterlist\n");
		return NULL;
	}

	/* Check we're within a 32-bit address space */
	if (BITS_PER_LONG < 64 &&
	    (vme_desc->src.addru || vme_desc->dst.addru)) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: 64bit address not supported\n");
		return NULL;
	}

	vme_attr = (direction == DMA_DEV_TO_MEM) ?
		&vme_desc->src : &vme_desc->dst;

	switch(vme_attr->data_width) {
	case VME_D32:
		vme_align = 4;
		break;
	case VME_D16:
		vme_align = 2;
		break;
	default:
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: DMA data width not supported %d\n",
			vme_attr->data_width);
		return NULL;
	}
	if (unlikely(vme_attr->addrl & (vme_align - 1))) {
		dev_err(&dchan->dev->device,
			"Can't prepare DMA transfer: wrong VME aligment/size with respect to data width %d: vme_addr:0x%x size:0x%x\n",
			vme_attr->data_width, vme_attr->addrl,
			vme_desc->length);
		return NULL;
	}
	desc = kzalloc(sizeof(struct vme_dma_tx_desc), GFP_NOWAIT);
	if (!desc)
		return NULL;

	dma_async_tx_descriptor_init(&desc->tx, dchan);
	desc->tx.tx_submit = vme_tx_submit;
	desc->direction = direction;
	desc->sgl = sgl;
	desc->sg_len = sg_len;
	memcpy(&desc->desc, vme_desc, sizeof(desc->desc));

	return &desc->tx;
}

/**
 * It starts a tasklet which will start the DMA transfers. We must use a tasklet
 * because this function can be called in interrupt context.
 */
static void vme_dmaengine_issue_pending(struct dma_chan *dchan)
{
	struct vme_dma_chan *tdchan = to_vme_dma_chan(dchan);

	if (list_empty(&tdchan->pending_list))
		return; /* Nothing pending to send on this channel */

	tasklet_schedule(&tdchan->dma_start_task);
}


/**
 * It allows drivers to send commands to the engine.
 * For the time being we only support the TERMINATE command
 */
static int vme_dmaengine_device_control(struct dma_chan *dchan,
					enum dma_ctrl_cmd cmd,
					unsigned long arg)
{
	struct vme_dma_chan *vdchan = to_vme_dma_chan(dchan);
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	int ret = 0;

	switch (cmd) {
	case DMA_TERMINATE_ALL:
		vme_dma_terminate_all(dchan);
		break;
	case DMA_PAUSE:
		/*
		 * Pause works only with linked-list mode. This engine will
		 * perform *only* linked-list transfers. So, pause always work
		 */
		ret = dma_mgr->ops->dma_set_ctrl(vdchan, VME_DMA_PAUSE);
		udelay(10);
		break;
	case DMA_RESUME:
		ret = dma_mgr->ops->dma_set_ctrl(vdchan, VME_DMA_START);
		break;
	default:
		ret = -ENXIO;
		break;
	}

	return ret;
}


/**
 * The DMA transfer is complete, remove it from the list and try to run
 * the next pending transfer
 */
static void vme_dmaengine_tx_complete(struct vme_dma_chan *vdchan)
{
	struct vme_dma_tx_desc *tx_desc;
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
	struct dmaengine_result dma_res;
	dma_async_tx_callback_result callback = NULL;
#else
	dma_async_tx_callback callback = NULL;
#endif
	void *callback_param = NULL;
	unsigned long flags;

	spin_lock_irqsave(&vdchan->lock, flags);
	tx_desc = vdchan->tx_desc_current;
	if (unlikely(!tx_desc)) {
		spin_unlock_irqrestore(&vdchan->lock, flags);
		dev_warn(&vdchan->dchan.dev->device,
			 "Spurious DMA interrupt\n");
		return;
	}

	dma_cookie_complete(&tx_desc->tx);
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
	dma_res.result = tx_desc->tx_res;
	callback = tx_desc->tx.callback_result;
#else
	callback = tx_desc->tx.callback;
#endif
	callback_param = tx_desc->tx.callback_param;

	kfree(tx_desc);

	vme_dma_release(vdchan);
	vdchan->tx_desc_current = NULL;
	spin_unlock_irqrestore(&vdchan->lock, flags);

	/* Inform the driver that the DMA is over */
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
	callback(callback_param, &dma_res);
#else
	callback(callback_param);
#endif

	/* try to send the next one */
	if (!list_empty(&vdchan->pending_list))
		tasklet_schedule(&vdchan->dma_start_task);
}





/**
 * It starts the first pending DMA transaction
 */
static void vme_dmaengine_start_task(unsigned long arg)
{
	struct vme_dma_chan *vdchan = (struct vme_dma_chan *)arg;
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	struct vme_dma_tx_desc *tx_desc;
	unsigned long flags;
	int ret;

	spin_lock_irqsave(&vdchan->lock, flags);
	/*
	 * How much time does it take for the BUSY bit to get high? Here
	 * I assume that as soon as I do START, BUSY become 1.  If this is not
	 * the case then we have a critical section as large as the time
	 * necessary to set BUSY.  The TSI148 manual is not clear about this.
	 */
	ret = dma_mgr->ops->dma_check_state(vdchan, VME_DMA_IS_BUSY);
	if (unlikely(ret) || unlikely(vdchan->tx_desc_current)) {
		dev_warn(&vdchan->dchan.dev->device,
			"Trying to run a DMA transfer while another one is still in progress\n");
		goto err_busy;
	}

	if (list_empty(&vdchan->pending_list))
		goto out_empty;

	tx_desc = list_first_entry(&vdchan->pending_list,
				   struct vme_dma_tx_desc, list);
	list_del(&tx_desc->list);

	vdchan->tx_desc_current = tx_desc;
	ret = dma_mgr->ops->dma_setup(vdchan);
	if (ret) {
		struct dmaengine_result dma_res;

		// abort and free resources
		tx_desc->tx.cookie = -EINVAL;

		vme_dma_release(vdchan);
		vdchan->tx_desc_current = NULL;

		dma_res.result = DMA_TRANS_ABORTED;
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
		tx_desc->tx.callback_result(tx_desc->tx.callback_param,
					    &dma_res);
#else
		tx_desc->tx.callback(tx_desc->tx.callback_param);
#endif
		kfree(tx_desc);
		goto err_setup;
	}
	ret = dma_mgr->ops->dma_set_ctrl(vdchan, VME_DMA_START);
	if (ret)
		dev_warn(&vdchan->dchan.dev->device,
			"Trying to start DMA transfer failed with:%d.\n", ret);
err_setup:
out_empty:
err_busy:
	spin_unlock_irqrestore(&vdchan->lock, flags);
}


/**
 * It handles DMA completion.
 */
void vme_dmaengine_irq_handler(int channel_mask,
			       struct vme_bridge_device *vbridge)
{
	int i;

	for (i = 0; i < vbridge->dma_mgr.nr_vdchan; i++) {
		if (!(channel_mask & (1 << i)))
			continue; /* no DMA done on this channel */

		vme_dmaengine_tx_complete(&vbridge->dma_mgr.vdchan[i]);
	}
}


/**
 * It initializes the DMA parameters for the DMA data transfers
 * for a given device
 * @vdev VME device
 */
static void __vme_dma_param_init(struct vme_bridge_dma_mgr *dma_mgr)
{
	clear_bit(VME_DEV_FLAG_FORCE_DMA_CFG, dma_mgr->flags);
	dma_mgr->dma_param.vme_block_size = VME_DMA_BSIZE_4096;
	dma_mgr->dma_param.pci_block_size = VME_DMA_BSIZE_4096;
	dma_mgr->dma_param.vme_backoff_time = VME_DMA_BACKOFF_0;
	dma_mgr->dma_param.pci_backoff_time = VME_DMA_BACKOFF_0;
}


/**
 * It configures the DMA engine
 */
int vme_dmaengine_init(struct vme_bridge_device *vbridge)
{
	struct vme_bridge_dma_mgr *dma_mgr = &vbridge->dma_mgr;
	int i, err = 0;

	err = dma_set_mask(&vbridge->pdev->dev, DMA_BIT_MASK(32));
	if (err) {
		dev_err(&vbridge->pdev->dev,
			"Bridge does not support 0x%llx PCI DMA addressing\n",
			DMA_BIT_MASK(32));
		return err;
	}

	dma_mgr->dma.dev = &vbridge->pdev->dev;

	INIT_LIST_HEAD(&dma_mgr->dma.channels);

	/* Capabilities */
	dma_cap_zero(dma_mgr->dma.cap_mask);
	dma_cap_set(DMA_SLAVE, dma_mgr->dma.cap_mask);
	dma_cap_set(DMA_PRIVATE, dma_mgr->dma.cap_mask);

	/* Operations */
	dma_mgr->dma.device_alloc_chan_resources =
		vme_dmaengine_alloc_chan_resources;
	dma_mgr->dma.device_free_chan_resources =
		vme_dmaengine_free_chan_resources;
	dma_mgr->dma.device_prep_slave_sg =
		vme_dmaengine_prep_slave_sg;
	dma_mgr->dma.device_control =
		vme_dmaengine_device_control;
	dma_mgr->dma.device_tx_status =
		dma_cookie_status;
	dma_mgr->dma.device_issue_pending =
		vme_dmaengine_issue_pending;

	// dma engine is using a dma pool
	if (dma_mgr->ops->dma_pool_create != NULL) {
		dma_mgr->dma_pool = dma_mgr->ops->dma_pool_create(vbridge->pdev);
		if (!dma_mgr->dma_pool) {
			dev_err(&vbridge->pdev->dev,
				"Can't allocate DMA pool\n");
			return -ENOMEM;
		}
	}

	dma_mgr->nr_vdchan = dma_mgr->ops->dma_get_nr_chan();
	dma_mgr->vdchan = kcalloc(dma_mgr->nr_vdchan,
				  sizeof(struct vme_dma_chan), GFP_KERNEL);
	if (!dma_mgr->vdchan) {
		err = -ENOMEM;
		goto err_vdchan;
	}
	for (i = 0; i < dma_mgr->nr_vdchan; i++) {
		struct vme_dma_chan *vdchan = &dma_mgr->vdchan[i];

		vdchan->chan_idx = i;
		vdchan->dchan.device = &dma_mgr->dma;
		INIT_LIST_HEAD(&vdchan->hw_desc_list);
		INIT_LIST_HEAD(&vdchan->pending_list);
		spin_lock_init(&vdchan->lock);
		init_waitqueue_head(&vdchan->wait);

		list_add_tail(&vdchan->dchan.device_node,
			      &dma_mgr->dma.channels);

		tasklet_init(&vdchan->dma_start_task,
			     vme_dmaengine_start_task,
			     (unsigned long)vdchan);
		err = dma_mgr->ops->dma_chan_init(vbridge, vdchan);
		if (err) {
			dev_err(&vbridge->pdev->dev,
				"DMA Channel specific init failed\n");
			goto err_chan_init;
		}
	}

	/* Register the engine */
	err = dma_async_device_register(&dma_mgr->dma);
	if (err)
		goto err_register;

	dma_mgr->dma_is_registered = 1;

	bitmap_zero(dma_mgr->flags, VME_BRIDGE_FLAGS_MAX);
	__vme_dma_param_init(dma_mgr);

	return 0;

err_register:
err_chan_init:
	kfree(dma_mgr->vdchan);
	dma_mgr->nr_vdchan = 0;
err_vdchan:
	pci_pool_destroy(dma_mgr->dma_pool);

	return err;
}

void vme_dmaengine_exit(struct vme_bridge_device *vbridge)
{
	int i;

	if (vbridge->dma_mgr.dma_is_registered)
		dma_async_device_unregister(&vbridge->dma_mgr.dma);
	if (vbridge->dma_mgr.dma_pool != NULL)
		pci_pool_destroy(vbridge->dma_mgr.dma_pool);
	for (i = 0; i < vbridge->dma_mgr.nr_vdchan; i++) {
		if (vbridge->dma_mgr.ops->dma_chan_release != NULL)
			vbridge->dma_mgr.ops->dma_chan_release(vbridge,
						&vbridge->dma_mgr.vdchan[i]);
	}
}
