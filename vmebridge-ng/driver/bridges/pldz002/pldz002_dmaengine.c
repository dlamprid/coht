// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * VME PLDZ002 dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#include <linux/dmaengine.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/scatterlist.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "pldz002.h"

static int pldz002_setup_dma_attr(struct vme_dma_tx_desc *tx_desc,
				  struct pldz002_dma_attr *dma_attr)
{
	struct vme_dma *desc = &tx_desc->desc;
	struct vme_dma_attr *vme_attr;
	int am, is_blt, align_vme;

	vme_attr = (desc->dir == VME_DMA_TO_DEVICE) ? &desc->dst : &desc->src;
	align_vme = 4;
	is_blt = 0;
	am = -1; // Used to detect errors
	switch (vme_attr->data_width) {
	case VME_D64:
		align_vme = 8;
		switch (vme_attr->am) {
		case VME_A32_USER_MBLT:
		case VME_A32_USER_BLT:
		case VME_A32_SUP_BLT:
			am = PLDZ002_DMA_AM_A32D64;
			is_blt = 1;
			break;
		case VME_A24_USER_MBLT:
			am = PLDZ002_DMA_AM_A24D64;
			is_blt = 1;
			break;
		default:    /* Mute warnings */
			break;
		}
		break;

	case VME_D32:
		switch (vme_attr->am) {
		case VME_A32_LCK:
		case VME_A32_USER_DATA_SCT:
		case VME_A32_USER_PRG_SCT:
		case VME_A32_SUP_DATA_SCT:
		case VME_A32_SUP_PRG_SCT:
			am = PLDZ002_DMA_AM_A32D32;
			break;

		case VME_A32_USER_MBLT:
			am = PLDZ002_DMA_AM_A32D64;
			is_blt = 1;
			break;

		case VME_A32_USER_BLT:
		case VME_A32_SUP_BLT:
			am = PLDZ002_DMA_AM_A32D32;
			is_blt = 1;
			break;

		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			am = PLDZ002_DMA_AM_A24D32;
			break;

		case VME_A24_USER_MBLT:
		case VME_A24_SUP_MBLT:
			am = PLDZ002_DMA_AM_A24D64;
			is_blt = 1;
			break;

		case VME_A24_USER_BLT:
		case VME_A24_SUP_BLT:
			am = PLDZ002_DMA_AM_A24D32;
			is_blt = 1;
			break;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			am = PLDZ002_DMA_AM_A16D32;
			break;
		default:    /* Mute warnings */
			break;
		}
		break;

	case VME_D16:
		switch (vme_attr->am) {
		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			am = PLDZ002_DMA_AM_A24D16;
			break;

		case VME_A24_USER_BLT:
		case VME_A24_SUP_BLT:
			am = PLDZ002_DMA_AM_A24D16;
			is_blt = 1;
			break;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			am = PLDZ002_DMA_AM_A16D16;
			break;
		default:    /* Mute warnings */
			break;
		}
		break;

	/* Not available */
	case VME_D8:
		switch (vme_attr->am) {
		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			am = PLDZ002_DMA_AM_A24D16;
			break;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			am = PLDZ002_DMA_AM_A16D16;
		default:    /* Mute warnings */
			break;
		}
		break;

	default:    /* Mute warnings */
		break;

	}

	if (am == -1) { // not supported
		pr_err(PFX "PLDZ002 doesn't support DMA for address modifier 0x%x and data width %d vme addr:0x%x\n",
		       vme_attr->am, vme_attr->data_width, vme_attr->addrl);
		return -EINVAL;
	}
	// Found valid dma attributes
	dma_attr->am = am;
	dma_attr->is_blt = is_blt;
	dma_attr->align_vme = align_vme;
	return 0;
}

/*
 * Configure DMA block descriptors in the SRAM to schedule the next DMA batch
 * Please note that, in case of DMA TX requiring more than one batch, from
 * the second batch this function is called from interrupt context.
 * This function takes around 8us so it's ok to call it from interrupt handler
 * instead of differring it into a tasklet
 */
static void pldz002_write_dma_bd(struct vme_dma_chan *vdchan)
{
	struct pldz002_dma_chan *chip_dchan = vdchan->private;
	struct scatterlist *sgl = vdchan->tx_desc_current->sgl;
	int sg_len = vdchan->tx_desc_current->sg_len;
	int i, len, novmeinc;
	uint32_t flg;
	void __iomem *bd_vaddr;
	uint32_t vme_addr;
	struct scatterlist *sgl_elem;

	len = 0;
	novmeinc = vdchan->tx_desc_current->desc.novmeinc;
	flg = 0;
	if (vdchan->tx_desc_current->direction == DMA_MEM_TO_DEV) {
		flg = PLDZ002_DMABD_SRC(PLDZ002_DMABD_DIR_PCI) |
		      PLDZ002_DMABD_DST(PLDZ002_DMABD_DIR_VME) |
		      (novmeinc ? PLDZ002_DMABD_NOINC_DST : 0);
		vme_addr = vdchan->tx_desc_current->desc.dst.addrl;
	} else {
		flg = PLDZ002_DMABD_SRC(PLDZ002_DMABD_DIR_VME) |
		      PLDZ002_DMABD_DST(PLDZ002_DMABD_DIR_PCI) |
		      (novmeinc ? PLDZ002_DMABD_NOINC_SRC : 0);
		vme_addr = vdchan->tx_desc_current->desc.src.addrl;
	}
	/*
	 * if it's not the first batch of the DMA transaction, vme_addr is
	 * given by current_vme_addr which has been stored at the end of the
	 * previous DMA batch.
	 */
	if (chip_dchan->curr_batch_idx != 0)
		vme_addr = chip_dchan->curr_vme_addr;
	flg |= chip_dchan->dma_attr.am;
	flg |= (chip_dchan->dma_attr.is_blt) ?
				PLDZ002_DMA_BLK_TX : PLDZ002_DMA_SINGLE_TX;

	len = sg_len % PLDZ002_DMA_MAX_BDS; /*Compute remainder
					     * (modulo MAX_BDS)
					     */
	/* remainder not null and it's the last batch len is the remainder */
	if (len && (chip_dchan->curr_batch_idx == (chip_dchan->nr_batch - 1)))
		len = len; // remaining number of BD
	else
		len = PLDZ002_DMA_MAX_BDS; // complete batch
	sgl_elem = &sgl[chip_dchan->curr_batch_idx * PLDZ002_DMA_MAX_BDS];
	for (i = 0; i < len; ++i) {
		bd_vaddr = chip_dchan->dma_bd_vaddr;
		bd_vaddr += (i * PLDZ002_DMA_BD_SIZE);
		if (vdchan->tx_desc_current->direction == DMA_MEM_TO_DEV) {
			iowrite32(vme_addr, bd_vaddr + 0x0);
			iowrite32(sg_dma_address(sgl_elem), bd_vaddr + 0x4);
		} else {
			iowrite32(sg_dma_address(sgl_elem), bd_vaddr + 0x0);
			iowrite32(vme_addr, bd_vaddr + 0x4);
		}
		/* DMA size: number of 4 bytes word - 1 (!!! see HW doc) */
		iowrite32((sg_dma_len(sgl_elem) >> 2) - 1, bd_vaddr + 0x8);
		if (i == (len - 1)) /* last buffer descriptor */
			flg |= PLDZ002_DMABD_END;
		iowrite32(flg, bd_vaddr + 0xc);
		if (!novmeinc)
			vme_addr += sg_dma_len(sgl_elem);
		++sgl_elem;
	}
	/* prepare for next DMA batch if any */
	chip_dchan->curr_batch_idx += 1; /* current dma batch index if many */
	chip_dchan->curr_vme_addr = vme_addr; /* current vme addr
					       * if many batch
					       */
}

/*
 * Note: this function can be called from IRQ context in case a DMA transaction
 * is made of multiple DMA batch
 */
void pldz002_schedule_next_dma_batch(struct vme_dma_chan *vdchan)
{
	struct pldz002_dma_chan *chip_dchan = vdchan->private;

	/* clear DMA error */
	iowrite8(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR,
		 chip_dchan->regs_vaddr + PLDZ002_DMASTA);
	pldz002_write_dma_bd(vdchan);
	/* start DMA and enable DMA interrupt */
	iowrite8(PLDZ002_DMASTA_EN | PLDZ002_DMASTA_IEN,
		 chip_dchan->regs_vaddr + PLDZ002_DMASTA);

}

static int pldz002_setup_dma_batch(struct vme_dma_tx_desc *tx_desc,
				   struct pldz002_dma_chan *chip_dchan)
{
	uint32_t nr_batch, dma_len;
	dma_addr_t dma_addr;
	uint64_t vme_addr; /* Pldz002 supports A64 for DMA */
	struct vme_dma_attr *vme_attr;
	int i;
	struct scatterlist *sg;

	nr_batch = (tx_desc->sg_len / PLDZ002_DMA_MAX_BDS) +
				(!!(tx_desc->sg_len % PLDZ002_DMA_MAX_BDS));

	vme_attr = (tx_desc->direction == DMA_MEM_TO_DEV) ?
			&tx_desc->desc.dst : &tx_desc->desc.src;
	vme_addr = ((uint64_t)(vme_attr->addru) << 32) | vme_attr->addrl;
	for_each_sg(tx_desc->sgl, sg, tx_desc->sg_len, i) {
		dma_len = sg_dma_len(sg);
		dma_addr = sg_dma_address(sg);
		if ((dma_len > PLDZ002_DMA_MAX_BLOCK_SIZE) ||
		    (dma_addr & (chip_dchan->dma_attr.align_vme - 1)) ||
		    (vme_addr & (chip_dchan->dma_attr.align_vme - 1))) {
			pr_err(PFX "%s: PLDZ002 DMA setup bad alignment/DMA size: vme:%08llx dma:%08llx dma_len(bytes):%x\n",
			       __func__, vme_addr, dma_addr, dma_len);
			return -EINVAL;
		}
	}
	/* DMA scatter list is fine */
	chip_dchan->nr_batch = nr_batch;
	chip_dchan->curr_batch_idx = 0;
	chip_dchan->curr_vme_addr = 0;

	return 0;
}

static int pldz002_dma_check_state(struct vme_dma_chan *vdchan,
		enum vme_dma_state_field state)
{
	struct pldz002_dma_chan *chip_dchan = vdchan->private;
	uint8_t status;
	int ret;

	status = ioread8(chip_dchan->regs_vaddr + PLDZ002_DMASTA);
	switch (state) {
	case VME_DMA_IS_BUSY:
		return (status & PLDZ002_DMASTA_EN) ? 1 : 0;
	case VME_DMA_IS_DONE:
		return !(status & PLDZ002_DMASTA_EN) ? 1 : 0;
	case VME_DMA_IS_PAUSE:
	default:
		return -EINVAL;
	}
	return ret;
}

static int pldz002_dma_set_ctrl(struct vme_dma_chan *vdchan,
		enum vme_dma_ctrl_field ctrl)
{
	struct pldz002_dma_chan *chip_dchan = vdchan->private;

	switch (ctrl) {
	case VME_DMA_LIST_MODE:
	case VME_DMA_DIRECT_MODE:
		return 0;
	case VME_DMA_START:
		pldz002_schedule_next_dma_batch(vdchan);
		break;
	case VME_DMA_ABORT:
		iowrite8(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR,
			 chip_dchan->regs_vaddr + PLDZ002_DMASTA);
		break;
	case VME_DMA_PAUSE:
	default:
		return -EINVAL;
	}
	return 0; // never happen
}

/**
 * pldz002_dma_free() - Free allocated resource used to setup DMA chain
 * @chan: DMA channel
 *
 */
static void pldz002_dma_free(struct vme_dma_chan *vdchan)
{
	// Nothing special to free after a DMA transaction.
}

/**
 * vme_dma_setup_chain() - Setup the linked list of pldz002 DMA descriptors
 * @chan: DMA channel descriptor
 * @dsat: DMA source attributes
 * @ddat: DMA destination attributes
 *
 */
static int pldz002_dma_setup(struct vme_dma_chan *vdchan)
{
	int rc;
	struct pldz002_dma_chan *chip_dchan = vdchan->private;

	/* Build dma attributes */
	rc = pldz002_setup_dma_attr(vdchan->tx_desc_current,
				    &chip_dchan->dma_attr);
	if (rc < 0) {
		pr_err("%s: dma setup attr failed\n", __func__);
		return rc;
	}

	/*
	 * compute number of batches: pldz002 can manage at once only 16 dma
	 * block descriptors
	 */
	rc = pldz002_setup_dma_batch(vdchan->tx_desc_current,
				     chip_dchan);
	if (rc < 0) {
		pr_err("%s: dma setup batch failed\n", __func__);
		return rc;
	}

	return 0;
}

static int pldz002_dma_get_nr_chan(void)
{
	return PLDZ002_NR_DMA_CHANNELS;
}

static int pldz002_dma_get_capabilities(void)
{
	/*
	 * pldz002 DMA controller doesn't have any tuning capability whether in
	 * terms of BLOCK size or backoff time
	 */
	return 0;
}

static int pldz002_dma_chan_init(struct vme_bridge_device *vbridge,
				struct vme_dma_chan *vdchan)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	struct pldz002_dma_chan *dchan;

	/* Allocates pldz002_dma_chan to hold specific information */
	dchan = kzalloc(sizeof(struct pldz002_dma_chan), GFP_KERNEL);
	if (dchan == NULL)
		return -ENOMEM;
	/*
	 * Init wait queue used to handle intermediate DMA interrupt in case
	 * the complete DMA TX requires several batches
	 */
	init_waitqueue_head(&dchan->wait_dma_batch);
	/* Init useful pldz002 virtual addresses used by DMA to set registers*/
	dchan->regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;
	dchan->dma_bd_vaddr = chip->regions[PLDZ002_REGION_SRAM].vaddr +
					PLDZ002_SRAM_DMA_BD_OFFSET;

	vdchan->private = dchan;

	return 0;
}

static void pldz002_dma_chan_release(struct vme_bridge_device *vbridge,
				struct vme_dma_chan *vdchan)
{
	if (vdchan->private != NULL)
		kfree(vdchan->private);
}

#define VME_PLDZ_DMA_ALIGN 4

static void pldz_dma_transfer_pre(struct vme_dma *desc_new,
				  struct vme_dma *desc, int to_user)
{
	int extra_bytes;

	memcpy(desc_new, desc, sizeof(*desc_new));

	extra_bytes = desc_new->length % VME_PLDZ_DMA_ALIGN;
	if (extra_bytes)
		desc_new->length -= extra_bytes;
}

/*
 * We have notice that ioread8 doesn't work.
 * Therefore those post read/write functions are using ioread16
 */
static void pldz_dma_transfer_post_align_read(struct vme_dma *desc,
					      void *vme_src,
					      int to_user, int extra_bytes)
{
	int i, err, nloop;
	unsigned long dst;
	uint16_t val[4]; // max capability a 64bits word

	dst = (((unsigned long)(desc->dst.addrl)) |
	       (((unsigned long)desc->dst.addru) << 32));
	dst += desc->length - extra_bytes;


	/* determine how many uint16 words to be read */

	nloop = (extra_bytes / 2) + !!(extra_bytes % 2);
	for (i = 0; i < nloop; ++i) {
		val[i] = ioread16(vme_src + (i * 2));
	}

	if (to_user)
		err = copy_to_user((void *)dst, (void *)val, extra_bytes);
	else
		memcpy((void *)dst, (void *)val, extra_bytes);
}

static void pldz_dma_transfer_post_align_write(struct vme_dma *desc,
					       void *vme_dst,
					       int to_user, int extra_bytes)
{
	int i, nloop, idx, err;
	uint16_t val[4]; // max capability a 64bits word
	unsigned long src;
	uint16_t val_int16;

	src = (((unsigned long)(desc->src.addrl)) |
	       (((unsigned long)desc->src.addru) << 32));
	src += desc->length - extra_bytes;

	if (to_user)
		err = copy_from_user((void *)val, (void *)src, extra_bytes);
	else
		memcpy((void *)val, (void *)src, extra_bytes);

	if (extra_bytes % 2) { /* odd number of extra bytes */
		idx = extra_bytes / 2;
		val_int16 = ioread16(vme_dst + (idx * 2));
		val_int16 &= 0xFF;
		val[idx] |= val_int16;
	}

	nloop = (extra_bytes / 2) + !!(extra_bytes % 2);
	for (i = 0; i < nloop; ++i) {
		iowrite16(val[i], vme_dst + (i * 2));
	}
}

static void pldz_dma_transfer_post(struct vme_dma *desc, int to_user)
{
	struct vme_mapping map;
	struct vme_dma_attr *vme;
	int extra_bytes, err, vme_offset;

	extra_bytes = desc->length % VME_PLDZ_DMA_ALIGN;
	if (!extra_bytes)
		return; /* nothing to do here */

	switch (desc->dir) {
	case VME_DMA_FROM_DEVICE:
		vme = &desc->src;
		break;
	case VME_DMA_TO_DEVICE:
		vme = &desc->dst;
		break;
	default:
		pr_err(PFX "%s Unknown DMA direction %d\n",
		       __func__, desc->dir);
		return;
	}

	map.vme_addru = 0;
	map.vme_addrl = vme->addrl & ~0xFFFF;
	vme_offset = (vme->addrl - map.vme_addrl) + desc->length - extra_bytes;

	switch(vme->am) {
	case VME_A24_USER_MBLT:
	case VME_A24_USER_BLT:
	case VME_A24_USER_DATA_SCT:
	case VME_A24_USER_PRG_SCT:
		map.am = VME_A24_USER_DATA_SCT;
		break;
	case VME_A32_USER_MBLT:
	case VME_A32_USER_BLT:
	case VME_A32_USER_DATA_SCT:
	case VME_A32_USER_PRG_SCT:
		map.am = VME_A32_USER_DATA_SCT;
		break;
	default:
		pr_err(PFX "%s address modifier 0x%x not supported\n",
		       __func__, vme->am);
		return;
	}

	pr_debug("%s:%d extra_bytes: %d, VME-addr: 0x%08x, VME-addr-map: 0x%08x, VME offset: 0x%08x, AM: 0x%02x\n",
		 __func__, __LINE__,
		 extra_bytes, vme->addrl, map.vme_addrl, vme_offset, map.am);

	map.data_width = VME_D16;
	map.sizeu = 0;
	map.sizel = 0x10000; /* minimun window size */
	err = vme_find_mapping(&map, 1);
	if (err) {
		pr_err(PFX "%s VME mapping failed: addr: 0x%08x, size: %u, am: 0x%x, data_width: %i\n",
		       __func__, map.vme_addrl, map.sizel,
		       map.am, map.data_width);
		return ;
	}

	if (desc->dir == VME_DMA_FROM_DEVICE)
		pldz_dma_transfer_post_align_read(desc,
						  map.kernel_va + vme_offset,
						  to_user,
						  extra_bytes);
	else
		pldz_dma_transfer_post_align_write(desc,
						   map.kernel_va + vme_offset,
						   to_user,
						   extra_bytes);

	vme_release_mapping(&map, 1);


}

static struct vme_bridge_dma_ops pldz002_dma_ops = {
	.dma_pool_create = NULL, // not used
	.dma_get_nr_chan = pldz002_dma_get_nr_chan,
	.dma_get_capabilities = pldz002_dma_get_capabilities,
	.dma_chan_init = pldz002_dma_chan_init,
	.dma_chan_release = pldz002_dma_chan_release,
	.dma_set_ctrl = pldz002_dma_set_ctrl,
	.dma_check_state = pldz002_dma_check_state,
	.dma_setup = pldz002_dma_setup,
	.dma_free = pldz002_dma_free,
	.dma_transfer_pre = pldz_dma_transfer_pre,
	.dma_transfer_post = pldz_dma_transfer_post,
};

int pldz002_dma_set_ops(struct vme_bridge_device *vbridge)
{
	/* set specific bridge dma ops */
	vbridge->dma_mgr.ops = &pldz002_dma_ops;

	return 0;
}
