"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import subprocess


class TestVmebusMatch(object):
    def test_vme_bridge_match(self):
        """Test if the driver is matching an existent bridge"""
        bridge = None
        with open("/sys/bus/vme/bridge_type") as f:
            bridge = f.read().strip()
        lspci = subprocess.run(["lspci", "-d", "0x10e3:0x0148"],
                               stdout=subprocess.PIPE, check=True)
        if len(lspci.stdout) != 0:
            assert bridge == "tundra-tsi148"
        else:
            lspci = subprocess.run(["lspci", "-d", "0x1a88:0x4d45"],
                                   stdout=subprocess.PIPE, check=True)
            if len(lspci.stdout) != 0:
                assert bridge == "men-pldz002"
            else:
                assert False
