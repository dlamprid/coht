// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * Author: Federico Vaga <federico.vaga@cern.ch>
 * Copyright 2017 CERN
 */

#include <linux/version.h>
#include <linux/sched.h>

#include "vme_bridge.h"


/**
 * Mapping template for CR/CSR space
 */
static const struct vme_mapping map_tmpl_cr = {
	.vme_addru = 0,
	.vme_addrl = 0,
	.am = VME_CR_CSR,
	.data_width = VME_D32,
	.sizeu = 0,
	.sizel = 0x80000,
};

static const char vme_cr_area_names[VME_CR_AREA_MAX][16] = {
	[VME_CR_AREA_CR] = "CR",
	[VME_CR_AREA_CR_USER] = "CR User",
	[VME_CR_AREA_CRAM] = "CRAM",
	[VME_CR_AREA_CSR_USER] = "CSR User",
	[VME_CR_AREA_SN] = "Serial Number",
};

#define VME_CR_AREA_BEG_OFFSET(area) (0x83 + ((area - 1) * 0x18))
#define VME_CR_AREA_END_OFFSET(area) (VME_CR_AREA_BEG_OFFSET(area) + 0xC)

/**
 *It dumps a given area into the given buffer
 */
static int vme_crcsr_dump(unsigned int slot, uint8_t *data,
			  loff_t off, size_t count)
{
	struct vme_mapping map = map_tmpl_cr;
	uint32_t *buf = (uint32_t *)data;
	int i, err;

	map.vme_addrl = map.sizel * slot;
	err = vme_find_mapping(&map, 1);
	if (err)
		return err;
	/* We dump it without endianess conversion */
	for (i = 0; i < (count / 4); ++i)
		buf[i] = ioread32(map.kernel_va + off + (i * 4));
	vme_release_mapping(&map, 1);

	return 0;
}

/**
 * It copies a CR/CSR field into the given buffer.
 * @slot: slot number [1, N]
 *
 * This removes the unused bytes
 */
static int vme_crcsr_field(unsigned int slot, uint8_t *data,
			  loff_t off, size_t count)
{
	struct vme_mapping map = map_tmpl_cr;
	uint32_t val;
	int i, err;

	if (slot > VME_SLOT_MAX)
		return -EINVAL;

	map.vme_addrl = map.sizel * slot;
	err = vme_find_mapping(&map, 1);
	if (err)
		return err;
	/* We dump it without endianess conversion */
	for (i = 0; i < count; ++i) {
		val = ioread32be(map.kernel_va + (off & 0xFFFFFFFC) + (i * 4));
		data[i] = (uint8_t)(val & 0xFF);
	}
	vme_release_mapping(&map, 1);

	return 0;
}

/**
 * It maps the CR/CSR space
 * @vdev VME device
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_map(struct vme_dev *vdev)
{
	int err;

	vdev->map_cr = map_tmpl_cr;
	vdev->map_cr.vme_addrl = vdev->map_cr.sizel * vdev->slot;

	err = vme_find_mapping(&vdev->map_cr, 1);
	if (err)
		memset(&vdev->map_cr, 0, sizeof(struct vme_mapping));

	return err;
}


/**
 * It unmaps the CR/CSR space
 * @vdev VME device
 */
void vme_csr_unmap(struct vme_dev *vdev)
{
	if (vdev->map_cr.kernel_va)
		vme_release_mapping(&vdev->map_cr, 1);
}

/**
 * It checks if at the given address there is a valid CR space
 * @slot: VMe slot number [1, N]
 *
 * Return: 1 if there is a valid CR space, otherwise 0
 */
int vme_cr_is_valid(unsigned int slot)
{
	uint8_t spec, c, r;

	/* TODO check sum */

	vme_crcsr_field(slot, &spec, 0x1B, 1);
	vme_crcsr_field(slot, &c, 0x1F, 1);
	vme_crcsr_field(slot, &r, 0x23, 1);

	/* Validate */
	if (c != 0x43 || r != 0x52 ||
	    (spec != VME_SPEC_VME64 && spec != VME_SPEC_VME64x))
		return 0;

	return 1;
}

/**
 * It checks if the module has a valid CR space
 * @vdev VME device
 * Return: 1 if the device has a valid CR space, otherwise 0
 *
 * We check the CR directly on the board instead of using the dumped one.
 * This beacuse we do not want to dump the entire CR space before discovering
 * if it is valid or not
 */
static inline int vme_has_cr(struct vme_dev *vdev)
{
	return vme_cr_is_valid(vdev->slot);
}


/**
 * It assembles in a single 32bit value a list of bytes
 * @val array on words to assemble
 * @n_byte number of bytes to assemble
 */
static uint32_t vme_crcsr_word_assemble(uint32_t *val, unsigned int n_byte)
{
	int i;
	uint32_t ret = 0;

	for (i = 0; i < n_byte; ++i) {
		ret <<= 8;
		ret |= (val[i] & 0xFF);
	}

	return ret;
}

/**
 * It reads a bit mask
 */
static void vme_crcsr_bitmap_assemble(uint32_t *val,
				      unsigned long *bitmap,
				      unsigned int n_byte)
{
	DECLARE_BITMAP(map, BITS_PER_BYTE * n_byte);
	unsigned int i;

	bitmap_zero(map, BITS_PER_BYTE * n_byte);
	bitmap_zero(bitmap, BITS_PER_BYTE * n_byte);
	/* byte by byte build the mask */
	for (i = 0; i < n_byte; ++i) {
		bitmap_shift_left(bitmap, bitmap, 8, BITS_PER_BYTE * n_byte);
		map[0] = val[i] & 0xFF;
		bitmap_or(bitmap, bitmap, map, BITS_PER_BYTE * n_byte);
	}
}


/**
 * It dumps a static area in the CR space
 * @vdev VME device
 * @area area to dump
 * Return: 0 on success, otherwise a negative errno number
 */
static int __vme_cr_area_dump(struct vme_dev *vdev, struct vme_cr_area *area)
{
	uint32_t *buf;
	int i;
	size_t size = area->offset_end - area->offset_beg;

	if (size == 0)
		return 0;

	if (unlikely(size < 0)) {
		dev_err(&vdev->dev,
			"Invalid area {beg: 0x%08x, end: 0x%08x}\n",
			area->offset_beg, area->offset_end);
		return -EINVAL;
	}

	area->data = kzalloc(size + 1, GFP_KERNEL);
	if (!area->data)
		return -ENOMEM;

	buf = (uint32_t *)area->data;
	for (i = 0; i < ((size + 1) / 4); ++i)
		buf[i] = ioread32be(vdev->map_cr.kernel_va + (i * 4));


	return 0;
}


static int vme_cr_area_dump(struct vme_dev *vdev, enum vme_cr_areas a,
			    void (*to_repr)(struct vme_cr_area *area))
{
	struct vme_cr_area *area = &vdev->spaces[a];
	int err;

	strncpy(area->name, vme_cr_area_names[a], 16);

	switch (a) {
	case VME_CR_AREA_CR:
		area->offset_beg = 0x0;
		area->offset_end = max(VME_DEV_VME64_SIZE, VME_DEV_VME64x_SIZE) - 1;
		break;
	default: {
		struct vme_cr_area *cr = &vdev->spaces[VME_CR_AREA_CR];
		uint32_t *buf = (uint32_t *)cr->data;

		if (!buf) {
			dev_err(&vdev->dev,
				"Cannot dump CR User. Invalid CR space.\n");
			return -EINVAL;
		}


		if (vdev->spec != VME_SPEC_VME64x)
			return -EINVAL;


		area->offset_beg = vme_crcsr_word_assemble(buf + VME_CR_AREA_BEG_OFFSET(a), 3);
		area->offset_end = vme_crcsr_word_assemble(buf + VME_CR_AREA_END_OFFSET(a), 3);
		break;
	}
	}

	err = __vme_cr_area_dump(vdev, area);
	if (err)
		return err;

	if (to_repr)
		to_repr(area);

	return 0;
}



/**
 * It gets the manufacturer ID
 * @addr base address to the CR space
 *
 * Return: the manufacturer ID
 */
uint8_t vme_cr_checksum(unsigned int slot)
{
	return 0;
}

/**
 * It gets the manufacturer ID
 * @addr base address to the CR space
 *
 * Return: the manufacturer ID
 */
uint32_t vme_cr_manufacturer(unsigned int slot)
{
	uint32_t val;

	vme_crcsr_field(slot, (uint8_t *)&val, 0x27, 3);

	return val;
}

/**
 * It gets the device ID
 * @addr base address to the CR space
 *
 * Return: the device ID
 */
uint32_t vme_cr_device(unsigned int slot)
{
	uint32_t val;

	vme_crcsr_field(slot, (uint8_t *)&val, 0x33, 4);

	return val;
}

/**
 * It gets the device ID
 * @addr base address to the CR space
 *
 * Return: the device ID
 */
uint32_t vme_cr_revision(unsigned int slot)
{
	uint32_t val;

	vme_crcsr_field(slot, (uint8_t *)&val, 0x43, 4);

	return val;
}

/**
 * It gets the device ID
 * @addr base address to the CR space
 *
 * Return: the device ID
 */
uint8_t vme_cr_irq_cap(unsigned int slot)
{
	uint8_t val;

	vme_crcsr_field(slot, &val, 0xF7, 1);

	return val;
}

/**
 * It creates a custom structure for the CR space
 * @area area to convert into CR representation
 */
void vme_repr_cr(struct vme_cr_area *area)
{
	uint32_t *data = (uint32_t *)area->data;
	struct vme_cr *cr;
	int i;

	if (area->repr)
		kfree(area->repr);
	area->repr = NULL;

	if (!data)
		return;

	cr = kzalloc(sizeof(struct vme_cr), GFP_KERNEL);
	if (!cr)
		return;

	/* VME64 */
	cr->checksum = vme_crcsr_word_assemble(data + 0x000 / 4, 1);
	cr->rom_len = vme_crcsr_word_assemble(data + 0x004 / 4, 3);
	cr->rom_dw = vme_crcsr_word_assemble(data + 0x010 / 4, 1);
	cr->csr_dw = vme_crcsr_word_assemble(data + 0x014 / 4, 1);
	cr->spec = vme_crcsr_word_assemble(data + 0x018 / 4, 1);
	cr->c = vme_crcsr_word_assemble(data + 0x01C / 4, 1);
	cr->r = vme_crcsr_word_assemble(data + 0x020 / 4, 1);
	cr->manufacturer = vme_crcsr_word_assemble(data + 0x024 / 4, 3);
	cr->device = vme_crcsr_word_assemble(data + 0x030 / 4, 4);
	cr->revision = vme_crcsr_word_assemble(data + 0x040 / 4, 4);
	cr->string_ptr = vme_crcsr_word_assemble(data + 0x050 / 4, 3);
	cr->program_id = vme_crcsr_word_assemble(data + 0x07C / 4, 1);

	/* VME64x */
	cr->beg_user_cr = vme_crcsr_word_assemble(data + 0x080 / 4, 3);
	cr->end_user_cr = vme_crcsr_word_assemble(data + 0x08C / 4, 3);
	cr->beg_cram = vme_crcsr_word_assemble(data + 0x098 / 4, 3);
	cr->end_cram = vme_crcsr_word_assemble(data + 0x0A4 / 4, 3);
	cr->beg_user_csr = vme_crcsr_word_assemble(data + 0x0B0 / 4, 3);
	cr->end_user_csr = vme_crcsr_word_assemble(data + 0x0BC / 4, 3);
	cr->beg_sn = vme_crcsr_word_assemble(data + 0x0C8 / 4, 3);
	cr->end_sn = vme_crcsr_word_assemble(data + 0x0D4 / 4, 3);
	cr->slave_par = vme_crcsr_word_assemble(data + 0x0E0 / 4, 1);
	cr->slave_par_usr = vme_crcsr_word_assemble(data + 0x0E4 / 4, 1);
	cr->master_par = vme_crcsr_word_assemble(data + 0x0E8 / 4, 1);
	cr->master_par_usr = vme_crcsr_word_assemble(data + 0x0EC / 4, 1);
	cr->irq_handler_cap = vme_crcsr_word_assemble(data + 0x0F0 / 4, 1);
	cr->irq_cap = vme_crcsr_word_assemble(data + 0x0F4 / 4, 1);
	cr->cram_dw = vme_crcsr_word_assemble(data + 0x0FC / 4, 1);
	for (i = 0; i < VME_CR_FUNCTION_MAX; ++i) {
		cr->func_dw[i] = vme_crcsr_word_assemble(data + (0x100 + (i * 0x4)) / 4, 1);
		vme_crcsr_bitmap_assemble(data + (0x120 + (i * 0x20))/ 4,
					  cr->func_amcap[i], 8);
		vme_crcsr_bitmap_assemble(data + (0x220 + (i * 0x80)) / 4,
					  cr->func_xamcap[i], 32);
		cr->func_adem[i] = vme_crcsr_word_assemble(data + (0x620 + (i * 0x10)) / 4, 4);
	}

	cr->master_dw = vme_crcsr_word_assemble(data + 0x6AC / 4, 3);
	vme_crcsr_bitmap_assemble(data + 0x6B0 / 4, cr->master_amcap, 8);
	vme_crcsr_bitmap_assemble(data + 0x6D0 / 4, cr->master_xamcap, 32);


	area->repr = cr;
}


/**
 * It dumps all the static areas
 * @vdev VME device
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_cr_area_dump_all(struct vme_dev *vdev)
{
	struct vme_cr *cr;
	int err;

	if (!vme_has_cr(vdev)) {
		dev_err(&vdev->dev, "Invalid CR space\n");
		return -EINVAL;
	}

	/* VME64 */
	err = vme_cr_area_dump(vdev, VME_CR_AREA_CR, vme_repr_cr);
	if (err)
		goto err;

	cr = vdev->spaces[VME_CR_AREA_CR].repr;
	if (!cr)
		return -EINVAL;

	vdev->spec = cr->spec;
	if (vdev->spec != VME_SPEC_VME64x)
		return 0;

	/* VME64x */
	err = vme_cr_area_dump(vdev, VME_CR_AREA_CR_USER, NULL);
	if (err)
		goto err;
	err = vme_cr_area_dump(vdev, VME_CR_AREA_SN, NULL);
	if (err)
		goto err;
	return 0;

err:
	dev_err(&vdev->dev, "Cannot dump CR spaces (%d)\n", err);
	return err;
}


/**
 * It releases all the static areas
 * @vdev VME device
 */
void vme_cr_area_release_all(struct vme_dev *vdev)
{
	int i;

	for (i = 0; i < VME_CR_AREA_MAX; ++i) {
		kfree(vdev->spaces[i].data);
		kfree(vdev->spaces[i].repr);
		memset(&vdev->spaces[i], 0 , sizeof(struct vme_cr_area));
	}
}


/**
 * It retrieves the CSR space from the board.
 * @vdev VME device
 * @csr pre-allocated CSR structure
 * Return: 0 on success, otherwise a negative errno number
 *
 * This is supported only with VME64 and VME64x specification
 */
int vme_csr_get(struct vme_dev *vdev, struct vme_csr *csr)
{
	struct vme_cr *cr = vdev->spaces[VME_CR_AREA_CR].repr;
	uint32_t tmp[4];
	int i, k;

	if (vdev->spec != VME_SPEC_VME64x &&
	    vdev->spec != VME_SPEC_VME64)
		return -EPERM;

	csr->bar = ioread32be(vdev->map_cr.kernel_va + 0x7FFFC) & 0xFF;
	csr->bit_set = ioread32be(vdev->map_cr.kernel_va + 0x7FFF8) & 0xFF;
	csr->bit_clr = ioread32be(vdev->map_cr.kernel_va + 0x7FFF4) & 0xFF;

	if (cr->spec != VME_SPEC_VME64x)
		return 0;

	csr->cram_owner = ioread32be(vdev->map_cr.kernel_va + 0x7FFF0) & 0xFF;
	csr->bit_set_usr = ioread32be(vdev->map_cr.kernel_va + 0x7FFEC) & 0xFF;
	csr->bit_clr_usr = ioread32be(vdev->map_cr.kernel_va + 0x7FFE8) & 0xFF;

	for (i = 0; i < VME_CR_FUNCTION_MAX; ++i) {
		for (k = 0; k < 4; ++k) {
			tmp[k] = ioread32be(vdev->map_cr.kernel_va + 0x7FF60 + (i * 0x10) + (k * 4));
			tmp[k] &= 0xFF;
		}
		csr->ader[i] = vme_crcsr_word_assemble(tmp, 4);
	}

	if (csr->bit_set != csr->bit_clr)
		dev_warn(&vdev->dev,
			 "Buggy CSR space."
			 "On read Bit Set and Bit Clear should be equal\n");

	if (csr->bit_set_usr != csr->bit_clr_usr)
		dev_warn(&vdev->dev,
			 "Buggy CSR space."
			 "On read Bit Set User and Bit Clear User should be equal\n");

	return 0;
}

#define ADER_N_BYTE 4
void vme_csr_ader_set_raw(struct vme_dev *vdev,
			  unsigned int func, uint32_t ader)
{
	int i;

	for (i = 0; i < ADER_N_BYTE; ++i) {
		unsigned int offset = (0x7FF60 + (func * 0x10) + (i * 4));
		uint32_t tmp = (ader >> ((ADER_N_BYTE - 1 - i) * 8)) & 0xFF;
		iowrite32be(tmp, vdev->map_cr.kernel_va + offset);
	}
}

int vme_csr_ader_set(struct vme_dev *vdev,
		     unsigned int function, uint32_t ader)
{
	struct vme_cr *cr;
	struct vme_csr csr;
	int err;


	if (vdev->spec != VME_SPEC_VME64x)
		return -EPERM;

	cr = vdev->spaces[VME_CR_AREA_CR].repr;

	if (cr->func_adem[function] & VME_CSR_ADEM_FAF) {
		dev_err(&vdev->dev,
			"Can't change ADER for function %d: FAF bit set\n",
			function);
		return -EPERM;
	}

	err = vme_csr_get(vdev, &csr);
	if (err)
		return err;

	if (csr.bit_set & VME_CSR_BIT_ENABLE) {
		dev_err(&vdev->dev,
			"Can't program ADER while the board is in use\n");
		return -EBUSY;
	}

	if (ader & VME_CSR_ADER_XAM_MODE) {
		int xam = VME_CSR_ADER_XAM(ader);

		if (!test_bit(xam, cr->func_xamcap[function])) {
			dev_err(&vdev->dev,
				"Function %d does not support XAM 0x%02x\n",
				function, xam);
			return -EINVAL;
		}
	} else {
		int am = VME_CSR_ADER_AM(ader);

		if (!test_bit(am, cr->func_amcap[function])) {
			dev_err(&vdev->dev,
				"Function %d does not support AM 0x%02x\n",
				function, am);
			return -EINVAL;
		}
	}

	if (VME_CSR_ADER_ADDR(ader) & ~VME_CSR_ADEM_MASK(cr->func_adem[function])) {
		dev_err(&vdev->dev,
			"Function %d does not support address mask 0x%08x\n",
			function, VME_CSR_ADER_ADDR(ader));
		return -EINVAL;
	}

	vme_csr_ader_set_raw(vdev, function, ader);

	return 0;
}
#undef ADER_N_BYTE


static int vme_csr_bit_usr_set_clr(struct vme_dev *vdev, uint8_t set, uint8_t clr)
{
	if (vdev->spec != VME_SPEC_VME64x)
		return -EPERM;

	iowrite32be((uint32_t)set, vdev->map_cr.kernel_va + VME_CSR_USR_BIT_SET);
	iowrite32be((uint32_t)clr, vdev->map_cr.kernel_va + VME_CSR_USR_BIT_CLR);

	return 0;
}


static int vme_csr_bit_set_clr(struct vme_dev *vdev, uint8_t set, uint8_t clr)
{
	if (vdev->spec != VME_SPEC_VME64 && vdev->spec != VME_SPEC_VME64x)
		return -EPERM;

	iowrite32be((uint32_t)set, vdev->map_cr.kernel_va + VME_CSR_BIT_SET);
	iowrite32be((uint32_t)clr, vdev->map_cr.kernel_va + VME_CSR_BIT_CLR);

	return 0;
}


/**
 * It sets/clears a CSR User bit
 * @vdev VME device
 * @mask mask to set (1 bit)
 * @val value to set
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_bit_usr(struct vme_dev *vdev, uint8_t mask, unsigned int val)
{
	int err;

	if (val)
		err = vme_csr_bit_usr_set_clr(vdev, mask, 0);
	else
		err = vme_csr_bit_usr_set_clr(vdev, 0, mask);

	return err;
}


/**
 * It sets/clears a CSR bit
 * @vdev VME device
 * @mask mask to set (1 bit)
 * @val value to set
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_bit(struct vme_dev *vdev, uint8_t mask, unsigned int val)
{
	int err;

	if (val)
		err = vme_csr_bit_set_clr(vdev, mask, 0);
	else
		err = vme_csr_bit_set_clr(vdev, 0, mask);

	return err;
}


/**
 * It enables or disables the module
 * @vdev VME device
 * @enable enable status
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_enable(struct vme_dev *vdev, unsigned int enable)
{
	return vme_csr_bit(vdev, VME_CSR_BIT_ENABLE, enable);
}
EXPORT_SYMBOL_GPL(vme_csr_enable);

/**
 * It resets or unresets the module
 * @vdev VME device
 * @enable enable status
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_reset(struct vme_dev *vdev, unsigned int enable)
{
	return vme_csr_bit(vdev, VME_CSR_BIT_RESET, enable);
}
EXPORT_SYMBOL_GPL(vme_csr_reset);


/**
 * It enables or disables the sysfail driver
 * @vdev VME device
 * @enable enable status
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_sysfail(struct vme_dev *vdev, unsigned int enable)
{
	return vme_csr_bit(vdev, VME_CSR_BIT_SYSFAIL, enable);
}
EXPORT_SYMBOL_GPL(vme_csr_sysfail);


static int vme_mmap_fits(struct vme_dev *vdev, int func,
			 struct vm_area_struct *vma)
{
	struct resource *res = &vdev->resource[func];
	unsigned long nr, start, size, vme_start;

	if (resource_size(res) == 0)
		return 0;
	nr = (vma->vm_end - vma->vm_start) >> PAGE_SHIFT;
	start = vma->vm_pgoff;
	size = ((resource_size(res) - 1) >> PAGE_SHIFT) + 1;
	vme_start = 0;
	if (start >= vme_start &&
	    start < vme_start + size &&
	    start + nr <= vme_start + size)
		return 1;
	return 0;
}


static int vme_mmap_resource(struct file *filp, struct kobject *kobj,
			     struct bin_attribute *attr,
			     struct vm_area_struct *vma)
{
	struct vme_dev *vdev = to_vme_dev(container_of(kobj, struct device,
						       kobj));
	struct resource *res = attr->private;
	int i, err;

	for (i = 0; i < VME_CR_FUNCTION_MAX; i++)
		if (res == &vdev->resource[i])
			break;
	if (i >= VME_CR_FUNCTION_MAX)
		return -ENODEV;

	if (!vme_mmap_fits(vdev, i, vma)) {
		WARN(1, "process \"%s\" tried to map 0x%08lx bytes "
		     "at page 0x%08lx on %s func %d (start 0x%16Lx, "
		     "size 0x%16Lx)\n",
		     current->comm, vma->vm_end-vma->vm_start, vma->vm_pgoff,
		     dev_name(&vdev->dev), i,
		     (u64)res->start,
		     (u64)resource_size(res));
		return -EINVAL;
	}

	vma->vm_flags |= VM_IO | VM_DONTCOPY | VM_DONTEXPAND;
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	vma->vm_pgoff += (res->start >> PAGE_SHIFT);

	/* FIXME we should check this, but we cannot do it in modules */
	/* if (res->flags & IORESOURCE_MEM && iomem_is_exclusive(res->start)) */
	/* 	return -EINVAL; */

	err = io_remap_pfn_range(vma,
				 vma->vm_start,
				 vma->vm_pgoff,
				 vma->vm_end - vma->vm_start,
				 vma->vm_page_prot);

	return err;
}


/**
 * It creates a resource binary attribute
 * @vdev VME device associated to the resource
 * @func function number
 * @res informatin about the resource
 *
 * Return: 0 on success, otherwise a negative errno number
 */
int vme_csr_resource_create(struct vme_dev *vdev, unsigned int func)
{
	struct bin_attribute *res_attr;
	int err;

	/* + 17 string size for name */
	res_attr = kzalloc(sizeof(struct bin_attribute) + 17, GFP_ATOMIC);
	if (!res_attr)
		return -ENOMEM;

	res_attr->attr.name = (char *)(res_attr + 1);
	sprintf((char *)res_attr->attr.name, "resource%d", func);
	res_attr->attr.mode = S_IRUSR | S_IWUSR;
	res_attr->size = resource_size(&vdev->resource[func]);
	res_attr->mmap = vme_mmap_resource;
	res_attr->private = (void *)&vdev->resource[func];
	err = sysfs_create_bin_file(&vdev->dev.kobj, res_attr);
	if (err)
		goto err_create_bin;

	vdev->res_attr[func] = res_attr;

	return 0;

err_create_bin:
	kfree(res_attr);
	return err;
}


/**
 * It removes a resource binary attribute
 * @vdev VME device from which we want remove the resource
 * @func function number
 */
void vme_csr_resource_remove(struct vme_dev *vdev, unsigned int func)
{
	if (!vdev->res_attr[func])
		return; /* nothing to do */

	sysfs_remove_bin_file(&vdev->dev.kobj,
			      vdev->res_attr[func]);
	kfree(vdev->res_attr[func]);
}


static ssize_t vme_crcsr_read(struct file *filp, struct kobject *kobj,
			   struct bin_attribute *bin_attr,
			   char *buf, loff_t off, size_t count)
{
	struct vme_dev *vdev = to_vme_dev(container_of(kobj,
						       struct device,
						       kobj));

	if (!vdev->slot)
		return -EINVAL;
	vme_crcsr_dump(vdev->slot, buf, off, count);
	return count;
}


struct bin_attribute vme_crcsr_bin_attr = {
	.attr = {
		.name = "cr-csr",
		.mode = 0444,
	},
	.size = VME_DEV_CRCSR_SIZE,
	.read = vme_crcsr_read,
	.write = NULL,
	.mmap = NULL,
};
