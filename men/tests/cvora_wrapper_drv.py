# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *
import pytest
import time


def test_cvora_drv_set_channel_mask():
    # check version
    cmd = """echo 'channel_mask 0xa5a5a5a5\nchannel_mask\nchannel_mask 0x5a5a5a5a\nchannel_mask\n' |CPU=''  """ + CVORATEST_BIN + """ | tail -n +3"""
    result = """(Cmd) 0
(Cmd) mask: a5a5a5a5
(Cmd) 0
(Cmd) mask: 5a5a5a5a
(Cmd) mask: 5a5a5a5a
(Cmd) 
"""
    assert cmp_result(cmd, result)
