/* SPDX-License-Identifier: GPL-2.0 */
/**
 * \file vmebus.h
 * \brief PCI-VME public API
 * \author Sebastien Dugue
 * \date 04/02/2009
 *
 *  This API presents in fact 2 APIs with some common definitions. One for
 * drivers and one for user applications. User applications cannot use the
 * driver specific parts enclosed in \#ifdef __KERNEL__ sections.
 *
 * Copyright (c) 2009 \em Sebastien \em Dugue
 */

#ifndef _VME_H
#define _VME_H

#ifdef __KERNEL__
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#else
#include <stdint.h>
#endif /* __KERNEL__ */

#include <linux/types.h>


#define VME_NAME_ID_LEN 32
struct vme_device_id {
	char name[VME_NAME_ID_LEN];
	uint32_t vendor;
	uint32_t device;
	uint32_t revision;
};

#define VME_IRQ_VECTOR_AUTO 0xFFFFFFFF
#define VME_IRQ_VECTOR_OLDAPI 0xFFFFFFFE

/**
 * Descriptor for static device registration
 */
struct vme_device_register {
	uint32_t struct_size;
	struct vme_device_id id;
	uint32_t am;
	uint32_t base_address;
	uint32_t size;
	uint32_t irq_vector;
	uint32_t irq_level;
};

#define PRIvdevREG \
	"ID %s %x:%x:%x, AM 0x%02x, ADDR 0x%08x, SIZE %d, VECT 0x%x, LEVEL %d"
#define PRIvdevREGVAL(_reg)\
	(_reg)->id.name, (_reg)->id.vendor, (_reg)->id.device, \
	(_reg)->id.revision, (_reg)->am, (_reg)->base_address, (_reg)->size, \
	(_reg)->irq_vector, (_reg)->irq_level

/*
 * VME window attributes
 */

/**
 * \brief Read prefetch size
 */
enum vme_read_prefetch_size {
	VME_PREFETCH_2	= 0,
	VME_PREFETCH_4,
	VME_PREFETCH_8,
	VME_PREFETCH_16
};

/**
 * \brief Data width
 */
enum vme_data_width {
	VME_D8 = 8,
	VME_D16 = 16,
	VME_D32 = 32,
	VME_D64 = 64
};

/**
 * \brief 2eSST data transfer speed
 */
enum vme_2esst_mode {
	VME_SST160 = 0,
	VME_SST267,
	VME_SST320
};

/**
 * \brief Address modifiers
 */
enum vme_address_modifier {
	VME_A64_MBLT		= 0,	/* 0x00 */
	VME_A64_SCT,			/* 0x01 */
	VME_A64_BLT		= 3,	/* 0x03 */
	VME_A64_LCK,			/* 0x04 */
	VME_A32_LCK,			/* 0x05 */
	VME_A32_USER_MBLT	= 8,	/* 0x08 */
	VME_A32_USER_DATA_SCT,		/* 0x09 */
	VME_A32_USER_PRG_SCT,		/* 0x0a */
	VME_A32_USER_BLT,		/* 0x0b */
	VME_A32_SUP_MBLT,		/* 0x0c */
	VME_A32_SUP_DATA_SCT,		/* 0x0d */
	VME_A32_SUP_PRG_SCT,		/* 0x0e */
	VME_A32_SUP_BLT,		/* 0x0f */
	VME_2e6U		= 0x20,	/* 0x20 */
	VME_2e3U,			/* 0x21 */
	VME_A16_USER		= 0x29,	/* 0x29 */
	VME_A16_LCK		= 0x2c,	/* 0x2c */
	VME_A16_SUP		= 0x2d,	/* 0x2d */
	VME_CR_CSR		= 0x2f,	/* 0x2f */
	VME_A40_SCT		= 0x34, /* 0x34 */
	VME_A40_LCK,			/* 0x35 */
	VME_A40_BLT		= 0x37, /* 0x37 */
	VME_A24_USER_MBLT,		/* 0x38 */
	VME_A24_USER_DATA_SCT,		/* 0x39 */
	VME_A24_USER_PRG_SCT,		/* 0x3a */
	VME_A24_USER_BLT,		/* 0x3b */
	VME_A24_SUP_MBLT,		/* 0x3c */
	VME_A24_SUP_DATA_SCT,		/* 0x3d */
	VME_A24_SUP_PRG_SCT,		/* 0x3e */
	VME_A24_SUP_BLT,		/* 0x3f */
};



/**
 * \brief PCI-VME mapping descriptor
 * \param window_num Hardware window number
 * \param kernel_va Kernel virtual address of the mapping for use by drivers
 * \param user_va User virtual address of the mapping for use by applications
 * \param fd User file descriptor for this mapping
 * \param window_enabled State of the hardware window
 * \param data_width VME data width
 * \param am VME address modifier
 * \param read_prefetch_enabled PCI read prefetch enabled state
 * \param read_prefetch_size PCI read prefetch size (in cache lines)
 * \param v2esst_mode VME 2eSST transfer speed
 * \param bcast_select VME 2eSST broadcast select
 * \param pci_addru PCI bus start address upper 32 bits
 * \param pci_addrl PCI bus start address lower 32 bits
 * \param sizeu Window size upper 32 bits
 * \param sizel Window size lower 32 bits
 * \param vme_addru VME bus start address upper 32 bits
 * \param vme_addrl VME bus start address lower 32 bits
 *
 * This data structure is used for describing both a hardware window
 * and a logical mapping on top of a hardware window. Therefore some of
 * the fields are only relevant to one of those two entities.
 */
struct vme_mapping {
	int	window_num;

	/* Reserved for kernel use */
	void	*kernel_va;

	/* Reserved for userspace */
	void	*user_va;
	int	fd;

	/* Window settings */
	int				window_enabled;
	enum vme_data_width		data_width;
	enum vme_address_modifier	am;
	int				read_prefetch_enabled;
	enum vme_read_prefetch_size	read_prefetch_size;
	enum vme_2esst_mode		v2esst_mode;
	int				bcast_select;
	unsigned int			pci_addru;
	unsigned int			pci_addrl;
	unsigned int			sizeu;
	unsigned int			sizel;
	unsigned int			vme_addru;
	unsigned int			vme_addrl;
};

/**
 * \brief VME RMW descriptor
 * \param vme_addru VME address for the RMW cycle upper 32 bits
 * \param vme_addrl VME address for the RMW cycle lower 32 bits
 * \param am VME address modifier
 * \param enable_mask Bitmask of the bit
 * \param compare_data
 * \param swap_data
 *
 */
struct vme_rmw {
	unsigned int vme_addru;
	unsigned int vme_addrl;
	enum vme_address_modifier am;
	unsigned int enable_mask;
	unsigned int compare_data;
	unsigned int swap_data;
};

/**
 * \brief DMA endpoint attributes
 * \param data_width VME data width (only used if endpoint is on the VME bus)
 * \param am VME address modifier (ditto)
 * \param v2esst_mode VME 2eSST transfer speed (ditto)
 * \param bcast_select VME 2eSST broadcast select (ditto)
 * \param addru Address upper 32 bits
 * \param addrl Address lower 32 bits
 *
 *  This data structure is used for describing the attributes of a DMA endpoint.
 * All the field excepted for the address are only relevant for an endpoint
 * on the VME bus.
 */
struct vme_dma_attr {
	enum vme_data_width		data_width;
	enum vme_address_modifier	am;
	enum vme_2esst_mode		v2esst_mode;
	unsigned int			bcast_select;
	unsigned int			addru;
	unsigned int			addrl;
};

/** \brief DMA block size on the PCI or VME bus */
enum vme_dma_block_size {
	VME_DMA_BSIZE_32 = 0,
	VME_DMA_BSIZE_64,
	VME_DMA_BSIZE_128,
	VME_DMA_BSIZE_256,
	VME_DMA_BSIZE_512,
	VME_DMA_BSIZE_1024,
	VME_DMA_BSIZE_2048,
	VME_DMA_BSIZE_4096,
	__VME_DMA_BSIZE_MAX,
};

/** \brief DMA backoff time (us) on the PCI or VME bus */
enum vme_dma_backoff {
	VME_DMA_BACKOFF_0 = 0,
	VME_DMA_BACKOFF_1,
	VME_DMA_BACKOFF_2,
	VME_DMA_BACKOFF_4,
	VME_DMA_BACKOFF_8,
	VME_DMA_BACKOFF_16,
	VME_DMA_BACKOFF_32,
	VME_DMA_BACKOFF_64,
	__VME_DMA_BACKOFF_MAX,
};

/**
 * \brief DMA control
 * \param vme_block_size VME bus block size when the source is VME
 * \param vme_backoff_time VME bus backoff time when the source is VME
 * \param pci_block_size PCI/X bus block size when the source is PCI
 * \param pci_backoff_time PCI bus backoff time when the source is PCI
 *
 */
struct vme_dma_ctrl {
	enum vme_dma_block_size	vme_block_size;
	enum vme_dma_backoff	vme_backoff_time;
	enum vme_dma_block_size	pci_block_size;
	enum vme_dma_backoff	pci_backoff_time;
};

/** \brief DMA transfer direction */
enum vme_dma_dir {
	VME_DMA_TO_DEVICE = 1,
	VME_DMA_FROM_DEVICE
};

/**
 * \brief VME DMA transfer descriptor
 * \param status Transfer status
 * \param length Transfer size in bytes
 * \param novmeinc Must be set to 1 when accessing a FIFO like device on the VME
 * \param dir Transfer direction
 * \param src Transfer source attributes
 * \param dst Transfer destination attributes
 * \param opt Transfer control
 *
 */
struct vme_dma {
	unsigned int		status;
	unsigned int		length;
	unsigned int		novmeinc;
	enum vme_dma_dir	dir;

	struct vme_dma_attr	src;
	struct vme_dma_attr	dst;

	struct vme_dma_ctrl	ctrl;
};

/**
 * \brief VME Bus Error
 * \param address Address of the bus error
 * \param am Address Modifier of the bus error
 */
struct vme_bus_error {
	__u64				address;
	enum vme_address_modifier	am;
};

/**
 * \brief VME Bus Error descriptor
 * \param error Address/AM of the bus error
 * \param valid Valid Flag: 0 -> no error, 1 -> error
 */
struct vme_bus_error_desc {
	struct vme_bus_error	error;
	int			valid;
};


/*! @name VME single access swapping policy
 *@{
 */
#define SINGLE_NO_SWAP          0
#define SINGLE_AUTO_SWAP        1
#define SINGLE_WORD_SWAP        2
#define SINGLE_BYTEWORD_SWAP    3
/*@}*/


/*! @name page qualifier
 *
 *@{
 */
#define VME_PG_SHARED  0x00
#define VME_PG_PRIVATE 0x02
/*@}*/


/**
 * \brief VME mapping attributes
 * \param iack VME IACK 0 -> IACK pages
 * \param rdpref VME read prefetch option 0 -> Disable
 * \param wrpost VME write posting option
 * \param swap VME swap options
 * \param sgmin page descriptor number returned by find_controller
 * \param dum -- dum[0] page qualifier (shared/private), dum[1] XPC ADP-type
 *               dum[2] - reserved, _must_ be 0.
 *
 * This structure is used for the find_controller() and return_controller()
 * LynxOS CES driver emulation.
 */
struct pdparam_master {
	unsigned long iack;
	unsigned long rdpref;
	unsigned long wrpost;
	unsigned long swap;
	unsigned long sgmin;
	unsigned long dum[3];
};



/**
 * \name Window management ioctl numbers
 * \{
 */
/** Get a physical window attributes */
#define VME_IOCTL_GET_WINDOW_ATTR _IOWR('V', 0, struct vme_mapping)
/** Create a physical window */
#define VME_IOCTL_CREATE_WINDOW _IOW('V', 1, struct vme_mapping)
/** Destroy a physical window */
#define VME_IOCTL_DESTROY_WINDOW _IOW('V', 2, int)
/** Create a mapping over a physical window */
#define VME_IOCTL_FIND_MAPPING _IOWR('V', 3, struct vme_mapping)
/** Remove a mapping */
#define VME_IOCTL_RELEASE_MAPPING _IOW('V', 4, struct vme_mapping)
/** Get the create on find failed flag */
#define VME_IOCTL_GET_CREATE_ON_FIND_FAIL _IOR('V', 5, unsigned int)
/** Set the create on find failed flag */
#define VME_IOCTL_SET_CREATE_ON_FIND_FAIL _IOW('V', 6, unsigned int)
/** Get the destroy on remove flag */
#define VME_IOCTL_GET_DESTROY_ON_REMOVE _IOR('V', 7, unsigned int)
/** Set the destroy on remove flag */
#define VME_IOCTL_SET_DESTROY_ON_REMOVE _IOW('V', 8, unsigned int)
/** Get bus error status -- DEPRECATED */
#define VME_IOCTL_GET_BUS_ERROR _IOR('V', 9, unsigned int)
/** Check (and possibly clear) the bus error status */
#define VME_IOCTL_CHECK_CLEAR_BUS_ERROR _IOWR('V', 10, \
					      struct vme_bus_error_desc)
/* \}*/

/**
 * DMA ioctls
 * \{
 */
/** Start a DMA transfer */
#define VME_IOCTL_START_DMA _IOWR('V', 10, struct vme_dma)
/* \}*/


enum vme_specification {
	VME_SPEC_UNKNOWN = 0,
	VME_SPEC_VME64,
	VME_SPEC_VME64x,
};

#define VME_DEV_VME64_SIZE (0x80)
#define VME_DEV_VME64x_SIZE (0x1000)

#define VME_DEV_CR_SIZE 4096
#define VME_DEV_CSR_SIZE 1024
#define VME_DEV_CSR_START 0x7FC00
#define VME_DEV_CSR_END (0x7FC00 + VME_DEV_CSR_SIZE)
#define VME_DEV_CRCSR_SIZE 0x80000

#define VME_CR_FUNCTION_MAX (8)
#define VME_SLOT_MIN 1
#define VME_SLOT_MAX 21

#define VME_IRQ_LEVEL_MIN 1
#define VME_IRQ_LEVEL_MAX 7

#ifdef __KERNEL__

#define VME_CRCSR_SIZE 0x80000
#define VME_CR_FUNCTION_MAX (8)

#define VME_DEV_VME64 BIT(2)
#define VME_DEV_VME64x BIT(3)

enum vme_cr_areas {
	VME_CR_AREA_CR = 0,
	VME_CR_AREA_CR_USER,
	VME_CR_AREA_CRAM,
	VME_CR_AREA_CSR_USER,
	VME_CR_AREA_SN,
	VME_CR_AREA_MAX,
};

/**
 * It describes a memory area in the CR (static) space
 * @name area's name
 * @beg area beginning
 * @end area ending
 * @data area binary data
 * @repr custom representation of the binary data
 */
struct vme_cr_area {
	char name[16];
	uint32_t offset_beg;
	uint32_t offset_end;
	void *data;
	void *repr;
};

/**
 *
 */
struct vme_cr {
	/* VME 64 */
	uint8_t checksum;
	uint32_t rom_len;
	uint8_t rom_dw;
	uint8_t csr_dw;
	uint8_t spec;
	uint8_t c;
	uint8_t r;
	uint32_t manufacturer;
	uint32_t device;
	uint32_t revision;
	uint32_t string_ptr;
	uint32_t program_id;
	/* VME 64x */
	uint32_t beg_user_cr;
	uint32_t end_user_cr;
	uint32_t beg_cram;
	uint32_t end_cram;
	uint32_t beg_user_csr;
	uint32_t end_user_csr;
	uint32_t beg_sn;
	uint32_t end_sn;
	uint8_t slave_par;
	uint8_t slave_par_usr;
	uint8_t master_par;
	uint8_t master_par_usr;
	uint8_t irq_handler_cap;
	uint8_t irq_cap;
	uint8_t cram_dw;
	uint8_t func_dw[VME_CR_FUNCTION_MAX];
	uint32_t func_adem[VME_CR_FUNCTION_MAX];
	unsigned long func_amcap[VME_CR_FUNCTION_MAX][BITS_TO_LONGS(64)];
	unsigned long func_xamcap[VME_CR_FUNCTION_MAX][BITS_TO_LONGS(256)];
	uint8_t master_dw;
	unsigned long master_amcap[BITS_TO_LONGS(64)];
	unsigned long master_xamcap[BITS_TO_LONGS(256)];

};

/**
 *
 */
struct vme_csr {
	uint8_t bar;
	uint8_t bit_set;
	uint8_t bit_clr;
	uint8_t cram_owner;
	uint8_t bit_set_usr;
	uint8_t bit_clr_usr;
	uint8_t reserved[2];
	uint32_t ader[VME_CR_FUNCTION_MAX];
};


#define VME_CSR_BIT_RESET BIT(7)
#define VME_CSR_BIT_SYSFAIL BIT(6)
#define VME_CSR_BIT_ENABLE BIT(4)

#define VME_CSR_ADEM_EFM BIT(0)
#define VME_CSR_ADEM_EFD BIT(1)
#define VME_CSR_ADEM_DFS BIT(2)
#define VME_CSR_ADEM_FAF BIT(3)
#define VME_CSR_ADEM_MASK(adem) (adem & 0xFFFFFF00)
#define VME_CSR_ADER_XAM_MODE BIT(0)
#define VME_CSR_ADER_ADDR(ader) ((ader & VME_CSR_ADER_XAM_MODE) ? \
				 ader & 0xFFFFFC00 :      \
				 ader & 0xFFFFFF00)
#define VME_CSR_ADER_AM(ader) ((ader >> 2) & 0x3F)
#define VME_CSR_ADER_XAM(ader) ((ader >> 2) & 0xFF)

#define VME_CSR_BIT_CLR (0x7FFF4)
#define VME_CSR_BIT_SET (0x7FFF8)
#define VME_CSR_USR_BIT_CLR (0x7FFE8)
#define VME_CSR_USR_BIT_SET (0x7FFEC)


/**
 * struct vme_dev
 *
 * @map window mapping for the function
 * @resource VME64x function memory regions
 * @res_attr sysfs file for resources
 */
struct vme_dev {
	struct device dev;
	struct vme_device_id devid;
	enum vme_specification spec;
	unsigned long flags;
	unsigned int id;
	unsigned int slot;
	unsigned int irq;
	unsigned int irq_vector;
	unsigned int irq_level;
	unsigned int am;
	unsigned int base_address;
	unsigned int size;

	struct vme_mapping map_cr;

	struct vme_dma_ctrl dma_param;
	struct vme_cr_area spaces[VME_CR_AREA_MAX];

	struct vme_mapping map[VME_CR_FUNCTION_MAX];
	struct resource resource[VME_CR_FUNCTION_MAX];
	struct bin_attribute *res_attr[VME_CR_FUNCTION_MAX];
};

#define to_vme_dev(x) container_of((x), struct vme_dev, dev)

static inline resource_size_t vme_resource_start(struct vme_dev *vdev,
						 unsigned int func)
{
	return vdev->resource[func].start;
}

static inline resource_size_t vme_resource_end(struct vme_dev *vdev,
					       unsigned int func)
{
	return vdev->resource[func].end;
}

static inline unsigned long vme_resource_flags(struct vme_dev *vdev,
					       unsigned int func)
{
	return vdev->resource[func].flags;
}

static inline resource_size_t vme_resource_len(struct vme_dev *vdev,
					       unsigned int func)
{
	if (vme_resource_start(vdev, func) == 0 &&
	    vme_resource_end(vdev, func) == vme_resource_start(vdev, func)) {
		return 0;
	}

	return vme_resource_end(vdev, func) -
	       vme_resource_start(vdev, func) +
		1;
}

#define VME_DEV_FLAG_DRIVER_REG BIT(0)
#define VME_DEV_FLAG_FORCE_DMA_CFG BIT(1)


/*
 * Those definitions are for drivers only and are not visible to userspace.
 */

/**
 * struct vme_driver - VME driver descriptor
 * @id_table list terminater by all 0s entry
 */
struct vme_driver {
	int (*match)(struct device *dev, unsigned int n);
	int (*probe)(struct device *dev, unsigned int n);
	int (*remove)(struct device *dev, unsigned int n);
	void (*shutdown)(struct device *dev, unsigned int n);
	int (*suspend)(struct device *dev, unsigned int n, pm_message_t pm);
	int (*resume)(struct device *dev, unsigned int n);
	struct device_driver driver;
	const struct vme_device_id *id_table;
};

#define to_vme_driver(x) container_of((x), struct vme_driver, driver)

typedef void (*vme_berr_handler_t)(struct vme_bus_error *);

/* API for new drivers */
extern int vme_register_device(struct vme_dev *vme_dev);
extern void vme_unregister_device(struct vme_dev *vme_dev);
extern int vme_register_driver(struct vme_driver *vme_driver,
			       unsigned int ndev);
extern void vme_unregister_driver(struct vme_driver *vme_driver);
extern int vme_request_irq(unsigned int vec, int (*handler)(void *),
			   void *arg, const char *name);
extern int vme_free_irq(unsigned int vec);
extern int vme_generate_interrupt(int level, int vector, signed long msecs);
extern int vme_device_irq_vector_and_level_set(struct device *dev,
					       unsigned int vect,
					       unsigned int lev);
extern int vme_device_slot_set(struct device *dev, unsigned int slot);

extern int vme_get_window_attr(struct vme_mapping *desc);
extern int vme_create_window(struct vme_mapping *desc);
extern int vme_destroy_window(int window_num);
extern int vme_find_mapping(struct vme_mapping *match, int force);
extern struct resource *vme_window_resource_get(unsigned int window_num);
extern int vme_release_mapping(struct vme_mapping *desc, int force);

extern int vme_do_dma(struct vme_dma *desc);
extern int vme_do_dma_kernel(struct vme_dma *desc);


extern int vme_bus_error_check(int clear);
extern struct vme_berr_handler
*vme_register_berr_handler(struct vme_bus_error *error, size_t size,
			   vme_berr_handler_t func);
extern void vme_unregister_berr_handler(struct vme_berr_handler *handler);

extern int vme_csr_enable(struct vme_dev *vdev, unsigned int enable);
extern int vme_csr_reset(struct vme_dev *vdev, unsigned int enable);
extern int vme_csr_sysfail(struct vme_dev *vdev, unsigned int enable);

static inline int vme_enable_device(struct vme_dev *vdev)
{
	return vme_csr_enable(vdev, 1);
}

static inline int vme_disable_device(struct vme_dev *vdev)
{
	return vme_csr_enable(vdev, 0);
}

#endif /* __KERNEL__ */


#endif /* _VME_H */
