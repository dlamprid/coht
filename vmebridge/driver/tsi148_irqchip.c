// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * tsi148.h - Low level support for the Tundra TSI148 PCI-VME Bridge Chip
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/version.h>
#include <linux/irq.h>
#include <linux/irqdomain.h>
#include "vme_bridge.h"


static int tsi148_dbg_info(struct seq_file *s, void *offset)
{
	struct vme_bridge_device *vbridge = s->private;
	int i;

	seq_printf(s, "%s:\n",dev_name(&vbridge->pdev->dev));

	seq_printf(s, "  redirect: %d\n", vbridge->irq);
	seq_printf(s, "  irq-mapping:\n");
	for (i = 0; i < VME_NUM_VECTORS; ++i) {
		seq_printf(s, "    - hardware: %d\n", i);
		seq_printf(s, "      linux: %d\n",
			   irq_find_mapping(vbridge->domain, i));
	}

	return 0;
}

static int tsi148_dbg_info_open(struct inode *inode, struct file *file)
{
	struct vme_bridge_device *vbridge = inode->i_private;

	return single_open(file, tsi148_dbg_info, vbridge);
}

static const struct file_operations tsi148_dbg_info_ops = {
	.owner = THIS_MODULE,
	.open  = tsi148_dbg_info_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};


/**
 * It initializes the debugfs interface
 * @vbridge: IRQ controler instance
 *
 * Return: 0 on success, otherwise a negative error number
 */
static int tsi148_debug_init(struct vme_bridge_device *vbridge)
{
	vbridge->dbg_dir = debugfs_create_dir(dev_name(&vbridge->pdev->dev), NULL);
	if (IS_ERR_OR_NULL(vbridge->dbg_dir)) {
		dev_err(&vbridge->pdev->dev,
			"Cannot create debugfs directory (%ld)\n",
			PTR_ERR(vbridge->dbg_dir));
		return PTR_ERR(vbridge->dbg_dir);
	}

	vbridge->dbg_info = debugfs_create_file(VBRIDGE_DBG_INFO_NAME, 0444,
					     vbridge->dbg_dir, vbridge,
					     &tsi148_dbg_info_ops);
	if (IS_ERR_OR_NULL(vbridge->dbg_info)) {
		dev_err(&vbridge->pdev->dev,
			"Cannot create debugfs file \"%s\" (%ld)\n",
			VBRIDGE_DBG_INFO_NAME, PTR_ERR(vbridge->dbg_info));
		return PTR_ERR(vbridge->dbg_info);
	}

	return 0;
}

/**
 * It removes the debugfs interface
 * @vbridge: IRQ controler instance
 */
static void tsi148_debug_exit(struct vme_bridge_device *vbridge)
{
	if (vbridge->dbg_dir)
		debugfs_remove_recursive(vbridge->dbg_dir);
}

/**
 * It unmask (a.k.a. enable) an interrupt line
 */
void tsi148_irq_unmask(struct irq_data *data)
{
	struct vme_bridge_device *vbridge = data->domain->host_data;

	set_bit(data->hwirq, vbridge->irq_mask);
}

/**
 * It mask (a.k.a. disable) an interrupt line
 */
void tsi148_irq_mask(struct irq_data *data)
{
	struct vme_bridge_device *vbridge = data->domain->host_data;

	clear_bit(data->hwirq, vbridge->irq_mask);
}

/**
 * It acknowledge an interrupt
 */
void tsi148_irq_ack(struct irq_data *data)
{
}

/*
 * IRQ Chip for Tundra TSI148 devices.
 */
static struct irq_chip tsi148_chip = {
	.name			= "TSI148",
	.irq_unmask		= tsi148_irq_unmask,
	.irq_mask		= tsi148_irq_mask,
	.irq_ack		= tsi148_irq_ack,
};


static int tsi148_irq_domain_map(struct irq_domain *h,
				 unsigned int virtirq,
				 irq_hw_number_t hwirq)
{
	struct vme_bridge_device *vbridge = h->host_data;

	irq_set_chip(virtirq, &tsi148_chip);
	__irq_set_handler(virtirq, handle_edge_irq, 0, NULL);
	irq_set_chip_data(virtirq, vbridge);
	/* all handlers are directly nested to the VME bridge one */
	irq_set_nested_thread(virtirq, 1);

	/*
	 * It MUST be no-thread (nested) because the VME handler need to have
	 * control over the handler execution in order to properly use
	 * DMA-pause-restart. Here the VME bridge handler is threaded, so in
	 * cascade will run within this thread
	 */

	return 0;
}

static struct irq_domain_ops tsi148_irq_domain_ops = {
	.map = tsi148_irq_domain_map,
};


/**
 * Configure the TSI148 to be linked to the standard PCI irq controller
 */
int tsi148_irq_domain_create(struct vme_bridge_device *vbridge)
{
	unsigned int irq;
	int i, err;

	vbridge->domain = irq_domain_add_linear(NULL, VME_NUM_VECTORS,
						&tsi148_irq_domain_ops, vbridge);
	if (!vbridge->domain)
		return -ENOMEM;

	/* Maps HW interrupt number with Linux IRQ number*/
	for (i = 0; i < VME_NUM_VECTORS; ++i) {
		irq = irq_create_mapping(vbridge->domain, i);
		if (!irq) {
			err = -EPERM;
			goto out;
		}
		if (i == 0)
			vbridge->base_irq = irq;
	}
	/* Disable all interrupts */
	bitmap_clear(vbridge->irq_mask, 0, VME_NUM_VECTORS);

	err = request_irq(vbridge->irq, vme_bridge_interrupt, 0,
			  "vmebus", vbridge);
	if (err)
		goto out;

	tsi148_debug_init(vbridge);

	return 0;

out:
	irq_domain_remove(vbridge->domain);
	vbridge->domain = NULL;
	return err;
}

void tsi148_irq_domain_destroy(struct vme_bridge_device *vbridge)
{
	int i;

	tsi148_debug_exit(vbridge);

	for (i = 0; i < VME_NUM_VECTORS; ++i)
		irq_dispose_mapping(irq_find_mapping(vbridge->domain, i));

	/* Disable all interrupts */
	bitmap_clear(vbridge->irq_mask, 0, VME_NUM_VECTORS);
	irq_domain_remove(vbridge->domain);
	vbridge->domain = NULL;

	free_irq(vbridge->irq, vbridge);
}
