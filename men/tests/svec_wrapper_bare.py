# Author Adam Wujek, CERN 2017

import pytest
import time
from test_config import *
from vme_wrapper_helpers import *
from svec_common import *

vme = vme_wrapper()

# ------------------ CR/CSR -----------------
def test_check_CR_signature_from_CR_CSR():
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x1c) & 0xff) == 0x43
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x20) & 0xff) == 0x52

def test_svec_create_random_file():
    check_return_code(*exec_cmd("dd if=/dev/urandom of=" + RANDOM_FILE + " bs=" + RANDOM_SIZE + " count=1"))

# ------------------ A24D16 -----------------
class Test_svec_A24D16(object):
    def test_set_A24D16_mode(self):
        vme.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)

    # using addr_fixtureD16-size_fixtureD16 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD16-addr_fixtureD16 data is replaced at evert address change)
    def test_rw(self, size_fixtureD16, addr_fixtureD16):
        if addr_fixtureD16 + size_fixtureD16 > SVEC_MEM_SIZE:
            size_fixtureD16 = SVEC_MEM_SIZE - addr_fixtureD16
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD16)) + " size " + str(hex(size_fixtureD16)))
        vme.write(A24D16, addr = SVEC_A24_ADDR + addr_fixtureD16, size = size_fixtureD16, ffile = RANDOM_FILE, verify = True)

    def test_rw_long(self):
        # long test
        vme.write(A24D16, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10)

# ------------------ A24D16 single-accesses+DMA -----------------
class Test_svec_A24D16_dma(object):
    def test_set_A24D16_mode(self):
        vme.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)

    # using addr_fixtureD16-size_fixtureD16 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD16-addr_fixtureD16 data is replaced at evert address change)
    def test_rw(self, size_fixtureD16_blt, addr_fixtureD16_blt):
        if addr_fixtureD16_blt + size_fixtureD16_blt > SVEC_MEM_SIZE:
            size_fixtureD16_blt = SVEC_MEM_SIZE - addr_fixtureD16_blt
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD16_blt)) + " size " + str(hex(size_fixtureD16_blt)))
        vme.write(A24D16, addr = SVEC_A24_ADDR + addr_fixtureD16_blt, size = size_fixtureD16_blt, ffile = RANDOM_FILE, verify = True, dma = True)

    def test_rw_long(self):
        # long test
        vme.write(A24D16, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10, dma = True)

# ------------------ A24D16_blt -----------------
class Test_svec_A24D16_blt(object):
    def test_set_A24D16blt_mode(self):
        vme.slave_set_space(SVEC_slot, A24D16_BLT, SVEC_A24_ADDR)

    # using addr_fixtureD16_blt-size_fixtureD16_blt is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD16_blt-addr_fixtureD16_blt data is replaced at evert address change)
    def test_rw(self, size_fixtureD16_blt, addr_fixtureD16_blt):
        # a25 does not support blt transfers when size or data % 4 == 2
        if (size_fixtureD16_blt % 4 == 2):
            pytest.skip('DMA does not support 2 byte transfers')
        if (addr_fixtureD16_blt % 4 == 2):
            pytest.skip('DMA does not support transfers from addresses not aligned to 4')
        if addr_fixtureD16_blt + size_fixtureD16_blt > SVEC_MEM_SIZE:
            size_fixtureD16_blt = SVEC_MEM_SIZE - addr_fixtureD16_blt
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD16_blt)) + " size " + str(hex(size_fixtureD16_blt)))
        vme.write(A24D16_BLT, addr = SVEC_A24_ADDR + addr_fixtureD16_blt, size = size_fixtureD16_blt, ffile = RANDOM_FILE, verify = True)

    def test_rw_long(self):
        # long test
        vme.write(A24D16_BLT, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10)

# ------------------ A24D32 -----------------
class Test_svec_A24D32(object):
    def test_set_A24D32_mode(self):
        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A24D32, addr = SVEC_A24_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True)
 
    def test_rw_long(self):
        # long test
        vme.write(A24D32, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10)

# ------------------ A24D32 single-accesses+DMA -----------------
class Test_svec_A24D32_dma(object):
    def test_set_A24D32_mode(self):
        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A24D32, addr = SVEC_A24_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True, dma = True)
 
    def test_rw_long(self):
        # long test
        vme.write(A24D32, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10, dma = True)

# ------------------ A24D32_blt -----------------
class Test_svec_A24D32_blt(object):
    def test_set_A24D32blt_mode(self):
        vme.slave_set_space(SVEC_slot, A24D16_BLT, SVEC_A24_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A24D32_BLT, addr = SVEC_A24_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True)

    def test_rw_long(self):
        # long test
        vme.write(A24D32_BLT, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10)

# ------------------ A32D64_mblt -----------------
class Test_svec_A24D64_mblt(object):
    def test_set_A24D64mblt_mode(self):
        vme.slave_set_space(SVEC_slot, A24D64_MBLT, SVEC_A24_ADDR)

    # using addr_fixtureD64-size_fixtureD64 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD64-addr_fixtureD64 data is replaced at evert address change)
    def test_rw(self, size_fixtureD64, addr_fixtureD64):
        if addr_fixtureD64 + size_fixtureD64 > SVEC_MEM_SIZE:
            size_fixtureD64 = SVEC_MEM_SIZE - addr_fixtureD64
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr_fixtureD64)) + " size " + str(hex(size_fixtureD64)))
        vme.write(A24D64_MBLT, addr = SVEC_A24_ADDR + addr_fixtureD64, size = size_fixtureD64, ffile = RANDOM_FILE, verify = True)

    # repeat 100 times the test
    def test_rw_long(self):
        vme.write(A24D64_MBLT, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 100)

# ------------------ A32D32 -----------------
class Test_svec_A32D32(object):
    def test_set_A32D32_mode(self):
        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A32_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A32D32, addr = SVEC_A32_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True)

    def test_rw_long(self):
        ## long test
        vme.write(A32D32, addr = SVEC_A32_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10)

# ------------------ A32D32 single-accesses+DMA -----------------
class Test_svec_A32D32_dma(object):
    def test_set_A32D32_mode(self):
        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A32_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A32D32, addr = SVEC_A32_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True, dma = True)

    def test_rw_long(self):
        ## long test
        vme.write(A32D32, addr = SVEC_A32_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 10, dma = True)

# ------------------ A32D32_blt -----------------
class Test_svec_A32D32_blt(object):
    def test_set_A32D32blt_mode(self):
        vme.slave_set_space(SVEC_slot, A32D32_BLT, SVEC_A32_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at evert address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        if addr_fixtureD32 + size_fixtureD32 > SVEC_MEM_SIZE:
            size_fixtureD32 = SVEC_MEM_SIZE - addr_fixtureD32
        print_cmd ("address " + str(hex(SVEC_A32_ADDR + addr_fixtureD32)) + " size " + str(hex(size_fixtureD32)))
        vme.write(A32D32_BLT, addr = SVEC_A32_ADDR + addr_fixtureD32, size = size_fixtureD32, ffile = RANDOM_FILE, verify = True)

    # repeat 100 times the test
    def test_rw_long(self):
        ## long test
        vme.write(A32D32_BLT, addr = SVEC_A32_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 100)

# ------------------ A32D64_mblt -----------------
class Test_svec_A32D64_mblt(object):
    def test_set_A32D64mblt_mode(self):
        vme.slave_set_space(SVEC_slot, A32D64_MBLT, SVEC_A32_ADDR)

    # using addr_fixtureD64-size_fixtureD64 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD64-addr_fixtureD64 data is replaced at evert address change)
    def test_rw(self, size_fixtureD64, addr_fixtureD64):
        if addr_fixtureD64 + size_fixtureD64 > SVEC_MEM_SIZE:
            size_fixtureD64 = SVEC_MEM_SIZE - addr_fixtureD64
        print_cmd ("address " + str(hex(SVEC_A32_ADDR + addr_fixtureD64)) + " size " + str(hex(size_fixtureD64)))
        vme.write(A32D64_MBLT, addr = SVEC_A32_ADDR + addr_fixtureD64, size = size_fixtureD64, ffile = RANDOM_FILE, verify = True)

    # repeat 100 times the test
    def test_rw_long(self):
        vme.write(A32D64_MBLT, addr = SVEC_A32_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, retries = 100)

# ------------------ A24 cross_mode_rw -----------------
class Test_svec_A24_cross_mode_rw(object):
    def test_write_A24D32_read_A24D32_BLT(self, size_fixtureD32, addr_fixtureD32):
        # write data using A24D32 read with A24D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A24D32_BLT, SVEC_A24_ADDR)
        vme.read(A24D32_BLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_BLT_read_A24D32(self, size_fixtureD32, addr_fixtureD32):
        # write data using A24D32_BLT read with A24D32
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A24D32_BLT, SVEC_A24_ADDR)
        vme.write(A24D32_BLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme.read(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        # -s silent; -n compate n bytes
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_read_A24D64_MBLT(self, size_fixtureD64, addr_fixtureD64):
        # write data using A24D32 read with A24D64_MBLT
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A24D64_MBLT, SVEC_A24_ADDR)
        vme.read(A24D64_MBLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D64_MBLT_read_A24D32(self, size_fixtureD64, addr_fixtureD64):
        # write data using A24D64_MBLT read with A24D32
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A24D64_MBLT, SVEC_A24_ADDR)
        vme.write(A24D64_MBLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme.read(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

# ------------------ A32 cross_mode_rw -----------------
class Test_svec_A32_cross_mode_rw(object):
    def test_write_A32D32_read_A32D32_BLT(self, size_fixtureD32, addr_fixtureD32):
        # write data using A32D32 read with A32D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A32D32_BLT, SVEC_A32_ADDR)
        vme.read(A32D32_BLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D32_BLT_read_A32D32(self, size_fixtureD32, addr_fixtureD32):
        # write data using A32D32_BLT read with A32D32
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A32D32_BLT, SVEC_A32_ADDR)
        vme.write(A32D32_BLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme.read(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        # -s silent; -n compate n bytes
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D32_read_A32D64_MBLT(self, size_fixtureD64, addr_fixtureD64):
        # write data using A32D32 read with A32D64_MBLT
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A32D64_MBLT, SVEC_A32_ADDR)
        vme.read(A32D64_MBLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D64_MBLT_read_A32D32(self, size_fixtureD64, addr_fixtureD64):
        # write data using A32D64_MBLT read with A32D32
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme.slave_set_space(SVEC_slot, A32D64_MBLT, SVEC_A32_ADDR)
        vme.write(A32D64_MBLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme.read(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))


