// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright 2018 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <linux/kallsyms.h>
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/mm.h>

#include "vme_bridge.h"
#include "vme_compat.h"

long get_user_pages_l(unsigned long start, unsigned long nr_pages,
		      unsigned int gup_flags, struct page **pages,
		      struct vm_area_struct **vmas)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 8, 0)
	return get_user_pages(start, nr_pages, gup_flags, pages, NULL);
#else
	unsigned int rw = (gup_flags & FOLL_WRITE ? 1 : 0);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 6, 0)
	return get_user_pages(start, nr_pages, rw, 0, pages, NULL);
#else
	return get_user_pages(current, current->mm, start, nr_pages, rw, 0,
			      pages, NULL);
#endif
#endif
}

/**
 * NOTE this is a dirty hack. Our driver is a kernel module,
 * so it has not access to all the kernel features. One of this
 * feature is the 'insert_resource' function which allows to add
 * a new resource in the resource tree. Here we use kallsyms to get
 * the pointer to that function.
 */
int insert_resource_l(struct resource *parent,
		      struct resource *new)
{
	static int (*insert_resource_p)(struct resource *parent,
					struct resource *new);

	insert_resource_p = (void *) kallsyms_lookup_name("insert_resource");

	return insert_resource_p(parent, new);
}
