// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __VMEUTILS__
#define __VMEUTILS__

#include <stdarg.h>
#include <inttypes.h>

#include <vmebus.h>

#define VMEUTILS_VERSION "0.0.1"
#define VMEDEV_NAME_LEN 32

extern const char program_name[];

/* Derived from Linux "Features Test Macro" header
 * Convenience macros to test the versions of gcc (or
 * a compatible compiler).
 * Use them like this:
 *  #if GNUC_PREREQ (2,8)
 *   ... code requiring gcc 2.8 or later ...
 *  #endif
*/
#if defined(__GNUC__) && defined(__GNUC_MINOR__)
# define GNUC_PREREQ(maj, min) \
	((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
#else
 #define GNUC_PREREQ(maj, min) 0
#endif

/*
 * BUILD_ASSERT_OR_ZERO - assert a build-time dependency, as an expression.
 * @cond: the compile-time condition which must be true.
 *
 * Your compile will fail if the condition isn't true, or can't be evaluated
 * by the compiler.  This can be used in an expression: its value is "0".
 *
 * Example:
 *	#define foo_to_char(foo)					\
 *		 ((char *)(foo)						\
 *		  + BUILD_ASSERT_OR_ZERO(offsetof(struct foo, string) == 0))
 */
#define BUILD_ASSERT_OR_ZERO(cond) \
	(sizeof(char [1 - 2*!(cond)]) - 1)

#if GNUC_PREREQ(3, 1)
 /* &arr[0] degrades to a pointer: a different type from an array */
# define BARF_UNLESS_AN_ARRAY(arr)						\
	BUILD_ASSERT_OR_ZERO(!__builtin_types_compatible_p(__typeof__(arr), \
							   __typeof__(&(arr)[0])))
#else
# define BARF_UNLESS_AN_ARRAY(arr) 0
#endif
/*
 * ARRAY_SIZE - get the number of elements in a visible array
 *  <at> x: the array whose size you want.
 *
 * This does not work on pointers, or arrays declared as [], or
 * function parameters.  With correct compiler support, such usage
 * will cause a build error (see the build_assert_or_zero macro).
 */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]) + BARF_UNLESS_AN_ARRAY(x))

/* Derived from Linux "Features Test Macro" header
 * Convenience macros to test the versions of gcc (or
 * a compatible compiler).
 * Use them like this:
 *  #if GNUC_PREREQ (2,8)
 *   ... code requiring gcc 2.8 or later ...
 *  #endif
*/
#if defined(__GNUC__) && defined(__GNUC_MINOR__)
# define GNUC_PREREQ(maj, min) \
	((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
#else
 #define GNUC_PREREQ(maj, min) 0
#endif

/*
 * BUILD_ASSERT_OR_ZERO - assert a build-time dependency, as an expression.
 * @cond: the compile-time condition which must be true.
 *
 * Your compile will fail if the condition isn't true, or can't be evaluated
 * by the compiler.  This can be used in an expression: its value is "0".
 *
 * Example:
 *	#define foo_to_char(foo)					\
 *		 ((char *)(foo)						\
 *		  + BUILD_ASSERT_OR_ZERO(offsetof(struct foo, string) == 0))
 */
#define BUILD_ASSERT_OR_ZERO(cond) \
	(sizeof(char [1 - 2*!(cond)]) - 1)

#if GNUC_PREREQ(3, 1)
 /* &arr[0] degrades to a pointer: a different type from an array */
# define BARF_UNLESS_AN_ARRAY(arr)						\
	BUILD_ASSERT_OR_ZERO(!__builtin_types_compatible_p(__typeof__(arr), \
							   __typeof__(&(arr)[0])))
#else
# define BARF_UNLESS_AN_ARRAY(arr) 0
#endif
/*
 * ARRAY_SIZE - get the number of elements in a visible array
 *  <at> x: the array whose size you want.
 *
 * This does not work on pointers, or arrays declared as [], or
 * function parameters.  With correct compiler support, such usage
 * will cause a build error (see the build_assert_or_zero macro).
 */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]) + BARF_UNLESS_AN_ARRAY(x))

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/**
 * Command descriptor
 * @cmd: command name
 * @fn: command function
 */
struct cmd_struct {
	const char *cmd;
	int (*fn)(int argc, char *argv[]);
};

/**
 * VME device descriptor
 * @next next device in the chain
 * @access
 * @name card name
 * @spec specification identifier
 * @slot slot number
 * @cr_dump configuration rom
 * @csr_dump control status registers
 */
struct vme_dev {
	struct vme_dev *next;
	struct vme_access *access;
	char name[VMEDEV_NAME_LEN];

	enum vme_specification spec;
	uint8_t slot;

	uint8_t *cr_dump;
	uint8_t *csr_dump;
};

/**
 * It stores all information about VME devices and how to get them
 * @devices linked-list of devices
 */
struct vme_access {
	struct vme_dev *devices;
};

/**
 * It stores information useful to filter VME devices
 * @slot slot number
 */
struct vme_filter {
	int slot;
};

enum vme_errors {
	__VME_ERRORS_MIN = 4096,
	VME_ESRANGE = __VME_ERRORS_MIN,
	__VME_ERRORS_MAX,
};

extern void vme_generic_error(const char *msg, ...);
extern void command_help(int argc, char *argv[], const char *help_msg);
extern void command_version(int argc, char *argv[]);
extern const char *vme_spec_to_string(enum vme_specification spec);
extern struct vme_access *vme_access_alloc(void);
extern void vme_access_cleanup(struct vme_access *a);
extern void vme_sysfs_scan_bus(struct vme_access *a, struct vme_filter *f);
extern int vme_sysfs_get_value(struct vme_dev *d, char *object);
extern void vme_sysfs_set_value(struct vme_dev *d, char *object,
				int val, char *str);
extern void vme_device_enable(struct vme_dev *d, unsigned int enable);
extern const char *vme_strerror(int err);

/* Commands */
extern int cmd_scan(int argc, char *argv[]);
extern int cmd_module(int argc, char *argv[]);
extern int cmd_register(int argc, char *argv[]);
#endif
