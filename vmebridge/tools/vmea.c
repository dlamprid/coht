/*
 * Author: Adam Wujek at CERN 2017
 * Tool based on vme4l_rwex from MEN
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>
#include <libvmebus.h>
#include <inttypes.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define OPT_WRITE 1
#define OPT_READ 2

#define CHK(expression)							\
	if( !(expression)) {						\
		printf("\n*** Error during: %s\nfile %s\nline %d\n",	\
			   #expression,__FILE__,__LINE__);		\
		printf("%s\n",strerror(errno));				\
		exit(1);						\
	}

#define _1G 	1000000000 		/* for ns->s */
#define _1MB 	(1*1024*1024)		/* for MB/s */

#define VERIFY_FILE_POSTFIX ".bad"

static char git_version[] = "git_version: " GIT_VERSION;

static void print_version(void);
static void MemDump(uint8_t *buf, uint32_t n, uint32_t fmt);

static void usage(char *prog)
{
	fprintf(stderr,
		"usage: %s [vme_addr] [size] [options]\n\n", prog);
	fprintf(stderr,
		"This tool can read/write at a specific VME address\n\n");
	fprintf(stderr,
		"Options\n");
	fprintf(stderr,
		"  -a 0x<NUM> : address modifier for VME bus (default 0x09)\n");
	fprintf(stderr,
		"  -w <NUM>   : write mode with a value to write\n");
	fprintf(stderr,
		"  -r         : read mode\n");
	fprintf(stderr,
		"  -d <NUM>   : data width in bits (default 32 bits)\n");
	fprintf(stderr,
		"  -n <NUM>   : nr. of runs reading/writing is done (default 1)\n");
	fprintf(stderr,
		"               (shown MB/s is average of all runs)\n");
	fprintf(stderr,
		"  -M         : enable DMA\n");
	fprintf(stderr,
		"  -m         : mmap (not used)\n");
	fprintf(stderr,
		"  -f <file>  : with -r dump binary data into a file\n");
	fprintf(stderr,
		"             : with -w read binary data from a file\n");
	fprintf(stderr,
		"  -e         : don't rewind a file after each read/write (only with -n option)\n");
	fprintf(stderr,
		"  -i         : FIFO mode, don't increment VME address\n");

	fprintf(stderr,
		"  -h         : print this help\n");
	fprintf(stderr,
		"  -x         : don't dump read data\n");
	fprintf(stderr,
		"  -V         : version\n");
	fprintf(stderr, "\n");
	fprintf(stderr,
		"Data Width\n");
	fprintf(stderr,
		"The supported data widths are 8, 16 and 32 for the access\n"
		"data width; 16 and 32 for the data width\n");
	fprintf(stderr, "\n");
	fprintf(stderr,
		"Address Modifier\n");
	fprintf(stderr,
		"The supported address modifier are the following:\n");
	fprintf(stderr, "  0x09 A32 user data\n");
	fprintf(stderr, "  0x29 A16 user data\n");
	fprintf(stderr, "  0x39 A24 user data\n");
	fprintf(stderr, "\n");
	print_version();
	exit(1);
}

static void print_version(void)
{
	fprintf(stderr, "%s\n", git_version);
	fprintf(stderr, "%s\n", libvmebus_version_s);
}

static int is_invalid_width(int data_width)
{
	return (data_width != 8 &&
		data_width != 16 &&
		data_width != 32 &&
		data_width != 64);
}

static int vme_read(int opt_dma_mode, void *map, uint32_t size, void *buf, uint32_t vme_addr, struct vme_dma *op)
{
	int rv = 0;
	if (!opt_dma_mode) {
		memcpy(buf, map, size);
		rv = size;
	} else {
		op->dir = VME_DMA_FROM_DEVICE;
		op->src.addrl = vme_addr;
		op->src.addru = 0;
		op->dst.addrl = (uint32_t)(uintptr_t) buf;
		op->dst.addru = (uint32_t)(((uintptr_t)(buf)) >> 32);
		CHK( (rv = vme_dma_write(op)) == 0);
		rv = size;
	}
	return rv;
}

static int vme_write(int opt_dma_mode, void *map, uint32_t size, void *buf, uint32_t vme_addr, struct vme_dma *op)
{
	int rv = 0;
	if (!opt_dma_mode) {
		memcpy(map, buf, size);
		rv = size;
	} else {
		op->dir = VME_DMA_TO_DEVICE;
		op->src.addrl = (uint32_t)(uintptr_t) buf;
		op->src.addru = (uint32_t)(((uintptr_t)(buf)) >> 32);
		op->dst.addrl = vme_addr;
		op->dst.addru = 0;
		CHK( (rv = vme_dma_read(op)) == 0);
		rv = size;
	}
	return rv;
}

int main(int argc, char *argv[])
{
	uint32_t vme_addr = 0xffffffff;
	uint32_t size = 0xffffffff;
	struct vme_mapping map;
	struct vme_mapping *mapp = &map;
	volatile void *ptr;
	unsigned int vmebase, opt_am = 0, data_width;
	int i, j;
	int c;
	int opt_write;
	int width;
	int opt_dma_mode = 0;
	uint32_t word;
	uint32_t word_swap;

	int opt_runs = 1;
	int opt_verify_write = 0;
	int opt_disable_file_rewind = 0;
	int opt_dump = 1; /* dump by default */
	int opt_fifo = 0;

	uint32_t vme_addr_page = 0xffffffff;
	uint32_t map_offset = 0;
	uint32_t map_size = 0;
	int f_desc = -1;
	int f_ver_desc = -1;
	char *file_name=NULL;
	char *file_name_ver = NULL;
	struct stat st;
	size_t file_size = 0;
	void *buf = NULL;
	void *buf_ver = NULL;
	struct timespec t1, t2;
	int rv, ret;
	double transferRate=0.0, timePerRun=0.0, timeTotal=0.0;
	int pos;
	int return_global = 0;
	struct vme_dma op;

	data_width = VME_D32;

	opt_write = 0;
	while ((c = getopt(argc, argv, "v:d:a:n:w:hV:Mf:exrci")) != -1) {
		switch (c) {
		case 'v':
			vmebase = strtoul(optarg, NULL, 0);
			break;
		case 'd':
			width = strtoul(optarg, NULL, 0);
			if (is_invalid_width(width)) {
				fprintf(stderr, "invalid data width %d\n", width);
				exit(1);
			}
			data_width = width;
			break;
		case 'a':
			opt_am = strtoul(optarg, NULL, 0);
			break;
		case 'n':
			opt_runs = strtoul(optarg, NULL, 0);
			break;
		case 'w':
			opt_write = OPT_WRITE;
			if (optarg)
				word = strtoul(optarg, NULL, 0);
			break;
		case 'r':
			opt_write = OPT_READ;
			break;
		case '?':
		case 'h':
			usage(argv[0]);
			break;
		case 'V':
			print_version();
			exit(1);
		case 'M':
			opt_dma_mode = 1;
			break;
		case 'f':
			file_name = optarg;
			break;
		case 'e':
			opt_disable_file_rewind = 1;
			break;
		case 'x':
			opt_dump = 0;
			break;
		case 'c':
			opt_verify_write = 1;
			break;
		case 'i':
			opt_fifo = 1;
			break;
		default:
			break;
		}
	}

	if (optind + 2 > argc) { /* two extra params */
		printf("*** missing vmeaddr or size!\n");
		exit(1);
	}

	if (!opt_am) {
		printf("Address modifier not specified\n");
		exit(1);
	}

	if (opt_write != OPT_WRITE && opt_write != OPT_READ) {
		printf("Please specify read(-r)/write(-w) parameter\n");
		exit(1);

	}
	vme_addr = strtoul(argv[optind], NULL, 0);
	size = strtoul(argv[optind + 1], NULL, 0);
	printf ("%s vme_addr=0x%x size=0x%x\n", opt_write == OPT_WRITE?"Write":"Read", vme_addr, size);

	if (opt_runs > 1 && opt_disable_file_rewind) {
		printf("Don't rewind a file\n");
	}

	if (opt_fifo)
		printf("Use FIFO mode (don't increase VME addresses over block)\n");
	if (file_name) {
  		if (opt_write == OPT_READ) { /* read from VME */
			f_desc = open(file_name, O_CREAT | O_SYNC | O_TRUNC | O_WRONLY, 0666);
		} else if (opt_write == OPT_WRITE) { /* write to VME */
			f_desc = open(file_name, O_RDONLY);
		}
		if (!f_desc) {
			printf("Unable to open file %s\n", file_name);
			exit(1);
		}

		if (opt_write == OPT_WRITE) { /* write to VME */
			/* get file size */
			fstat(f_desc, &st);
			file_size = st.st_size;
			printf("file size %zu bytes\n", file_size);

			if (size == 0xffffffff) {
				size = file_size;
			}
		}
	}

	if (vme_addr == 0xffffffff || size == 0xffffffff ){
		printf("*** missing vmeaddr or size!\n");
		usage(argv[0]);
	}

	/* add a page to <size> in case start offset is > 0 */
	CHK( (buf = calloc(1, size)) != NULL);
	if (opt_verify_write) { /* allocate second buffer for write verification */
		CHK( (buf_ver = calloc(1, size)) != NULL);
	}

	if (opt_dma_mode) {
		memset(&op, 0x00, sizeof(struct vme_dma));
		op.novmeinc = opt_fifo;
		op.length = size;
		op.src.data_width = op.dst.data_width = data_width;
		op.src.am = op.dst.am = opt_am;
		op.ctrl.vme_block_size = VME_DMA_BSIZE_4096;
		op.ctrl.vme_backoff_time = VME_DMA_BACKOFF_0;
		op.ctrl.pci_block_size = VME_DMA_BSIZE_4096;
		op.ctrl.pci_backoff_time = VME_DMA_BACKOFF_0;
	} else {
		vme_addr_page = vme_addr & ~(getpagesize() - 1);
		map_offset = vme_addr - vme_addr_page;
		map_size = (map_offset + size);
		
		/* Prepare mmap description */
		memset(mapp, 0, sizeof(*mapp));
		mapp->am = 		opt_am;
		mapp->data_width = 	data_width; /*(data_width == 8) ? 16 : data_width;*/
		mapp->sizel = 		map_size;
		mapp->vme_addrl =	vme_addr_page; /* pass page aligned address */

		/* Do mmap */
		printf("Page aligned addr 0x%x, size 0x%x\n", vme_addr_page, map_size);
		ptr = vme_map(mapp, 1);
		if (!ptr) {
			printf("Could not map address 0x%08x, size 0x%x\n", vme_addr, size);
			exit(1);
		}

		printf("mapped %p\n", ptr);
	}

	if (f_desc > -1 && opt_write == OPT_WRITE) {
		if (opt_disable_file_rewind && (size * opt_runs > file_size)) {
			printf("File too small to feed %d runs! Transfer size %u (0x%"PRIx32"), file size %zu (0x%zx)\n",
			       opt_runs, size, size, file_size, file_size);
			exit(1);
		}
		if (!opt_disable_file_rewind && (size > file_size)) {
			printf("File too small! Transfer size %u (0x%"PRIx32"), file size %zu (0x%zx)\n",
			       size, size, file_size, file_size);
			exit(1);
		}

	}
	
	for (i = 1; i <= opt_runs; i++) {
		if (opt_write == OPT_READ) { /* read */
			/* measure time right before and after VME access without mem dumps */
			clock_gettime( CLOCK_MONOTONIC, &t1 );
			rv = vme_read(opt_dma_mode, (void*)(ptr + map_offset), size, buf, vme_addr, &op);
			clock_gettime( CLOCK_MONOTONIC, &t2 );

			if (opt_dump)
				MemDump( buf, rv, 1 );

			if (file_name) {
				ret = write(f_desc, buf, rv);
				if (ret != rv) {
					printf("Unable to write to a file %s (ret=%d)\n", file_name, ret);
					exit(1);
				}
			}
		} else { /* write */
			if (file_name) {
				ret = read(f_desc, buf, size);
				if (ret != size) {
					printf("Unable to read from a file %s (ret=%d)\n", file_name, ret);
					exit(1);
				}
			} else {
				uint8_t *p;
				p = buf;
				word_swap = htonl(word);
				memcpy(buf, ((uint8_t *) &word_swap) + 4 - data_width / 8, data_width / 8);

				if (word)
					p += data_width / 8;
				/* if filename is not specified fill buffer */
				for(j = 1; j < size - (data_width / 8) + 1; j++)
					*p++ = (j + word) & 0xff;
			}

			clock_gettime( CLOCK_MONOTONIC, &t1 );
			rv = vme_write(opt_dma_mode, (void*)(ptr + map_offset), size, buf, vme_addr, &op);
			clock_gettime( CLOCK_MONOTONIC, &t2 );

			if (opt_verify_write) {
				/* verify written data */
				rv = vme_read(opt_dma_mode, (void*)(ptr + map_offset), size, buf_ver, vme_addr, &op);
				if ((pos = memcmp(buf, buf_ver, size))) {
					printf("Error during write verification at the position %d.\n", abs(pos));
					return_global = 1;
					file_name_ver = calloc(1, strlen(file_name) + strlen(VERIFY_FILE_POSTFIX) + 1 + 9);
					if (!file_name_ver) {
						printf("Unable to allocate memory\n");
						exit(1);
					}
					if (opt_runs > 1)
						sprintf(file_name_ver, "%s" VERIFY_FILE_POSTFIX "%d", file_name, i);
					else
						sprintf(file_name_ver, "%s" VERIFY_FILE_POSTFIX, file_name);
					printf("Writing read data into a file %s.\n", file_name_ver);
					f_ver_desc = open(file_name_ver, O_CREAT | O_SYNC | O_TRUNC | O_WRONLY, 0666);
					if (!f_ver_desc ) {
						printf("Unable to open file %s\n", file_name_ver);
						exit(1);
					}
					/* write bad data */
					ret = write(f_ver_desc, buf_ver, rv);
					if (ret != rv) {
						printf("Unable to write to a file %s (ret=%d)\n", file_name_ver, ret);
						exit(1);
					}
					/* close descritor */
					if (f_ver_desc) {
						close(f_ver_desc);
					}
					free(file_name_ver);
					file_name_ver = NULL;
				} else {
					printf("Verification OK (%d bytes)\n", size);
				}
			}
		}

		if (buf)
			memset(buf, 0, size);
		if (buf_ver)
			memset(buf_ver, 0, size);

		if (f_desc > -1 && !opt_disable_file_rewind) {
			/* rewind the file if an option is set */
			lseek(f_desc, 0, SEEK_SET);
		}
		/* timeTotal(max) =  +1.7E+308, enough till eternity.. */
		timePerRun = (float)((t2.tv_sec - t1.tv_sec) * _1G + (t2.tv_nsec - t1.tv_nsec))/_1G;
		timeTotal += timePerRun;
	}

	transferRate = (((float)size * (float)opt_runs) / timeTotal) /_1MB;

	if (!opt_dma_mode) {
		vme_unmap(mapp, 1);
	}

	if (opt_verify_write && buf_ver) {
		free(buf_ver);
		buf_ver = NULL;
	}

	if (buf) {
		free(buf);
		buf = NULL;
	}

	printf("%d %s access%s finished. average Time: %.3f s => average transfer rate: %.3f MB/s\n",
		   opt_runs,
		   opt_write == OPT_READ ? "read" : "write",
		   (opt_runs > 1) ? "es" : "",
		   timeTotal / (float)opt_runs,
		   transferRate );


	return return_global;
}

static void MemDump(uint8_t *buf, uint32_t n, uint32_t fmt)
{
	uint8_t *k, *k0, *kmax = buf+n;
	int32_t i;
	printf("----\n");
	for (k=k0=buf; k0<kmax; k0+=16) {
		printf("%04x: ", (unsigned int)(k-buf));

		switch(fmt) {	/* dump hex: */
			case 4: /* long aligned */
				for (k=k0,i=0; i<16; i+=4, k+=4)
					if (k<kmax)  printf("%08"PRIx32" ",*(uint32_t*)k);
					else         printf("         ");
				break;
			case 2: /* word aligned */
				for (k=k0,i=0; i<16; i+=2, k+=2)
					if (k<kmax)  printf("%04x ",*(uint16_t*)k & 0xffff);
					else         printf("     ");
				break;
			default: /* byte aligned */
				for (k=k0,i=0; i<16; i++, k++)
					if (k<kmax)  printf("%02x ",*k & 0xff);
					else         printf("   ");
		}

		for (k=k0, i=0; i<16 && k<kmax; i++, k++)           /* dump ascii */
			if ( *(uint8_t*)k>=32 && *(uint8_t*)k<=127 )
				printf("%c", *k);
			else
				printf(".");

		printf("\n");
	}
	printf("----\n");
}
