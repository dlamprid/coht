import pytest
import os
import subprocess

class TestIrqAssignment(object):
    def test_sysfs_exist(self, vme_device_static):
        vdev = vme_device_static
        assert os.path.exists(os.path.join(vdev.sysfs_path(), "irq")) == True
        assert os.path.exists(os.path.join(vdev.sysfs_path(), "irq_level")) == True
        assert os.path.exists(os.path.join(vdev.sysfs_path(), "irq_vector")) == True
