// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_window.c - PCI-VME window management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge window management support:
 *
 *    - Window creation and deletion
 *    - Mapping creation and removal
 *    - Procfs interface to windows and mappings information
 */

#include <linux/list.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/fs.h>

#include "vmebus.h"
#include "vme_bridge.h"

#if 0
/*
 * Flag controlling whether to create a new window if a mapping cannot
 * be found.
 */
unsigned int vme_create_on_find_fail;
/*
 * Flag controlling whether removing the last mapping on a window should
 * also destroys the window.
 */
unsigned int vme_destroy_on_remove;
#endif

/**
 * vme_window_release() - release file method for the VME window device
 * @inode: Device inode
 * @file: Device file descriptor
 *
 *  The release method is in charge of releasing all the mappings made by
 * the process closing the device file.
 */
int vme_window_release(struct inode *inode, struct file *file)
{
	int i;
	struct window *window;
	struct mapping *mapping;
	struct mapping *tmp;

	for (i = 0; i < vbridge_gbl->wind_mgr.nr_winds; i++) {
		window = &vbridge_gbl->wind_mgr.winds[i];

		if (mutex_lock_interruptible(&window->lock))
			return -ERESTARTSYS;

		if ((!window->active) || (list_empty(&window->mappings)))
			goto try_next;

		list_for_each_entry_safe(mapping, tmp,
					 &window->mappings, list) {

			if (mapping->client.file == file) {
				/*
				 * OK, that mapping is held by the process
				 * release it.
				 */
				list_del(&mapping->list);
				kfree(mapping);
				window->users--;
			}
		}

try_next:
		mutex_unlock(&window->lock);
	}

	return 0;
}

/**
 * add_mapping() - Helper function to add a mapping to a window
 * @window: Window to add the mapping to
 * @desc: Mapping descriptor
 * @file: owner of the mapping (NULL if it comes from the kernel)
 *
 * It is assumed that the window mutex is held on entry to this function.
 */
static int
add_mapping(struct window *window, struct vme_mapping *desc, struct file *file)
{
	struct mapping *mapping;

	/* Create a logical mapping for this hardware window */
	mapping = kzalloc(sizeof(struct mapping), GFP_KERNEL);
	if (!mapping)
		return -ENOMEM;

	/* Save the window descriptor for this window. */
	memcpy(&mapping->desc, desc, sizeof(struct vme_mapping));

	/* Store the task's info only if the request comes from user-space */
	if (file) {
		mapping->client.file = file;
		mapping->client.pid_nr = task_pid_nr(current);
		strcpy(mapping->client.name, current->comm);
	} else {
		strcpy(mapping->client.name, "kernel");
	}

	/* Insert mapping at end of window mappings list */
	list_add_tail(&mapping->list, &window->mappings);

	/* Increment user count */
	window->users++;

	return 0;
}


/**
 * It resets the mapping descriptor
 * @desc descriptor to reset
 */
static void __mapping_descriptor_reset(struct vme_mapping *desc)
{
	desc->kernel_va = NULL;
	desc->pci_addru = 0;
	desc->pci_addrl = 0;
	desc->window_num = -1;
}


/**
 * remove_mapping() - Helper function to remove a mapping from a window
 * @window: Window to remove the mapping from
 * @desc: Mapping descriptor
 * @file: struct file of the owner of the mapping (NULL if the kernel owns it)
 *
 * The specified descriptor is searched into the window mapping list by
 * only matching the VME address, the size and the virtual address.
 *
 * It is assumed that the window mutex is held on entry to this function.
 */
static int remove_mapping(struct window *window, struct vme_mapping *desc,
			  struct file *file)
{
	struct mapping *mapping;
	struct mapping *tmp;

	list_for_each_entry_safe(mapping, tmp, &window->mappings, list) {

		if ((mapping->desc.vme_addru == desc->vme_addru) &&
		    (mapping->desc.vme_addrl == desc->vme_addrl) &&
		    (mapping->desc.sizeu == desc->sizeu) &&
		    (mapping->desc.sizel == desc->sizel) &&
		    (mapping->desc.kernel_va == desc->kernel_va) &&
		    (!file || mapping->client.file == file)) {
			/* Found the matching mapping */
			list_del(&mapping->list);
			kfree(mapping);
			window->users--;
			__mapping_descriptor_reset(desc);

			return 0;
		}
	}

	return -EINVAL;
}


/**
 * vme_get_window_attr() - Get a PCI-VME hardware window attributes
 * @desc: Contains the window number we're interested in
 *
 * Get the specified window attributes.
 *
 * This function is used by the VME_IOCTL_GET_WINDOW_ATTR ioctl from
 * user applications but can also be used by drivers stacked on top
 * of this one.
 *
 * Return 0 on success, or %EINVAL if the window number is out of bounds.
 */
int vme_get_window_attr(struct vme_mapping *desc)
{
	struct vme_bridge_window_mgr *wind_mgr = &vbridge_gbl->wind_mgr;
	int window_num = desc->window_num;

	if ((window_num < 0) || (window_num >= wind_mgr->nr_winds))
		return -EINVAL;

	if (mutex_lock_interruptible(&wind_mgr->winds[window_num].lock))
		return -ERESTARTSYS;

	memcpy(desc, &wind_mgr->winds[window_num].desc,
	       sizeof(struct vme_mapping));

	if (wind_mgr->ops->get_window_attr != NULL)
		wind_mgr->ops->get_window_attr(vbridge_gbl, desc);

	mutex_unlock(&wind_mgr->winds[window_num].lock);

	return 0;
}
EXPORT_SYMBOL_GPL(vme_get_window_attr);

/**
 * vme_create_window() - Create and map a PCI-VME window
 * @desc: Descriptor of the window to create
 *
 * Create and map a PCI-VME window according to the &struct vme_mapping
 * parameter.
 *
 * This function is used by the VME_IOCTL_CREATE_WINDOW ioctl from
 * user applications but can also be used by drivers stacked on top
 * of this one.
 *
 * Return 0 on success, or a standard kernel error code on failure.
 */
int vme_create_window(struct vme_mapping *desc)
{
	int window_num = desc->window_num;
	struct window *window;
	int rc = 0;

	/* A little bit of checking */
	if ((window_num < 0) || (window_num >= vbridge_gbl->wind_mgr.nr_winds))
		return -EINVAL;

	if (desc->sizel == 0)
		return -EINVAL;

	/* Round down the initial VME address to a 64K boundary */
	if (desc->vme_addrl & 0xffff) {
		unsigned int lowaddr = desc->vme_addrl & ~0xffff;

		pr_info(PFX "%s - aligning VME address %08x to 64K boundary %08x.\n",
			__func__, desc->vme_addrl, lowaddr);
		desc->vme_addrl = lowaddr;
		desc->sizel += desc->vme_addrl - lowaddr;
	}

	/*
	 * Round up the mapping size to a 64K boundary
	 * Note that vme_addrl is already aligned
	 */
	if (desc->sizel & 0xffff) {
		unsigned int newsize = (desc->sizel + 0x10000) & ~0xffff;

		pr_info(PFX "%s - rounding up size %08x to 64K boundary %08x.\n",
			__func__, desc->sizel, newsize);
		desc->sizel = newsize;
	}

	/*
	 * OK from now on we don't want someone else mucking with our
	 * window.
	 */
	window = &vbridge_gbl->wind_mgr.winds[window_num];

	if (mutex_lock_interruptible(&window->lock))
		return -ERESTARTSYS;

	if (window->active) {
		rc = -EBUSY;
		goto out_unlock;
	}

	/* Allocate and map a PCI address space for the window */
	window->name = kmalloc(32, GFP_KERNEL);

	if (!window->name)
		/* Not fatal, we can live with a nameless resource */
		pr_warn(PFX "%s - failed to allocate resource name\n",
		       __func__);
	else
		sprintf((char *)window->name, "VME Window %d", window_num);

	/* Copy the descriptor */
	memcpy(&window->desc, desc, sizeof(struct vme_mapping));

	/* Now setup the chip for that window */
	rc = vbridge_gbl->wind_mgr.ops->setup_window(vbridge_gbl, window);

	if (rc) {
		pr_err(PFX "%s - %d", __func__, __LINE__);
		goto out_free;
	}

	/* Mark the window as active now */
	window->active = 1;

	mutex_unlock(&window->lock);

	return 0;

out_free:
	kfree(window->name);
	window->name = NULL;
	memset(&window->desc, 0, sizeof(struct vme_mapping));

out_unlock:
	mutex_unlock(&window->lock);

	return rc;
}
EXPORT_SYMBOL_GPL(vme_create_window);

/**
 * vme_destroy_window() - Unmap and remove a PCI-VME window
 * @window_num: Window Number of the window to be destroyed
 *
 * Unmap and remove the PCI-VME window specified in the &struct vme_mapping
 * parameter also release all the mappings on top of that window.
 *
 * This function is used by the VME_IOCTL_DESTROY_WINDOW ioctl from
 * user applications but can also be used by drivers stacked on top
 * of this one.
 *
 * NOTE: destroying a window also forcibly remove all the mappings
 *       ont top of that window.
 *
 * Return 0 on success, or a standard kernel error code on failure.
 */
int vme_destroy_window(int window_num)
{
	struct window *window;
	struct mapping *mapping;
	struct mapping *tmp;
	int rc = 0;

	if ((window_num < 0) || (window_num >= vbridge_gbl->wind_mgr.nr_winds))
		return -EINVAL;

	/*
	 * Prevent somebody else from changing our window from under us
	 */
	window = &vbridge_gbl->wind_mgr.winds[window_num];

	if (mutex_lock_interruptible(&window->lock))
		return -ERESTARTSYS;

	/*
	 * Maybe we should silently ignore trying to destroy an unused
	 * window.
	 */
	if (!window->active) {
		rc = -EINVAL;
		goto out_unlock;
	}

	/* Remove all mappings */
	if (window->users > 0) {
		list_for_each_entry_safe(mapping, tmp,
					 &window->mappings, list) {
			list_del(&mapping->list);
			kfree(mapping);
			window->users--;
		}
	}

	if (window->users)
		pr_err("%s: %d mappings still alive on window %d\n",
		       __func__, window->users, window_num);

	/* Mark the window as unused */
	window->active = 0;

	vbridge_gbl->wind_mgr.ops->release_window(vbridge_gbl, window);

	kfree(window->name);

out_unlock:
	mutex_unlock(&window->lock);

	return rc;
}
EXPORT_SYMBOL_GPL(vme_destroy_window);

static int vme_mapping_sanity_check(const struct vme_mapping *mapping)
{
	unsigned int vme_addru = mapping->vme_addru;
	unsigned int vme_addrl = mapping->vme_addrl;
	unsigned int err = 0;

	switch (mapping->am) {
	case VME_A16_USER:
	case VME_A16_LCK:
	case VME_A16_SUP:
		if (vme_addru || vme_addrl & ~0xffff)
			err = 16;
		break;
	case VME_A24_USER_MBLT:
	case VME_A24_USER_DATA_SCT:
	case VME_A24_USER_PRG_SCT:
	case VME_A24_USER_BLT:
	case VME_A24_SUP_MBLT:
	case VME_A24_SUP_DATA_SCT:
	case VME_A24_SUP_PRG_SCT:
	case VME_A24_SUP_BLT:
		if (vme_addru || vme_addrl & ~0xffffff)
			err = 24;
		break;
	case VME_A32_LCK:
	case VME_A32_USER_MBLT:
	case VME_A32_USER_DATA_SCT:
	case VME_A32_USER_PRG_SCT:
	case VME_A32_USER_BLT:
	case VME_A32_SUP_MBLT:
	case VME_A32_SUP_DATA_SCT:
	case VME_A32_SUP_PRG_SCT:
	case VME_A32_SUP_BLT:
		if (vme_addru)
			err = 32;
		break;
	case VME_A40_SCT:
	case VME_A40_LCK:
	case VME_A40_BLT:
		if (vme_addru & ~0xff)
			err = 40;
		break;
	default:
		break;
	}

	if (err) {
		pr_err(PFX "Error: 0x%08x %08x out of A%d range\n",
			vme_addru, vme_addrl, err);
		return -EINVAL;
	}

	return 0;
}

static int
__vme_find_mapping(struct vme_mapping *match, int force, struct file *file)
{
	int i;
	int rc = 0;
	struct window *window;
	unsigned int offset;
	struct vme_mapping wnd;

	rc = vme_mapping_sanity_check(match);
	if (rc)
		return rc;

	for (i = 0; i < vbridge_gbl->wind_mgr.nr_winds; i++) {
		window = &vbridge_gbl->wind_mgr.winds[i];

		if (mutex_lock_interruptible(&window->lock))
			return -ERESTARTSYS;

		/* First check if window is in use */
		if (!window->active)
			goto try_next;

		/*
		 * Check if the window matches what we're looking for.
		 *
		 * Right now we only deal with 32-bit (or lower) address space
		 * windows,
		 */

		/* Check that the window is enabled in the hardware */
		if (!window->desc.window_enabled)
			goto try_next;

		/* Check that the window has a <= 32-bit address space */
		if ((window->desc.vme_addru != 0) || (window->desc.sizeu != 0))
			goto try_next;

		/* Check the address modifier and data width */
		if ((window->desc.am != match->am) ||
		    (window->desc.data_width != match->data_width))
			goto try_next;

		/* Check the boundaries */
		if ((window->desc.vme_addrl > match->vme_addrl) ||
		    ((window->desc.vme_addrl + window->desc.sizel) <
		     (match->vme_addrl + match->sizel)))
			goto try_next;

		/* Check the 2eSST transfer speed if 2eSST is enabled */
		if ((window->desc.am == VME_2e6U) &&
		    (window->desc.v2esst_mode != match->v2esst_mode))
			goto try_next;

		/*
		 * Good, we found one mapping
		 * NOTE: we're exiting the loop with window->lock still held
		 */
		break;

try_next:
		mutex_unlock(&window->lock);
	}

	/* Window found */
	if (i < vbridge_gbl->wind_mgr.nr_winds) {
		offset = match->vme_addrl - window->desc.vme_addrl;

		/* Now set the virtual address of the mapping */
		match->kernel_va = window->desc.kernel_va + offset;
		match->pci_addrl = window->desc.pci_addrl + offset;

		/* Assign window number */
		match->window_num = i;

		/* Add the new mapping to the window */
		rc = add_mapping(window, match, file);

		mutex_unlock(&window->lock);

		return rc;
	}

	/*
	 * Bad luck, no matching window found - create a new one if
	 * force is set.
	 */
	if (!force)
		return -EBUSY;

	/* Get the first unused window */
	for (i = 0; i < vbridge_gbl->wind_mgr.nr_winds; i++) {
		window = &vbridge_gbl->wind_mgr.winds[i];

		if (!window->active)
			break;
	}

	if (i >= vbridge_gbl->wind_mgr.nr_winds)
		/* No more window available - bail out */
		return -EBUSY;

	/*
	 * Setup the physical window descriptor that can hold the requested
	 * mapping. The VME address and size may be realigned by the low level
	 * code in the descriptor, so make a private copy for window creation.
	 */
	memcpy(&wnd, match, sizeof(struct vme_mapping));

	wnd.window_num = i;

	rc = vme_create_window(&wnd);

	if (rc)
		return rc;

	/* Now set the virtual address of the mapping */
	window = &vbridge_gbl->wind_mgr.winds[wnd.window_num];

	offset = match->vme_addrl - window->desc.vme_addrl;
	match->kernel_va = window->desc.kernel_va + offset;
	match->pci_addrl = window->desc.pci_addrl + offset;
	match->window_num = wnd.window_num;


	/* And add that mapping to it */
	rc = add_mapping(window, match, file);

	return rc;
}

static int
__vme_release_mapping(struct vme_mapping *desc, int force, struct file *file)
{
	int window_num = desc->window_num;
	struct window *window;
	int rc = 0;

	if ((window_num < 0) || (window_num >= vbridge_gbl->wind_mgr.nr_winds))
		return -EINVAL;

	window = &vbridge_gbl->wind_mgr.winds[window_num];

	if (mutex_lock_interruptible(&window->lock))
		return -ERESTARTSYS;

	if (!window->active) {
		rc = -EINVAL;
		goto out_unlock;
	}

	/* Remove the mapping */
	rc = remove_mapping(window, desc, file);

	if (rc)
		goto out_unlock;

	/* Check if there are no more users of this window */
	if ((window->users == 0) && force) {
		mutex_unlock(&window->lock);
		return vme_destroy_window(window_num);
	}

out_unlock:
	mutex_unlock(&window->lock);

	return rc;
}

/**
 * vme_find_mapping() - Find a window matching the specified descriptor
 * @match: Descriptor of the window to look for
 * @force: Force window creation if no match was found.
 *
 *  Try to find a window matching the specified descriptor. If a window is
 * found, then its number is returned in the &struct vme_mapping parameter.
 *
 *  If no window match and force is set, then a new window is created
 * to hold that mapping.
 *
 * This function is used by the VME_IOCTL_FIND_MAPPING ioctl from
 * user applications but can also be used by drivers stacked on top
 * of this one.
 *
 *  Returns 0 on success, or a standard kernel error code.
 */
int vme_find_mapping(struct vme_mapping *match, int force)
{
	return __vme_find_mapping(match, force, NULL);
}
EXPORT_SYMBOL_GPL(vme_find_mapping);


/**
 * vme_window_resource_get() - Get the kernel resource associated to a window
 * @window_num: window number to query
 *
 * @return a valid pointer on success ERR_PTR() otherwise
 */
struct resource *vme_window_resource_get(unsigned int window_num)
{
	struct window *win;

	if (window_num >= vbridge_gbl->wind_mgr.nr_winds)
		return ERR_PTR(-EINVAL);

	win = &vbridge_gbl->wind_mgr.winds[window_num];
	if (!win->active)
		return ERR_PTR(-EPERM);


	return win->rsrc;
}
EXPORT_SYMBOL_GPL(vme_window_resource_get);

/**
 * vme_release_mapping() - Release a mapping allocated with vme_find_mapping
 *
 * @desc: Descriptor of the mapping to release
 * @force: force window destruction
 *
 * Release a VME mapping. If the mapping is the last one on that window and
 * force is set then the window is also destroyed.
 *
 * This function is used by the VME_IOCTL_RELEASE_MAPPING ioctl from
 * user applications but can also be used by drivers stacked on top
 * of this one.
 *
 * @return 0                          - on success.
 * @return standard kernel error code - if failed.
 */
int vme_release_mapping(struct vme_mapping *desc, int force)
{
	return __vme_release_mapping(desc, force, NULL);
}
EXPORT_SYMBOL_GPL(vme_release_mapping);

static int vme_destroy_window_ioctl(int __user *argp)
{
	int window_num;

	if (get_user(window_num, argp))
		return -EFAULT;

	return vme_destroy_window(window_num);
}

static int vme_bus_error_check_clear_ioctl(struct vme_bus_error __user *argp)
{
	struct vme_bus_error_desc desc;

	if (copy_from_user(&desc, argp, sizeof(struct vme_bus_error_desc)))
		return -EFAULT;

	desc.valid = vme_bus_error_check_clear(&desc.error);

	if (copy_to_user(argp, &desc, sizeof(struct vme_bus_error_desc)))
		return -EFAULT;

	return 0;
}

/**
 * vme_window_ioctl() - ioctl file method for the VME window device
 * @file: Device file descriptor
 * @cmd: ioctl number
 * @arg: ioctl argument
 *
 *  Currently the VME window device supports the following ioctls:
 *
 *    VME_IOCTL_GET_WINDOW_ATTR
 *    VME_IOCTL_CREATE_WINDOW
 *    VME_IOCTL_DESTROY_WINDOW
 *    VME_IOCTL_FIND_MAPPING
 *    VME_IOCTL_RELEASE_MAPPING
 *    VME_IOCTL_GET_CREATE_ON_FIND_FAIL
 *    VME_IOCTL_SET_CREATE_ON_FIND_FAIL
 *    VME_IOCTL_GET_DESTROY_ON_REMOVE
 *    VME_IOCTL_SET_DESTROY_ON_REMOVE
 */
long vme_window_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int rc;
	struct vme_mapping desc;
	void __user *argp = (void __user *)arg;

	switch (cmd) {
	case VME_IOCTL_GET_WINDOW_ATTR:
		/*
		 * Get a window attributes.
		 *
		 * arg is a pointer to a struct vme_mapping with only
		 * the window number specified.
		 */
		if (copy_from_user(&desc, (void *)argp,
				   sizeof(struct vme_mapping)))
			return -EFAULT;

		rc = vme_get_window_attr(&desc);

		if (rc)
			return rc;

		if (copy_to_user((void *)argp, &desc,
				 sizeof(struct vme_mapping)))
			return -EFAULT;

		break;

	case VME_IOCTL_CREATE_WINDOW:
		/* Create and map a window.
		 *
		 * arg is a pointer to a struct vme_mapping specifying
		 * the window number as well as its attributes.
		 */
		if (copy_from_user(&desc, (void *)argp,
				   sizeof(struct vme_mapping))) {
			return -EFAULT;
		}

		rc = vme_create_window(&desc);

		if (rc)
			return rc;

		if (copy_to_user((void *)argp, &desc,
				 sizeof(struct vme_mapping)))
			return -EFAULT;

		break;

	case VME_IOCTL_DESTROY_WINDOW:
		/* Unmap and destroy a window.
		 *
		 * arg is a pointer to the window number
		 */
		return vme_destroy_window_ioctl((int __user *)argp);

	case VME_IOCTL_FIND_MAPPING:
		/*
		 * Find a window suitable for this mapping.
		 *
		 * arg is a pointer to a struct vme_mapping specifying
		 * the attributes of the window to look for. If a match is
		 * found then the virtual address for the requested mapping
		 * is set in the window descriptor otherwise if
		 * vme_create_on_find_fail is set then create a new window to
		 * hold that mapping.
		 */
		if (copy_from_user(&desc, (void *)argp,
				   sizeof(struct vme_mapping)))
			return -EFAULT;

		rc = __vme_find_mapping(&desc, vbridge_gbl->mod_param.vme_create_on_find_fail,
					file);

		if (rc)
			return rc;

		if (copy_to_user((void *)argp, &desc,
				 sizeof(struct vme_mapping)))
			return -EFAULT;

		break;

	case VME_IOCTL_RELEASE_MAPPING:
		/* remove a mapping.
		 *
		 * arg is a pointer to a struct vme_mapping specifying
		 * the attributes of the mapping to be removed.
		 * If the mapping is the last on that window and if
		 * vme_destroy_on_remove is set then the window is also
		 * destroyed.
		 */
		if (copy_from_user(&desc, (void *)argp,
				   sizeof(struct vme_mapping)))
			return -EFAULT;

		rc = __vme_release_mapping(&desc, vbridge_gbl->mod_param.vme_destroy_on_remove,
					   file);

		break;

	case VME_IOCTL_GET_CREATE_ON_FIND_FAIL:
		rc = put_user(vbridge_gbl->mod_param.vme_create_on_find_fail,
			      (unsigned int __user *)argp);
		break;

	case VME_IOCTL_SET_CREATE_ON_FIND_FAIL:
		rc = get_user(vbridge_gbl->mod_param.vme_create_on_find_fail,
			      (unsigned int __user *)argp);
		break;

	case VME_IOCTL_GET_DESTROY_ON_REMOVE:
		rc = put_user(vbridge_gbl->mod_param.vme_destroy_on_remove,
			      (unsigned int __user *)argp);
		break;

	case VME_IOCTL_SET_DESTROY_ON_REMOVE:
		rc = get_user(vbridge_gbl->mod_param.vme_destroy_on_remove,
			      (unsigned int __user *)argp);
		break;
	case VME_IOCTL_GET_BUS_ERROR:
		rc = put_user(vme_bus_error_check(1),
			      (unsigned int __user *)argp);
		break;
	case VME_IOCTL_CHECK_CLEAR_BUS_ERROR:
		return vme_bus_error_check_clear_ioctl(argp);

	default:
		rc = -ENOIOCTLCMD;
	}

	return rc;
}

/**
 * vme_remap_pfn_range() - Small wrapper for io_remap_pfn_range
 * @vma: User vma to map to
 *
 *  This is a small helper function which sets the approriate vma flags
 * and protection before calling io_remap_pfn_range().
 *
 *  The following flags are added to vm_flags:
 *    - VM_IO         This is an iomem region
 *    - VM_RESERVED   Do not swap that mapping
 *    - VM_DONTCOPY   Don't copy that mapping on fork
 *    - VM_DONTEXPAND Prevent resizing with mremap()
 *
 *  The mapping is also set non-cacheable via the vm_page_prot field.
 */
#ifndef VM_RESERVED
#define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif
static int vme_remap_pfn_range(struct vm_area_struct *vma)
{

	vma->vm_flags |= VM_IO | VM_RESERVED | VM_DONTCOPY | VM_DONTEXPAND;
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

	return io_remap_pfn_range(vma,
				  vma->vm_start,
				  vma->vm_pgoff,
				  vma->vm_end - vma->vm_start,
				  vma->vm_page_prot);
}

/*
 * Check that a particular VME mapping corresponds to a region to be mmapped.
 *
 * mmap() works with whole pages. Thus we need to see VME mappings as blocks
 * of pages, even if that means we see them bigger than they really are.
 */
static int vme_check_mmap_region(int windownr, struct vme_mapping *mapping,
			unsigned int addr, unsigned int size)
{
	struct vme_mapping *window = &vbridge_gbl->wind_mgr.winds[windownr].desc;
	unsigned int aligned_start;
	unsigned int aligned_end;
	unsigned int end;

	/* check that the requested region doesn't stick out of the window */
	if (addr < window->pci_addrl ||
	    addr + size > window->pci_addrl + window->sizel) {
		return 0;
	}

	/* compute the page-aligned boundaries of the VME mapping */
	aligned_start = mapping->pci_addrl & PAGE_MASK;
	end = mapping->pci_addrl + mapping->sizel;
	if (end & (~PAGE_MASK))
		aligned_end = (end & PAGE_MASK) + PAGE_SIZE;
	else
		aligned_end = end;

	/* and compare these boundaries to those of the requested region */
	if (aligned_start == addr && aligned_end == addr + size)
		return 1;

	return 0;
}

/**
 * vme_window_mmap() - Map to userspace a VME address mapping
 * @file: Device file descriptor
 * @vma: User vma to map to
 *
 * The PCI physical address is in vma->vm_pgoff and is guaranteed to be unique
 * among all mappings. The range size is given by (vma->vm_end - vma->vm_start)
 */

int vme_window_mmap(struct file *file, struct vm_area_struct *vma)
{
	int i;
	int rc = -EINVAL;
	struct window *window;
	struct mapping *mapping;
	unsigned int addr;
	unsigned int size;

	/* Find the mapping this mmap call refers to */
	for (i = 0; i < vbridge_gbl->wind_mgr.nr_winds; i++) {
		window = &vbridge_gbl->wind_mgr.winds[i];

		if (mutex_lock_interruptible(&window->lock))
			return -ERESTARTSYS;

		/* Check the window is active and contains mappings */
		if ((!window->active) || (list_empty(&window->mappings)))
			goto try_next;

		list_for_each_entry(mapping, &window->mappings, list) {
			addr = vma->vm_pgoff << PAGE_SHIFT;
			size = vma->vm_end - vma->vm_start;

			if (mapping->client.file == file &&
			    vme_check_mmap_region(i, &mapping->desc, addr, size)) {
				rc = vme_remap_pfn_range(vma);
				mutex_unlock(&window->lock);

				return rc;
			}
		}


try_next:
		mutex_unlock(&window->lock);
	}

	return rc;
}

/**
 * vme_window_init() - Initialize VME windows handling
 *
 *  Not much to do here aside from initializing the windows mutexes and
 * mapping lists.
 */
int vme_window_init(struct vme_bridge_device *vbridge)
{
	int i, nr_winds;
	struct vme_bridge_window_mgr *wind_mgr = &vbridge->wind_mgr;

	nr_winds = wind_mgr->ops->get_nr_windows();
	wind_mgr->winds = kcalloc(nr_winds, sizeof(struct window), GFP_KERNEL);
	if (wind_mgr->winds == NULL) {
		pr_err(PFX "Could not allocate vme_bridge windows\n");
		return -ENOMEM;
	}
	wind_mgr->nr_winds = nr_winds;

	for (i = 0; i < wind_mgr->nr_winds; i++) {
		INIT_LIST_HEAD(&wind_mgr->winds[i].mappings);
		mutex_init(&wind_mgr->winds[i].lock);
		wind_mgr->winds[i].active = 0;
	}
	return 0;
}

/**
 * vme_window_exit() - Cleanup for module unload
 *
 * Unmap all the windows that were mapped.
 */
void vme_window_exit(void)
{
	int i;

	for (i = 0; i < vbridge_gbl->wind_mgr.nr_winds; i++) {
		if (vbridge_gbl->wind_mgr.winds[i].active)
			vme_destroy_window(i);
	}
	kfree(vbridge_gbl->wind_mgr.winds);
}
