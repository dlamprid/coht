#!/bin/bash

# Author Adam Wujek, CERN
#
# Load drivers from a directory named as the hostname (if present)

function install_drv {
    DRV_LOC="$1"
    if [ -r "$DRV_DIR""$1" ]; then
	DRV_LOC="$DRV_DIR""$1"
    fi
    shift
    $OUTPUT "loading $DRV_LOC" "$@"
    /sbin/insmod "$DRV_LOC" "$@"
}

OUTPUT=":"

# strip all after "." like cern.ch
HOSTNAME_NO_DOMAIN=${HOSTNAME%%.*}

if [ -d $HOSTNAME_NO_DOMAIN ]; then
    DRV_DIR+="$HOSTNAME_NO_DOMAIN"/
    echo "Loading drivers from the $HOSTNAME_NO_DOMAIN directory"
fi
