"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import subprocess
import time
import os

class PyVmeDevice(object):
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __init__(self, slot):
        self.slot = slot
        self.path_to_slot = "/sys/bus/vme/devices/slot.{:02d}/".format(slot)
        self.path_to_dev = os.path.join(self.path_to_slot,
                                        "vme.{:d}".format(slot))

    def register(self, am = None, addr = None, size = None,
                 irq_vector = None, irq_level = None,
                 devname = None):
        vme_register_argmuments = ["--slot", str(self.slot),]
        if am is not None:
            vme_register_argmuments.append("--address-modifier")
            vme_register_argmuments.append("0x{:x}".format(am))
        if addr is not None:
            vme_register_argmuments.append("--address")
            vme_register_argmuments.append("0x{:x}".format(addr))
        if size is not None:
            vme_register_argmuments.append("--size")
            vme_register_argmuments.append(str(size))
        if irq_vector is not None:
            vme_register_argmuments.append("--irq-vector")
            vme_register_argmuments.append(str(irq_vector))
        if irq_level is not None:
            vme_register_argmuments.append("--irq-level")
            vme_register_argmuments.append(str(irq_level))
        if devname is not None:
            vme_register_argmuments.append("--name")
            vme_register_argmuments.append(str(devname))

        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-register", ]
        vme_register_command.extend(vme_register_argmuments)
        subprocess.run(vme_register_command, check=True)

    def rescan(self):
        with open(os.path.join(self.path_to_slot, "rescan"), "w") as f:
            f.write("1")

    def unregister(self):
        try:
            with open(os.path.join(self.path_to_dev, "remove"), "w") as f:
                f.write("1")
        except OSError:
            pass
        time.sleep(1)

    def is_registered(self):
        return os.path.exists(self.path_to_dev)

    def disable(self):
        with open(os.path.join(self.path_to_dev, "enable"), "w") as f:
            f.write("0")

    def enable(self):
        with open(os.path.join(self.path_to_dev, "enable"), "w") as f:
            f.write("1")

    def is_enabled(self):
        with open(os.path.join(self.path_to_dev, "enable"), "r") as f:
            return bool(int(f.read()))

    def __fetch_csr(self):
        with open(os.path.join(self.path_to_dev, "csr"), "rb") as fcsr:
            return fcsr.read()

    def ader_set(self, func, addr, am, dsfr=0, xam=0):
        self.disable()
        with open(os.path.join(self.path_to_dev, "ader"), "w") as f_ader:
            ader = (addr & 0xFFFFFF00) | ((am & 0xFF) << 2) | ((dsfr & 0x1) << 1) | ((xam & 0x1) << 0)
            f_ader.write("{:d} 0x{:x}".format(func, ader))
        self.enable()

    def ader_get(self, func):
        csr_bin = self.__fetch_csr()
        ader = 0
        ader = ader | (csr_bin[867 + (func * 0x10)] << 24)
        ader = ader | (csr_bin[871 + (func * 0x10)] << 16)
        ader = ader | (csr_bin[875 + (func * 0x10)] << 8)
        ader = ader | (csr_bin[879 + (func * 0x10)] << 0)

        return ader
