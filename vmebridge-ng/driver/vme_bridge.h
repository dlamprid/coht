/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * vme_bridge.h - PCI-VME bridge driver definitions
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the data structures and definitions internal to the
 * driver.
 */

#ifndef _VME_BRIDGE_H
#define _VME_BRIDGE_H

#include <linux/device.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/bitmap.h>
#include <linux/spinlock.h>
#include <linux/idr.h>

#include "vmebus.h"
#include "vme_dmaengine.h"
#include "vme_irq.h"
#include "vme_window.h"
#include "vme_procfs.h"

#define PFX			"VME Bridge: "
#define DRV_MODULE_NAME		"vme_bridge"
#define DRV_MODULE_VERSION	"1.7"
#define DRV_MODULE_RELDATE	"Feb, 04 2019"

/* List of supported PCI VME bridge pci IDs and names*/
#define PCI_TUNDRA_CHIP_NAME		"tundra-tsi148"
#define PCI_VENDOR_ID_TUNDRA		0x10e3
#define PCI_DEVICE_ID_TUNDRA_TSI148	0x0148
#define PCI_MEN_CHIP_NAME		"men-pldz002"
#define PCI_VENDOR_ID_MEN		0x1a88
#define PCI_DEVICE_ID_MEN_PLDZ002	0x4d45

/*
 * We just keep the last VME error caught, protecting it with a spinlock.
 * A new VME bus error overwrites it.
 * This is very simple yet good enough for most (sane) purposes.
 * A linked list of handlers is kept for async notification of bus errors.
 */
struct vme_verr {
	spinlock_t			lock;
	struct vme_bus_error_desc	desc;
	struct list_head		h_list;
};

#define VME_SLOT_FLAGS_MAX 32
#define VME_SLOT_F_BUSY BIT(0)

/**
 * Describe a VME slot
 * @dev:
 * @index: slot number in range [1, 21]
 * @lock: protect: flags
 * @flags: flags
 */
struct vme_slot {
	struct device dev;
	uint8_t index;
	spinlock_t lock;
	DECLARE_BITMAP(flags, VME_SLOT_FLAGS_MAX);
};

static inline struct vme_slot *to_vme_slot(struct device *d)
{
	return container_of(d, struct vme_slot, dev);
}

struct vme_bridge_device;

struct vme_bridge_chip_ops {
	char          *(*get_chip_name)(void);
	int            (*request_regions)(struct vme_bridge_device *vme_bridge);
	void           (*release_regions)(struct vme_bridge_device *vme_bridge);
	void __iomem  *(*get_regsaddr)(struct vme_bridge_device *vme_bridge);
	int            (*get_slotnum)(struct vme_bridge_device *vme_bridge);
	int            (*get_syscon)(struct vme_bridge_device *vme_bridge);
	void           (*init_bridge)(struct vme_bridge_device *vme_bridge);
	int            (*bus_error_check)(struct vme_bridge_device *vme_bridge,
					   int clear);
};

struct vme_bridge_chip_mgr {
	struct vme_bridge_chip_ops *ops;
	void *private; /* private pointer to hold specific HW info */
};

struct interrupt_stats  {
	unsigned int	count;
	char		*name;
};

/**
 * struct vme_bridge_mod_param
 * @vme_create_on_find_fail When set, if mapping is not found allows
 *                          new window creation
 * @vme_destroy_on_remove When set removing the last mapping on a window,
 *                        destroy the window
 * @vme_report_bus_errors When set, prints kernel msg whenever a VME Bus Error
 *                        is detected
 */
struct vme_bridge_mod_param {
	unsigned int vme_create_on_find_fail;
	unsigned int vme_destroy_on_remove;
	unsigned int vme_report_bus_errors;
};

/**
 * struct vme_bridge_device - VME bridge descriptor
 * @flags bit mask for flags
 * @dma_param manaual tuning parameters for DMA transfers
 * @lock it protects the following variables: flags
 * @bus_slot: list al all possible slots on the bus
 */

struct vme_bridge_device {

	struct vme_bridge_irq_mgr    irq_mgr;
	struct vme_bridge_dma_mgr    dma_mgr;
	struct vme_bridge_window_mgr wind_mgr;
	struct vme_bridge_chip_mgr   chip_mgr;
	struct vme_bridge_procfs_mgr procfs_mgr;

	struct bus_type *vme_bus_type;

	struct pci_dev	*pdev;
	struct vme_slot bus_slot[VME_SLOT_MAX];
	spinlock_t lock;
	uint8_t		rev;	/* bridge revision */
	char		*name;  /* bridge name */
	int		slot;	/* the VME slot we're in */
	int		syscon;	/* syscon status */
	struct vme_verr	verr;
	struct vme_bridge_mod_param mod_param;

	struct dentry *dbg_dir;
#define VBRIDGE_DBG_INFO_NAME "info"
	struct dentry *dbg_info;
#define VBRIDGE_DBG_GEN_IRQ_NAME "generate_interrupt"
	struct dentry *dbg_gen_irq;


	struct interrupt_stats *int_stats;
	int int_stats_count;
};

extern struct bus_type vme_bus_type;
extern const struct device_type vme_slot_type;

static inline struct vme_bridge_device *to_vme_bridge_device(struct pci_dev *d)
{
	return container_of(&d, struct vme_bridge_device, pdev);
}

/**
 * struct vme_berr_handler - VME bus error handler descriptor
 * @h_list:	handler's list entry
 * @offset:	VME Bus error descriptor (Initial address + Address Modifier)
 * @size:	Size of the VME address range of interest
 * @func:	Handler function
 */
struct vme_berr_handler {
	struct list_head	h_list;
	struct vme_bus_error	error;
	size_t			size;
	vme_berr_handler_t	func;
};

/* Use the standard VME Major */
#define VME_MAJOR	221

/*
 * Define our own minor numbers
 * If a device get added, do not forget to update devlist[] in vme_bridge.c
 */
#define VME_MINOR_MWINDOW	0
#define VME_MINOR_DMA		1
#define VME_MINOR_CTL		3
#define VME_MINOR_REGS		4

#define VME_MINOR_NR	(VME_MINOR_LM + 1)

/* Devices access rules */
#define DEV_RW		1	/* Device implements read/write */

struct dev_entry {
	unsigned int		minor;
	char			*name;
	unsigned int		flags;
	const struct file_operations *fops;
};

/*
 * The vmebridge global instance declaration
 * (definition is done in vmebridge.c)
 */
extern struct vme_bridge_device *vbridge_gbl;

/* Prototypes */
/* vme_bridge.c */
extern int tsi148_set_ops(struct vme_bridge_device *vme_bridge);
extern int pldz002_set_ops(struct vme_bridge_device *vme_bridge);

/* vme_irq.c */
extern int vme_interrupts_init(struct vme_bridge_device *vbridge);
extern void vme_interrupts_exit(struct vme_bridge_device *vbridge);
extern void vme_handle_interrupt(int irq_mask,
				 struct vme_bridge_device *vbridge);
extern void vme_bus_error_dispatch(struct vme_bridge_device *vbridge,
				   struct vme_bus_error *error);

/* vme dmaengine */
extern int vme_dmaengine_init(struct vme_bridge_device *vbridge);
extern void vme_dmaengine_exit(struct vme_bridge_device *vbridge);
extern void vme_dmaengine_irq_handler(int channel_mask,
				      struct vme_bridge_device *vbridge);

/* vme_window.c */
extern int vme_window_release(struct inode *inode, struct file *file);
extern long vme_window_ioctl(struct file *file, unsigned int cmd,
			     unsigned long arg);
extern int vme_window_mmap(struct file *file, struct vm_area_struct *vma);
extern int vme_window_init(struct vme_bridge_device *vbridge);
extern void vme_window_exit(void);

/* vme_dma.c */
extern long vme_dma_ioctl(struct file *file, unsigned int cmd,
			  unsigned long arg);
extern int vme_dma_init(void);
extern void vme_dma_exit(void);

/* vme_misc.c */
extern ssize_t vme_misc_read(struct file *file, char *buf,
			     size_t count, loff_t *ppos);
extern ssize_t vme_misc_write(struct file *file, const char *buf,
			      size_t count, loff_t *ppos);
extern long vme_misc_ioctl(struct file *file, unsigned int cmd,
			   unsigned long arg);
extern int vme_bus_error_check(int clear);
extern int vme_bus_error_check_clear(struct vme_bus_error *err);

extern int vme_bus_register(struct vme_bridge_device *b);
extern void vme_bus_unregister(struct vme_bridge_device *b);
extern int vme_device_irq_assign(struct vme_dev *vme_dev, int start, int end);
extern void vme_device_irq_unassign(struct vme_dev *vme_dev);

/* vme_crcsr */
extern int vme_csr_map(struct vme_dev *vdev);
extern void vme_csr_unmap(struct vme_dev *vdev);
extern int vme_cr_area_dump_all(struct vme_dev *vdev);
extern void vme_cr_area_release_all(struct vme_dev *vdev);
extern int vme_cr_get(struct vme_dev *vdev, struct vme_cr *cr);
extern int vme_csr_get(struct vme_dev *vdev, struct vme_csr *csr);
extern int vme_csr_ader_set(struct vme_dev *vdev,
			    unsigned int function, uint32_t ader);
extern void vme_csr_ader_set_raw(struct vme_dev *vdev,
				 unsigned int func, uint32_t ader);
extern int vme_csr_bit(struct vme_dev *vdev, uint8_t mask, unsigned int val);
extern int vme_csr_bit_usr(struct vme_dev *vdev,
			   uint8_t mask, unsigned int val);
extern int vme_csr_resource_create(struct vme_dev *vdev, unsigned int func);
extern void vme_csr_resource_remove(struct vme_dev *vdev, unsigned int func);
extern struct bin_attribute vme_cr_bin_attr;
extern struct bin_attribute vme_csr_bin_attr;
extern int vme_cr_is_valid(unsigned int slot);

extern uint32_t vme_cr_manufacturer(unsigned int slot);
extern uint32_t vme_cr_device(unsigned int slot);
extern uint32_t vme_cr_revision(unsigned int slot);
extern uint8_t vme_cr_irq_cap(unsigned int slot);

/* vme_debug.c */
extern int vme_dbg_init(struct vme_bridge_device *vbridge);
extern void vme_dbg_exit(struct vme_bridge_device *vbridge);

/* Procfs stuff grouped here for comodity */
#ifdef CONFIG_PROC_FS
void vme_procfs_register(struct vme_bridge_device *vbridge);
void vme_procfs_unregister(struct vme_bridge_device *vbridge);
#else
void vme_procfs_register(struct vme_bridge_device *vbridge) {}
void vme_procfs_unregister(struct vme_bridge_device *vbridge) {}
#endif /* CONFIG_PROC_FS */

#endif /* _VME_BRIDGE_H */
