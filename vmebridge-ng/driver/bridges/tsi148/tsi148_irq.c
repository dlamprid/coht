// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_irq.c - PCI-VME bridge interrupt management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge interrupt management code.
 */

#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/irqdomain.h>

#include "tsi148.h"
#include "vme_irq.h"
#include "vme_bridge.h"


static struct interrupt_stats int_stats[] = {
	{.name = "DMA0"}, {.name = "DMA1"},
	{.name = "MB0"}, {.name = "MB1"}, {.name = "MB2"}, {.name = "MB3"},
	{.name = "LM0"}, {.name = "LM1"}, {.name = "LM2"}, {.name = "LM3"},
	{.name = "IRQ1"}, {.name = "IRQ2"}, {.name = "IRQ3"}, {.name = "IRQ4"},
	{.name = "IRQ5"}, {.name = "IRQ6"}, {.name = "IRQ7"},
	{.name = "PERR"}, {.name = "VERR"}, {.name = "SPURIOUS"}
};

static inline void reg_join(u32 high, u32 low, u64 *out)
{
	*out = (unsigned long long)high << 32;
	*out |= low;
}

static void account_dma_interrupt(int channel_mask)
{
	if (channel_mask & 1)
		int_stats[TSI148_INT_DMA0].count++;

	if (channel_mask & 2)
		int_stats[TSI148_INT_DMA1].count++;
}

/**
 * tsi148_handle_pci_error() - Handle a PCI bus error reported by the bridge
 *
 *  This handler simply displays a message on the console and clears
 * the error.
 */
static void tsi148_handle_pci_error(struct tsi148_regs *regs)
{
	/* Display raw error information */
	pr_err(PFX
	       "PCI error at address: %.8x %.8x - attributes: %.8x\n"
	       "PCI-X attributes: %.8x - PCI-X split completion: %.8x\n",
	       ioread32be(&regs->lcsr.edpau),
	       ioread32be(&regs->lcsr.edpal),
	       ioread32be(&regs->lcsr.edpat),
	       ioread32be(&regs->lcsr.edpxa),
	       ioread32be(&regs->lcsr.edpxs));

	/* Clear error */
	iowrite32be(TSI148_LCSR_EDPAT_EDPCL, &regs->lcsr.edpat);
}

static void handle_pci_error(struct vme_bridge_device *vbridge)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	tsi148_handle_pci_error(chip->pci_map);

	int_stats[TSI148_INT_PERR].count++;
}

/**
 * tsi148_handle_vme_error() - Handle a VME bus error reported by the bridge
 *
 * Retrieve and report the offending VME address and Address Modifier, clearing
 * the error.
 */
static void tsi148_vme_error(struct vme_bridge_device *vbridge,
			     struct vme_bus_error *error,
			     struct tsi148_regs *regs)
{
	u32 attr;
	u32 addru, addrl;

	/* Store the address */
	addru = ioread32be(&regs->lcsr.veau);
	addrl = ioread32be(&regs->lcsr.veal);
	reg_join(addru, addrl, &error->address);

	/* Get the Address Modifier from the attributes */
	attr = ioread32be(&regs->lcsr.veat);
	error->am = (attr & TSI148_LCSR_VEAT_AM) >> TSI148_LCSR_VEAT_AM_SHIFT;
	/* Overflow detected: not much we can do about it */
	if (attr & TSI148_LCSR_VEAT_VEOF)
		pr_err(PFX "VME Bus Exception Overflow Detected\n");

}

static void tsi148_handle_vme_error_interrupt(struct vme_bridge_device *vbridge)
{
	struct vme_bus_error error;
	unsigned long flags;
	spinlock_t *lock;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_regs *regs = chip->pci_map;

	lock = &vbridge->verr.lock;

	spin_lock_irqsave(lock, flags);

	tsi148_vme_error(vbridge, &error, regs);
	/* Clear error */
	iowrite32be(TSI148_LCSR_VEAT_VESCL, &regs->lcsr.veat);
	/* dispatch bus error to clients */
	vme_bus_error_dispatch(vbridge, &error);

	spin_unlock_irqrestore(lock, flags);
	/* update bus error interrupt counter */
	int_stats[TSI148_INT_VERR].count++;
}

static void tsi148_handle_mbox_interrupt(int mb_mask)
{
	if (mb_mask & 1)
		int_stats[TSI148_INT_MB0].count++;

	if (mb_mask & 2)
		int_stats[TSI148_INT_MB1].count++;

	if (mb_mask & 4)
		int_stats[TSI148_INT_MB2].count++;

	if (mb_mask & 8)
		int_stats[TSI148_INT_MB3].count++;
}

static void tsi148_handle_lm_interrupt(int lm_mask)
{
	if (lm_mask & 1)
		int_stats[TSI148_INT_LM0].count++;

	if (lm_mask & 2)
		int_stats[TSI148_INT_LM1].count++;

	if (lm_mask & 4)
		int_stats[TSI148_INT_LM2].count++;

	if (lm_mask & 8)
		int_stats[TSI148_INT_LM3].count++;
}


/* Generate an 8-bit IACK cycle and get the vector */
int tsi148_iack8(void *chip, int irq)
{
	struct tsi148_chip *tsi148_chip = chip;
	int vec = ioread8(&tsi148_chip->pci_map->lcsr.viack[(irq * 4) + 3]);
	/* update stats for this irq level */
	int_stats[TSI148_INT_IRQ1 + irq - 1].count++;
	return vec & 0xff;
}


/**
 * vme_bridge_interrupt() - VME bridge main interrupt handler
 *
 */
irqreturn_t tsi148_interrupt_handler(int irq, void *arg)
{
	struct vme_bridge_device *vbridge = arg;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_regs *regs = chip->vme_map;
	unsigned int raised;
	unsigned int mask, chan_mask;

	/*
	 * We need to read the interrupt status from the VME bus to make
	 * sure the internal FIFO has been flushed of pending writes.
	 */
	while ((raised = tsi148_get_int_status(regs)) != 0) {
		/*
		 * Clearing of the interrupts must be done by writing to the
		 * INTS register through the VME bus.
		 */
		tsi148_clear_int(regs, raised);

		mask = raised & vbridge->irq_mgr.vme_interrupts_enabled;

		/* Only handle enabled interrupts */
		if (!mask) {
			int_stats[TSI148_INT_SPURIOUS].count++;
			return IRQ_NONE;
		}

		if (mask & TSI148_LCSR_INT_DMA_M) {
			chan_mask = (mask & TSI148_LCSR_INT_DMA_M) >>
						TSI148_LCSR_INT_DMA_SHIFT;
			account_dma_interrupt(chan_mask);
			vme_dmaengine_irq_handler(chan_mask, vbridge);
			mask &= ~TSI148_LCSR_INT_DMA_M;
		}

		if (mask & TSI148_LCSR_INT_PERR) {
			handle_pci_error(vbridge);
			mask &= ~TSI148_LCSR_INT_PERR;
		}

		if (mask & TSI148_LCSR_INT_VERR) {
			tsi148_handle_vme_error_interrupt(vbridge);
			mask &= ~TSI148_LCSR_INT_VERR;
		}

		if (mask & TSI148_LCSR_INT_MB_M) {
			tsi148_handle_mbox_interrupt(
				(mask & TSI148_LCSR_INT_MB_M) >>
				 TSI148_LCSR_INT_MB_SHIFT);
			mask &= ~TSI148_LCSR_INT_MB_M;
		}

		if (mask & TSI148_LCSR_INT_LM_M) {
			int lm_mask;

			lm_mask = mask & TSI148_LCSR_INT_LM_M;
			lm_mask >>= TSI148_LCSR_INT_LM_SHIFT;
			tsi148_handle_lm_interrupt(lm_mask);
			mask &= ~TSI148_LCSR_INT_LM_M;
		}

		if (mask & TSI148_LCSR_INT_IRQM) {
			vme_handle_interrupt(mask & TSI148_LCSR_INT_IRQM,
					     vbridge);
			mask &= ~TSI148_LCSR_INT_IRQM;
		}

		/* Check that we handled everything */
		if (mask)
			pr_warn(PFX
			       "Unhandled interrupt %08x (enabled %08x)\n",
			       mask, vbridge->irq_mgr.vme_interrupts_enabled);
	}

	return IRQ_HANDLED;
}

/**
 * tsi148_generate_interrupt() - Generate an interrupt on the VME bus
 * @level: IRQ level (1-7)
 * @vector: IRQ vector (0-255)
 * @msecs: Timeout for IACK in milliseconds
 *
 *  This function generates an interrupt on the VME bus and waits for IACK
 * for msecs milliseconds.
 *
 *  Returns 0 on success or -ETIME if the timeout expired.
 *
 */
int tsi148_generate_interrupt(struct vme_bridge_device *vbridge,
			      int level, int vector, signed long msecs)
{
	unsigned int val;
	signed long timeout = msecs_to_jiffies(msecs);
	struct tsi148_regs *regs;
	struct tsi148_chip *chip;

	/* Generate VME IRQ */
	chip = vbridge->chip_mgr.private;
	regs = chip->pci_map;
	val = ioread32be(&regs->lcsr.vicr) & ~TSI148_LCSR_VICR_STID_M;
	val |= ((level << 8) | vector);
	iowrite32be(val, &regs->lcsr.vicr);

	/* Wait until IACK */
	while (ioread32be(&regs->lcsr.vicr) & TSI148_LCSR_VICR_IRQS) {
		set_current_state(TASK_INTERRUPTIBLE);
		timeout = schedule_timeout(timeout);

		if (timeout == 0)
			break;
	}

	if (ioread32be(&regs->lcsr.vicr) & TSI148_LCSR_VICR_IRQS) {
		/* No IACK received, clear the IRQ */
		val = ioread32be(&regs->lcsr.vicr);

		val &= ~(TSI148_LCSR_VICR_IRQL_M | TSI148_LCSR_VICR_STID_M);
		val |= TSI148_LCSR_VICR_IRQC;
		iowrite32be(val, &regs->lcsr.vicr);

		return -ETIME;
	}

	return 0;
}


#define TSI148_IRQ_MASK (TSI148_LCSR_INT_DMA1 | TSI148_LCSR_INT_DMA0 | \
			 TSI148_LCSR_INT_LM3  | TSI148_LCSR_INT_LM2  | \
			 TSI148_LCSR_INT_LM1  | TSI148_LCSR_INT_LM0  | \
			 TSI148_LCSR_INT_MB3  | TSI148_LCSR_INT_MB2  | \
			 TSI148_LCSR_INT_MB1  | TSI148_LCSR_INT_MB0  | \
			 TSI148_LCSR_INT_PERR | TSI148_LCSR_INT_VERR)
#define TSI148_IRQ_MASK_SYSCON (TSI148_LCSR_INT_IRQ7 | TSI148_LCSR_INT_IRQ6 | \
				TSI148_LCSR_INT_IRQ5 | TSI148_LCSR_INT_IRQ4 | \
				TSI148_LCSR_INT_IRQ3 | TSI148_LCSR_INT_IRQ2 | \
				TSI148_LCSR_INT_IRQ1)

static int tsi148_enable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask, enabled, new, res;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	/*
	 * Comment found in the current implementation:
	 * "in the IRQ handler,interrupt status should be read from the VME bus
	 * to make sure the internal FIFO has been flushed of pending writes."
	 * Therefore the CRG address space is mapped into a VME window
	 */
	if (chip->vme_map == NULL) {
		res = tsi148_vme_map_crg(chip);
		if (res)
			return res;
	}

	/* Enable DMA, mailbox, VIRQ (syscon only) & LM Interrupts */
	intmask = TSI148_IRQ_MASK;
	if (vbridge->syscon)
		intmask |= TSI148_IRQ_MASK_SYSCON;

	pr_debug(PFX "Enabling interrupts %08x\n", intmask);
	enabled = tsi148_get_int_enabled(chip->pci_map);
	new = enabled | intmask;
	vbridge->irq_mgr.vme_interrupts_enabled = new;
	return tsi148_set_interrupts(chip->pci_map, new);
}

static int tsi148_disable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask, enabled, new;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	/* Interrupts are going to be disabled, unmap CRG address space */
	tsi148_vme_unmap_crg(chip);

	/* Enable DMA, mailbox, VIRQ (syscon only) & LM Interrupts */
	intmask = TSI148_IRQ_MASK;
	if (vbridge->syscon)
		intmask |= TSI148_IRQ_MASK_SYSCON;

	enabled = tsi148_get_int_enabled(chip->pci_map);
	new = enabled & ~intmask;
	vbridge->irq_mgr.vme_interrupts_enabled = new;
	return tsi148_set_interrupts(chip->pci_map, new);
}

static struct vme_bridge_irq_ops tsi148_irq_ops = {
	.enable_interrupts = tsi148_enable_interrupts,
	.disable_interrupts = tsi148_disable_interrupts,
	.interrupt_handler = tsi148_interrupt_handler,
	.iack = tsi148_iack8,
	.generate_interrupt = tsi148_generate_interrupt,
};

int tsi148_irq_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->irq_mgr.ops = &tsi148_irq_ops;
	vbridge->int_stats = int_stats;
	vbridge->int_stats_count = ARRAY_SIZE(int_stats);
	return 0;
}
