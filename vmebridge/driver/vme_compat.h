// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) 2018 CERN
 * Author: Federico Vaga <federico.vaga@vaga.pv.it>
 */
#ifndef __VME_COMPAT_H__
#define __VME_COMPAT_H__

#include <linux/version.h>
#include <linux/mm_types.h>
#include <linux/ioport.h>
#include <linux/device.h>

#if KERNEL_VERSION(3, 11, 0) > LINUX_VERSION_CODE
#define __ATTR_RW(_name) __ATTR(_name, (S_IWUSR | S_IRUGO),	\
			 _name##_show, _name##_store)
#define __ATTR_WO(_name) {						\
	.attr	= { .name = __stringify(_name), .mode = S_IWUSR },	\
	.store	= _name##_store,					\
}
#define DEVICE_ATTR_RW(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_RW(_name)
#define DEVICE_ATTR_RO(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_RO(_name)
#define DEVICE_ATTR_WO(_name) \
	struct device_attribute dev_attr_##_name = __ATTR_WO(_name)
#endif

#if KERNEL_VERSION(3, 10, 0) > LINUX_VERSION_CODE
/* macros to create static binary attributes easier */
#define __BIN_ATTR(_name, _mode, _read, _write, _size) {		\
	.attr = { .name = __stringify(_name), .mode = _mode },		\
	.read	= _read,						\
	.write	= _write,						\
	.size	= _size,						\
}

#define BIN_ATTR(_name, _mode, _read, _write, _size)			\
struct bin_attribute bin_attr_##_name = __BIN_ATTR(_name, _mode, _read, \
					_write, _size)
#endif

#if KERNEL_VERSION(3,12,0) > LINUX_VERSION_CODE
#ifdef RHEL_RELEASE_VERSION
#if RHEL_RELEASE_VERSION(7, 0) > RHEL_RELEASE_CODE
#define BUS_ATTR_RW(_name) ;
#define BUS_ATTR_WO(_name) ;
#endif
#else
#define BUS_ATTR_RW(_name) ;
#define BUS_ATTR_WO(_name) ;
#endif
#endif

extern long get_user_pages_l(unsigned long start, unsigned long nr_pages,
			     unsigned int gup_flags, struct page **pages,
			     struct vm_area_struct **vmas);

extern int insert_resource_l(struct resource *parent, struct resource *new);
#endif
