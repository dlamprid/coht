// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#include <linux/dmaengine.h>
#include <linux/delay.h>
#include <linux/sched.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "tsi148.h"


/**
 * vme_setup_dma_attributes() - Build the DMA attributes word
 * @v2esst_mode: VME 2eSST transfer speed
 * @data_width: VME data width (D16 or D32 only)
 * @am: VME address modifier
 *
 * This function builds the DMA attributes word. All parameters are
 * checked.
 *
 * NOTE: The function relies on the fact that the DSAT and DDAT registers have
 *       the same layout.
 *
 * Returns %EINVAL if any parameter is not consistent.
 */
static int tsi148_setup_dma_attributes(enum vme_2esst_mode v2esst_mode,
				       enum vme_data_width data_width,
				       enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	switch (v2esst_mode) {
	case VME_SST160:
	case VME_SST267:
	case VME_SST320:
		attrs |= ((v2esst_mode << TSI148_LCSR_DSAT_2eSSTM_SHIFT) &
			  TSI148_LCSR_DSAT_2eSSTM_M);
		break;
	default:
		pr_err("%s: invalid v2esst_mode %d\n",
		       __func__, v2esst_mode);
		return -EINVAL;
	}

	switch (data_width) {
	case VME_D16:
		attrs |= ((TSI148_DW_16 << TSI148_LCSR_DSAT_DBW_SHIFT) &
			  TSI148_LCSR_DSAT_DBW_M);
		break;
	case VME_D32:
		attrs |= ((TSI148_DW_32 << TSI148_LCSR_DSAT_DBW_SHIFT) &
			  TSI148_LCSR_DSAT_DBW_M);
		break;
	default:
		pr_err("%s: invalid data_width %d\n",
		       __func__, data_width);
		return -EINVAL;
	}

	if (tsi148_am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access)) {
		pr_err("%s: invalid am %x\n",
		       __func__, am);
		return -EINVAL;
	}

	switch (transfer_mode) {
	case TSI148_SCT:
	case TSI148_BLT:
	case TSI148_MBLT:
	case TSI148_2eVME:
	case TSI148_2eSST:
	case TSI148_2eSSTB:
		attrs |= ((transfer_mode << TSI148_LCSR_DSAT_TM_SHIFT) &
			  TSI148_LCSR_DSAT_TM_M);
		break;
	default:
		pr_err("%s: invalid transfer_mode %d\n",
		       __func__, transfer_mode);
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_DSAT_SUP;
		break;
	case TSI148_USER:
		break;
	default:
		pr_err("%s: invalid user_access %d\n",
		       __func__, user_access);
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_DSAT_PGM;
		break;
	case TSI148_DATA:
		break;
	default:
		pr_err("%s: invalid data_access %d\n",
		       __func__, data_access);
		return -EINVAL;
	}

	switch (addr_size) {
	case TSI148_A16:
	case TSI148_A24:
	case TSI148_A32:
	case TSI148_A64:
	case TSI148_CRCSR:
	case TSI148_USER1:
	case TSI148_USER2:
	case TSI148_USER3:
	case TSI148_USER4:
		attrs |= ((addr_size << TSI148_LCSR_DSAT_AMODE_SHIFT) &
			  TSI148_LCSR_DSAT_AMODE_M);
		break;
	default:
		pr_err("%s: invalid addr_size %d\n",
		       __func__, addr_size);
		return -EINVAL;
	}

	return attrs;
}

/**
 * tsi148_dma_setup_src() - Setup the source attributes for a DMA transfer
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_src(struct vme_dma *desc)
{
	int rc;

	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* src = PCI */
		rc = TSI148_LCSR_DSAT_TYP_PCI;
		break;
	case VME_DMA_FROM_DEVICE: /* src = VME */
		rc = tsi148_setup_dma_attributes(desc->src.v2esst_mode,
						 desc->src.data_width,
						 desc->src.am);

		if (rc >= 0)
			rc |= TSI148_LCSR_DSAT_TYP_VME;

		break;
	default:
		pr_err("%s: invalid direction %d\n",
		       __func__, desc->dir);
		rc = -EINVAL;
	}

	return rc;
}

/**
 * tsi148_dma_setup_dst() - Setup the destination attributes for a DMA transfer
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_dst(struct vme_dma *desc)
{
	int rc;

	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* dst = VME */
		rc = tsi148_setup_dma_attributes(desc->dst.v2esst_mode,
						 desc->dst.data_width,
						 desc->dst.am);

		if (rc >= 0)
			rc |= TSI148_LCSR_DDAT_TYP_VME;

		break;
	case VME_DMA_FROM_DEVICE: /* dst = PCI */
		rc = TSI148_LCSR_DDAT_TYP_PCI;
		break;
	default:
		pr_err("%s: invalid direction %d\n",
		       __func__, desc->dir);
		rc = -EINVAL;
	}

	return rc;
}

/**
 * tsi148_dma_setup_ctl() - Setup the DMA control word
 * @desc: DMA transfer descriptor
 *
 */
static int tsi148_dma_setup_ctl(struct vme_dma *desc)
{
	int rc = 0;

	/* A little bit of checking. Could be done earlier, dunno. */
	if ((desc->ctrl.vme_block_size < VME_DMA_BSIZE_32) ||
	    (desc->ctrl.vme_block_size > VME_DMA_BSIZE_4096))
		return -EINVAL;

	if ((desc->ctrl.vme_backoff_time < VME_DMA_BACKOFF_0) ||
	    (desc->ctrl.vme_backoff_time > VME_DMA_BACKOFF_64))
		return -EINVAL;

	if ((desc->ctrl.pci_block_size < VME_DMA_BSIZE_32) ||
	    (desc->ctrl.pci_block_size > VME_DMA_BSIZE_4096))
		return -EINVAL;

	if ((desc->ctrl.pci_backoff_time < VME_DMA_BACKOFF_0) ||
	    (desc->ctrl.pci_backoff_time > VME_DMA_BACKOFF_64))
		return -EINVAL;

	rc |= (desc->ctrl.vme_block_size << TSI148_LCSR_DCTL_VBKS_SHIFT) &
		TSI148_LCSR_DCTL_VBKS_M;

	rc |= (desc->ctrl.vme_backoff_time << TSI148_LCSR_DCTL_VBOT_SHIFT) &
		TSI148_LCSR_DCTL_VBOT_M;

	rc |= (desc->ctrl.pci_block_size << TSI148_LCSR_DCTL_PBKS_SHIFT) &
		TSI148_LCSR_DCTL_PBKS_M;

	rc |= (desc->ctrl.pci_backoff_time << TSI148_LCSR_DCTL_PBOT_SHIFT) &
		TSI148_LCSR_DCTL_PBOT_M;

	return rc;
}



/*
 * Setup the hardware descriptors for the link list.
 * Beware that the fields have to be setup in big endian mode.
 */
static int tsi148_fill_dma_desc(struct vme_dma_tx_desc *ttx_desc,
				struct tsi148_dma_desc *tsi,
				unsigned int vme_addr, dma_addr_t dma_addr,
				unsigned int len,
				unsigned int dsat, unsigned int ddat)
{
	struct vme_dma *desc = &ttx_desc->desc;

	/* Setup the source and destination addresses */
	tsi->dsau = 0;
	tsi->ddau = 0;

	switch (ttx_desc->direction) {
	case DMA_MEM_TO_DEV: /* src = PCI - dst = VME */
		tsi->dsal = cpu_to_be32(dma_addr);
		tsi->ddal = cpu_to_be32(vme_addr);
		tsi->ddbs = cpu_to_be32(desc->dst.bcast_select);
		break;

	case DMA_DEV_TO_MEM: /* src = VME - dst = PCI */
		tsi->dsal = cpu_to_be32(vme_addr);
		tsi->ddal = cpu_to_be32(dma_addr);
		tsi->ddbs = 0;
		break;

	default:
		return -EINVAL;
	}

	tsi->dcnt = cpu_to_be32(len);
	tsi->dsat = cpu_to_be32(dsat);
	tsi->ddat = cpu_to_be32(ddat);

	/* By default behave as if we were at the end of the list */
	tsi->dnlau = 0;
	tsi->dnlal = cpu_to_be32(TSI148_LCSR_DNLAL_LLA);

	return 0;
}

static int
hwdesc_init(struct vme_dma_chan *vdchan, dma_addr_t *phys,
	    struct tsi148_dma_desc **virt, struct hw_desc_entry **hw_desc)
{
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	struct hw_desc_entry *entry;

	/* Allocate a HW DMA descriptor from the pool */
	*virt = pci_pool_alloc(dma_mgr->dma_pool, GFP_KERNEL, phys);
	if (!*virt)
		return -ENOMEM;

	/* keep the virt. and phys. addresses of the descriptor in a list */
	*hw_desc = kmalloc(sizeof(struct hw_desc_entry), GFP_KERNEL);
	if (!*hw_desc) {
		pci_pool_free(dma_mgr->dma_pool, *virt, *phys);
		return -ENOMEM;
	}

	entry = *hw_desc;
	entry->va = *virt;
	entry->phys = *phys;
	list_add_tail(&entry->list, &vdchan->hw_desc_list);

	return 0;
}

#ifdef DEBUG_DMA
static void tsi148_dma_debug_info(struct tsi148_dma_desc *tsi, int i, int j)
{
	pr_debug(PFX "descriptor %d-%d @%p\n", i, j, tsi);
	pr_debug(PFX "  src : %08x:%08x  %08x\n",
		be32_to_cpu(tsi->dsau), be32_to_cpu(tsi->dsal),
		be32_to_cpu(tsi->dsat));
	pr_debug(PFX "  dst : %08x:%08x  %08x\n",
		be32_to_cpu(tsi->ddau), be32_to_cpu(tsi->ddal),
		be32_to_cpu(tsi->ddat));
	pr_debug(PFX "  cnt : %08x\n",	be32_to_cpu(tsi->dcnt));
	pr_debug(PFX "  nxt : %08x:%08x\n",
		be32_to_cpu(tsi->dnlau), be32_to_cpu(tsi->dnlal));
}
#else
static void tsi148_dma_debug_info(struct tsi148_dma_desc *tsi, int i, int j)
{
}
#endif


/*
 * verbatim from the VME64 standard:
 * 2.3.7 Block Transfer Capabilities, p. 42.
 *	RULE 2.12a: D08(EO), D16, D32 and MD32 block transfer cycles
 *	  (BLT) *MUST NOT* cross any 256 byte boundary.
 *	RULE 2.78: MBLT cycles *MUST NOT* cross any 2048 byte boundary.
 */
static inline int __tsi148_get_bshift(int am)
{
	switch (am) {
	case VME_A64_MBLT:
	case VME_A32_USER_MBLT:
	case VME_A32_SUP_MBLT:
	case VME_A24_USER_MBLT:
	case VME_A24_SUP_MBLT:
		return 11;
	case VME_A64_BLT:
	case VME_A32_USER_BLT:
	case VME_A32_SUP_BLT:
	case VME_A40_BLT:
	case VME_A24_USER_BLT:
	case VME_A24_SUP_BLT:
		return 8;
	default:
		return 0;
	}
}

static int tsi148_get_bshift(struct vme_dma_tx_desc *ttx_desc)
{
	struct vme_dma *desc = &ttx_desc->desc;
	int am;

	if (ttx_desc->direction == DMA_DEV_TO_MEM)
		am = desc->src.am;
	else
		am = desc->dst.am;
	return __tsi148_get_bshift(am);
}

/* Note: block sizes are always powers of 2 */
static inline unsigned long tsi148_get_bsize(int bshift)
{
	return 1 << bshift;
}

static inline unsigned long tsi148_get_bmask(unsigned long bsize)
{
	return bsize - 1;
}

/*
 * Add a certain chunk of data to the TSI DMA linked list.
 * Note that this function deals with physical addresses on the
 * host CPU and VME addresses on the VME bus.
 */
static int
tsi148_dma_link_add(struct vme_dma_tx_desc *ttx_desc,
		    struct tsi148_dma_desc **virt,
		unsigned int vme_addr, dma_addr_t dma_addr, unsigned int size,
		int numpages, int index, int bshift, unsigned int dsat,
		unsigned int ddat)

{
	struct vme_dma_chan *vdchan = to_vme_dma_chan(ttx_desc->tx.chan);
	struct hw_desc_entry *hw_desc = NULL;
	struct tsi148_dma_desc *curr;
	struct tsi148_dma_desc *next = NULL;
	struct vme_dma *desc = &ttx_desc->desc;
	dma_addr_t phys_next;
	dma_addr_t dma;
	dma_addr_t dma_end;
	unsigned int vme;
	unsigned int vme_end;
	unsigned int len;
	int rc;
	int i = 0;

	/*
	 * The function expects **virt to be already initialised.
	 * When calling this function in a loop, it will always set virt
	 * to point to the next link in the chain, or to NULL when the
	 * current link is the last in the chain.
	 */
	BUG_ON(virt == NULL || *virt == NULL);
	curr = *virt;

	vme = vme_addr;
	vme_end = vme_addr + size;

	dma = dma_addr;
	dma_end = dma_addr + size;

	len = size;

	while (vme < vme_end && dma < dma_end) {

		/* calculate the length up to the next block boundary */
		if (bshift) {
			unsigned long bsize = tsi148_get_bsize(bshift);
			unsigned long bmask = tsi148_get_bmask(bsize);
			unsigned int unaligned = vme & bmask;

			len = bsize - unaligned;
		}

		/* check the VME block won't overflow */
		if (dma + len > dma_end)
			len = dma_end - dma;

		tsi148_fill_dma_desc(ttx_desc, curr, vme, dma, len, dsat,
				     ddat);

		/* increment the VME address unless novmeinc is set */
		if (!desc->novmeinc)
			vme += len;
		dma += len;

		/* chain consecutive links together */
		if (index < numpages - 1 || dma < dma_end) {
			rc = hwdesc_init(vdchan, &phys_next, &next, &hw_desc);
			if (rc)
				return rc;

			curr->dnlau = 0;
			curr->dnlal = cpu_to_be32(phys_next &
						TSI148_LCSR_DNLAL_DNLAL_M);
		}

		tsi148_dma_debug_info(curr, index, i);

		curr = next;
		i++;
	}

	*virt = next;
	return 0;
}

static inline int get_vmeaddr(struct vme_dma *desc, unsigned int *vme_addr)
{
	switch (desc->dir) {
	case VME_DMA_TO_DEVICE: /* src = PCI - dst = VME */
		*vme_addr = desc->dst.addrl;
		return 0;
	case VME_DMA_FROM_DEVICE: /* src = VME - dst = PCI */
		*vme_addr = desc->src.addrl;
		return 0;
	default:
		return -EINVAL;
	}
}

/**
 * tsi148_dma_get_status() - Get the status of a DMA channel
 * @chan: DMA channel descriptor
 *
 **/
static inline int tsi148_dma_get_status(struct tsi148_dma *dma_regs)
{
	return ioread32be(&dma_regs->dsta);
}

static inline void tsi148_dctl_bit_set(struct tsi148_dma *dma_regs,
				unsigned long bitmask)
{
	unsigned int dctl;

	dctl = ioread32be(&dma_regs->dctl);
	dctl |= bitmask;
	iowrite32be(dctl, &dma_regs->dctl);

}

static inline void tsi148_dctl_bit_clr(struct tsi148_dma *dma_regs,
				unsigned long bitmask)
{
	unsigned int dctl;

	dctl = ioread32be(&dma_regs->dctl);
	dctl &= ~bitmask;
	iowrite32be(dctl, &dma_regs->dctl);

}

static int tsi148_dma_check_state(struct vme_dma_chan *vdchan,
		enum vme_dma_state_field state)
{
	struct tsi148_dma *dma_regs = vdchan->private;
	int ret;

	switch (state) {
	case VME_DMA_IS_BUSY:
		ret = tsi148_dma_get_status(dma_regs) & TSI148_LCSR_DSTA_BSY;
		break;
	case VME_DMA_IS_DONE:
		ret = tsi148_dma_get_status(dma_regs) & TSI148_LCSR_DSTA_DON;
		break;
	case VME_DMA_IS_PAUSE:
		ret = tsi148_dma_get_status(dma_regs) & TSI148_LCSR_DSTA_PAU;
		break;
	default:
		return -EINVAL;
	}
	return ret;
}

static int tsi148_dma_set_ctrl(struct vme_dma_chan *vdchan,
		enum vme_dma_ctrl_field ctrl)
{
	struct tsi148_dma *dma_regs = vdchan->private;

	switch (ctrl) {
	case VME_DMA_LIST_MODE:
		tsi148_dctl_bit_clr(dma_regs, TSI148_LCSR_DCTL_MOD);
		break;
	case VME_DMA_DIRECT_MODE:
		tsi148_dctl_bit_set(dma_regs, TSI148_LCSR_DCTL_MOD);
		break;
	case VME_DMA_PAUSE:
		tsi148_dctl_bit_set(dma_regs, TSI148_LCSR_DCTL_PAU);
		break;
	case VME_DMA_START:
		tsi148_dctl_bit_set(dma_regs, TSI148_LCSR_DCTL_DGO);
		break;
	case VME_DMA_ABORT:
		tsi148_dctl_bit_set(dma_regs, TSI148_LCSR_DCTL_ABT);
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

/**
 * tsi148_dma_free_chain() - Free all the linked HW DMA descriptors of a channel
 * @chan: DMA channel
 *
 */
void tsi148_dma_free(struct vme_dma_chan *vdchan)
{
	struct vme_bridge_dma_mgr *dma_mgr =
				to_vme_bridge_dma_mgr(vdchan->dchan.device);
	struct hw_desc_entry *hw_desc;
	struct hw_desc_entry *tmp;
	int count = 1;

	list_for_each_entry_safe(hw_desc, tmp, &vdchan->hw_desc_list, list) {
		pci_pool_free(dma_mgr->dma_pool, hw_desc->va, hw_desc->phys);
		list_del(&hw_desc->list);
		kfree(hw_desc);
		count++;
	}
}


/**
 * vme_dma_setup_chain() - Setup the linked list of TSI148 DMA descriptors
 * @chan: DMA channel descriptor
 * @dsat: DMA source attributes
 * @ddat: DMA destination attributes
 *
 */
static int tsi148_dma_setup_chain(struct vme_dma_tx_desc *ttx_desc,
				  unsigned int dsat, unsigned int ddat)
{
	struct vme_dma_chan *vdchan = to_vme_dma_chan(ttx_desc->tx.chan);
	int i;
	int rc;
	struct scatterlist *sg;
	struct vme_dma *desc = &ttx_desc->desc;
	dma_addr_t phys_start;
	struct tsi148_dma_desc *curr = NULL;
	struct hw_desc_entry *hw_desc = NULL;
	unsigned int vme_addr = 0;
	dma_addr_t dma_addr;
	unsigned int len;
	int bshift = tsi148_get_bshift(ttx_desc);
	struct tsi148_dma *dma_regs;

	rc = hwdesc_init(vdchan, &phys_start, &curr, &hw_desc);
	if (rc)
		return rc;

	rc = get_vmeaddr(desc, &vme_addr);
	if (rc)
		goto out_free;

	for_each_sg(ttx_desc->sgl, sg, ttx_desc->sg_len, i) {
		dma_addr = sg_dma_address(sg);
		len = sg_dma_len(sg);

		rc = tsi148_dma_link_add(ttx_desc, &curr, vme_addr, dma_addr,
					 len, ttx_desc->sg_len, i, bshift,
					 dsat, ddat);
		if (rc)
			goto out_free;
		/* For non incrementing DMA, reset the VME address */
		if (!desc->novmeinc)
			vme_addr += len;
	}

	/* Now program the DMA registers with the first entry in the list */
	dma_regs = vdchan->private;
	iowrite32be(0, &dma_regs->dma_desc.dnlau);
	iowrite32be(phys_start & TSI148_LCSR_DNLAL_DNLAL_M,
		    &dma_regs->dma_desc.dnlal);

	/* always use the scatter-gather list */
	tsi148_dctl_bit_clr(vdchan->private, TSI148_LCSR_DCTL_MOD);

	return 0;

out_free:
	tsi148_dma_free(vdchan);


	return rc;
}


/**
 * vme_dma_setup() - Setup a TSI148 DMA channel for a transfer
 * @chan: DMA channel descriptor
 *
 */
static int tsi148_dma_setup(struct vme_dma_chan *dchan)
{
	struct vme_dma *desc = &dchan->tx_desc_current->desc;
	unsigned int dsat;
	unsigned int ddat;
	unsigned int dctl;
	int rc;

	/* Setup DMA source attributes */
	rc = tsi148_dma_setup_src(desc);
	if (rc < 0) {
		pr_err("%s: src setup failed\n", __func__);
		return rc;
	}

	dsat = rc;

	/* Setup DMA destination attributes */
	rc = tsi148_dma_setup_dst(desc);
	if (rc < 0) {
		pr_err("%s: dst setup failed\n", __func__);
		return rc;
	}

	ddat = rc;

	/* Setup DMA control */
	rc = tsi148_dma_setup_ctl(desc);
	if (rc < 0) {
		pr_err("%s: ctl setup failed\n", __func__);
		return rc;
	}

	dctl = rc;

	rc = tsi148_dma_setup_chain(dchan->tx_desc_current, dsat, ddat);

	return rc;
}


static struct dma_pool *tsi148_dma_pool_create(struct pci_dev *pdev)
{
	/*
	 * Create the DMA chained descriptor pool.
	 * Those descriptors must be 64-bit aligned as specified in the
	 * TSI148 User Manual. Also do not allow descriptors to cross a
	 * page boundary as the 2 pages may not be contiguous.
	 */
	return pci_pool_create("tsi148_dma_desc_pool", pdev,
			       sizeof(struct tsi148_dma_desc), 4, 4096);
}

static int tsi148_dma_get_nr_chan(void)
{
	return TSI148_NR_DMA_CHANNELS;
}

static int tsi148_dma_get_capabilities(void)
{
	/* TSI148 DMA has a programmable block size and back-off timer */
	return VME_DMA_TRANSFER_BACKOFF_TIME | VME_DMA_TRANSFER_BLK_SZ;
}

static int tsi148_dma_chan_init(struct vme_bridge_device *vbridge,
				struct vme_dma_chan *vdchan)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	/* private pointer points to chan dma ctrl registers */
	vdchan->private = &chip->pci_map->lcsr.dma[vdchan->chan_idx];
	/* always use the scatter-gather list */
	tsi148_dctl_bit_clr(vdchan->private, TSI148_LCSR_DCTL_MOD);
	return 0;
}

static void tsi148_dma_transfer_pre(struct vme_dma *desc_new,
				    struct vme_dma *desc, int to_user)
{
	memcpy(desc_new, desc, sizeof(*desc_new));
}

static struct vme_bridge_dma_ops tsi148_dma_ops = {
	.dma_pool_create = tsi148_dma_pool_create,
	.dma_get_nr_chan = tsi148_dma_get_nr_chan,
	.dma_get_capabilities = tsi148_dma_get_capabilities,
	.dma_chan_init = tsi148_dma_chan_init,
	.dma_chan_release = NULL, /* nothing specific to release */
	.dma_set_ctrl = tsi148_dma_set_ctrl,
	.dma_check_state = tsi148_dma_check_state,
	.dma_setup = tsi148_dma_setup,
	.dma_free = tsi148_dma_free,
	.dma_transfer_pre = tsi148_dma_transfer_pre,
	.dma_transfer_post = NULL,
};

int tsi148_dma_set_ops(struct vme_bridge_device *vbridge)
{

	/* set specific bridge dma ops */
	vbridge->dma_mgr.ops = &tsi148_dma_ops;

	return 0;
}
