/* SPDX-License-Identifier: GPL-2.0-or-later */
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#ifndef _VME__WINDOW_H_
#define _VME__WINDOW_H_

#include <linux/kernel.h>


/**
 * struct vme_taskinfo - Store information about a mapping's user
 *
 * @pid_nr:	pid number
 * @name:	name of the process
 * @file:	struct file to identify the owner of the mapping. Set it to
 *		NULL when the request comes from the kernel. In that case
 *		@pid_nr and @name won't be filled in.
 *
 * @note	on the name length.
 *		As it's not only the process name that is stored here, but also
 *		module name -- lengths should be MAX allowed for the module name
 */
struct vme_taskinfo {
	pid_t	pid_nr;
	char	name[MODULE_NAME_LEN];
	struct file	*file;
};

/**
 * struct mapping - Logical mapping descriptor
 * @list: List of the mappings
 * @mapping: The mapping descriptor
 * @client: The user of this mapping
 *
 * This structure holds the information concerning logical mappings
 * made on top a hardware windows.
 */
struct mapping {
	struct list_head	list;
	struct vme_mapping	desc;
	struct vme_taskinfo	client;
};

/**
 * struct window - Hardware window descriptor.
 * @lock: Mutex protecting the descriptor
 * @active: Flag indicating whether the window is in use
 * @rsrc: PCI bus resource of the window
 * @desc: This physical window descriptor
 * @mappings: List of mappings using this window
 * @users: Number of users of this window
 *
 * This structure holds the information concerning hardware
 * windows.
 *
 */
struct window {
	char			*name;
	struct mutex		lock;
	unsigned int		active;
	struct resource		*rsrc;
	struct vme_mapping	desc;
	struct list_head	mappings;
	int			users;
};

struct vme_bridge_device; /* forward declaration */
struct vme_bridge_window_ops {
	int (*get_nr_windows)(void);
	void (*get_window_attr)(struct vme_bridge_device *vbridge,
				 struct vme_mapping *desc);
	int (*setup_window)(struct vme_bridge_device *vbridge,
			     struct window *wind);
	void (*release_window)(struct vme_bridge_device *vbridge,
				struct window *wind);
};

/**
 * wind_mgr
 */
struct vme_bridge_window_mgr {

	struct window          *winds;
	int                    nr_winds;

	struct vme_bridge_window_ops *ops;

	void *private; /* pointer to hold bridge irq specific HW info */
};

#endif /* _VME__WINDOW_H_ */
