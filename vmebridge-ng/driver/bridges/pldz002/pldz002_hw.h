/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * pldz002_hw.h - Low level support for the PLDZ002 IP Core which is the
 * PCI-VME Bridge from MEN
 */

#ifndef _PLDZ002_HW_H
#define _PLDZ002_HW_H

#ifndef PLDZ002_REV
#define PLDZ002_REV 7
#endif

#define PCI_VENDOR_ID_MEN		0x1a88
#define PCI_DEVICE_ID_MEN_PLDZ002	0x4d45
#define PCI_SUBVENDOR_ID_MEN		0x00d5
#define PCI_SUBDEVICE_ID_MEN_PLDZ002	0x5a91

#define PLDZ002_BARS_COUNT 5
#define PLDZ002_BARS_MASK ((1 << PLDZ002_BARS_COUNT) - 1)

#define PLDZ002_NR_WINDOWS 8      /* Max number of VME windows */
#define PLDZ002_NR_DMA_CHANNELS 1 /* Number of DMA channel */

/* PLDZ002 3 memory regions permanently mapped */
/* Region: VME control registers */
#define PLDZ002_REGS_OFFSET   0x10000 /* REGS region offset in BAR0 */
#define PLDZ002_REGS_SIZE     0x100   /* REGS region size */
/* Region: Interrupt acknowledge registers */
#define PLDZ002_IACK_OFFSET   0x10100 /* IACK region offset in BAR0 */
#define PLDZ002_IACK_SIZE     0x10    /* IACK region size */
/* Region SRAM: DMA Buffer Descriptors and Mailbox Data Registers local RAM */
#define PLDZ002_SRAM_OFFSET	   0xFF800 /* SRAM regs region offset in BAR1 */
#define PLDZ002_SRAM_SIZE	   0x800   /* SRAM region size */

#define PLDZ002_VME_A16D16_OFFSET 0x20000    /* offset in BAR0 */
#define PLDZ002_VME_A16D32_OFFSET 0x30000    /* offset in BAR0 */
#define PLDZ002_VME_A16_SIZE      0x10000    /* A16 space size */
#define PLDZ002_VME_A24D16_OFFSET 0x0        /* offset in BAR2 */
#define PLDZ002_VME_A24D32_OFFSET 0x1000000  /* offset in BAR2 */
#define PLDZ002_VME_A24_SIZE      0x1000000  /* A24 space size */
#define PLDZ002_VME_A32D32_OFFSET 0x0	     /* offset in BAR3 */
#define PLDZ002_VME_A32_SIZE      0x20000000 /* A32 space size */
#define PLDZ002_VME_CRCSR_OFFSET  0x0        /* offset in BAR4 */
#define PLDZ002_VME_CRCSR_SIZE    0x1000000  /* CR/CSR space size */

/* DMA buffer descriptor located in SRAM and DMA Address modifiers */
#define PLDZ002_SRAM_DMA_BD_OFFSET 0x100   /**< DMA BD offset in SRAM regs */
#define PLDZ002_DMA_MAX_BDS        0x10    /**< max number of DMA BDs */
#define PLDZ002_DMA_BD_SIZE	   0x10    /**< DMA buffer descriptor size */
#define PLDZ002_DMA_MAX_BLOCK_SIZE 0x40000 /**< Max BD dma size in bytes */
#define PLDZ002_DMA_AM_A16D16	   (0x1<<4)
#define PLDZ002_DMA_AM_A16D32      (0x5<<4)
#define PLDZ002_DMA_AM_A24D16      (0x0<<4)
#define PLDZ002_DMA_AM_A24D32      (0x4<<4)
#define PLDZ002_DMA_AM_A24D64      (0xc<<4)
#define PLDZ002_DMA_AM_A32D32      (0x6<<4)
#define PLDZ002_DMA_AM_A32D64      (0xe<<4)
#define PLDZ002_DMA_BLK_TX         (0x0<<3) /* DMA block transfert */
#define PLDZ002_DMA_SINGLE_TX      (0x1<<3) /* DMA single transfert */
#define PLDZ002_DMABD_NOINC_DST    0x02
#define PLDZ002_DMABD_NOINC_SRC    0x04
#define PLDZ002_DMABD_DIR_VME      0x02 /* DMA source or destination is VME */
#define PLDZ002_DMABD_DIR_PCI      0x04 /* DMA source or destination is PCI */
#define PLDZ002_DMABD_SRC(x)       ((x)<<16) /* Macro to set SRC (VME or PCI)  */
#define PLDZ002_DMABD_DST(x)       ((x)<<12) /* Macro to set DEST (VME or PCI)  */
#define PLDZ002_DMABD_END          0x01 /* Indicates last buffer descriptor */

/* CTRL 0x00 */
#define PLDZ002_INTR		0x00 /**< Interrupt Control Register (r/w) */
#define PLDZ002_INTR_IL_MASK	0x07 /**< IL0..2 mask	*/
#define PLDZ002_INTR_INTEN	0x08 /**< INTEN bit	*/

/* CTRL 0x04 */
#define PLDZ002_INTID           0x04 /**< VME Interrupt STATUS/ID Reg (r/w) */

/* CTRL 0x08 */
#define PLDZ002_ISTAT           0x08 /**< Interrupt Status Register (r) */

/* CTRL 0x0C */
#define PLDZ002_IMASK		0x0c /**< Interrupt Mask Register (r/w) */
#define PLDZ002_IMASK_ACFEN	0x1 /**< ACFEN bit */
#define PLDZ002_IMASK_IEN(lev)  ((1<<(lev))&0xfe) /**< set IEN1..7 bit */

/* CTRL 0x10 */
#define PLDZ002_MSTR		0x0010 /**< Master Control Register (r/w) */
#define PLDZ002_MSTR_RMW	0x01   /**< RMW bit */
#define PLDZ002_MSTR_REQ	0x02   /**< REQ bit */
#define PLDZ002_MSTR_BERR	0x04   /**< BERR bit */
#define PLDZ002_MSTR_IBERREN	0x08   /**< IBERREN bit	*/
#define PLDZ002_MSTR_POSTWR	0x10   /**< POSTWR bit */
#define PLDZ002_MSTR_AONLY	0x20   /**< AONLY bit */
#define PLDZ002_MSTR_VFAIR	0x40   /**< VFAIR bit */

/* CTRL 0x14 */
#define PLDZ002_SLV24		      0x0014 /**< Slave Control Reg. A24 (r/w) */
#define PLDZ002_SLV24_SLEN24	      0x0010 /**< A24 slave enable */
#define PLDZ002_SLV24_BASE_MASK_UPPER 0x000f /**< A24 base upper mask */
#define PLDZ002_SLV24_BASE_MASK_LOWER 0x0f00 /**< A24 base lower mask */
#define PLDZ002_SLV24_SIZE_MASK	      0xf000 /**< A24 size mask */
#define PLDZ002_SLV24_SIZE_1MB	      0x0000 /**< A24 size 1MB */
#define PLDZ002_SLV24_SIZE_512K	      0x8000 /**< A24 size 512 kB */
#define PLDZ002_SLV24_SIZE_256K	      0xC000 /**< A24 size 256 kB */
#define PLDZ002_SLV24_SIZE_128K	      0xE000 /**< A24 size 128 kB */
#define PLDZ002_SLV24_SIZE_64K	      0xF000 /**< A24 size 64  kB */
#define PLDZ002_SLV24_BASE_MASK_UPPER_SHIFT 20 /**< A24 base upper mask shift */
#define PLDZ002_SLV24_BASE_MASK_LOWER_SHIFT 8  /**< A24 base lower mask shift */

/* CTRL 0x18 */
#define PLDZ002_SYSCTL	      0x0018 /**< System Controller Register (r/w) */
#define PLDZ002_SYSCTL_SYSCON 0x01   /**< SYSCON bit */
#define PLDZ002_SYSCTL_SYSRES 0x02   /**< SYSRES bit */
#define PLDZ002_SYSCTL_ATO    0x04   /**< ATO bit */

/* CTRL 0x1C */
#define PLDZ002_LONGADD 0x001c /**< Upper 3 Adress Bits for A32 (r/w) */

/* CTRL 0x20 */
#define PLDZ002_MAIL_IRQ_CTRL 0x0020 /**< Mailbox Interrupt Enable Reg. (r/w) */

/* CTRL 0x24 */
#define PLDZ002_MAIL_IRQ_STAT 0x0024 /**< Mailbox Interrupt Status Reg. (r/w) */

/* CTRL 0x2C */
#define PLDZ002_DMASTA			0x002C /**< DMA Status Register (r/w)*/
#define PLDZ002_DMASTA_EN		0x01   /**< DMA_EN bit */
#define PLDZ002_DMASTA_IEN		0x02   /**< DMA_IEN bit */
#define PLDZ002_DMASTA_IRQ		0x04   /**< DMA_IRQ bit	*/
#define PLDZ002_DMASTA_ERR		0x08   /**< DMA_ERR bit	*/

/* CTRL 0x30 */
#define PLDZ002_SLV16 0x0030 /**< Slave Control Register A16 (r/w) */

/* CTRL 0x34 */
#define PLDZ002_SLV32	 0x0034 /**< Slave Control Reg A32 (r/w) */
#define PLDZ002_SLVxx_EN 0x00000010 /**< slave enable */

/* CTRL 0x38 */
#define PLDZ002_LM_STAT_CTRL_0 0x0038 /**< location monitor 0 status/ctrl(r/w) */

/* CTRL 0x3C */
#define PLDZ002_LM_STAT_CTRL_1 0x003C /**< location monitor 1 status/ctrl(r/w) */

#define PLDZ002_LM_STAT_CTRL_ADDR_MSK 0xC0 /**< address 3 and 4 */
#define PLDZ002_LM_STAT_CTRL_WR       0x20 /**< trigger at write */
#define PLDZ002_LM_STAT_CTRL_RD       0x10 /**< trigger at read */
#define PLDZ002_LM_STAT_CTRL_IRQ      0x08 /**< IRQ status */
#define PLDZ002_LM_STAT_CTRL_A16_MODE 0x04 /**< 10 - A16 */
#define PLDZ002_LM_STAT_CTRL_A24_MODE 0x06 /**< 11 - A24 */
#define PLDZ002_LM_STAT_CTRL_A32_MODE 0x00 /**< 00 - A32 */
#define PLDZ002_LM_STAT_CTRL_IRQ_EN   0x01 /**< IRQ enable */

/* CTRL 0x48 */
#define PLDZ002_SLV24_PCI 0x0048 /**< Slave Control Register A24 to PCI (r/w) */

/* CTRL 0x4c */
#define PLDZ002_SLV32_PCI 0x004c /**< Slave Control Register A32 to PCI (r/w) */

#define PLDZ002_INT_IRQM (0x7f<<1) /* IRQ mask */
#define PLDZ002_INT_IRQ7 (1<<7)    /* IRQ7 */
#define PLDZ002_INT_IRQ6 (1<<6)    /* IRQ6 */
#define PLDZ002_INT_IRQ5 (1<<5)    /* IRQ5 */
#define PLDZ002_INT_IRQ4 (1<<4)    /* IRQ4 */
#define PLDZ002_INT_IRQ3 (1<<3)    /* IRQ3 */
#define PLDZ002_INT_IRQ2 (1<<2)    /* IRQ2 */
#define PLDZ002_INT_IRQ1 (1<<1)    /* IRQ1 */

/* CTRL 0x58 */
#define PLDZ002_BERR_ADDR 0x58 /**< Bus Error Address Register (r) */

/* CTRL 0x5C */
#define PLDZ002_BERR_ACC	 0x5C /**< Bus Error Access Register (r) */
#define PLDZ002_BERR_ACC_AM_MASK 0x3f /**< AM causing bus error (bit[5:0]) */

#endif /* _PLDZ002_HW_H */
