# Author Adam Wujek, CERN 2018

import pytest
import time
from test_config import *
from vme_men_helpers import *
from vme_mdkt_helpers import *

from svec_common import *

vme = vme_mdkt()
vme_men = vme_men()

# ------------------ CR/CSR -----------------
def test_check_CR_signature_from_CR_CSR():
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x1c) & 0xff) == 0x43
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x20) & 0xff) == 0x52

def test_svec_create_random_file():
    check_return_code(*exec_cmd("dd if=/dev/urandom of=" + RANDOM_FILE + " bs=" + RANDOM_SIZE + " count=1"))

# ------------------ A24 cross_mode_read -----------------
class Test_svec_A24_cross_mode_read(object):
    def test_write_A24D16_read_A24D16(self, size_fixtureD16, addr_fixtureD16):
        # write data using A24D16 read with A24D16_BLT
        addr = addr_fixtureD16
        size = size_fixtureD16
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)
        vme_men.write(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D16_read_A24D16_dma(self, size_fixtureD16_blt, addr_fixtureD16_blt):
        # write data using A24D16 read with A24D16_BLT
        addr = addr_fixtureD16_blt
        size = size_fixtureD16_blt
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)
        vme_men.write(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE, dma = True)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D16_read_A24D16_BLT(self, size_fixtureD16_blt, addr_fixtureD16_blt):
        # write data using A24D16 read with A24D16_BLT
        addr = addr_fixtureD16_blt
        size = size_fixtureD16_blt
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)
        vme_men.write(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme_men.slave_set_space(SVEC_slot, A24D16_BLT, SVEC_A24_ADDR)
        vme.read(A24D16_BLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_read_A24D32(self, size_fixtureD32, addr_fixtureD32):
        # write data using A24D32 read with A24D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme_men.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_read_A24D32_dma(self, size_fixtureD32, addr_fixtureD32):
        # write data using A24D32 read with A24D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme_men.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE, dma = True)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_read_A24D32_BLT(self, size_fixtureD32, addr_fixtureD32):
        # write data using A24D32 read with A24D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme_men.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme_men.slave_set_space(SVEC_slot, A24D32_BLT, SVEC_A24_ADDR)
        vme.read(A24D32_BLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A24D32_read_A24D64_MBLT(self, size_fixtureD64, addr_fixtureD64):
        # write data using A24D32 read with A24D64_MBLT
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)
        vme_men.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme_men.slave_set_space(SVEC_slot, A24D64_MBLT, SVEC_A24_ADDR)
        vme.read(A24D64_MBLT, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

# ------------------ A32 cross_mode_rw -----------------
class Test_svec_A32_cross_mode_read(object):
    def test_write_A32D32_read_A32D32(self, size_fixtureD32, addr_fixtureD32):
        # write data using A32D32 read with A32D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme_men.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D32_read_A32D32_dma(self, size_fixtureD32, addr_fixtureD32):
        # write data using A32D32 read with A32D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme_men.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme.read(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE, dma = True)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D32_read_A32D32_BLT(self, size_fixtureD32, addr_fixtureD32):
        # write data using A32D32 read with A32D32_BLT
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme_men.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme_men.slave_set_space(SVEC_slot, A32D32_BLT, SVEC_A32_ADDR)
        vme.read(A32D32_BLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_write_A32D32_read_A32D64_MBLT(self, size_fixtureD64, addr_fixtureD64):
        # write data using A32D32 read with A32D64_MBLT
        addr = addr_fixtureD64
        size = size_fixtureD64
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr

        vme_men.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)
        vme_men.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True)
        vme_men.slave_set_space(SVEC_slot, A32D64_MBLT, SVEC_A32_ADDR)
        vme.read(A32D64_MBLT, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))
