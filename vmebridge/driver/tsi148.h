// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * tsi148.h - Low level support for the Tundra TSI148 PCI-VME Bridge Chip
 *
 * Copyright (c) 2009 Sebastien Dugue
 *
 * Borrowed and modified from the tsi148.h file from:
 *   Tom Armistead, Updated and maintained by Ajit Prem and
 *   copyrighted 2004 Motorola Inc.
 *
 */

#ifndef _TSI148_H
#define _TSI148_H

#include <linux/interrupt.h>
#include <linux/version.h>
#include <asm-generic/iomap.h>

#include "vmebus.h"
#include "tsi148_hw.h"


struct vme_bridge_device; /* HACK to get the symbol.
			     The headers should be cleaned */

extern struct tsi148_chip *tsi148_chip_global;

/**
 * struct hw_desc_entry - Hardware descriptor
 * @list: Descriptors list
 * @va: Virtual address of the descriptor
 * @phys: Bus address of the descriptor
 *
 *  This data structure is used internally to keep track of the hardware
 * descriptors that are allocated in order to free them when the transfer
 * is done.
 */
struct hw_desc_entry {
	struct list_head	list;
	void			*va;
	dma_addr_t		phys;
};



/* Low level misc inline stuff */
static inline int tsi148_get_slotnum(struct tsi148_chip *regs)
{
	return  ioread32be(&regs->lcsr.vstat) & 0x1f;
}

static inline int tsi148_get_syscon(struct tsi148_chip *regs)
{
	int syscon = 0;

	if (ioread32be(&regs->lcsr.vstat) & 0x100)
		syscon = 1;

	return syscon;
}

static inline int tsi148_set_interrupts(struct tsi148_chip *regs,
					unsigned int mask)
{
	iowrite32be(mask, &regs->lcsr.inteo);
	iowrite32be(mask, &regs->lcsr.inten);

	/* Quick sanity check */
	if ((ioread32be(&regs->lcsr.inteo) != mask) ||
	    (ioread32be(&regs->lcsr.inten) != mask))
		return -1;

	return 0;
}

static inline unsigned int tsi148_get_int_enabled(struct tsi148_chip *regs)
{
	return ioread32be(&regs->lcsr.inteo);
}

static inline unsigned int tsi148_get_int_status(struct tsi148_chip *regs)
{
	return ioread32be(&regs->lcsr.ints);
}

static inline void tsi148_clear_int(struct tsi148_chip *regs, unsigned int mask)
{
	iowrite32be(mask, &regs->lcsr.intc);
}

static inline int tsi148_iack8(struct tsi148_chip *regs, int irq)
{
	int vec = ioread8(&regs->lcsr.viack[(irq * 4) + 3]);

	return vec & 0xff;
}

static inline int tsi148_bus_error_chk(struct tsi148_chip *regs, int clear)
{
	unsigned int veat = ioread32be(&regs->lcsr.veat);

	if (veat & TSI148_LCSR_VEAT_VES) {
		if (clear)
			iowrite32be(TSI148_LCSR_VEAT_VESCL, &regs->lcsr.veat);
		return 1;
	}

	return 0;
}


extern void tsi148_handle_pci_error(void);
extern void tsi148_handle_vme_error(struct vme_bus_error *);
extern int tsi148_generate_interrupt(int, int, signed long);

extern void tsi148_get_window_attr(struct vme_mapping *);
extern int tsi148_create_window(struct vme_mapping *);
extern void tsi148_remove_window(struct vme_mapping *);

extern int tsi148_setup_crg(unsigned int, enum vme_address_modifier);
extern void tsi148_disable_crg(struct tsi148_chip *);

extern void tsi148_quiesce(struct tsi148_chip *);
extern void tsi148_init(struct tsi148_chip *);

int am_to_attr(enum vme_address_modifier am,
	       unsigned int *addr_size,
	       unsigned int *transfer_mode,
	       unsigned int *user_access,
	       unsigned int *data_access);

#endif /* _TSI148_H */
