/*-
 * Copyright (c) 2010 Samuel I. Gonsalvez <siglesia@cern.ch>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. Neither the name of copyright holders nor the names of its
*    contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
* TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <stdint.h>

#include "libvmod.h"

static char git_version[] = "git_version: " GIT_VERSION;

struct vmodttl_dev_info {
	int device;
	int fd;
	int irq_mask;
	struct vmodttl_config conf;
};

/* Thread for incomming messages */
void * wait_for_msg(void *data)
{
        struct vmodttl_dev_info *dev_info = (struct vmodttl_dev_info *)data;
        int             i;
        uint16_t	irq_val;
        int             n;
        int             dev;
        int             val;
	unsigned char	buf[2];

        while(1) {
		n = vmodttl_wait(dev_info->fd, &irq_val); /* sleep until message arrive */
		if(n <= 0) {
			fprintf(stderr,"panic read ...\n");
			//return((void *)1);
			continue;
		}
		fprintf(stdout, "portB|portA value on irq: 0x%x\n", irq_val);
	}
}

int main (int argc, char *argv[])
{
        //int tmp;
	//char txt;
	struct vmodttl_dev_info dev_info;
	int i, j;
	pthread_t thread;


	printf("  ______________________________________  \n");
	printf(" |                                      | \n");
	printf(" |       VMOD-TTL Testing program       | \n");
	printf(" |______________________________________| \n");
	printf(" |                                      | \n");
	printf(" | Created by: Samuel I. Gonsalvez      | \n");
	printf(" | Email: siglesia@cern.ch              | \n");
	printf(" |______________________________________| \n");
	printf("\n");
	printf("%s\n", git_version);
	printf("%s\n", libvmod_version_s);

	if (argc == 2)
	{
		dev_info.device = atoi(argv[1]);
	}
	else
	{
		printf("\n Please use: test_read_write <lun_of_the_device>.\n");
		exit(-1);
	}

	dev_info.fd = vmodttl_open(dev_info.device);
	if(dev_info.fd < 0) {
		perror("open failed");
		return -1;
	}

	printf("\n Port configuration \n");
	printf("\n Channel A [ (0) Input - (1) Output]: ");
	scanf("%d", &dev_info.conf.dir_a);
	if (!dev_info.conf.dir_a){
		dev_info.conf.mode_a = 0;
	}else{
		printf("\n Channel A [ (0) Open Drain - (1) Open Collector ]\n");
		//scanf("%d", &tmp);
		dev_info.conf.mode_a = 0;
	}

	printf("\n Channel B [ (0) Input - (1) Output]: ");
	scanf("%d", &dev_info.conf.dir_b);

	if (!dev_info.conf.dir_b){
		dev_info.conf.mode_b = 0;

	}else{
		printf("\n Channel B [ (0) Open Drain - (1) Open Collector ]\n");
		//scanf("%d", &tmp);
		dev_info.conf.mode_b = 0;
	}

	printf("\n Channel C [ (0) Open Drain - (1) Open Collector ]\n");
	//scanf("%d", &tmp);
	dev_info.conf.mode_c = 0;
	printf("\n Up time of the pulse in the data strobe (useg): \n");
	//scanf("%d", &tmp);
	dev_info.conf.us_pulse = 0;

	printf("\n In all the channels: [ (0) Non-inverting Logic - (1) Inverting logic]: \n");
	//scanf("%d", &tmp);
	dev_info.conf.inverting_logic = 0;

	for(i = 0; i< NUM_BITS; i++) {
		dev_info.conf.conf_pattern_a[i] = OFF;
		dev_info.conf.conf_pattern_b[i] = OFF;
	}

	dev_info.conf.pattern_mode_a = OR;
	dev_info.conf.pattern_mode_b = OR;

	if (vmodttl_io_config(dev_info.fd, dev_info.conf) < 0)
		exit(-1);


	for (i = 0; i < 2; ++i) { /* port A and B */
		for(j = 0; j < NUM_BITS; j++) {
			vmodttl_pattern(dev_info.fd, i, j, ZERO_TO_ONE);
		}
	}

	printf("\n set irq_mask (0xFFFF): ");
	scanf("%x", &dev_info.irq_mask);
	if (vmodttl_connect(dev_info.fd, dev_info.irq_mask) < 0) {
		exit(-1);
	}


	pthread_create( &thread, NULL, wait_for_msg, (void *)&dev_info );

	//sleep(60);
	while(1){
		sleep(1);
	}

	close(dev_info.fd);
	vmodttl_close(dev_info.fd);

	return 0;
}
