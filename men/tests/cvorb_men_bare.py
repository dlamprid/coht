# Author Adam Wujek, CERN 2017

from test_config import *
from vme_men_helpers import *
import pytest
import time

vme = vme_men()
def test_check_VHDL_version():
    assert vme.read_val(A16D32, CVORB_addr+0x10) == 0x00420323

def test_write_0x5a5a5a5a_to_ch0_FCT_enable_mask_upper():
    vme.write(A16D32, CVORB_addr + 0x8c, value = 0x5a5a5a5a)
    val = vme.read_val(A16D32, CVORB_addr + 0x8c)
    assert val == 0x5a5a5a5a

def test_write_0xa5a5a5a5_to_ch0_FCT_enable_mask_upper():
    vme.write(A16D32, CVORB_addr + 0x8c, value = 0xa5a5a5a5)
    val = vme.read_val(A16D32, CVORB_addr + 0x8c)
    assert val == 0xa5a5a5a5
