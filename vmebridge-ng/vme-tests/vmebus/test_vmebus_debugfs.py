"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import os
import stat


class TestVmebusDebugfs(object):
    """It tests basic things about the VME bus"""

    def test_debugfs_dir(self):
        """there must be a 'vme-bridge' directory"""
        path = "/sys/kernel/debug/vme-bridge"
        assert os.path.isdir(path)

    def test_debugfs_info(self):
        """There must be an 'info' file with permissions RO"""
        path = "/sys/kernel/debug/vme-bridge/info"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IRUSR

    def test_debugfs_generate_interrupt(self):
        """There must be a 'generate_interrupt' file with permissions WO"""
        path = "/sys/kernel/debug/vme-bridge/generate_interrupt"
        assert os.path.exists(path)
        mode = stat.S_IMODE(os.stat(path).st_mode)
        modeown = mode & (stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        assert modeown == stat.S_IWUSR

    @pytest.mark.parametrize("param", ["1", "1 1"])
    def test_debugfs_generate_interrupt_missing_param(self, param):
        """The file 'generate_interrupt' must refuse to work with the following inputs"""
        path = "/sys/kernel/debug/vme-bridge/generate_interrupt"
        with pytest.raises(OSError):
            with open(path, "w") as f:
                f.write(param)

    @pytest.mark.parametrize("param", ["1 0x1 1",  # invalid vector format
                                       "1 256 1",  # invalid vector
                                       "1 1234 1",  # invalid vector
                                       "0 1 1",  # invalid level
                                       "8 1 1",  # invalid level
                                       ])
    def test_debugfs_generate_interrupt_invalid_param(self, param):
        """The file 'generate_interrupt' must refuse to work with the following inputs"""
        path = "/sys/kernel/debug/vme-bridge/generate_interrupt"
        with pytest.raises(OSError):
            with open(path, "w") as f:
                f.write(param)
