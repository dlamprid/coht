// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2019
 * Author Federico Vaga <federico.vaga@cern.ch>
 * Author Sebastien Dugue
 */

#ifdef CONFIG_PROC_FS

#include <linux/version.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "vme_bridge.h"
#include "vme_compat.h"


/* VME address modifiers names */
static char *amod[] = {
	"A64MBLT", "A64", "Invalid 0x02", "A64BLT",
	"A64LCK", "A32LCK", "Invalid 0x06", "Invalid 0x07",
	"A32MBLT USER", "A32 USER DATA", "A32 USER PROG", "A32BLT USER",
	"A32MBLT SUP", "A32 SUP DATA", "A32 SUP PROG", "A32BLT SUP",
	"Invalid 0x10", "Invalid 0x11", "Invalid 0x12", "Invalid 0x13",
	"Invalid 0x14", "Invalid 0x15", "Invalid 0x16", "Invalid 0x17",
	"Invalid 0x18", "Invalid 0x19", "Invalid 0x1a", "Invalid 0x1b",
	"Invalid 0x1c", "Invalid 0x1d", "Invalid 0x1e", "Invalid 0x1f",
	"2e6U", "2e3U", "Invalid 0x22", "Invalid 0x23",
	"Invalid 0x24", "Invalid 0x25", "Invalid 0x26", "Invalid 0x27",
	"Invalid 0x28", "A16 USER", "Invalid 0x2a", "Invalid 0x2b",
	"A16LCK", "A16 SUP", "Invalid 0x2e", "CR/CSR",
	"Invalid 0x30", "Invalid 0x31", "Invalid 0x32", "Invalid 0x33",
	"A40", "A40LCK", "Invalid 0x36", "A40BLT",
	"A24MBLT USER", "A24 USER DATA", "A24 USER PROG", "A24BLT USER",
	"A24MBLT SUP", "A24 SUP DATA", "A24 SUP PROG", "A24BLT SUP"
};

static int vme_window_proc_show_mapping(struct seq_file *m, int num,
					struct mapping *mapping)
{
	char *pfs;
	struct vme_mapping *desc = &mapping->desc;

/*
 *        va       PCI     VME      Size     Address Modifier   Data   Prefetch
 *                                              Description     Width   Size
 * dd: xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx - ssssssssssssssss /  sss    sssss
 * client: ddddd ssssssssssssssss
 */
	seq_printf(m, "    %2d: %p %.8x %.8x %.8x - ",
		   num,
		   desc->kernel_va, desc->pci_addrl,
		   desc->vme_addrl, desc->sizel);

	if (desc->read_prefetch_enabled) {
		switch (desc->read_prefetch_size) {
		case VME_PREFETCH_2:
			pfs = "PFS2";
			break;
		case VME_PREFETCH_4:
			pfs = "PFS4";
			break;
		case VME_PREFETCH_8:
			pfs = "PFS8";
			break;
		case VME_PREFETCH_16:
			pfs = "PFS16";
			break;
		default:
			pfs = "?";
			break;
		}
	} else {
		pfs = "NOPF";
	}

	seq_printf(m, "(0x%02x)%s / D%2d %5s\n",
		   desc->am, amod[desc->am], desc->data_width, pfs);
	seq_puts(m, "        client: ");

	if (mapping->client.pid_nr)
		seq_printf(m, "%d ", (unsigned int)mapping->client.pid_nr);

	if (mapping->client.name[0])
		seq_printf(m, "%s", mapping->client.name);

	seq_puts(m, "\n");
	return 0;
}

static int vme_window_proc_show_window(struct seq_file *m, int window_num)
{
	struct vme_bridge_device *vbridge = m->private;
	char *pfs;
	struct window *window = &vbridge->wind_mgr.winds[window_num];
	struct mapping *mapping;
	struct vme_mapping *desc;
	int count = 0;


	seq_printf(m, "Window %d:  ", window_num);

	if (!window->active)
		seq_puts(m, "Not Active\n");
	else {
		seq_puts(m, "Active - ");
		if (window->users == 0)
			seq_puts(m, "No users\n");
		else
			seq_printf(m, "%2d user%c\n", window->users,
				     (window->users > 1)?'s':' ');
	}

	if (!window->active) {
		seq_puts(m, "\n");
		return 0;
	}

	desc = &window->desc;
	seq_printf(m, "        %p %.8x %.8x %.8x - ",
		   desc->kernel_va, desc->pci_addrl,
		   desc->vme_addrl, desc->sizel);

	if (desc->read_prefetch_enabled) {
		switch (desc->read_prefetch_size) {
		case VME_PREFETCH_2:
			pfs = "PFS2";
			break;
		case VME_PREFETCH_4:
			pfs = "PFS4";
			break;
		case VME_PREFETCH_8:
			pfs = "PFS8";
			break;
		case VME_PREFETCH_16:
			pfs = "PFS16";
			break;
		default:
			pfs = "?";
			break;
		}
	} else {
		pfs = "NOPF";
	}

	seq_printf(m, "(0x%02x)%s / D%2d %5s\n",
		   desc->am, amod[desc->am], desc->data_width, pfs);

	if (list_empty(&window->mappings)) {
		seq_puts(m, "\n");
		return 0;
	}

	seq_puts(m, "\n  Mappings:\n");
	list_for_each_entry(mapping, &window->mappings, list) {
		vme_window_proc_show_mapping(m, count, mapping);
		count++;
	}
	seq_puts(m, "\n");

	return 0;
}

static int vme_window_proc_show(struct seq_file *m, void *data)
{
	struct vme_bridge_device *vbridge = m->private;
	int i;

	seq_puts(m, "\nPCI-VME Windows\n");
	seq_puts(m, "===============\n\n");
	seq_puts(m, "        va       PCI      VME      Size      Address Modifier Data  Prefetch\n");
	seq_puts(m, "                                             and Description  Width Size\n");
	seq_puts(m, "----------------------------------------------------------------------------\n\n");

	for (i = 0; i < vbridge->wind_mgr.nr_winds; i++)
		vme_window_proc_show_window(m, i);

	return 0;
}

static int vme_window_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_window_proc_show, PDE_DATA(inode));
}

static const struct file_operations vme_window_proc_ops = {
	.open           = vme_window_proc_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};

static int vme_info_proc_show(struct seq_file *m, void *data)
{
	struct vme_bridge_device *vbridge = m->private;

	seq_printf(m, "PCI-VME driver v%s (%s)\n\n",
		   DRV_MODULE_VERSION, DRV_MODULE_RELDATE);

	seq_printf(m, "chip revision:     %08X\n", vbridge->rev);
	seq_printf(m, "chip IRQ:          %d\n", vbridge->pdev->irq);
	seq_printf(m, "VME slot:          %d\n", vbridge->slot);
	seq_printf(m, "VME SysCon:        %s\n",
		 vbridge->syscon ? "Yes" : "No");
	seq_printf(m, "chip registers at: 0x%p\n",
		   vbridge->chip_mgr.ops->get_regsaddr(vbridge));
	seq_puts(m, "\n");
	return 0;
}

static int vme_info_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_info_proc_show, PDE_DATA(inode));
}

static const struct file_operations vme_info_proc_ops = {
	.open           = vme_info_proc_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};


static int vme_interrupts_proc_show(struct seq_file *m, void *data)
{
	struct vme_bridge_device *vbridge = m->private;
	int i;

	seq_puts(m, " Source       Count\n");
	seq_puts(m, "--------------------------\n\n");

	for (i = 0; i < vbridge->int_stats_count; i++)
		seq_printf(m, "%-8s      %d\n", vbridge->int_stats[i].name,
			   vbridge->int_stats[i].count);

	return 0;
}

static int vme_interrupts_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_interrupts_proc_show, PDE_DATA(inode));
}

static const struct file_operations vme_interrupts_proc_ops = {
	.open           = vme_interrupts_proc_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};

void vme_procfs_register(struct vme_bridge_device *vbridge)
{
	struct proc_dir_entry *entry;

	/* Create /proc/vme directory */
	vbridge->procfs_mgr.proc_root = proc_mkdir("vme", NULL);

	/* Create /proc/vme/info file */
	entry = proc_create_data("info", S_IFREG | 0444,
				 vbridge->procfs_mgr.proc_root,
				 &vme_info_proc_ops, vbridge);
	if (!entry)
		pr_warn(PFX "Failed to create proc info node\n");

	/* Create /proc/vme/windows file */
	entry = proc_create_data("windows", S_IFREG | 0444,
				 vbridge->procfs_mgr.proc_root,
			    &vme_window_proc_ops, vbridge);
	if (!entry)
		pr_warn(PFX "Failed to create proc windows node\n");

	entry = proc_create_data("interrupts", S_IFREG | 0444,
				 vbridge->procfs_mgr.proc_root,
				 &vme_interrupts_proc_ops,
				 vbridge);
	if (!entry)
		pr_warn(PFX "Failed to create proc interrupts node\n");


	/* Create specific bridge proc entries */
	if (vbridge->procfs_mgr.ops &&
	    vbridge->procfs_mgr.ops->procfs_register)
		vbridge->procfs_mgr.ops->procfs_register(vbridge);
}

void vme_procfs_unregister(struct vme_bridge_device *vbridge)
{
	/* remove specific bridge proc entries */
	if (vbridge->procfs_mgr.ops &&
	    vbridge->procfs_mgr.ops->procfs_unregister)
		vbridge->procfs_mgr.ops->procfs_unregister(vbridge);
	remove_proc_entry("interrupts", vbridge->procfs_mgr.proc_root);
	remove_proc_entry("windows", vbridge->procfs_mgr.proc_root);
	remove_proc_entry("info", vbridge->procfs_mgr.proc_root);
	remove_proc_entry("vme", NULL);
}

#else
void vme_procfs_register(struct vme_bridge_device *vbridge) {}
void vme_procfs_unregister(struct vme_bridge_device *vbridge) {}
#endif /* CONFIG_PROC_FS */
