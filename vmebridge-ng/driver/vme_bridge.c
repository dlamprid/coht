// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_bridge.c - PCI-VME bridge driver
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge support. It is highly coupled to
 * the Tundra TSI148 bridge chip capabilities.
 */
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/irqdomain.h>
#include <linux/semaphore.h>

#include "vme_bridge.h"
#include "vme_compat.h"

static struct class *vme_class;

/*
 * Driver file operation entry points
 */
static int vme_open(struct inode *, struct file *);
static int vme_release(struct inode *, struct file *);
static ssize_t vme_read(struct file *, char *, size_t, loff_t *);
static ssize_t vme_write(struct file *, const char *, size_t, loff_t *);
static long vme_ioctl(struct file *, unsigned int, unsigned long);
static int vme_mmap(struct file *, struct vm_area_struct *);

static const struct file_operations vme_fops = {
	.owner		= THIS_MODULE,
	.open		= vme_open,
	.release	= vme_release,
	.read		= vme_read,
	.write		= vme_write,
	.unlocked_ioctl	= vme_ioctl,
	.mmap		= vme_mmap,
};

static const struct file_operations vme_mwindow_fops = {
	.owner		= THIS_MODULE,
	.release	= vme_window_release,
	.unlocked_ioctl	= vme_window_ioctl,
	.mmap		= vme_window_mmap,
};

static const struct file_operations vme_dma_fops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl	= vme_dma_ioctl,
};

static const struct file_operations vme_misc_fops = {
	.owner		= THIS_MODULE,
	.read		= vme_misc_read,
	.write		= vme_misc_write,
	.unlocked_ioctl	= vme_misc_ioctl,
};

const struct dev_entry devlist[] = {
	{VME_MINOR_MWINDOW, "vme_mwindow", 0,      &vme_mwindow_fops},
	{VME_MINOR_DMA,     "vme_dma",     0,      &vme_dma_fops},
	{VME_MINOR_CTL,     "vme_ctl",     0,      &vme_misc_fops},
	{VME_MINOR_REGS,    "vme_regs",    DEV_RW, &vme_misc_fops}
};

static int vme_open(struct inode *inode, struct file *file)
{
	unsigned int minor = iminor(inode);
	const struct file_operations *f_op = NULL;
	int rc = 0;

	switch (minor) {
	case VME_MINOR_MWINDOW:
		f_op = &vme_mwindow_fops;
		break;

	case VME_MINOR_DMA:
		f_op = &vme_dma_fops;
		break;

	case VME_MINOR_CTL:
	case VME_MINOR_REGS:
		f_op = &vme_misc_fops;
		break;

	default:
		return -ENXIO;
	}

	if (f_op && f_op->open)
		rc = f_op->open(inode, file);

	return rc;
}

static int vme_release(struct inode *inode, struct file *file)
{
	unsigned int minor = iminor(inode);
	const struct file_operations *f_op = NULL;

	switch (minor) {
	case VME_MINOR_MWINDOW:
		f_op = &vme_mwindow_fops;
		break;

	case VME_MINOR_DMA:
		f_op = &vme_dma_fops;
		break;

	case VME_MINOR_CTL:
	case VME_MINOR_REGS:
		f_op = &vme_misc_fops;
		break;

	default:
		return -ENXIO;
	}

	if (f_op && f_op->release)
		return f_op->release(inode, file);

	return 0;
}

static ssize_t vme_read(struct file *file, char *buf, size_t count,
			loff_t *ppos)
{
	unsigned int minor = iminor(file->f_path.dentry->d_inode);
	const struct file_operations *f_op = NULL;

	/*
	 * Do some sanity checking before handling processing to the
	 * specific device.
	 */
	if (!(devlist[minor].flags & DEV_RW))
		return -ENODEV;

	switch (minor) {
	case VME_MINOR_REGS:
		f_op = &vme_misc_fops;
		break;

	default:
		return -ENXIO;
	}

	if (f_op && f_op->read)
		return f_op->read(file, buf, count, ppos);

	return 0;
}

static ssize_t vme_write(struct file *file, const char *buf, size_t count,
			 loff_t *ppos)
{
	unsigned int minor = iminor(file->f_path.dentry->d_inode);
	const struct file_operations *f_op = NULL;

	/*
	 * Do some sanity checking before handling processing to the
	 * specific device.
	 */
	if (!(devlist[minor].flags & DEV_RW))
		return -ENODEV;

	switch (minor) {
	case VME_MINOR_REGS:
		f_op = &vme_misc_fops;
		break;

	default:
		return -ENXIO;
	}

	if (f_op && f_op->write)
		return f_op->write(file, buf, count, ppos);

	return 0;
}

long vme_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	unsigned int minor = iminor(file->f_path.dentry->d_inode);
	const struct file_operations *f_op = NULL;

	switch (minor) {
	case VME_MINOR_MWINDOW:
		f_op = &vme_mwindow_fops;
		break;

	case VME_MINOR_DMA:
		f_op = &vme_dma_fops;
		break;
	default:
		return -ENXIO;
	}

	if (f_op && f_op->unlocked_ioctl)
		return f_op->unlocked_ioctl(file, cmd, arg);

	return 0;
}

static int vme_mmap(struct file *file, struct vm_area_struct *vma)
{
	unsigned int minor = iminor(file->f_path.dentry->d_inode);
	const struct file_operations *f_op = NULL;

	switch (minor) {
	case VME_MINOR_MWINDOW:
		f_op = &vme_mwindow_fops;
		break;
	default:
		return -ENXIO;
	}

	if (f_op && f_op->mmap)
		return f_op->mmap(file, vma);

	return 0;
}

/**
 * vme_bridge_create_devices() - Create the device nodes for the bridge
 *
 * Returns 0 on success, or a standard kernel error.
 */
static int vme_bridge_create_devices(void)
{
	int i;
	int rc;

	vme_class = class_create(THIS_MODULE, "vme");

	if (IS_ERR(vme_class)) {
		rc = PTR_ERR(vme_class);
		pr_err(PFX "Failed to create vme class\n");
		return rc;
	}

	if (register_chrdev(VME_MAJOR, "vme", &vme_fops)) {
		pr_err(PFX "Failed to register device\n");
		class_destroy(vme_class);
		return -ENODEV;
	}

	for (i = 0; i < ARRAY_SIZE(devlist); i++) {
		device_create(vme_class, NULL,
			      MKDEV(VME_MAJOR, devlist[i].minor), "%s",
			      devlist[i].name);
	}

	return 0;
}

/**
 * vme_bridge_remove_devices() - Remove the device nodes for the bridge
 *
 */
static void vme_bridge_remove_devices(void)
{
	int i;

	unregister_chrdev(VME_MAJOR, "vme");

	for (i = 0; i < ARRAY_SIZE(devlist); i++)
		device_destroy(vme_class, MKDEV(VME_MAJOR, devlist[i].minor));

	class_destroy(vme_class);
}

static inline void vme_bus_error_init(struct vme_verr *verr)
{
	spin_lock_init(&verr->lock);
	verr->desc.valid = 0;
	INIT_LIST_HEAD(&verr->h_list);
}

/**
 * Linux kernel's function pci_find_parent_resource has changed in commit:
 * f44116ae PCI: Remove pci_find_parent_resource() use for allocation
 * This commit changed functionality of this function in a way that it cannot
 * be used anymore. Due to this there is a local copy of old version of the
 * function.
 *
 * Function vme_bridge_init uses pci_find_parent_resource to find PCI bridge's
 * resource with flag IORESOURCE_MEM. By not specifying range of resource, old
 * version of the function returns a range of allocated region. However, new
 * function expects the range of resource to contain subrange of PCI bridge.
 * The problem is that PCI bridge resource mapping is not known at this point.
 * Even more this is the information that we're looking for.
 */
/**
 * pci_find_parent_resource - return resource region of parent bus of given
 * region
 * @dev: PCI device structure contains resources to be searched
 * @res: child resource record for which parent is sought
 *
 *  For given resource region of given device, return the resource
 *  region of parent bus the given region is contained in or where
 *  it should be allocated from.
 */
static struct resource *
pci_find_parent_resource_local(const struct pci_dev *dev, struct resource *res)
{
	const struct pci_bus *bus = dev->bus;
	int i;
	struct resource *best = NULL, *r;

	pci_bus_for_each_resource(bus, r, i) {
		if (!r)
			continue;
		if (res->start &&
		    !(res->start >= r->start && res->end <= r->end))
			continue;	/* Not contained */
		if ((res->flags ^ r->flags) & (IORESOURCE_IO | IORESOURCE_MEM))
			continue;	/* Wrong type */
		if (!((res->flags ^ r->flags) & IORESOURCE_PREFETCH))
			return r;	/* Exact match */
		/* We can't insert a non-prefetch resource inside a
		 * prefetchable parent ..
		 */
		if (r->flags & IORESOURCE_PREFETCH)
			continue;
		/* .. but we can put a prefetchable resource inside a
		 * non-prefetchable one
		 */
		if (!best)
			best = r;
	}
	return best;
}

void __dbg(int line)
{
	int rc;
	unsigned int tmp;
	unsigned int vid, did;
	struct vme_mapping crg_desc;

	/* Create a VME mapping for the CRG address space */
	crg_desc.am = VME_A24_USER_DATA_SCT;
	crg_desc.read_prefetch_enabled = 0;
	crg_desc.data_width = VME_D32;
	crg_desc.sizeu = 0;
	crg_desc.sizel = 0x1000;
	crg_desc.vme_addru = 0;
	crg_desc.vme_addrl = 0x00fff000;

	rc = vme_find_mapping(&crg_desc, 0);
	if (rc != 0) {
		pr_err(PFX "Failed to create CRG mapping\n");
		return;
	}

	/* Check that the mapping is OK */
	tmp = ioread32(crg_desc.kernel_va);
	vid = tmp & 0xffff;
	did = (tmp >> 16) & 0xffff;
	pr_debug(PFX "%s()-%d: vid:0x%x did:0x%x\n",
		 __func__, line, vid, did);
}

/**
 * vme_bridge_init() - Initialize the device
 * @dev: PCI device to register
 * @id: Entry in piix_pci_tbl matching with @pdev
 *
 * Called from kernel PCI layer.  Here we initialize the TSI148 VME bridge,
 * do some sanity checks and finally allocate the driver resources.
 *
 * RETURNS:
 * Zero on success, or -ERRNO value.
 */
static int vme_bridge_init(struct pci_dev *pdev,
			   struct vme_bridge_device *vbridge)
{
	int rc;
	struct resource res;
	struct resource *parent_rsrc;

	rc = pci_enable_device(pdev);
	if (rc) {
		pr_err(PFX "Could not enable device %s\n",
		       pci_name(pdev));
		return rc;
	}

	/* Enable bus mastering */
	pci_set_master(pdev);

	spin_lock_init(&vbridge->lock);

	pci_set_drvdata(pdev, vbridge);
	vbridge->pdev = pdev;

	/* initialise the bus error struct */
	vme_bus_error_init(&vbridge->verr);

	/* Get chip revision */
	pci_read_config_byte(pdev, PCI_REVISION_ID, &vbridge->rev);
	pr_info(PFX "found %s VME bridge %s rev 0x%.4x\n",
	       vbridge->pdev->dev.driver->name, vbridge->name, vbridge->rev);

	/* Check there is enough address space opened in the parent bridge */
	memset(&res, 0, sizeof(res));
	res.flags = IORESOURCE_MEM;

	parent_rsrc = pci_find_parent_resource_local(vbridge->pdev, &res);

	if (parent_rsrc == 0) {
		pr_err(PFX
		       "cannot get VME parent device PCI resource\n");
		rc = -ENODEV;
		goto out_err;
	}

	if ((parent_rsrc->end - parent_rsrc->start) < 0x2000000) {
		pr_err(PFX
		       "Not enough PCI memory space available: %lx\n",
		       (unsigned long)(parent_rsrc->end - parent_rsrc->start));
		rc = -ENOMEM;
		goto out_err;
	}

	pr_debug(PFX "Parent bridge window size: 0x%lx\n",
		 (unsigned long)(resource_size(parent_rsrc)));

	/* Map the bridge register space */
	rc = vbridge->chip_mgr.ops->request_regions(vbridge);
	if (rc != 0)
		goto out_err;

	if (vbridge->chip_mgr.ops->get_slotnum) {
		/* Find out which slot we're in unless the user specified it */
		vbridge->slot = vbridge->chip_mgr.ops->get_slotnum(vbridge);
		if (!vbridge->slot)
			pr_info(PFX "on VME slot 0, i.e. no VME64x backplane");
		else if (vbridge->slot < 0 || vbridge->slot > 21)
			pr_warn(PFX "Weird VME slot %d\n", vbridge->slot);
	}


	/* Get System controller status */
	vbridge->syscon = vbridge->chip_mgr.ops->get_syscon(vbridge);

	pr_debug(PFX "Register space @ %p - VME slot: %d SysCon: %s\n",
		 vbridge->chip_mgr.ops->get_regsaddr(vbridge), vbridge->slot,
		 vbridge->syscon ? "yes" : "no");

	/* Init the hardware */
	vbridge->chip_mgr.ops->init_bridge(vbridge);

	/* Initialize window management */
	rc = vme_window_init(vbridge);
	if (rc != 0)
		goto out_err;

	rc = vme_dmaengine_init(vbridge);
	if (rc)
		dev_err(&vbridge->pdev->dev, "DMA will not be available\n");

	/* Register char devices */
	rc = vme_bridge_create_devices();
	if (rc != 0)
		goto out_unmap_pci;

	vme_procfs_register(vbridge);

	/* Initialize and enable interrupts */
	rc = vme_interrupts_init(vbridge);
	if (rc != 0)
		goto out_remove_devices;


	rc = vme_bus_register(vbridge);
	if (rc)
		goto out_bus;

	vme_dbg_init(vbridge);

	return 0;

out_bus:
	vme_interrupts_exit(vbridge);
out_remove_devices:
	vme_procfs_unregister(vbridge);
	vme_bridge_remove_devices();

out_unmap_pci:
	vme_dmaengine_exit(vbridge);
	vbridge->chip_mgr.ops->release_regions(vbridge);

out_err:
	pci_disable_device(vbridge->pdev);

	return rc;
}

/*
 * VME Bus
 *
 * Since there's no auto-discovery in VME, vme_driver_register already receives
 * the number (n) of devices that the calling driver may control. Usually this
 * information is obtained from user-space by the caller via insmod parameters.
 *
 * These n devices are created with id's ranging from 0 to n-1. For each of the
 * devices, the .match and possibly .probe methods are called.
 *
 * If .match fails for a particular device, the device is removed.
 */

static void remove_callback(struct device *dev)
{
	vme_unregister_device(to_vme_dev(dev));
}

static ssize_t remove_store(struct device *dev, struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int i, rm, ret = 0;

	i = kstrtoint(buf, 0, &rm);
	if (i < 0)
		return -EINVAL;
	/* An attribute cannot be unregistered by one of its own methods,
	 * so we have to use this roundabout approach.
	 */
	if (rm)
		ret = device_schedule_callback(dev, remove_callback);

	return ret ? ret : count;
}
static DEVICE_ATTR_WO(remove);

/**
 * Chek if the device we want to register (data->vme_dev) can be registered.
 * The most important control is about memory overlapping. We cannot have two
 * devices on the same memory area.
 * Control that the slot is not yet claimed.
 */
static int vme_device_conflict(struct device *dev, void *data)
{
	struct vme_dev *vme_dev = to_vme_dev(data);
	struct vme_dev *tmp = to_vme_dev(dev);

	if (vme_dev->slot == tmp->slot) {
		pr_err(PFX "Cannot register two devices on the same slot %d\n",
		       tmp->slot);
		return -EBUSY;
	}

	if ((vme_dev->base_address >= tmp->base_address &&
	     vme_dev->base_address < tmp->base_address + tmp->size) ||
	    (vme_dev->base_address <= tmp->base_address &&
	     vme_dev->base_address + vme_dev->size > tmp->base_address)) {
		pr_err(PFX "Cannot register two devices with memory overlap\n\texistent: [0x%08x, 0x%08x], new: [0x%08x, 0x%08x]\n",
		       tmp->base_address, tmp->base_address + tmp->size,
		       vme_dev->base_address,
		       vme_dev->base_address + vme_dev->size);
		return -EBUSY;
	}

	return 0;
}

/**
 * Check if the given device can be registered
 */
static int vme_device_validate(struct device *dev)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	/*
	 * Check only within the PCI-VME bridge on which it has been
	 * registered
	 */
	if (vdev->spec == VME_SPEC_UNKNOWN || vdev->spec == VME_SPEC_VME64) {
		int err = device_for_each_child(dev->parent, dev, vme_device_conflict);
		if (err)
			return err;
	}
	return 0;
}

/**
 * It shows the available VME functions/resources
 */
static ssize_t resources_show(struct device *dev,
			      struct device_attribute *attr,
			      char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	char *str = buf;
	int i;

	for (i = 0; i < VME_CR_FUNCTION_MAX; ++i) {
		struct resource *res = &vdev->resource[i];

		if (res)
			str += sprintf(str, "0x%016llx 0x%016llx 0x%016llx\n",
				       (unsigned long long) res->start,
				       (unsigned long long) res->end,
				       (unsigned long long) res->flags);
		else
			str += sprintf(str, "0x%016llx 0x%016llx 0x%016llx\n",
				       0ULL, 0ULL, 0ULL);
	}

	return (str - buf);
}
static DEVICE_ATTR_RO(resources);


/**
 * It shows the slot number of the given device
 */
static ssize_t address_space_show(struct device *dev,
				  struct device_attribute *attr,
				  char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "0x%X,0x%X\n",
			vdev->base_address,
			vdev->base_address + vdev->size);
}
static DEVICE_ATTR_RO(address_space);

/**
 * It shows the interrupt vector number of the given device
 */
static ssize_t irq_vector_show(struct device *dev,
				   struct device_attribute *attr,
				   char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "%d\n", vdev->irq_vector);
}
static DEVICE_ATTR_RO(irq_vector);

/**
 * It shows the interrupt level (a.k.a. line) of the given device
 */
static ssize_t irq_level_show(struct device *dev,
			      struct device_attribute *attr,
			      char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "%d\n", vdev->irq_level);
}
static DEVICE_ATTR_RO(irq_level);

/**
 * It shows the Linux irq number of the given device
 */
static ssize_t irq_show(struct device *dev,
			struct device_attribute *attr,
			char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "%d\n", vdev->irq);
}
static DEVICE_ATTR_RO(irq);


/**
 * It shows the Linux irq number of the given device
 */
static ssize_t legacy_registration_show(struct device *dev,
					struct device_attribute *attr,
					char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "%d\n",
			!!(vdev->flags & VME_DEV_FLAG_DRIVER_REG));
}
static DEVICE_ATTR_RO(legacy_registration);


static ssize_t specification_show(struct device *dev,
				  struct device_attribute *attr,
				  char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "%d\n", vdev->spec);
}
static DEVICE_ATTR_RO(specification);

/**
 * It shows the manufacturer ID on sysfs
 */
static ssize_t vendor_show(struct device *dev,
			   struct device_attribute *attr,
			   char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "0x%08x\n", vdev->devid.vendor);
}
static DEVICE_ATTR_RO(vendor);

/**
 * It shows the board ID on sysfs
 */
static ssize_t device_show(struct device *dev,
			   struct device_attribute *attr,
			   char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "0x%08x\n", vdev->devid.device);
}
static DEVICE_ATTR_RO(device);


/**
 * It shows the revision ID on sysfs
 */
static ssize_t revision_show(struct device *dev,
			     struct device_attribute *attr,
			     char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	return snprintf(buf, PAGE_SIZE, "0x%08x\n", vdev->devid.revision);
}
static DEVICE_ATTR_RO(revision);


/**
 * It shows the bit status
 */
static ssize_t vme_csr_bit_show(struct device *dev,
				struct device_attribute *attr,
				char *buf, uint8_t mask)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	struct vme_csr csr;
	int err;

	err = vme_csr_get(vdev, &csr);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "%d\n", !!(csr.bit_set & mask));
}


/**
 * It changes the bit status
 */
static ssize_t vme_csr_bit_store(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count, uint8_t mask)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	uint32_t val;
	int err;

	err = kstrtou32(buf, 10, &val);
	if (err) {
		dev_err(dev, "Cannot convert \"%s\" to integer\n", buf);
		return err;
	}

	err = vme_csr_bit(vdev, mask, !!val);
	if (err)
		return err;

	return count;
}


/**
 * It shows the enable status of the module
 */
static ssize_t enable_show(struct device *dev,
			   struct device_attribute *attr,
			   char *buf)
{
	return vme_csr_bit_show(dev, attr, buf, VME_CSR_BIT_ENABLE);
}


/**
 * It enables or disables the module
 */
static ssize_t enable_store(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	return vme_csr_bit_store(dev, attr, buf, count, VME_CSR_BIT_ENABLE);
}
static DEVICE_ATTR_RW(enable);


/**
 * It shows the reset status of the module
 */
static ssize_t reset_show(struct device *dev,
			  struct device_attribute *attr,
			  char *buf)
{
	return vme_csr_bit_show(dev, attr, buf, VME_CSR_BIT_RESET);
}


/**
 * It drives the module reset line
 */
static ssize_t reset_store(struct device *dev,
			   struct device_attribute *attr,
			   const char *buf, size_t count)
{
	return vme_csr_bit_store(dev, attr, buf, count, VME_CSR_BIT_RESET);
}
static DEVICE_ATTR_RW(reset);


/**
 * It shows the enable status of the sysfail driver
 */
static ssize_t sysfail_show(struct device *dev,
			    struct device_attribute *attr,
			    char *buf)
{
	return vme_csr_bit_show(dev, attr, buf, VME_CSR_BIT_SYSFAIL);
}


/**
 * It enables or disables the module
 */
static ssize_t sysfail_store(struct device *dev,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	return vme_csr_bit_store(dev, attr, buf, count, VME_CSR_BIT_SYSFAIL);
}

static DEVICE_ATTR_RW(sysfail);


/**
 * It shows the enable status of the sysfail driver
 */
static ssize_t user_bit_show(struct device *dev,
			     struct device_attribute *attr,
			     char *buf)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	struct vme_csr csr;
	int err;

	err = vme_csr_get(vdev, &csr);
	if (err)
		return err;

	return snprintf(buf, PAGE_SIZE, "0x%02x\n", csr.bit_set_usr);
}


/**
 * It sets/clear the user bits
 */
static ssize_t user_bit_store(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	uint32_t val;
	int err;

	err = kstrtou32(buf, 16, &val);
	if (err) {
		dev_err(dev, "Cannot convert \"%s\" to integer\n", buf);
		return err;
	}

	err = vme_csr_bit_usr(vdev, val, 1);   /* set */
	if (err)
		return err;

	err = vme_csr_bit_usr(vdev, ~val, 0);  /* clear */
	if (err)
		return err;

	return count;
}
static DEVICE_ATTR_RW(user_bit);


static void vme_func_resource_destroy(struct vme_dev *vdev, unsigned int func)
{
	char *name;

	if (unlikely(vdev->spec != VME_SPEC_VME64x))
		return; /* Nothing to do */

	/* Remove previous mapping if it exist */
	if (unlikely(resource_type(&vdev->resource[func]) != IORESOURCE_MEM))
		return; /* Nothing to */

	name = (char *)vdev->resource[func].name;

	vme_csr_resource_remove(vdev, func);
	release_resource(&vdev->resource[func]);
	memset(&vdev->resource[func], 0, sizeof(struct resource));
	vme_release_mapping(&vdev->map[func], 1);
	kfree(name);
}

static int vme_func_resource_create(struct vme_dev *vdev, unsigned int func,
				    uint32_t ader)
{
	struct vme_cr *cr = vdev->spaces[VME_CR_AREA_CR].repr;
	struct resource *res_p, *parent;
	char *name;
	uint32_t mask;
	int err;

	if (unlikely(vdev->spec != VME_SPEC_VME64x)) {
		dev_err(&vdev->dev,
			"Resources (functions) are available only for VME64x boards\n");
		return -EPERM;
	}

	if (ader & VME_CSR_ADER_XAM_MODE) {
		dev_warn(&vdev->dev, "XAM not supported yet by the resource mechanism\n");
		return 0;
	}

	name = kasprintf(GFP_KERNEL, "VME card {slot: %02d, function: %d}",
			 vdev->slot, func);
	if (!name)
		return -ENOMEM;

	mask = VME_CSR_ADEM_MASK(cr->func_adem[func]);

	switch (cr->func_dw[func]) {
	case 0x81:
	case 0x82:
		vdev->map[func].data_width = VME_D8;
		break;
	case 0x83:
		vdev->map[func].data_width = VME_D16;
		break;
	case 0x84:
	case 0x85:
		vdev->map[func].data_width = VME_D32;
		break;

	}

	vdev->map[func].am = VME_CSR_ADER_AM(ader);
	vdev->map[func].vme_addru = 0;
	vdev->map[func].vme_addrl = VME_CSR_ADER_ADDR(ader);
	vdev->map[func].sizeu = 0;
	vdev->map[func].sizel = ((~mask) & (mask - 1)) + 1;
	err = vme_find_mapping(&vdev->map[func], 1);
	if (err) {
		dev_err(&vdev->dev, "Failed to map resource %d (err: %d)\n",
			func, err);
		goto err_mapping;
	}

	res_p = &vdev->resource[func];
	res_p->name = name;
	res_p->start = vdev->map[func].pci_addrl;
	res_p->end = res_p->start + vdev->map[func].sizel - 1;
	res_p->flags = IORESOURCE_MEM;
	parent = vme_window_resource_get(vdev->map[func].window_num);
	err = insert_resource_l(parent, res_p);
	/*FIXME not aligned: is it ok? Probably yes because */
	if (err) {
		dev_err(&vdev->dev, "Can't insert new resource %pR\n",
			res_p);
		goto err_res;
	}

	err = vme_csr_resource_create(vdev, func);
	if (err)
		goto err_create;

	return 0;

err_create:
	release_resource(res_p);
err_res:
	vme_release_mapping(&vdev->map[func], 1);
err_mapping:
	kfree(name);
	memset(&vdev->resource[func], 0, sizeof(struct resource));
	return err;
}


static ssize_t ader_store(struct device *dev,
			  struct device_attribute *attr,
			  const char *buf, size_t count)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	uint32_t ader;
	int i, func, err;

	i = sscanf(buf, "%d 0x%x", &func, &ader);
	if (i != 2)
		return -EINVAL;

	err = vme_csr_ader_set(vdev, func, ader);
	if (err)
		return err;

	/* Remove previous resource */
	vme_func_resource_destroy(vdev, func);
	/* Create new resource */
	err = vme_func_resource_create(vdev, func, ader);
	if (err) {
		vme_csr_ader_set_raw(vdev, func, 0);
		return err;
	}

	return count;
}
static DEVICE_ATTR_WO(ader);

/* VME attributes */
static struct attribute *vme_dev_attr_list_basic[] = {
	&dev_attr_remove.attr,
	&dev_attr_irq.attr,
	&dev_attr_irq_level.attr,
	&dev_attr_irq_vector.attr,
	&dev_attr_legacy_registration.attr,
	&dev_attr_specification.attr,
	NULL,
};

static const struct attribute_group vme_dev_group_basic = {
	.attrs = vme_dev_attr_list_basic,
};

static struct attribute *vme_dev_attr_list_nostd[] = {
	&dev_attr_address_space.attr,
	NULL,
};

static const struct attribute_group vme_dev_group_nostd = {
	.attrs = vme_dev_attr_list_nostd,
};

/* VME64 attributes */
static struct attribute *vme_dev_attr_list_vme64[] = {
	&dev_attr_vendor.attr,
	&dev_attr_device.attr,
	&dev_attr_revision.attr,
	&dev_attr_enable.attr,
	&dev_attr_reset.attr,
	&dev_attr_sysfail.attr,
	NULL,
};
static const struct attribute_group vme_dev_group_vme64 = {
	.attrs = vme_dev_attr_list_vme64,
};

/* VME64x attributes */
static struct attribute *vme_dev_attr_list_vme64x[] = {
	&dev_attr_ader.attr,
	&dev_attr_user_bit.attr,
	&dev_attr_resources.attr,
	NULL,
};


static const struct attribute_group vme_dev_group_vme64x = {
	.attrs = vme_dev_attr_list_vme64x,
};


/* Group lists */
static const struct attribute_group *vme_dev_groups_nostd[] = {
	&vme_dev_group_basic,
	&vme_dev_group_nostd,
	NULL,
};

static const struct attribute_group *vme_dev_groups_vme64[] = {
	&vme_dev_group_basic,
	&vme_dev_group_vme64,
	NULL,
};

static const struct attribute_group *vme_dev_groups_vme64x[] = {
	&vme_dev_group_basic,
	&vme_dev_group_vme64,
	&vme_dev_group_vme64x,
	NULL,
};


static void vme_device_type_release(struct device *dev)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	vme_cr_area_release_all(vdev);

	vme_csr_unmap(vdev);
	kfree(vdev);
}

const struct device_type vme_dev_type[] = {
	[VME_SPEC_UNKNOWN] = {
		.name = "Unknown",
		.groups = vme_dev_groups_nostd,
		.release = vme_device_type_release,
	},
	[VME_SPEC_VME64] = {
		.name = "VME64",
		.groups = vme_dev_groups_vme64,
		.release = vme_device_type_release,
	},
	[VME_SPEC_VME64x] = {
		.name = "VME64x",
		.groups = vme_dev_groups_vme64x,
		.release = vme_device_type_release,
	},
};

/**
 * It sets the slot number
 * @dev the device instance for a VME device (the one got from probe())
 * @slot slot number [1, N]
 *
 * If you are using the old API, call this function as soon as you know
 * those two parameters (typically on vme->probe()).
 *
 * Return: 0 on success, otherwise -EINVAL if one of the parameters is
 * not valid
 */
int vme_device_slot_set(struct device *dev, unsigned int slot)
{
	struct vme_dev *vdev = to_vme_dev(dev);

	vdev->slot = slot;

	return 0;

}
EXPORT_SYMBOL_GPL(vme_device_slot_set);

/**
 * Initialize a new device
 * @vme_dev VME device
 */
static void vme_initialize_device(struct vme_dev *vme_dev)
{
	if (vme_dev->slot >= VME_SLOT_MIN && vme_dev->slot <= VME_SLOT_MAX) {
		struct vme_slot *slot;

		vme_dev->dev.parent =
				&vbridge_gbl->bus_slot[vme_dev->slot - 1].dev;
		slot = to_vme_slot(vme_dev->dev.parent);
		BUG_ON(slot->index != vme_dev->slot);
	}
	vme_dev->dev.bus = vbridge_gbl->vme_bus_type;
	/* dma mask depends from bridge capability */
	vme_dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	vme_dev->dev.dma_mask = &vme_dev->dev.coherent_dma_mask;

	vme_dev->spec = VME_SPEC_UNKNOWN;
	vme_dev->dev.type = &vme_dev_type[vme_dev->spec];
}

/**
 * It tells if a device belong to a driver that generated the
 * device itself
 */
static int __vme_unregister_find_device(struct device *dev, void *data)
{
	struct vme_driver *vme_driver = data;

	return (dev->platform_data == vme_driver);
}

void vme_unregister_driver(struct vme_driver *vme_driver)
{
	struct device *dev;

	while (1) {
		/*
		 * The `driver_for_each_device()` function cannot be used
		 * to remove devcies because of some internal reference
		 * counter that leads to a deadlock. Instead, we can look
		 * for devices that match our criterias and loop over them
		 * until we remove them all
		 */
		dev = driver_find_device(&vme_driver->driver, NULL,
			       vme_driver, __vme_unregister_find_device);
		if (!dev)
			break;
		device_unregister(dev);
	}
	driver_unregister(&vme_driver->driver);
}
EXPORT_SYMBOL_GPL(vme_unregister_driver);

int vme_register_driver(struct vme_driver *vme_driver, unsigned int ndev)
{
	int error, i;
	unsigned int id;

	vme_driver->driver.bus	= vbridge_gbl->vme_bus_type;

	error = driver_register(&vme_driver->driver);
	if (error)
		return error;

	for (id = 0; id < ndev; id++) {
		struct vme_dev *vme_dev;

		vme_dev = kzalloc(sizeof(*vme_dev), GFP_KERNEL);
		if (!vme_dev) {
			error = -ENOMEM;
			break;
		}

		vme_dev->slot = -1; /* set by drv */
		vme_dev->irq_vector = VME_IRQ_VECTOR_OLDAPI; /* set by drv */

		vme_initialize_device(vme_dev);
		dev_set_name(&vme_dev->dev, "%s.%u",
			     vme_driver->driver.name, id);
		vme_dev->flags |= VME_DEV_FLAG_DRIVER_REG;
		vme_dev->dev.platform_data	= vme_driver;
		vme_dev->id			= id;

		for (i = 0; i < VME_CR_FUNCTION_MAX; ++i)
			memset(&vme_dev->resource[i], 0,
			       sizeof(vme_dev->resource[i]));

		error = device_register(&vme_dev->dev);
		if (error) {
			put_device(&vme_dev->dev);
			break;
		}

		if (!vme_dev->dev.platform_data)
			device_unregister(&vme_dev->dev);
	}

	if (error)
		vme_unregister_driver(vme_driver);

	return error;
}
EXPORT_SYMBOL_GPL(vme_register_driver);

void vme_unregister_device(struct vme_dev *vme_dev)
{
	struct vme_bridge_device *vbridge;
	int i;

	for (i = 0; i < VME_CR_FUNCTION_MAX; ++i)
		vme_func_resource_destroy(vme_dev, i);

	if (vme_dev->spec > VME_SPEC_UNKNOWN) {
		sysfs_remove_bin_file(&vme_dev->dev.kobj,
				      &vme_cr_bin_attr);
		sysfs_remove_bin_file(&vme_dev->dev.kobj,
				      &vme_csr_bin_attr);
	}

	device_unregister(&vme_dev->dev);

	if (vme_dev->flags & VME_DEV_FLAG_DRIVER_REG)
		return;

	/* For normal registration we have a proper hierarchy */
	vbridge = pci_get_drvdata(to_pci_dev(vme_dev->dev.parent->parent));
	vme_device_irq_unassign(vme_dev);
}
EXPORT_SYMBOL_GPL(vme_unregister_device);


int vme_register_device(struct vme_dev *vme_dev)
{
	int err, i, tmp1, tmp2;
	struct vme_bridge_device *vbridge;

	vme_initialize_device(vme_dev);

	vbridge = pci_get_drvdata(to_pci_dev(vme_dev->dev.parent->parent));

	dev_set_name(&vme_dev->dev, "vme.%u",
		     vme_dev->slot);

	vme_dev->flags &= ~VME_DEV_FLAG_DRIVER_REG;

	/* Assign a Linux IRQ number */
	if (vme_dev->irq_vector < VME_NUM_VECTORS) {
		tmp1 = vme_dev->irq_vector;
		tmp2 = vme_dev->irq_vector + 1;
	} else {
		switch (vme_dev->irq_vector) {
		case VME_IRQ_VECTOR_AUTO:
			tmp1 = 0;
			tmp2 = VME_NUM_VECTORS;
			break;
		case VME_IRQ_VECTOR_OLDAPI:
		default:
			pr_err(PFX "Invalid IRQ vector assignment option\n");
			return -EINVAL;
		}
	}
	err = vme_device_irq_assign(vme_dev, tmp1, tmp2);
	if (err) {
		pr_err(PFX "Can't assign VME IRQ vector\n");
		return -EBUSY;
	}

	pr_info(PFX "Registering device: [slot: %d, am: 0x%x, addr: 0x%x, size: 0x%x, irq-vector: 0x%x, irq-level: %d, id: %s:0x%x:0x%x:0x%x]\n",
		vme_dev->slot,
		vme_dev->am,
		vme_dev->base_address,
		vme_dev->size,
		vme_dev->irq_vector,
		vme_dev->irq_level,
		vme_dev->devid.name,
		vme_dev->devid.vendor,
		vme_dev->devid.device,
		vme_dev->devid.revision);
	pr_info(PFX "Assign Linux IRQ number %d to vector 0x%x\n",
		vme_dev->irq, vme_dev->irq_vector);

	err = vme_csr_map(vme_dev); /* we will detect errors later */
	vme_cr_area_dump_all(vme_dev);

	if (vme_dev->spaces[VME_CR_AREA_CR].repr) {
		struct vme_cr *cr = vme_dev->spaces[VME_CR_AREA_CR].repr;

		vme_dev->spec = cr->spec;
		vme_dev->devid.vendor = cr->manufacturer;
		vme_dev->devid.device = cr->device;
		vme_dev->devid.revision = cr->revision;
	}

	err = vme_device_validate(&vme_dev->dev);
	if (err)
		goto err_val;

	if (vme_dev->spec <= VME_SPEC_UNKNOWN ||
	    vme_dev->spec > VME_SPEC_VME64x) {
		vme_dev->spec = VME_SPEC_UNKNOWN;
		/*
		 * We do not need the CSR space when the card does
		 * not support it.
		 */


		/* FIXME But since we have broken vme-slaves we can't do it */
		/* vme_csr_unmap(vme_dev);*/
	}

	vme_dev->dev.type = &vme_dev_type[vme_dev->spec];
	for (i = 0; i < VME_CR_FUNCTION_MAX; ++i)
		memset(&vme_dev->resource[i], 0, sizeof(struct resource));

	err = device_register(&vme_dev->dev);
	if (err) {
		put_device(&vme_dev->dev);
		goto err_dev;
	}

	if (vme_dev->spec > VME_SPEC_UNKNOWN) {
		err = sysfs_create_bin_file(&vme_dev->dev.kobj,
					    &vme_cr_bin_attr);
		if (err)
			goto err_sysfs_bin;
		err = sysfs_create_bin_file(&vme_dev->dev.kobj,
					    &vme_csr_bin_attr);
		if (err) {
			sysfs_remove_bin_file(&vme_dev->dev.kobj,
					      &vme_cr_bin_attr);

			goto err_sysfs_bin;
		}
	}

	return 0;

err_sysfs_bin:
	device_unregister(&vme_dev->dev);
err_dev:
err_val:
	vme_device_irq_unassign(vme_dev);
	return err;
}
EXPORT_SYMBOL_GPL(vme_register_device);

static char version[] =
	"PCI-VME bridge: V" DRV_MODULE_VERSION " (" DRV_MODULE_RELDATE ")";

static struct pci_device_id vme_bridge_ids[] = {
	{PCI_DEVICE(PCI_VENDOR_ID_TUNDRA, PCI_DEVICE_ID_TUNDRA_TSI148)},
	{PCI_DEVICE(PCI_VENDOR_ID_MEN, PCI_DEVICE_ID_MEN_PLDZ002)},
	{0, },
};

/**
 * vme_bridge_remove() - Cleanup for module unloading
 * @dev: PCI device to unregister
 *
 */
void vme_bridge_remove(struct pci_dev *pdev)
{
	struct vme_bridge_device *vbridge = pci_get_drvdata(pdev);

	vme_dbg_exit(vbridge);
	vme_bus_unregister(vbridge);
	vme_interrupts_exit(vbridge);
	vme_window_exit();

	vme_dmaengine_exit(vbridge);

	vbridge->chip_mgr.ops->release_regions(vbridge);

	pci_disable_device(pdev);

	vme_procfs_unregister(vbridge);

	vme_bridge_remove_devices();
	class_destroy(vme_class);

	kfree(vbridge);
}

/*
 * Some exported features like vme_driver_register, vme_device_regsiter,
 * vme_generate_interrupt, reuqire to retrieve the vmebridge global instance
 * Removing this global instance requires probably to change the API of some
 * exported services.
 * To be evaluated more precisely later on.
 */
struct vme_bridge_device *vbridge_gbl;

static unsigned int vme_create_on_find_fail;
static unsigned int vme_destroy_on_remove;
static unsigned int vme_report_bus_errors;

static int vme_bridge_probe(struct pci_dev *pdev,
			    const struct pci_device_id *id)
{
	struct vme_bridge_device *vbridge;
	int rc = -ENODEV;

	vbridge = kzalloc(sizeof(struct vme_bridge_device), GFP_KERNEL);
	if (!vbridge) {
		rc = -ENOMEM;
		goto out_err;
	}

	/* Set module parameters */
	vbridge->mod_param.vme_create_on_find_fail = vme_create_on_find_fail;
	vbridge->mod_param.vme_destroy_on_remove = vme_destroy_on_remove;
	vbridge->mod_param.vme_report_bus_errors = vme_report_bus_errors;

	switch (pdev->vendor) {
	case PCI_VENDOR_ID_TUNDRA:
		rc = tsi148_set_ops(vbridge);
		if (rc < 0)
			goto out_free;
		vbridge->name = PCI_TUNDRA_CHIP_NAME;
		break;
	case PCI_VENDOR_ID_MEN:
		rc = pldz002_set_ops(vbridge);
		if (rc < 0)
			goto out_free;
		vbridge->name = PCI_MEN_CHIP_NAME;
		break;
	default:
		goto out_free; // unknown HW
	}

	dev_info(&pdev->dev, "%s\n", version);
	/*
	 * init the vme_bridge global instance reuires by a couple of exported
	 * features
	 * TODO: something to solve later on
	 */
	vbridge_gbl = vbridge;

	rc = vme_bridge_init(pdev, vbridge);
	if (rc)
		goto out_free;
	return 0;

out_free:
	kfree(vbridge);
out_err:
	return rc;
}

static struct pci_driver vme_bridge_driver = {
	.name = DRV_MODULE_NAME,
	.id_table = vme_bridge_ids,
	.probe = vme_bridge_probe,
	.remove = vme_bridge_remove,
};

module_pci_driver(vme_bridge_driver);

module_param(vme_create_on_find_fail, int, 0444);
MODULE_PARM_DESC(vme_create_on_find_fail, "When set, allows creating a new window if a mapping cannot be found");

module_param(vme_destroy_on_remove, int, 0444);
MODULE_PARM_DESC(vme_destroy_on_remove, "When set, removing the last mapping on a window also destroy the window");

module_param(vme_report_bus_errors, int, 0644);
MODULE_PARM_DESC(vme_report_bus_errors, "When set, prints a message to the kernel log whenever a VME Bus Error is detected");

MODULE_AUTHOR("");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("PCI-VME Bridge driver");
MODULE_VERSION(DRV_MODULE_VERSION);
MODULE_VERSION(GIT_VERSION);
MODULE_DEVICE_TABLE(pci, vme_bridge_ids);
