# Author Adam Wujek, CERN 2017
import pytest
import time
from test_config import *

SVEC_CRCSR_addr=SVEC_slot*0x80000
FUN0ADER=0x7FF60
FUN1ADER=0x7FF70
RANDOM_SIZE="1048576"
RANDOM_FILE="/tmp/svec_random.dd"
SVEC_MEM_SIZE=256*1024
TMP_FILE="/tmp/tmp.dd"

# data sizes to be used for AXXD16 accesses
@pytest.fixture(scope="function", params=[2, 4, 6, 8, 16, 32, 0x100, 0x1000, 0x1100, 0x4000, SVEC_MEM_SIZE])
def size_fixtureD16(request):
    param = request.param
    yield param

# data sizes to be used for AXXD16_blt accesses
# unfortunately a25 does not support BLT transfers od datasize % 4 == 2
@pytest.fixture(scope="function", params=[4, 8, 12, 16, 32, 0x100, 0x1000, 0x1100, 0x4000, SVEC_MEM_SIZE])
def size_fixtureD16_blt(request):
    param = request.param
    yield param

# data sizes to be used for AXXD32 and AXXD32_blt accesses
@pytest.fixture(scope="function", params=[4, 8, 12, 16, 32, 0x100, 0x1000, 0x1100, 0x4000, SVEC_MEM_SIZE])
def size_fixtureD32(request):
    param = request.param
    yield param

# data sizes to be used for AXXD64_mblt accesses
@pytest.fixture(scope="function", params=[8, 16, 24, 32, 0x100, 0x1000, SVEC_MEM_SIZE])
def size_fixtureD64(request):
    param = request.param
    yield param

# address offsets to be used for AXXD16  accesses
@pytest.fixture(scope="function", params=[0, 2, 4, 6, 8, 32, 0x100, 0x1000, 0x1100, 0x4000])
def addr_fixtureD16(request):
    param = request.param
    yield param

# address offsets to be used for AXXD16_blt accesses
# unfortunately a25 does not support BLT transfers with address % 4 == 2
@pytest.fixture(scope="function", params=[0, 4, 8, 12, 16, 0x100, 0x1000, 0x1100, 0x4000])
def addr_fixtureD16_blt(request):
    param = request.param
    yield param

# address offsets to be used for AXXD32 and AXXD32_blt accesses
@pytest.fixture(scope="function", params=[0, 4, 8, 12, 16, 0x100, 0x1000, 0x1100, 0x4000])
def addr_fixtureD32(request):
    param = request.param
    yield param

# address offsets to be used for A32D64_mblt accesses
@pytest.fixture(scope="function", params=[0, 8, 16, 24, 0x100, 0x1000, 0x1100, 0x4000])
def addr_fixtureD64(request):
    param = request.param
    yield param

# address offsets to be used for A32D64_mblt accesses
@pytest.fixture(scope="function", params=[0, 4, 8, 16, 24, 0x100, 0x1000 - 4, 0x1000, 0x1000 +4, 0x1100, 0x4000])
def buff_offset_fixtureD32(request):
    param = request.param
    yield param

@pytest.fixture(scope="function", params=[0, 8, 16, 24, 0x100, 0x1000 - 8, 0x1000, 0x1000 + 8, 0x1100, 0x4000])
def buff_offset_fixtureD64(request):
    param = request.param
    yield param
