#!/bin/sh

# Author: Adam Wujek
# Based on previous BE-CO-HT scripts
# 2019

TRANSFER=/etc/transfer.ref
DEVICE_NAME_FMC=FMC_GMT_SIM
DEVICE_NAME_SVEC=FMC-SVEC-A24
DRIVER_NAME=fmc-gmt-sim
FMC_GMT_FIRMWARE=/acc/local/share/firmware/svec-gmt-simulator/svec-gmt-simulator-20190122.bin

OUTPUT=""
RUN=""

while getopts hvn:D:d:t: o
do	case $o in
	v)	OUTPUT="echo" ;;		# verbose
	n)	RUN=":" ;;			# dry run
	D)	DEVICE_NAME_FMC="$OPTARG" ;;
	S)	DEVICE_NAME_SVEC="$OPTARG" ;;
	d)	DRIVER_NAME="$OPTARG" ;;
	t)	TRANSFER="$OPTARG" ;;
	[h?])	echo >&2 "usage: $0 [-?hvn] [-D fmc_device_name] [-S svec_device_name] [-d driver_name] [-t transfer]"
		exit ;;
	esac
done

# this is superfluous; driver installation priorities take care of
# this pre-requisite, and when spec+fdelay is required, it is the
# fmc bus who actually knows what to install
#(cat /proc/modules | grep -q '^svec ' ) || (cd ../svec/ ; sh install_svec.sh)

# no driver so far
#echo "Installing fine delay driver..."
#/sbin/insmod $DRIVER_NAME.ko gateware=$FDELAY verbose=1

# FIXME: fd-luns.awk is VME-dependent; entirely different for SPEC
echo "$DRIVER_NAME: making device nodes"
FMC_SIM_SLOTS=`awk -f ./fd-slot.awk $DEVICE_NAME_FMC $TRANSFER`
ret=$?
if [ $ret != 0 ]; then
    echo "$0 Error! awk returned $ret"
    exit 1
fi

for slot in $FMC_SIM_SLOTS;
do
    SVEC_LUN=`awk -f ./fd-luns.awk $DEVICE_NAME_SVEC $slot $TRANSFER`
    # $SVEC_LUN in /dev/gmt-sim.$SVEC_LUN, shall be replaced by something else
    echo "Found $DEVICE_NAME_FMC in slot $slot, svec's lun $SVEC_LUN"
    $RUN $OUTPUT ln -sf svec.$SVEC_LUN /dev/gmt-sim.$SVEC_LUN
    $RUN $OUTPUT dd if="$FMC_GMT_FIRMWARE" of=/dev/gmt-sim.$SVEC_LUN bs=10MB count=1
    $RUN $OUTPUT ln -sf /sys/bus/vme/devices/svec.$SVEC_LUN/vme_base /dev/gmt-sim_vme_base.$SVEC_LUN
    $RUN $OUTPUT ln -sf /sys/bus/vme/devices/svec.$SVEC_LUN/vme_am /dev/gmt-sim_vme_am.$SVEC_LUN
done
