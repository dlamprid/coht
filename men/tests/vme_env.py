# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *

import os
import stat
import pytest

def env_check_cmd(cmd_name):
    cmd = "command -v %s" % (cmd_name)
    #print (cmd)
    process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output, error = process.communicate()
    return process.returncode

def env_check_character_device(device):
    mode = os.stat("/dev/" + device).st_mode
    return (stat.S_ISCHR(mode))


def test_presence_of_command_vme():
    assert env_check_cmd(VME_BIN) == 0

def test_presence_of_command_vme4l_rwex():
    assert env_check_cmd(VME_RWEX_BIN) == 0

def test_presence_of_command_vme4l_ctrl():
    assert env_check_cmd(VME_CTRL_BIN) == 0

def test_presence_of_command_vmeio():
    assert env_check_cmd(VMEIO_BIN) == 0

def test_presence_of_command_ctrtest():
    assert env_check_cmd(CTRTEST_BIN) == 0

def test_presence_of_command_cvoratest():
    assert env_check_cmd(CVORATEST_BIN) == 0

def test_presence_of_command_cvorbtest():
    assert env_check_cmd(CVORBTEST_BIN) == 0

def test_presence_of_command_sis33test():
    assert env_check_cmd(SISTEST_BIN) == 0

def test_presence_of_character_device_vme4l_a16d32():
    assert env_check_character_device("vme4l_a16d32")

def test_presence_of_character_device_vme4l_a24d32():
    assert env_check_character_device("vme4l_a24d32")

def test_presence_of_character_device_vme4l_a32d32():
    assert env_check_character_device("vme4l_a32d32")

def test_presence_of_character_device_vme4l_a32d64_blt():
    assert env_check_character_device("vme4l_a32d64_blt")

def test_presence_of_character_device_vme4l_cr_csr():
    assert env_check_character_device("vme4l_cr_csr")
