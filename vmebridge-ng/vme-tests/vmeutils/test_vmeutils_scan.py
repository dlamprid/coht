"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import subprocess

class TestVmeutilsRegister(object):
    """It tests that the tool vme-register"""

    def test_register_invalid_param_01(self):
        """Invalid slot number 0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-scan",
                                "--slot", str(0),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_02(self):
        """Invalid slot number from 0"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-scan",
                                "--from", str(0),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)

    def test_register_invalid_param_03(self):
        """Invalid range"""
        vme_register_command = ["/user/fvaga/deployment/L867/bin/vme-scan",
                                "--from", str(21), "--to", str(1),]
        with pytest.raises(subprocess.CalledProcessError):
            subprocess.run(vme_register_command, check=True)
