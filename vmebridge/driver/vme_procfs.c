// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2019
 * Author Federico Vaga <federico.vaga@cern.ch>
 * Author Sebastien Dugue
 */

#ifdef CONFIG_PROC_FS

#include <linux/io.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "vme_bridge.h"
#include "tsi148.h"
#include "tsi148_window.h"


/* VME address modifiers names */
static char *amod[] = {
	"A64MBLT", "A64", "Invalid 0x02", "A64BLT",
	"A64LCK", "A32LCK", "Invalid 0x06", "Invalid 0x07",
	"A32MBLT USER", "A32 USER DATA", "A32 USER PROG", "A32BLT USER",
	"A32MBLT SUP", "A32 SUP DATA", "A32 SUP PROG", "A32BLT SUP",
	"Invalid 0x10", "Invalid 0x11", "Invalid 0x12", "Invalid 0x13",
	"Invalid 0x14", "Invalid 0x15", "Invalid 0x16", "Invalid 0x17",
	"Invalid 0x18", "Invalid 0x19", "Invalid 0x1a", "Invalid 0x1b",
	"Invalid 0x1c", "Invalid 0x1d", "Invalid 0x1e", "Invalid 0x1f",
	"2e6U", "2e3U", "Invalid 0x22", "Invalid 0x23",
	"Invalid 0x24", "Invalid 0x25", "Invalid 0x26", "Invalid 0x27",
	"Invalid 0x28", "A16 USER", "Invalid 0x2a", "Invalid 0x2b",
	"A16LCK", "A16 SUP", "Invalid 0x2e", "CR/CSR",
	"Invalid 0x30", "Invalid 0x31", "Invalid 0x32", "Invalid 0x33",
	"A40", "A40LCK", "Invalid 0x36", "A40BLT",
	"A24MBLT USER", "A24 USER DATA", "A24 USER PROG", "A24BLT USER",
	"A24MBLT SUP", "A24 SUP DATA", "A24 SUP PROG", "A24BLT SUP"
};

static int vme_window_proc_show_mapping(struct seq_file *m, int num, struct mapping *mapping)
{
        char *pfs;
        struct vme_mapping *desc = &mapping->desc;

/*
           va       PCI     VME      Size     Address Modifier   Data   Prefetch
                                                 Description     Width   Size
    dd: xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx - ssssssssssssssss /  sss    sssss
    client: ddddd ssssssssssssssss
*/
        seq_printf(m, "    %2d: %p %.8x %.8x %.8x - ",
                     num,
                     desc->kernel_va, desc->pci_addrl,
                     desc->vme_addrl, desc->sizel);

        if (desc->read_prefetch_enabled)
                switch (desc->read_prefetch_size) {
                case VME_PREFETCH_2:
                        pfs = "PFS2";
                        break;
                case VME_PREFETCH_4:
                        pfs = "PFS4";
                        break;
                case VME_PREFETCH_8:
                        pfs = "PFS8";
                        break;
                case VME_PREFETCH_16:
                        pfs = "PFS16";
                        break;
                default:
                        pfs = "?";
                        break;
                }
        else
                pfs = "NOPF";

        seq_printf(m, "(0x%02x)%s / D%2d %5s\n",
                   desc->am, amod[desc->am], desc->data_width, pfs);
        seq_printf(m, "        client: ");

        if (mapping->client.pid_nr)
                seq_printf(m, "%d ", (unsigned int)mapping->client.pid_nr);

        if (mapping->client.name[0])
                seq_printf(m, "%s", mapping->client.name);

        seq_printf(m, "\n");
        return 0;
}

static int vme_window_proc_show_window(struct seq_file *m, int window_num)
{
        char *pfs;
        struct window *window = &window_table[window_num];
        struct mapping *mapping;
        struct vme_mapping *desc;
        int count = 0;


        seq_printf(m, "Window %d:  ", window_num);

        if (!window->active)
                seq_printf(m, "Not Active\n");
        else {
                seq_printf(m, "Active - ");
                if (window->users == 0)
                        seq_printf(m, "No users\n");
                else
                        seq_printf(m, "%2d user%c\n", window->users,
                                     (window->users > 1)?'s':' ');
        }

        if (!window->active) {
                seq_printf(m, "\n");
                return 0;
        }

        desc = &window->desc;
        seq_printf(m, "        %p %.8x %.8x %.8x - ",
                   desc->kernel_va, desc->pci_addrl,
                   desc->vme_addrl, desc->sizel);

        if (desc->read_prefetch_enabled)
               switch (desc->read_prefetch_size) {
                        case VME_PREFETCH_2:
                                pfs = "PFS2";
                                break;
                        case VME_PREFETCH_4:
                                pfs = "PFS4";
                                break;
                        case VME_PREFETCH_8:
                                pfs = "PFS8";
                                break;
                        case VME_PREFETCH_16:
                                pfs = "PFS16";
                                break;
                        default:
                                pfs = "?";
                                break;
                        }
                else
                        pfs = "NOPF";

        seq_printf(m, "(0x%02x)%s / D%2d %5s\n",
                   desc->am, amod[desc->am], desc->data_width, pfs);

        if (list_empty(&window->mappings)) {
                seq_printf(m, "\n");
                return 0;
        }

        seq_printf(m, "\n  Mappings:\n");
        list_for_each_entry(mapping, &window->mappings, list) {
                vme_window_proc_show_mapping(m, count, mapping);
                count++;
        }
        seq_printf(m, "\n");

    return 0;
}

static int vme_window_proc_show(struct seq_file *m, void *data)
{
        int i;

        seq_printf(m, "\nPCI-VME Windows\n");
        seq_printf(m, "===============\n\n");
        seq_printf(m, "        va       PCI      VME      Size      Address Modifier Data  Prefetch\n");
        seq_printf(m, "                                             and Description  Width Size\n");
        seq_printf(m, "----------------------------------------------------------------------------\n\n");

        for (i = 0; i < TSI148_NUM_OUT_WINDOWS; i++)
                vme_window_proc_show_window(m, i);

        return 0;
}

static int vme_window_proc_open(struct inode *inode, struct file *file)
{
        return single_open(file, vme_window_proc_show, NULL);
}

static const struct file_operations vme_window_proc_ops = {
        .open           = vme_window_proc_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};

/**
 * tsi148_proc_pcfs_show() - Dump the PCFS register group
 *
 */
static int tsi148_proc_pcfs_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	unsigned int tmp;

	seq_printf(m, "\nPCFS Register Group:\n");

	tmp = ioread32(&tsi148->pcfs.veni);
	seq_printf(m, "\tpciveni   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcidevi   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pcfs.cmmd);
	seq_printf(m, "\tpcicmd    0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcistat   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pcfs.rev_class);
	seq_printf(m, "\tpcirev    0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpciclass  0x%06x\n", (tmp >> 8) & 0xffffff);
	tmp = ioread32(&tsi148->pcfs.clsz);
	seq_printf(m, "\tpciclsz   0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpcimlat   0x%02x\n", (tmp>>8) & 0xff);
	tmp = ioread32(&tsi148->pcfs.mbarl);
	seq_printf(m, "\tpcimbarl  0x%08x\n", tmp);
	tmp = ioread32(&tsi148->pcfs.mbaru);
	seq_printf(m, "\tpcimbaru  0x%08x\n", tmp);
	tmp = ioread32(&tsi148->pcfs.subv);
	seq_printf(m, "\tpcisubv   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcisubi   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pcfs.intl);
	seq_printf(m, "\tpciintl   0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpciintp   0x%02x\n", (tmp>>8) & 0xff);
	seq_printf(m, "\tpcimngn   0x%02x\n", (tmp>>16) & 0xff);
	seq_printf(m, "\tpcimxla   0x%02x\n", (tmp>>24) & 0xff);
	tmp = ioread32(&tsi148->pcfs.pcix_cap_id);
	seq_printf(m, "\tpcixcap   0x%02x\n", tmp & 0xff);
	tmp = ioread32(&tsi148->pcfs.pcix_status);
	seq_printf(m, "\tpcixstat  0x%08x\n", tmp);

	return 0;
}

static int tsi148_proc_pcfs_open(struct inode *inode, struct file *file)
{
	return single_open(file, tsi148_proc_pcfs_show, tsi148_chip_global);
}

static const struct file_operations tsi148_proc_pcfs_ops = {
        .open           = tsi148_proc_pcfs_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};


/**
 * tsi148_proc_lcsr_show() - Dump the LCSR register group
 *
 */
static int tsi148_proc_lcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	int i;

	seq_printf(m, "\n");

	/* Display outbound decoders */
	seq_printf(m, "Local Control and Status Register Group (LCSR):\n");
	seq_printf(m, "\nOutbound Translations:\n");
	seq_printf(m, "No. otat         otsau:otsal        oteau:oteal"
		     "         otofu:otofl\n");
	for (i = 0; i < 8; i++) {
		unsigned int otat, otsau, otsal, oteau, oteal, otofu, otofl;

		otat = ioread32be(&tsi148->lcsr.otrans[i].otat);
		otsau = ioread32be(&tsi148->lcsr.otrans[i].otsau);
		otsal = ioread32be(&tsi148->lcsr.otrans[i].otsal);
		oteau = ioread32be(&tsi148->lcsr.otrans[i].oteau);
		oteal = ioread32be(&tsi148->lcsr.otrans[i].oteal);
		otofu = ioread32be(&tsi148->lcsr.otrans[i].otofu);
		otofl = ioread32be(&tsi148->lcsr.otrans[i].otofl);
		seq_printf(m, "%d:  %08X  %08X:%08X  %08X:%08X:  %08X:%08X\n",
			   i, otat, otsau, otsal, oteau, oteal, otofu, otofl);
	}

	/* Display inbound decoders */
	seq_printf(m, "\nInbound Translations:\n");
	seq_printf(m, "No. itat         itsau:itsal        iteau:iteal"
		     "         itofu:itofl\n");
	for (i = 0; i < 8; i++) {
		unsigned int itat, itsau, itsal, iteau, iteal, itofu, itofl;

		itat = ioread32be(&tsi148->lcsr.itrans[i].itat);
		itsau = ioread32be(&tsi148->lcsr.itrans[i].itsau);
		itsal = ioread32be(&tsi148->lcsr.itrans[i].itsal);
		iteau = ioread32be(&tsi148->lcsr.itrans[i].iteau);
		iteal = ioread32be(&tsi148->lcsr.itrans[i].iteal);
		itofu = ioread32be(&tsi148->lcsr.itrans[i].itofu);
		itofl = ioread32be(&tsi148->lcsr.itrans[i].itofl);
		seq_printf(m, "%d:  %08X  %08X:%08X  %08X:%08X:  %08X:%08X\n",
			   i, itat, itsau, itsal, iteau, iteal, itofu, itofl);
	}

	seq_printf(m, "\nVME Bus Control:\n");
	seq_printf(m, "\tvmctrl  0x%08x\n", ioread32be(&tsi148->lcsr.vmctrl));
	seq_printf(m, "\tvctrl   0x%08x\n", ioread32be(&tsi148->lcsr.vctrl));
	seq_printf(m, "\tvstat   0x%08x\n", ioread32be(&tsi148->lcsr.vstat));

	seq_printf(m, "PCI Status:\n");
	seq_printf(m, "\tpcsr    0x%08x\n", ioread32be(&tsi148->lcsr.pstat));

	seq_printf(m, "VME Exception Status:\n");
	seq_printf(m, "\tveau:veal 0x%08x:%08x\n",
		   ioread32be(&tsi148->lcsr.veau),
		   ioread32be(&tsi148->lcsr.veal));
	seq_printf(m, "\tveat    0x%08x\n", ioread32be(&tsi148->lcsr.veat));

	seq_printf(m, "PCI Error Status:\n");
	seq_printf(m, "\tedpau:edpal 0x%08x:%08x\n",
		   ioread32be(&tsi148->lcsr.edpau),
		   ioread32be(&tsi148->lcsr.edpal));
	seq_printf(m, "\tedpxa   0x%08x\n", ioread32be(&tsi148->lcsr.edpxa));
	seq_printf(m, "\tedpxs   0x%08x\n", ioread32be(&tsi148->lcsr.edpxs));
	seq_printf(m, "\tedpat   0x%08x\n", ioread32be(&tsi148->lcsr.edpat));

	seq_printf(m, "Inbound Translation Location Monitor:\n");
	seq_printf(m, "\tlmbau:lmbal 0x%08x:%08x:\n",
		   ioread32be(&tsi148->lcsr.lmbau),
		   ioread32be(&tsi148->lcsr.lmbal));
	seq_printf(m, "\tlmat    0x%08x\n", ioread32be(&tsi148->lcsr.lmat));

	seq_printf(m, "Local bus Interrupt Control:\n");
	seq_printf(m, "\tinten   0x%08x\n", ioread32be(&tsi148->lcsr.inten));
	seq_printf(m, "\tinteo   0x%08x\n", ioread32be(&tsi148->lcsr.inteo));
	seq_printf(m, "\tints    0x%08x\n", ioread32be(&tsi148->lcsr.ints));
	seq_printf(m, "\tintc    0x%08x\n", ioread32be(&tsi148->lcsr.intc));
	seq_printf(m, "\tintm1   0x%08x\n", ioread32be(&tsi148->lcsr.intm1));
	seq_printf(m, "\tintm2   0x%08x\n", ioread32be(&tsi148->lcsr.intm2));

	seq_printf(m, "VMEbus Interrupt Control:\n");
	seq_printf(m, "\tbcu64   0x%08x\n", ioread32be(&tsi148->lcsr.bcu));
	seq_printf(m, "\tbcl64   0x%08x\n", ioread32be(&tsi148->lcsr.bcl));
	seq_printf(m, "\tbpgtr   0x%08x\n", ioread32be(&tsi148->lcsr.bpgtr));
	seq_printf(m, "\tbpctr   0x%08x\n", ioread32be(&tsi148->lcsr.bpctr));
	seq_printf(m, "\tvicr    0x%08x\n", ioread32be(&tsi148->lcsr.vicr));

	seq_printf(m, "RMW Register Group:\n");
	seq_printf(m, "\trmwau  0x%08x\n", ioread32be(&tsi148->lcsr.rmwau));
	seq_printf(m, "\trmwal  0x%08x\n", ioread32be(&tsi148->lcsr.rmwal));
	seq_printf(m, "\trmwen  0x%08x\n", ioread32be(&tsi148->lcsr.rmwen));
	seq_printf(m, "\trmwc   0x%08x\n", ioread32be(&tsi148->lcsr.rmwc));
	seq_printf(m, "\trmws   0x%08x\n", ioread32be(&tsi148->lcsr.rmws));

	for(i = 0; i < TSI148_NUM_DMA_CHANNELS; i++){
		seq_printf(m, "DMA Controler %d Registers\n", i);
		seq_printf(m, "\tdctl   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dctl));
		seq_printf(m, "\tdsta   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dsta));
		seq_printf(m, "\tdcsau  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dcsau));
		seq_printf(m, "\tdcsal  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dcsal));
		seq_printf(m, "\tdcdau  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dcdau));
		seq_printf(m, "\tdcdal  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dcdal));
		seq_printf(m, "\tdclau  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dclau));
		seq_printf(m, "\tdclal  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dclal));
		seq_printf(m, "\tdsau   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dsau));
		seq_printf(m, "\tdsal   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dsal));
		seq_printf(m, "\tddau   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.ddau));
		seq_printf(m, "\tddal   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.ddal));
		seq_printf(m, "\tdsat   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dsat));
		seq_printf(m, "\tddat   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.ddat));
		seq_printf(m, "\tdnlau  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dnlau));
		seq_printf(m, "\tdnlal  0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dnlal));
		seq_printf(m, "\tdcnt   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.dcnt));
		seq_printf(m, "\tddbs   0x%08x\n",
			   ioread32be(&tsi148->lcsr.dma[i].dma_desc.ddbs));
	}
	return 0;
}

static int tsi148_proc_lcsr_open(struct inode *inode, struct file *file)
{
        return single_open(file, tsi148_proc_lcsr_show, tsi148_chip_global);
}

static const struct file_operations tsi148_proc_lcsr_ops = {
        .open           = tsi148_proc_lcsr_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};


/**
 * tsi148_proc_gcsr_show() - Dump the GCSR register group
 *
 */
static int tsi148_proc_gcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	int i;
	unsigned int tmp;

	seq_printf(m, "\nGlobal Control and Status Register Group (GCSR):\n");

	tmp = ioread32be(&tsi148->gcsr.devi);
	seq_printf(m, "\tveni   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tdevi   0x%04x\n", (tmp >> 16) & 0xffff);

	tmp = ioread32be(&tsi148->gcsr.ctrl);
	seq_printf(m, "\tgctrl  0x%04x\n", (tmp >> 16) & 0xffff);
	seq_printf(m, "\tga     0x%02x\n", (tmp >> 8) & 0x3f);
	seq_printf(m, "\trevid  0x%02x\n", tmp & 0xff);

	seq_printf(m, "Semaphores:\n");
	tmp = ioread32be(&tsi148->gcsr.semaphore[0]);
	seq_printf(m, "\tsem0   0x%02x\n", (tmp >> 24) & 0xff);
	seq_printf(m, "\tsem1   0x%02x\n", (tmp >> 16) & 0xff);
	seq_printf(m, "\tsem2   0x%02x\n", (tmp >> 8) & 0xff);
	seq_printf(m, "\tsem3   0x%02x\n", tmp & 0xff);

	tmp = ioread32be(&tsi148->gcsr.semaphore[4]);
	seq_printf(m, "\tsem4   0x%02x\n", (tmp >> 24) & 0xff);
	seq_printf(m, "\tsem5   0x%02x\n", (tmp >> 16) & 0xff);
	seq_printf(m, "\tsem6   0x%02x\n", (tmp >> 8) & 0xff);
	seq_printf(m, "\tsem7   0x%02x\n", tmp & 0xff);

	seq_printf(m, "Mailboxes:\n");
	for (i = 0; i <= TSI148_NUM_MAILBOXES; i++){
		seq_printf(m, "\t Mailbox #%d: 0x%08x\n", i,
			     ioread32be(&tsi148->gcsr.mbox[i]));
	}
	return 0;
}

static int tsi148_proc_gcsr_open(struct inode *inode, struct file *file)
{
        return single_open(file, tsi148_proc_gcsr_show, tsi148_chip_global);
}

static const struct file_operations tsi148_proc_gcsr_ops = {
        .open           = tsi148_proc_gcsr_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};

/**
 * tsi148_proc_crcsr_show() - Dump the CRCSR register group
 *
 */
static int tsi148_proc_crcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;

	seq_printf(m, "\nCR/CSR Register Group:\n");
	seq_printf(m, "\tbcr   0x%08x\n", ioread32be(&tsi148->crcsr.csrbcr));
	seq_printf(m, "\tbsr   0x%08x\n", ioread32be(&tsi148->crcsr.csrbsr));
	seq_printf(m, "\tbar   0x%08x\n", ioread32be(&tsi148->crcsr.cbar));
	return 0;
}

static int tsi148_proc_crcsr_open(struct inode *inode, struct file *file)
{
        return single_open(file, tsi148_proc_crcsr_show, tsi148_chip_global);
}

static const struct file_operations tsi148_proc_crcsr_ops = {
        .open           = tsi148_proc_crcsr_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};


/**
 * tsi148_procfs_register() - Create the VME proc tree
 * @vme_root: Root directory of the VME proc tree
 */
void tsi148_procfs_register(struct vme_bridge_device *vbridge)
{
	struct proc_dir_entry *entry;

	vbridge->proc_tsi148 = proc_mkdir("tsi148", vbridge->proc_root);

	entry = proc_create("pcfs", S_IFREG | S_IRUGO, vbridge->proc_tsi148,
			    &tsi148_proc_pcfs_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc pcfs node\n");

	entry = proc_create("lcsr", S_IFREG | S_IRUGO, vbridge->proc_tsi148,
			    &tsi148_proc_lcsr_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc lcsr node\n");

	entry = proc_create("gcsr", S_IFREG | S_IRUGO, vbridge->proc_tsi148,
			    &tsi148_proc_gcsr_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc gcsr node\n");

	entry = proc_create("crcsr", S_IFREG | S_IRUGO, vbridge->proc_tsi148,
			    &tsi148_proc_crcsr_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc crcsr node\n");
}

/**
 * tsi148_procfs_unregister() - Remove the VME proc tree
 *
 */
void tsi148_procfs_unregister(struct vme_bridge_device *vbridge)
{
	remove_proc_entry("pcfs", vbridge->proc_tsi148);
	remove_proc_entry("lcsr", vbridge->proc_tsi148);
	remove_proc_entry("gcsr", vbridge->proc_tsi148);
	remove_proc_entry("crcsr", vbridge->proc_tsi148);
	remove_proc_entry("tsi148", vbridge->proc_root);
}

static int vme_info_proc_show(struct seq_file *m, void *data)
{
    seq_printf(m, "PCI-VME driver v%s (%s)\n\n",
		 DRV_MODULE_VERSION, DRV_MODULE_RELDATE);

    seq_printf(m, "chip revision:     %08X\n", vme_bridge->rev);
    seq_printf(m, "chip IRQ:          %d\n", vme_bridge->irq);
    seq_printf(m, "VME slot:          %d\n", vme_bridge->slot);
    seq_printf(m, "VME SysCon:        %s\n",
		 vme_bridge->syscon ? "Yes" : "No");
    seq_printf(m, "chip registers at: 0x%p\n", vme_bridge->regs);
    seq_printf(m, "\n");
    return 0;
}

static int vme_info_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, vme_info_proc_show, NULL);
}

static const struct file_operations vme_info_proc_ops = {
        .open           = vme_info_proc_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};


void vme_procfs_register(struct vme_bridge_device *vbridge)
{
	struct proc_dir_entry *entry;

	/* Create /proc/vme directory */
	vbridge->proc_root = proc_mkdir("vme", NULL);

	/* Create /proc/vme/info file */
	entry = proc_create("info", S_IFREG | S_IRUGO, vbridge->proc_root,
			    &vme_info_proc_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc info node\n");

	/* Create /proc/vme/windows file */
	entry = proc_create("windows", S_IFREG | S_IRUGO, vbridge->proc_root,
			    &vme_window_proc_ops);
	if (!entry)
		printk(KERN_WARNING PFX "Failed to create proc windows node\n");

	/* Create specific TSI148 proc entries */
	tsi148_procfs_register(vbridge);
}

void vme_procfs_unregister(struct vme_bridge_device *vbridge)
{
	tsi148_procfs_unregister(vbridge);
	remove_proc_entry("windows", vbridge->proc_root);
	remove_proc_entry("info", vbridge->proc_root);
	remove_proc_entry("vme", NULL);
}

#else
void vme_procfs_register(struct vme_bridge_device *vbridge) {}
void vme_procfs_unregister(struct vme_bridge_device *vbridge) {}
#endif /* CONFIG_PROC_FS */
