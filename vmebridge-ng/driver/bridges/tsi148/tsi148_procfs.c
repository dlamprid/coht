// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2019
 * Author Federico Vaga <federico.vaga@cern.ch>
 * Author Sebastien Dugue
 */

#ifdef CONFIG_PROC_FS

#include <linux/version.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "vme_bridge.h"
#include "vme_compat.h"
#include "tsi148.h"


/**
 * tsi148_proc_pcfs_show() - Dump the PCFS register group
 *
 */
static int tsi148_proc_pcfs_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	unsigned int tmp;

	seq_puts(m, "\nPCFS Register Group:\n");

	tmp = ioread32(&tsi148->pci_map->pcfs.veni);
	seq_printf(m, "\tpciveni   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcidevi   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pci_map->pcfs.cmmd);
	seq_printf(m, "\tpcicmd    0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcistat   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pci_map->pcfs.rev_class);
	seq_printf(m, "\tpcirev    0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpciclass  0x%06x\n", (tmp >> 8) & 0xffffff);
	tmp = ioread32(&tsi148->pci_map->pcfs.clsz);
	seq_printf(m, "\tpciclsz   0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpcimlat   0x%02x\n", (tmp>>8) & 0xff);
	tmp = ioread32(&tsi148->pci_map->pcfs.mbarl);
	seq_printf(m, "\tpcimbarl  0x%08x\n", tmp);
	tmp = ioread32(&tsi148->pci_map->pcfs.mbaru);
	seq_printf(m, "\tpcimbaru  0x%08x\n", tmp);
	tmp = ioread32(&tsi148->pci_map->pcfs.subv);
	seq_printf(m, "\tpcisubv   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tpcisubi   0x%04x\n", (tmp>>16) & 0xffff);
	tmp = ioread32(&tsi148->pci_map->pcfs.intl);
	seq_printf(m, "\tpciintl   0x%02x\n", tmp & 0xff);
	seq_printf(m, "\tpciintp   0x%02x\n", (tmp>>8) & 0xff);
	seq_printf(m, "\tpcimngn   0x%02x\n", (tmp>>16) & 0xff);
	seq_printf(m, "\tpcimxla   0x%02x\n", (tmp>>24) & 0xff);
	tmp = ioread32(&tsi148->pci_map->pcfs.pcix_cap_id);
	seq_printf(m, "\tpcixcap   0x%02x\n", tmp & 0xff);
	tmp = ioread32(&tsi148->pci_map->pcfs.pcix_status);
	seq_printf(m, "\tpcixstat  0x%08x\n", tmp);

	return 0;
}

static int tsi148_proc_pcfs_open(struct inode *inode, struct file *file)
{
	return single_open(file, tsi148_proc_pcfs_show, PDE_DATA(inode));
}

static const struct file_operations tsi148_proc_pcfs_ops = {
	.open           = tsi148_proc_pcfs_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};


/**
 * tsi148_proc_lcsr_show() - Dump the LCSR register group
 *
 */
static int tsi148_proc_lcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	int i;

	seq_puts(m, "\n");

	/* Display outbound decoders */
	seq_puts(m, "Local Control and Status Register Group (LCSR):\n");
	seq_puts(m, "\nOutbound Translations:\n");
	seq_puts(m, "No. otat         otsau:otsal        oteau:oteal         otofu:otofl\n");
	for (i = 0; i < 8; i++) {
		unsigned int otat, otsau, otsal, oteau, oteal, otofu, otofl;

		otat = ioread32be(&tsi148->pci_map->lcsr.otrans[i].otat);
		otsau = ioread32be(&tsi148->pci_map->lcsr.otrans[i].otsau);
		otsal = ioread32be(&tsi148->pci_map->lcsr.otrans[i].otsal);
		oteau = ioread32be(&tsi148->pci_map->lcsr.otrans[i].oteau);
		oteal = ioread32be(&tsi148->pci_map->lcsr.otrans[i].oteal);
		otofu = ioread32be(&tsi148->pci_map->lcsr.otrans[i].otofu);
		otofl = ioread32be(&tsi148->pci_map->lcsr.otrans[i].otofl);
		seq_printf(m, "%d:  %08X  %08X:%08X  %08X:%08X:  %08X:%08X\n",
			   i, otat, otsau, otsal, oteau, oteal, otofu, otofl);
	}

	/* Display inbound decoders */
	seq_puts(m, "\nInbound Translations:\n");
	seq_puts(m, "No. itat         itsau:itsal        iteau:iteal         itofu:itofl\n");
	for (i = 0; i < 8; i++) {
		unsigned int itat, itsau, itsal, iteau, iteal, itofu, itofl;

		itat = ioread32be(&tsi148->pci_map->lcsr.itrans[i].itat);
		itsau = ioread32be(&tsi148->pci_map->lcsr.itrans[i].itsau);
		itsal = ioread32be(&tsi148->pci_map->lcsr.itrans[i].itsal);
		iteau = ioread32be(&tsi148->pci_map->lcsr.itrans[i].iteau);
		iteal = ioread32be(&tsi148->pci_map->lcsr.itrans[i].iteal);
		itofu = ioread32be(&tsi148->pci_map->lcsr.itrans[i].itofu);
		itofl = ioread32be(&tsi148->pci_map->lcsr.itrans[i].itofl);
		seq_printf(m, "%d:  %08X  %08X:%08X  %08X:%08X:  %08X:%08X\n",
			   i, itat, itsau, itsal, iteau, iteal, itofu, itofl);
	}

	seq_puts(m, "\nVME Bus Control:\n");
	seq_printf(m, "\tvmctrl  0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.vmctrl));
	seq_printf(m, "\tvctrl   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.vctrl));
	seq_printf(m, "\tvstat   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.vstat));

	seq_puts(m, "PCI Status:\n");
	seq_printf(m, "\tpcsr    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.pstat));

	seq_puts(m, "VME Exception Status:\n");
	seq_printf(m, "\tveau:veal 0x%08x:%08x\n",
		   ioread32be(&tsi148->pci_map->lcsr.veau),
		   ioread32be(&tsi148->pci_map->lcsr.veal));
	seq_printf(m, "\tveat    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.veat));

	seq_puts(m, "PCI Error Status:\n");
	seq_printf(m, "\tedpau:edpal 0x%08x:%08x\n",
		   ioread32be(&tsi148->pci_map->lcsr.edpau),
		   ioread32be(&tsi148->pci_map->lcsr.edpal));
	seq_printf(m, "\tedpxa   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.edpxa));
	seq_printf(m, "\tedpxs   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.edpxs));
	seq_printf(m, "\tedpat   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.edpat));

	seq_puts(m, "Inbound Translation Location Monitor:\n");
	seq_printf(m, "\tlmbau:lmbal 0x%08x:%08x:\n",
		   ioread32be(&tsi148->pci_map->lcsr.lmbau),
		   ioread32be(&tsi148->pci_map->lcsr.lmbal));
	seq_printf(m, "\tlmat    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.lmat));

	seq_puts(m, "Local bus Interrupt Control:\n");
	seq_printf(m, "\tinten   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.inten));
	seq_printf(m, "\tinteo   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.inteo));
	seq_printf(m, "\tints    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.ints));
	seq_printf(m, "\tintc    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.intc));
	seq_printf(m, "\tintm1   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.intm1));
	seq_printf(m, "\tintm2   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.intm2));

	seq_puts(m, "VMEbus Interrupt Control:\n");
	seq_printf(m, "\tbcu64   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.bcu));
	seq_printf(m, "\tbcl64   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.bcl));
	seq_printf(m, "\tbpgtr   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.bpgtr));
	seq_printf(m, "\tbpctr   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.bpctr));
	seq_printf(m, "\tvicr    0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.vicr));

	seq_puts(m, "RMW Register Group:\n");
	seq_printf(m, "\trmwau  0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.rmwau));
	seq_printf(m, "\trmwal  0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.rmwal));
	seq_printf(m, "\trmwen  0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.rmwen));
	seq_printf(m, "\trmwc   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.rmwc));
	seq_printf(m, "\trmws   0x%08x\n", ioread32be(&tsi148->pci_map->lcsr.rmws));

	for (i = 0; i < TSI148_NR_DMA_CHANNELS; i++) {
		seq_printf(m, "DMA Controler %d Registers\n", i);
		seq_printf(m, "\tdctl   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dctl));
		seq_printf(m, "\tdsta   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dsta));
		seq_printf(m, "\tdcsau  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dcsau));
		seq_printf(m, "\tdcsal  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dcsal));
		seq_printf(m, "\tdcdau  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dcdau));
		seq_printf(m, "\tdcdal  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dcdal));
		seq_printf(m, "\tdclau  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dclau));
		seq_printf(m, "\tdclal  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dclal));
		seq_printf(m, "\tdsau   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dsau));
		seq_printf(m, "\tdsal   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dsal));
		seq_printf(m, "\tddau   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.ddau));
		seq_printf(m, "\tddal   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.ddal));
		seq_printf(m, "\tdsat   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dsat));
		seq_printf(m, "\tddat   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.ddat));
		seq_printf(m, "\tdnlau  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dnlau));
		seq_printf(m, "\tdnlal  0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dnlal));
		seq_printf(m, "\tdcnt   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.dcnt));
		seq_printf(m, "\tddbs   0x%08x\n",
			   ioread32be(&tsi148->pci_map->lcsr.dma[i].dma_desc.ddbs));
	}
	return 0;
}

static int tsi148_proc_lcsr_open(struct inode *inode, struct file *file)
{
	return single_open(file, tsi148_proc_lcsr_show, PDE_DATA(inode));
}

static const struct file_operations tsi148_proc_lcsr_ops = {
	.open           = tsi148_proc_lcsr_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};


/**
 * tsi148_proc_gcsr_show() - Dump the GCSR register group
 *
 */
static int tsi148_proc_gcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;
	int i;
	unsigned int tmp;

	seq_puts(m, "\nGlobal Control and Status Register Group (GCSR):\n");

	tmp = ioread32be(&tsi148->pci_map->gcsr.devi);
	seq_printf(m, "\tveni   0x%04x\n", tmp & 0xffff);
	seq_printf(m, "\tdevi   0x%04x\n", (tmp >> 16) & 0xffff);

	tmp = ioread32be(&tsi148->pci_map->gcsr.ctrl);
	seq_printf(m, "\tgctrl  0x%04x\n", (tmp >> 16) & 0xffff);
	seq_printf(m, "\tga     0x%02x\n", (tmp >> 8) & 0x3f);
	seq_printf(m, "\trevid  0x%02x\n", tmp & 0xff);

	seq_puts(m, "Semaphores:\n");
	tmp = ioread32be(&tsi148->pci_map->gcsr.semaphore[0]);
	seq_printf(m, "\tsem0   0x%02x\n", (tmp >> 24) & 0xff);
	seq_printf(m, "\tsem1   0x%02x\n", (tmp >> 16) & 0xff);
	seq_printf(m, "\tsem2   0x%02x\n", (tmp >> 8) & 0xff);
	seq_printf(m, "\tsem3   0x%02x\n", tmp & 0xff);

	tmp = ioread32be(&tsi148->pci_map->gcsr.semaphore[4]);
	seq_printf(m, "\tsem4   0x%02x\n", (tmp >> 24) & 0xff);
	seq_printf(m, "\tsem5   0x%02x\n", (tmp >> 16) & 0xff);
	seq_printf(m, "\tsem6   0x%02x\n", (tmp >> 8) & 0xff);
	seq_printf(m, "\tsem7   0x%02x\n", tmp & 0xff);

	seq_puts(m, "Mailboxes:\n");
	for (i = 0; i <= TSI148_NR_MAILBOXES; i++) {
		seq_printf(m, "\t Mailbox #%d: 0x%08x\n", i,
			     ioread32be(&tsi148->pci_map->gcsr.mbox[i]));
	}
	return 0;
}

static int tsi148_proc_gcsr_open(struct inode *inode, struct file *file)
{
	return single_open(file, tsi148_proc_gcsr_show, PDE_DATA(inode));
}

static const struct file_operations tsi148_proc_gcsr_ops = {
	.open           = tsi148_proc_gcsr_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};

/**
 * tsi148_proc_crcsr_show() - Dump the CRCSR register group
 *
 */
static int tsi148_proc_crcsr_show(struct seq_file *m, void *data)
{
	struct tsi148_chip *tsi148 = m->private;

	seq_puts(m, "\nCR/CSR Register Group:\n");
	seq_printf(m, "\tbcr   0x%08x\n",
		   ioread32be(&tsi148->pci_map->crcsr.csrbcr));
	seq_printf(m, "\tbsr   0x%08x\n",
		   ioread32be(&tsi148->pci_map->crcsr.csrbsr));
	seq_printf(m, "\tbar   0x%08x\n",
		   ioread32be(&tsi148->pci_map->crcsr.cbar));
	return 0;
}

static int tsi148_proc_crcsr_open(struct inode *inode, struct file *file)
{
	return single_open(file, tsi148_proc_crcsr_show, PDE_DATA(inode));
}

static const struct file_operations tsi148_proc_crcsr_ops = {
	.open           = tsi148_proc_crcsr_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};


/**
 * tsi148_procfs_register() - Create the VME proc tree
 * @vme_root: Root directory of the VME proc tree
 */
static void tsi148_procfs_register(struct vme_bridge_device *vbridge)
{
	struct proc_dir_entry *entry;

	vbridge->procfs_mgr.proc_chip = proc_mkdir("tsi148",
			vbridge->procfs_mgr.proc_root);

	entry = proc_create_data("pcfs", S_IFREG | 04444,
			vbridge->procfs_mgr.proc_chip,
			&tsi148_proc_pcfs_ops, vbridge->chip_mgr.private);
	if (!entry)
		pr_warn(PFX "Failed to create proc pcfs node\n");

	entry = proc_create_data("lcsr", S_IFREG | 04444,
			vbridge->procfs_mgr.proc_chip,
			&tsi148_proc_lcsr_ops, vbridge->chip_mgr.private);
	if (!entry)
		pr_warn(PFX "Failed to create proc lcsr node\n");

	entry = proc_create_data("gcsr", S_IFREG | 04444,
			vbridge->procfs_mgr.proc_chip,
			&tsi148_proc_gcsr_ops, vbridge->chip_mgr.private);
	if (!entry)
		pr_warn(PFX "Failed to create proc gcsr node\n");

	entry = proc_create_data("crcsr", S_IFREG | 04444,
			vbridge->procfs_mgr.proc_chip,
			&tsi148_proc_crcsr_ops, vbridge->chip_mgr.private);
	if (!entry)
		pr_warn(PFX "Failed to create proc crcsr node\n");
}

/**
 * tsi148_procfs_unregister() - Remove the VME proc tree
 *
 */
static void tsi148_procfs_unregister(struct vme_bridge_device *vbridge)
{
	remove_proc_entry("pcfs", vbridge->procfs_mgr.proc_chip);
	remove_proc_entry("lcsr", vbridge->procfs_mgr.proc_chip);
	remove_proc_entry("gcsr", vbridge->procfs_mgr.proc_chip);
	remove_proc_entry("crcsr", vbridge->procfs_mgr.proc_chip);
	remove_proc_entry("tsi148", vbridge->procfs_mgr.proc_root);
}

#else

static void tsi148__procfs_register(struct vme_bridge_device *vbridge) {}
static void tsi148__procfs_unregister(struct vme_bridge_device *vbridge) {}

#endif /* CONFIG_PROC_FS */

static struct vme_bridge_procfs_ops tsi148_procfs_ops = {
	.procfs_register = tsi148_procfs_register,
	.procfs_unregister = tsi148_procfs_unregister,
};

int tsi148_procfs_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->procfs_mgr.ops = &tsi148_procfs_ops;
	return 0;
}
