# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *
import pytest
import time


def test_cvorb_drv_set_channel_mask():
    # check version
    cmd = """echo 'dev' | """ + CVORBTEST_BIN + """ | tail -n +5"""
    result = """current\t 0\t cvorb.0\tCVORB at VME-A32 0x00000400
"""
    assert cmp_result(cmd, result)
