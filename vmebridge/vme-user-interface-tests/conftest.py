import pytest
import subprocess



class VMEDevice(object):
    def __init__(self, sysdevname, slot, am, addr = None, size = None, irq_vector = None, irq_level = None, devname = None):
        self.sysdevname = sysdevname
        self.slot = slot
        self.am = am
        self.addr = addr
        self.size = size
        self.irq_vector = irq_vector
        self.irq_level = irq_level
        self.devname = devname

    def register(self):
        vme_register_argmuments = ["--slot",
                                   str(self.slot),
                                   "--address-modifier" ,
                                   "0x{:x}".format(self.am)]
        if self.addr is not None:
            vme_register_argmuments.append("--address")
            vme_register_argmuments.append("0x{:x}".format(self.addr))
        if self.size is not None:
            vme_register_argmuments.append("--size")
            vme_register_argmuments.append(str(self.size))
        if self.irq_vector is not None:
            vme_register_argmuments.append("--irq-vector")
            vme_register_argmuments.append(str(self.irq_vector))
        if self.irq_level is not None:
            vme_register_argmuments.append("--irq-level")
            vme_register_argmuments.append(str(self.irq_level))
        if self.devname is not None:
            vme_register_argmuments.append("--name")
            vme_register_argmuments.append(str(self.devname))

        vme_register_command = ["vme-register", ]
        vme_register_command.extend(vme_register_argmuments)
        subprocess.run(vme_register_command, check=True)

    def unregister(self):
        vme_module_argmuments = ["--slot",
                                 str(self.slot),
                                 "--remove",
                                ]
        vme_module_command = ["vme-module", ]
        vme_module_command.extend(vme_module_argmuments)
        subprocess.run(vme_module_command, check=True)

    def sysfs_path(self):
        return "/sys/bus/vme/devices/slot.{:02d}/{:s}".format(self.slot,
                                                              self.sysdevname)

@pytest.fixture(scope="module")
def vme_dev_param():
    return {"slot": 11,
            "am": 0x39,
            "addr": 0x580000,
            "size": 524288,
            "irq_vector": 0xd9,
            "irq_level": 2,
            "devname": "fmc-svec-a24"
            }

@pytest.fixture(scope="module")
def vme_device_static(vme_dev_param):
    vme_device_static = VMEDevice("vme.{:02d}".format(vme_dev_param["slot"]),
                                  vme_dev_param["slot"],
                                  vme_dev_param["am"],
                                  vme_dev_param["addr"],
                                  vme_dev_param["size"],
                                  vme_dev_param["irq_vector"],
                                  vme_dev_param["irq_level"],
                                  vme_dev_param["devname"])
    vme_device_static.register()
    yield vme_device_static
    vme_device_static.unregister()
