# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *
import pytest
import time


def test_sis_drv_check_device():
    # check version
    cmd = """echo 'device' | sudo """ + SISTEST_BIN + """ | tail -n +3"""
    result = """current\t 0\t sis33.0\tStruck SIS3320-250v05 at VME-A32 0x10000000 irqv 165 irql 2\t 8\t12
"""
    assert cmp_result(cmd, result)

def test_sis_drv_start_auto():
    # check result of a command
    cmd = """echo 'start_auto 1' | sudo """ + SISTEST_BIN + """ | tail -n +3"""
    result = """1
"""
    assert cmp_result(cmd, result)

def test_sis_drv_stop_auto():
    # check result of a command
    cmd = """echo 'stop_auto 1' | sudo """ + SISTEST_BIN + """ | tail -n +3"""
    result = """1
"""
    assert cmp_result(cmd, result)

def test_sis_drv_acquisition():
    # check result of a acquisition command
    cmd = """echo 'acq 0 1 10000' | sudo """ + SISTEST_BIN + """ | cut -d$'\\n' -f 2-4"""
    result = """acq 0 1 10000
events: 1
ev_length: 4096
"""
    assert cmp_result(cmd, result)

def test_sis_drv_acquisition_fetch():
    # check version
    cmd = """echo 'start_auto 1; stop_auto 1; acq 0 1 10000 ; sh "sleep 1"; fetch' | sudo """ + SISTEST_BIN
    result = 11
    rc, output, error = exec_cmd(cmd)

    if error:
        print ("Error:\n" + error)
        print ("NOTE: Printing logs to a serial console can make this testcase to fail!")
        assert 0

    assert output.count('\n')+1 == result

def test_sis_drv_acquisition_fetch_plot():
    # check version
    cmd = """echo 'start_auto 1; stop_auto 1; acq 0 1 10000 ; sh "sleep 1"; fetch; plot' | sudo """ + SISTEST_BIN
    result = 35
    rc, output, error = exec_cmd(cmd)

    if error:
        print ("Error:\n" + error)
        print ("NOTE: Printing logs to a serial console can make this testcase to fail!")
        assert 0

    assert output.count('\n') == result
