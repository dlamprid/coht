// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2009 CERN
 * Author Federico Vaga <federico.vaga@cern.ch>
 * Author Sebastien Dugue
 */
#ifndef _TSI148_WINDOW_H_
#define _TSI148_WINDOW_H_
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/types.h>

#include "vmebus.h"

/**
 * struct window - Hardware window descriptor.
 * @lock: Mutex protecting the descriptor
 * @active: Flag indicating whether the window is in use
 * @rsrc: PCI bus resource of the window
 * @desc: This physical window descriptor
 * @mappings: List of mappings using this window
 * @users: Number of users of this window
 *
 * This structure holds the information concerning hardware
 * windows.
 *
 */
struct window {
	struct mutex		lock;
	unsigned int		active;
	struct resource		rsrc;
	struct vme_mapping	desc;
	struct list_head	mappings;
	int 			users;
};

/**
 * struct vme_taskinfo - Store information about a mapping's user
 *
 * @pid_nr:	pid number
 * @name:	name of the process
 * @file:	struct file to identify the owner of the mapping. Set it to
 *		NULL when the request comes from the kernel. In that case
 *		@pid_nr and @name won't be filled in.
 *
 * @note	on the name length.
 *		As it's not only the process name that is stored here, but also
 *		module name -- lengths should be MAX allowed for the module name
 */
struct vme_taskinfo {
	pid_t	pid_nr;
	char	name[MODULE_NAME_LEN];
	struct file	*file;
};

/**
 * struct mapping - Logical mapping descriptor
 * @list: List of the mappings
 * @mapping: The mapping descriptor
 * @client: The user of this mapping
 *
 * This structure holds the information concerning logical mappings
 * made on top a hardware windows.
 */
struct mapping {
	struct list_head	list;
	struct vme_mapping	desc;
	struct vme_taskinfo	client;
};

extern struct window window_table[TSI148_NUM_OUT_WINDOWS];

#endif
