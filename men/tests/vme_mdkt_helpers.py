# Author Adam Wujek, CERN 2017

from test_config import *
from helpers import *

import subprocess

class vme_mdkt(vme_common):
    def vme_do(self, mode = None, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = False, retries = None, ffile = None, verify = None, buff_offset = None, dump_data = False, dma = None, novmeinc = False):
        if value == None:
            value = 0
        if mode == WRITE:
            assert 0, "Write not supported"
        #if mode == READ:
        #mode = "-r"
        data_width = self.get_data_width(space, data_width)
        if size == None:
            size = data_width
        spc_map = {
            A16D16: 0,
            A16D16_DMA: 0,
            A16D32: 2,
            A16D32_DMA: 2,
            A24D16: 4,
            A24D16_DMA: 4,
            A24D16_BLT: 5,
            A24D32: 6,
            A24D32_DMA: 6,
            A24D32_BLT: 7,
            A24D64_MBLT: 1,
            A32D32: 8,
            A32D32_DMA: 8,
            A32D32_BLT: 9,
            A32D64_MBLT: 10,
            CRCSR: 30,
            CRCSRD8: 30,
            CRCSRD16: 30,
            CRCSRD32: 30
        }

        spc = spc_map.get(space, -1)

        cmd_space = "echo %d   | sudo tee /sys/module/men_dma_kernel_test/parameters/space" % (spc)
        cmd_size  = "echo 0x%x | sudo tee /sys/module/men_dma_kernel_test/parameters/size" % (size)
        cmd_width = "echo %d   | sudo tee /sys/module/men_dma_kernel_test/parameters/width" % (data_width)
        cmd_addr  = "echo 0x%x | sudo tee /sys/module/men_dma_kernel_test/parameters/addr" % (addr)
        cmd_trig  = "cat /proc/vme_test"

        if mmap == True:
            assert 0, "mmap not supported"
        if verify == True:
            assert 0, "verify not supported"
        if ffile != None:
            cmd_trig  = "cat /proc/vme_test_bin > %s" % (ffile)
        if retries != None:
            assert 0, "retries not supported"
        if buff_offset != None:
            assert 0, "buff_offset not supported"
        if self.is_dma_space(space, dma):
            cmd_sgl  = "echo 1 | sudo tee /sys/module/men_dma_kernel_test/parameters/sgl"
        else:
            cmd_sgl  = "echo 0 | sudo tee /sys/module/men_dma_kernel_test/parameters/sgl"

        if novmeinc == True:
            assert 0, "novmeinc not supported"
        if extra_cmd != None:
            assert 0, "extra_cmd not supported"

        print_cmd (cmd_space)
        print_cmd (cmd_size)
        print_cmd (cmd_width)
        print_cmd (cmd_addr)
        if cmd_sgl != "true":
            print_cmd (cmd_sgl)
        print_cmd (cmd_trig)


        assert addr != None, "No VME address specified"
        assert data_width != None, "No data width nor address space specified"
        assert mode != None,  "No mode (Read/Write) specified"
        assert spc >= 0, "No address modifier specified"

        cmd = cmd_space + " && " + cmd_size  + " && " + cmd_width + " && " + cmd_addr + " && " + cmd_sgl + " && " + cmd_trig

        print_cmd (cmd)

        process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, error = process.communicate()
        rc = process.returncode
        check_return_code(rc, output, error)
        return rc, output, error

    def read(self, space = None, addr = None, size = None , data_width = None, extra_cmd = None, mmap = None, ffile = None, retries = None, dump_data = False, dma = None, novmeinc = False):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, retries = retries, dump_data = dump_data, dma = dma, novmeinc = novmeinc)
        return rc, output, error

    def read_val(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, ffile = None, dma = None):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, dump_data = True, dma = dma)
        data_width = self.get_data_width(space, data_width) # make sure data_width is valid; even when only space is provided
        output_dec = output.decode('utf-8').split("\n");
        data_index = output_dec.index("----") + 1
        # get the line with the value
        value_split = output_dec[data_index].split()
        # get the value
        value_list = value_split[1:data_width+1]
        value = int("0x" + "".join(value_list), 16)
        return value
