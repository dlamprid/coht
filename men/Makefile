# include the build environment
include ../common.mk

#include version information
include ./versions.mk

DIRS = \
	mdis_a25_vme \
	men-wrapper \
	men-dma-kernel-test \

LIN_KERNEL_DIR=$(KERNELSRC)
export LIN_KERNEL_DIR

export MEN_LIN_DIR = $(CURDIR)/menlinux
export MODS_INSTALL_DIR = $(CURDIR)/$(CPU)/drv
export BIN_INSTALL_DIR = $(CURDIR)/$(CPU)/bin
export LIB_INSTALL_DIR = $(CURDIR)/$(CPU)/lib
export DESC_INSTALL_DIR = $(CURDIR)/$(CPU)/etc
export DEVNODE_INSTALL_DIR = $(CURDIR)/$(CPU)/dev
export ALL_DBGS?=nodbg

.PHONY: all clean cleanall install $(DIRS)

clean: TARGET = clean
cleanall: clean
clean: clean-menlinux

all clean cleanall: $(DIRS)

$(DIRS):
	$(MAKE) -C $@ $(TARGET)


men-wrapper: mdis_a25_vme
men-dma-kernel-test: mdis_a25_vme

clean-menlinux:
	$(RM) -rf menlinux/BUILD/MDIS/DEVTOOLS/OBJ
	$(RM) -rf menlinux/BUILD/MDIS/DEVTOOLS/BIN
	$(RM) menlinux/BUILD/MDIS/DEVTOOLS/.kernelsettings



install: install-men_wrapper
install: install-vme_men
install: install-men_dma_kernel_test

# men-wrapper
men-wrapper_DRIVERS=men-wrapper/men_wrapper.ko
install-men_wrapper:
	$(MAKE) install_drivers_global PRODUCT_NAME=$(PRODUCT_NAME) DRIVERS_LIST="$(men-wrapper_DRIVERS)"

# men-dma-kernel-test
men-dma-kernel-test_DRIVERS=men-dma-kernel-test/men_dma_kernel_test.ko
install-men_dma_kernel_test:
	$(MAKE) install_drivers_global PRODUCT_NAME=$(PRODUCT_NAME) DRIVERS_LIST="$(men-dma-kernel-test_DRIVERS)"


# vme_men
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_dbg/men_dbg.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_vme4l-core/men_vme4l-core.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_oss/men_oss.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_chameleon/men_chameleon.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_chameleon_io/men_chameleon_io.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_lx_chameleon/men_lx_chameleon.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_pldz002_cham/men_pldz002_cham.ko
vme-men_DRIVERS+=mdis_a25_vme/OBJ/$(ALL_DBGS)/men_serflash/men_serflash.ko
vme-men_PROGS+=mdis_a25_vme/BIN/vme4l_rwex
vme-men_PROGS+=mdis_a25_vme/BIN/vme4l_ctrl
vme-men_PROGS+=mdis_a25_vme/BIN/vme4l_crcsr
vme-men_PROGS+=mdis_a25_vme/BIN/fpga_load_a25
vme-men_PROGS+=tools/crcsr_query
vme-men_PROGS+=tools/men_a25_load_firmware

install-vme_men:
	$(CP) mdis_a25_vme/BIN/fpga_load mdis_a25_vme/BIN/fpga_load_a25
	$(MAKE) install_drivers_global PRODUCT_NAME=$(PRODUCT_NAME) DRIVERS_LIST="$(vme-men_DRIVERS)"
	$(MAKE) install_prog_global    PRODUCT_NAME=$(PRODUCT_NAME) PROGS_LIST="$(vme-men_PROGS)" \
		PROG_MAJOR=$(VER_MAJOR) PROG_MINOR=$(VER_MINOR) PROG_PATCH=$(VER_PATCH) \
		VER_PREV=$(VER_PREV) VER_CURRENT=$(VER_CURRENT) VER_NEXT=$(VER_NEXT)

SCRIPTS_LIST= \
	scripts/create_nodes.sh \
	scripts/load_drivers.sh \
	scripts/remove_drivers.sh \
	scripts/install_drivers.sh \
	scripts/install_drv.sh \
	scripts/check_version.sh \


# use default instalation rule for headers
install: install_scripts_global
install: install_extra_link

install_extra_link:
	@echo "    Create extra links in $(INST_BIN_PATH):"
	$(V)$(INSTALL_DIR_CMD) $(INSTALL_DIR_PARAMS) $(INST_BIN_PATH)
# NOTE: below some paths contains INST_LIB_PATH_RELATIVE and INST_BIN_PATH by purpose
	$(V)$(foreach FILE,$(vme-men_PROGS),\
		export FILE_NO_CPU=$(subst .$(CPU),,$(notdir $(FILE)));\
		echo "        $(INST_BIN_PATH)/$$FILE_NO_CPU -> /$(INST_LIB_PATH_RELATIVE)/$(PROG_MAJOR).$(PROG_MINOR)/bin/$$FILE_NO_CPU"; \
		$(INSTALL_LINK) $(INSTALL_LINK_PARAMS) /$(INST_LIB_PATH_RELATIVE)/$(PROG_MAJOR).$(PROG_MINOR)/bin/$$FILE_NO_CPU $(INST_BIN_PATH)/$$FILE_NO_CPU;\
		)
