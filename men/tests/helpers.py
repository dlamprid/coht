# Author Adam Wujek, CERN 2017

from test_config import *
import subprocess

READ = "READ"
WRITE = "WRITE"

A16D16 = "A16D16"
A16D16_DMA = "A16D16_DMA"
A16D32 = "A16D32"
A16D32_DMA = "A16D32_DMA"
A24D16 = "A24D16"
A24D16_DMA = "A24D16_DMA"
A24D16_BLT = "A24D16_BLT"
A24D32 = "A24D32"
A24D32_DMA = "A24D32_DMA"
A24D32_BLT = "A24D32_BLT"
A24D64_MBLT = "A24D64_MBLT"
A32D32 = "A32D32"
A32D32_DMA = "A32D32_DMA"
A32D32_BLT = "A32D32_BLT"
A32D64_MBLT = "A32D64_MBLT"
CRCSR = "CRCSR"
CRCSRD8 = "CRCSRD8"
CRCSRD16 = "CRCSRD16"
CRCSRD32 = "CRCSRD32"

def print_cmd(cmd):
	if cfg_print_cmd == "y":
		print (cmd)

def print_error(str_err):
	if cfg_print_error == "y":
		print (str_err)


def exec_cmd(cmd):
    print_cmd (cmd)
    process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output, error = process.communicate()
    output_dec = output.decode('utf-8')
    error_dec = error.decode('utf-8')
    rc = process.returncode
    print_cmd ("Output:\n" + output_dec)
    return rc, output, error

def cmp_result(cmd, result):
    rc, output, error = exec_cmd(cmd)
    output_dec = output.decode('utf-8')
    error_dec = error.decode('utf-8')
    if rc !=0 :
        print_error ("Non-zero return code %d" % rc)
        return rc
    if output_dec != result:
        print_error ("Expected:\n" + result)
    return output_dec == result

def check_return_code(rc, output, error):
    if rc != 0:
        print_error ("Non-zero return code (%d)" % rc)
        print_error (output.decode('utf-8'))
        print_error (error.decode('utf-8'))
        assert rc == 0

class vme_common:
    def write(self, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = None, ffile = None, verify = None, retries = None, buff_offset = None, dump_data = False, dma = None):
        assert 0, "write not supported"
    def read(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, ffile = None, retries = None, dump_data = False, dma = None, novmeinc = False):
        assert 0, "read not supported "
    def read_val(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, ffile = None, dma = None):
        assert 0, "read val not supported"

    def get_data_width(self, space = None, data_width = None):
        assert space != None or data_width != None, "Space nor data_width defined!"
        if (space == None): # only data width defined
            return data_width

        if space == (CRCSRD8):
            data_width_ret = 1
        if space in (A16D16, A16D16_DMA, A24D16, A24D16_DMA, A24D16_BLT, CRCSRD16):
            data_width_ret = 2
        if space in (A16D32, A16D32_DMA, A24D32, A24D32_DMA, A24D32_BLT, A32D32, A32D32_DMA, A32D32_BLT, CRCSR, CRCSRD32):
            data_width_ret = 4
        if space in (A24D64_MBLT, A32D64_MBLT):
            data_width_ret = 8

        if data_width != None: # space and data_width defined, compare
            assert data_width == data_width_ret

        return data_width_ret

    def is_dma_space(self, space = None, dma = None):
        assert space != None or data_width != None, "Space nor dma defined!"
        if (dma != None): # only data width defined
            return dma

        if space in (A16D16_DMA, A16D32_DMA, A24D16_DMA, A16D32_DMA, A24D32_DMA, A32D32_DMA):
            return True

        return False


    def get_vme_block_size(self, space = None):
        assert space != None, "Space not defined!"

        vme_block_size = 0

        if space == (CRCSRD8):
            vme_block_size = 4
        if space in (A16D16, A24D16, CRCSRD16):
            vme_block_size = 4 # in DMA mode size is 4 bytes!
        if space in (A16D32, A24D32, A32D32, CRCSR, CRCSRD32):
            vme_block_size = 4
        if space in (A24D16_BLT, A24D32_BLT, A32D32_BLT, CRCSRD16):
            vme_block_size = 256 # depends on FPGA implementation
        if space in (A24D64_MBLT, A32D64_MBLT):
            vme_block_size = 2048 # depends on FPGA implementation

        assert vme_block_size != 0, "Wrong address space!"

        return vme_block_size

    def slave_set_space(self, slot, space, ader):
        CRCSR_addr = slot * 0x80000
        fun_ader = None
        am = None
        if space in (A24D16, A24D16_DMA, A24D16_BLT, A24D32, A24D32_DMA, A24D32_BLT, A24D64_MBLT):
            fun_ader = FUN1ADER
        if space in (A32D32, A32D32_DMA, A32D32_BLT, A32D64_MBLT):
            fun_ader = FUN0ADER

        if space in (A24D16, A24D16_DMA, A24D32, A24D32_DMA):
            am = 0x39
        if space in (A24D16_BLT, A24D32_BLT):
            am = 0x3b
        if space == A24D64_MBLT:
            am = 0x38
        if space in (A32D32, A32D32_DMA):
            am = 0x9
        if space == A32D32_BLT:
            am = 0xb
        if space == A32D64_MBLT:
            am = 0x8

        assert slot > 0, "Wrong slot number"
        assert fun_ader != None, "Wrong address space %s" % space
        assert am != None, "Wrong address space %s" % space
        self.write(CRCSRD32, addr = CRCSR_addr + fun_ader + 0, value = (ader >> 24) & 0xff)
        self.write(CRCSRD32, addr = CRCSR_addr + fun_ader + 4, value = (ader >> 16) & 0xff)
        self.write(CRCSRD32, addr = CRCSR_addr + fun_ader + 8, value = (ader >> 8) & 0xff)
        self.write(CRCSRD32, addr = CRCSR_addr + fun_ader + 12, value = ((ader >> 0) | (am << 2)) & 0xff)
        #verify data
        assert self.read_val(CRCSRD32, addr = CRCSR_addr + fun_ader + 0) == ((ader >> 24) & 0xff)
        assert self.read_val(CRCSRD32, addr = CRCSR_addr + fun_ader + 4) == ((ader >> 16) & 0xff)
        assert self.read_val(CRCSRD32, CRCSR_addr + fun_ader + 8) == ((ader >> 8) & 0xff)
        assert self.read_val(CRCSRD32, CRCSR_addr + fun_ader + 12) == (((ader >> 0) | (am << 2)) & 0xff)

    # get performance value
    def perf(self, mode, space = None, addr = None, data_width = None, size = None, value = None, extra_cmd = None, mmap = None, ffile = None, verify = None, retries = None, buff_offset = None, dma = None):
        rc, output, error = self.vme_do(mode = mode, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, retries = retries, ffile = ffile, buff_offset = buff_offset, dump_data = False, dma = dma)
        data_width = self.get_data_width(space, data_width) # make sure data_width is valid; even when only space is provided
        output_dec = output.decode('utf-8').split("\n");
        for line in output_dec:
            if "finished. average Time:" in line:
                print (line)
                break
        # get the line with the value
        value_split = line.split(" ")
        # get the value
        value_list = value_split[12]
        value = float(value_list)
        return value
