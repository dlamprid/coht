# Author Adam Wujek, CERN 2017

from test_config import *
from vme_wrapper_helpers import *
import pytest
import time


def test_ctrv_drv_check_ver():
    # check version
    cmd = """echo "ver" | sudo """ + CTRTEST_BIN + """ | cut -d$'\\n' -f 7,8,10,13-16"""
    result = """Hardware Type: CTRV VME (8 Channel)
On this network:CPS PSB LEI ADE SPS 
VHDL Compiled: [1292244858] Mon-13/Dec/2010 13:54:18
HPTDC Chip Version: 0xFFFFFFFF No HPTDC chip installed
Hardware Type: CTRV VME (8 Channel)
/usr/local/ctr/Vhdl.versions:1292244858 Ctrv: Mon-13/Dec/2010 13:54:18
VHDL version:1292244858 is correct and up to date
"""
    assert cmp_result(cmd, result)

def test_ctrv_drv_check_interrupts():
    # run 300 interrupts 1kHz
    cmd = """echo '300(wi 0x1000) q' | sudo """ + CTRTEST_BIN + """ | grep -e Mod -e Int -e Time -e OneKHz | wc -l"""
    result = "300\n"
    assert cmp_result(cmd, result)
