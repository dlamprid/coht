// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "vmeutils.h"

const char program_name[] = "vme";
static const char *help_msg =
	"Usage: vme <command> [options] [--help]\n"
	"\n"
	"--help          show help message\n"
	"\n"
	"Commands:\n"
	"  scan\n"
	"  module\n"
	"  register\n"
	"\n";


static const struct cmd_struct commands[] = {
	{"scan", cmd_scan},
	{"module", cmd_module},
	{"register", cmd_register},
};

static const struct cmd_struct *get_command(const char *s)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		const struct cmd_struct *p = commands + i;
		if (!strcmp(s, p->cmd))
			return p;
	}
	return NULL;
}

static void cmd_help(int argc, char *argv[])
{
	if (argc == 2 && !strcmp(argv[1], "--help")) {
		fputs(help_msg, stdout);
		exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[])
{
	const struct cmd_struct *cmd;
	int ret;

	command_version(argc, argv);
	cmd_help(argc, argv);
	cmd = get_command(argv[1]);
	if (!cmd) {
		fprintf(stderr, "%s: unknown command %s\n",
			program_name, argv[1]);
		exit(EXIT_FAILURE);
	}

	argc--;
	argv++;
	ret = cmd->fn(argc, argv);
	if (ret < 0) {
		fprintf(stderr, "%s-%s failed: %s\n",
			program_name, cmd->cmd, vme_strerror(errno));
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
