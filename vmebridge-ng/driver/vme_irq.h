/* SPDX-License-Identifier: GPL-2.0-or-later */
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#ifndef _VME__IRQ_H_
#define _VME__IRQ_H_

#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/idr.h>

#define VME_NUM_VECTORS 256

struct vme_bridge_device;

struct vme_bridge_irq_ops {
	/* get IRQ sources */
	int          (*enable_interrupts)(struct vme_bridge_device *vbridge);
	int          (*disable_interrupts)(struct vme_bridge_device *vbridge);
	irqreturn_t  (*interrupt_handler)(int irq, void *arg);
	int          (*iack)(void *chip, int level);
	int          (*generate_interrupt)(struct vme_bridge_device *vbridge,
				   int level, int vector, signed long msecs);
};

/**
 * @dma_pool: pool of contiguous memory where to put DMA transfer descriptors
 */
struct vme_bridge_irq_mgr {
	DECLARE_BITMAP(irq_mask, VME_NUM_VECTORS);
	struct irq_domain	  *domain;
	struct ida                ida_irq;
	int			  msi_enabled;
	// Keep the vme interrupts enabled mask
	unsigned int		  vme_interrupts_enabled;

	struct vme_bridge_irq_ops *ops;

	void			  *private; /* to hold specific HW info */
};

#endif /* _VME__IRQ_H_ */
