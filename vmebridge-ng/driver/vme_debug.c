// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2020
 * Author Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/debugfs.h>
#include <linux/irq.h>
#include <linux/irqdomain.h>

#include "vme_irq.h"
#include "vme_bridge.h"

static int vme_dbg_info(struct seq_file *s, void *offset)
{
	struct vme_bridge_device *vbridge = s->private;
	int i;

	seq_printf(s, "%s:\n", dev_name(&vbridge->pdev->dev));

	seq_printf(s, "  redirect: %d\n", vbridge->pdev->irq);
	seq_puts(s, "  irq-mapping:\n");
	for (i = 0; i < VME_NUM_VECTORS; ++i) {
		seq_printf(s, "    - hardware: %d\n", i);
		seq_printf(s, "      linux: %d\n",
			   irq_find_mapping(vbridge->irq_mgr.domain, i));
	}

	return 0;
}

static int vme_dbg_info_open(struct inode *inode, struct file *file)
{
	struct vme_bridge_device *vbridge = inode->i_private;

	return single_open(file, vme_dbg_info, vbridge);
}

static const struct file_operations vme_dbg_info_ops = {
	.owner = THIS_MODULE,
	.open  = vme_dbg_info_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

#define VBRIDGE_DBG_GEN_IRQ_BUF_LEN 32
static ssize_t vme_dbg_gen_irq_write(struct file *file,
				     const char __user *buf,
				     size_t count, loff_t *ppos)
{
	unsigned int level, vector, timeout;
	char buf_l[VBRIDGE_DBG_GEN_IRQ_BUF_LEN];
	int err, ret;

	if (VBRIDGE_DBG_GEN_IRQ_BUF_LEN < count)
		return -EFAULT;
	err = copy_from_user(buf_l, buf, VBRIDGE_DBG_GEN_IRQ_BUF_LEN);
	if (err)
		return -EFAULT;

	ret = sscanf(buf_l, "%u %u %u", &level, &vector, &timeout);
	if (ret != 3) {
		pr_err(PFX VBRIDGE_DBG_GEN_IRQ_NAME " invalid format expected '%%u, %%u, %%u', got %s\n", buf_l);
		return -EINVAL;
	}

	pr_info(PFX  VBRIDGE_DBG_GEN_IRQ_NAME "level: %u, vector: %u, timeout_ms: %u\n",
		level, vector, timeout);
	err = vme_generate_interrupt(level, vector, timeout);

	return err ? err : count;
}

static const struct file_operations vme_dbg_gen_irq_ops = {
	.owner = THIS_MODULE,
	.open  = simple_open,
	.write = vme_dbg_gen_irq_write,
};

int vme_dbg_init(struct vme_bridge_device *vbridge)
{
	vbridge->dbg_dir = debugfs_create_dir("vme-bridge", NULL);
	if (IS_ERR_OR_NULL(vbridge->dbg_dir)) {
		dev_err(&vbridge->pdev->dev,
			"Cannot create debugfs directory (%ld)\n",
			PTR_ERR(vbridge->dbg_dir));
		return PTR_ERR(vbridge->dbg_dir);
	}

	vbridge->dbg_info = debugfs_create_file(VBRIDGE_DBG_INFO_NAME, 0444,
					     vbridge->dbg_dir, vbridge,
					     &vme_dbg_info_ops);
	if (IS_ERR_OR_NULL(vbridge->dbg_info)) {
		dev_err(&vbridge->pdev->dev,
			"Cannot create debugfs file \"%s\" (%ld)\n",
			VBRIDGE_DBG_INFO_NAME, PTR_ERR(vbridge->dbg_info));
		return PTR_ERR(vbridge->dbg_info);
	}

	vbridge->dbg_gen_irq = debugfs_create_file(VBRIDGE_DBG_GEN_IRQ_NAME,
						   0220,
						   vbridge->dbg_dir, vbridge,
						   &vme_dbg_gen_irq_ops);
	if (IS_ERR_OR_NULL(vbridge->dbg_gen_irq)) {
		dev_err(&vbridge->pdev->dev,
			"Cannot create debugfs file \"%s\" (%ld)\n",
			VBRIDGE_DBG_GEN_IRQ_NAME,
			PTR_ERR(vbridge->dbg_gen_irq));
		return PTR_ERR(vbridge->dbg_gen_irq);
	}

	return 0;
}

/**
 * It removes the debugfs interface
 * @vbridge: VME bridge device
 */
void vme_dbg_exit(struct vme_bridge_device *vbridge)
{
	debugfs_remove_recursive(vbridge->dbg_dir);
}
